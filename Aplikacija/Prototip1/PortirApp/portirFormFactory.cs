﻿using MetroFramework.Forms;
using static SharedComponents.Forms.Login;

namespace PortirApp
{
    public class PortirFormFactory : FormFactory
    {
        public override MetroForm MakeForm(string username)
        {
            return new GlavnaForma(username);
        }
    }
}
