﻿using Persistence.Entities;

namespace PortirApp
{
    public static class Ekstenzije
    {
        public static bool Prisutan(this Zaposleni z)
        {
            foreach (Prisustvo p in z.Prisustva)
            {
                if (p.VremeOdlaska == null)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
