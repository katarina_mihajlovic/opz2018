﻿using MetroFramework.Forms;
using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Persistence.Entities;
using static SharedComponents.Forms.Login;
using SharedComponents.Forms;

namespace PortirApp
{
    public partial class GlavnaForma : MetroForm, IUsernameOwner
    {
        public string Username { get; set; }

        public GlavnaForma(string username)
        {
            InitializeComponent();
            Username = username;
        }

        private void btnOdlazak_Click(object sender, EventArgs e)
        {
            Int32? i = gridToLeave.SelectedRows[0].Cells[0].Value as Int32?;
            if (i == null)
            {
                MessageBox.Show("Greska po indeksu.");
                return;
            }

            using (OpzContext kontekst = new OpzContext())
            {
                Zaposleni zap = kontekst.ListaZaposlenih.Find(i);
                Prisustvo pri = (from prisustvo in zap.Prisustva
                                 where prisustvo.VremeOdlaska == null
                                 select prisustvo).ToList().ElementAt(0);

                pri.VremeOdlaska = DateTime.Now;
                kontekst.SaveChanges();
            }
            backgroundWorker1.RunWorkerAsync(txtSearch.Text.ToUpper());
        }

        private void btnDolazak_Click(object sender, EventArgs e)
        {
            Int32? i = gridToCome.SelectedRows[0].Cells[0].Value as Int32?;
            if (i == null)
            {
                MessageBox.Show("Greska po indeksu.");
                return;
            }

            using (OpzContext kontekst = new OpzContext())
            {
                Zaposleni zap = kontekst.ListaZaposlenih.Find(i);

                Prisustvo prisustvo = new Prisustvo()
                {
                    Zaposleni = zap,
                    VremeDolaska = DateTime.Now,
                    VremeOdlaska = null
                };
                kontekst.ListaPrisustva.Add(prisustvo);
                kontekst.SaveChanges();
            }
            backgroundWorker1.RunWorkerAsync(txtSearch.Text.ToUpper());
        }

        private void GlavnaForma_Load(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync(String.Empty);
        }

        private void ListRefresh(object forcome, object forgo)
        {
            gridToCome.DataSource = forcome;
            gridToLeave.DataSource = forgo;

            gridToCome.Columns[0].HeaderText = "Broj zaposlenog";
            gridToLeave.Columns[0].HeaderText = "Broj zaposlenog";
            gridToCome.Columns[2].HeaderText = "Srednje slovo";
            gridToLeave.Columns[2].HeaderText = "Srednje slovo";
            gridToCome.Columns[4].HeaderText = "Radno mesto";
            gridToLeave.Columns[4].HeaderText = "Radno mesto";

            foreach (DataGridViewColumn dgvcol in gridToCome.Columns)
            {
                if (dgvcol.Index == 0) continue;
                dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            foreach (DataGridViewColumn dgvcol in gridToLeave.Columns)
            {
                if (dgvcol.Index == 0) continue;
                dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync(txtSearch.Text.ToUpper());
            }
        }

        delegate void Del1(object a, object b);
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Del2 spnstop = StopSpinning;
            String filter = (String)e.Argument;
            Invoke(spnstart);
            object forcome, forgo;
            using (OpzContext kontekst = new OpzContext())
            {
                forcome = (from zap in kontekst.ListaZaposlenih
                           where (zap.DatumKrajaRada == null && (zap.Id.ToString().ToUpper().Contains(filter) || zap.Ime.ToUpper().Contains(filter) || zap.Prezime.ToUpper().Contains(filter)))
                           && (
                               (from p in zap.Prisustva
                                where p.VremeOdlaska == null
                                select p).ToList().Count == 0
                           )
                           orderby zap.Id
                           select new
                           {
                                BrojZaposlenog = zap.Id,
                                Ime = zap.Ime,
                                SrednjeSlovo = zap.SSlovo,
                                Prezime = zap.Prezime,
                                RadnoMesto = zap.RadnoMesto
                           }).ToList();

                forgo = (from zap in kontekst.ListaZaposlenih
                         where (zap.DatumKrajaRada == null && (zap.Id.ToString().ToUpper().Contains(filter) || zap.Ime.ToUpper().Contains(filter) || zap.Prezime.ToUpper().Contains(filter)))
                         && (
                               (from p in zap.Prisustva
                                where p.VremeOdlaska == null
                                select p).ToList().Count == 1
                         )
                         orderby zap.Id
                         select new
                         {
                                BrojZaposlenog = zap.Id,
                                Ime = zap.Ime,
                                SrednjeSlovo = zap.SSlovo,
                                Prezime = zap.Prezime,
                                RadnoMesto = zap.RadnoMesto
                         }).ToList();
            }

            Del1 del = ListRefresh;
           // Thread.Sleep(2000);
            Invoke(del, new object[] { forcome, forgo });
        }

        private delegate void Del2();
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Del2 spnstop = StopSpinning;
            //zaustavljanje spinnera
            Invoke(spnstop);
        }

        #region Spinner
        private void StartSpinning()
        {
            lblSpinner.Visible = true;
            circularProgressBar1.Visible = true;

            metroButton1.Enabled = false;
            metroButton2.Enabled = false;
            btnSearch.Enabled = false;
            btnDolazak.Enabled = false;
            btnOdlazak.Enabled = false;
        }

        private void StopSpinning()
        {
            lblSpinner.Visible = false;
            circularProgressBar1.Visible = false;

            metroButton1.Enabled = true;
            metroButton2.Enabled = true;
            btnSearch.Enabled = true;
            btnDolazak.Enabled = true;
            btnOdlazak.Enabled = true;
        }
        #endregion

        private void metroButton1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            PromenaLozinkeForm f = new PromenaLozinkeForm();
            if (f.ShowDialog(this) == DialogResult.OK) this.DialogResult = DialogResult.OK;
        }

        private void GlavnaForma_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                e.Cancel = true;
            }
        }
    }
}
