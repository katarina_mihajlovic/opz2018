﻿using SharedComponents.Forms;
using System;
using System.Windows.Forms;
using static SharedComponents.Forms.Login;

namespace PortirApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Uloga[] uloge = new Uloga[]
            {
                new Uloga("Portir", new PortirFormFactory())
            };

            Application.Run(new Login(uloge));
        }
    }
}
