﻿using SharedComponents.Forms;
using System;
using System.Windows.Forms;
using static SharedComponents.Forms.Login;

namespace Prototip_5
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Uloga[] uloge = new Uloga[]
            {
                new Uloga("Administrator", new AdminFormFactory()),
                new Uloga("Menadžer", new ManagerFormFactory())
            };

            Application.Run(new Login(uloge));
            //Application.Run(new MenadzerGlavnaForma("tezej")); //sad je mladen
            //Application.Run(new AdminMainForm("marjan10"));
        }
    }
}
