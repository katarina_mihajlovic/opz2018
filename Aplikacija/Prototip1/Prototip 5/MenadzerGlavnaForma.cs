﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Windows.Forms;
using MetroFramework.Forms;
using Persistence.Entities;
using SharedComponents.Presenters;
using SharedComponents.Forms;
using static SharedComponents.Forms.Login;

namespace Prototip_5
{
    public partial class MenadzerGlavnaForma : MetroForm, IUsernameOwner
    {
        public string Username { get; set; }

        private int idMenadzera;

		public MenadzerGlavnaForma(String username)
		{
            InitializeComponent();
            Username = username;
            using (OpzContext context = new OpzContext())
            {
                var rezultat = (from x in context.ListaZaposlenih
                                where x.KorisnickoIme == username
                                select x).ToList();
                Zaposleni menadzer = rezultat[0];
                idMenadzera = menadzer.Id;

                lblMenadzerIme.Text = menadzer.Ime + " " + menadzer.SSlovo + ". " + menadzer.Prezime;
                lblRadnoMesto.Text = menadzer.RadnoMesto;
                if (menadzer.Slike != null && menadzer.Slike.Count > 0)
                {
                    var lista = menadzer.Slike.OrderByDescending(x => x.Datum).ToList();
                    using (var ms = new MemoryStream(lista[0].Image))
                    {
                        imgMenadzer.Image = Image.FromStream(ms);
                    }
                }
            }
            LoadList();
        }

		private void LoadList()
		{
            using (OpzContext context = new OpzContext())
            {
                metroGrid1.DataSource = (from zap in context.ListaZaposlenih
                                         where zap.Nadredjeni.Id == idMenadzera
                                         orderby zap.Id
                                         select new
                                         {
                                             BrojZaposlenog = zap.Id,
                                             Ime = zap.Ime,
                                             SrednjeSlovo = zap.SSlovo,
                                             Prezime = zap.Prezime,
                                             RadnoMesto = zap.RadnoMesto
                                         }).ToList();
            }

			metroGrid1.Columns[0].HeaderText = "Broj zaposlenog";
			metroGrid1.Columns[2].HeaderText = "Srednje slovo";
			metroGrid1.Columns[4].HeaderText = "Radno mesto";
            foreach (DataGridViewColumn dgvcol in metroGrid1.Columns)
            {
                if (dgvcol.Index == 0) continue;
                dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

        }

		private void metroGrid1_DoubleClick(object sender, EventArgs e)
		{
			if (metroGrid1.SelectedRows.Count != 1) return;
			Int32? id = metroGrid1.SelectedRows[0].Cells[0].Value as Int32?;
			
			if (id != null)
			{
				var f = new MenadzerOsnovniPodaciForm();
				var p = new MenadzerOsnovniPodaciPresenter(id.Value, f);
				f.ShowDialog();
			}
		}

        private void btnOdjava_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnLozinka_Click(object sender, EventArgs e)
        {
            PromenaLozinkeForm f = new PromenaLozinkeForm();
            if (f.ShowDialog(this) == DialogResult.OK)
                this.DialogResult = DialogResult.OK;
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            int id = -1;
            using (OpzContext a = new OpzContext())
            {
                id = (from p in a.ListaZaposlenih where p.KorisnickoIme == Username select p.Id).Single();
            }
            var m = new OdsutniZaposleniChartForm(id);
            m.ShowDialog();
        }
    }
}
