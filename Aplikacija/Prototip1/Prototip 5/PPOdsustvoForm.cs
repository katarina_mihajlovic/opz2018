﻿using MetroFramework.Forms;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Persistence.Entities;
using MetroFramework;

namespace Prototip_5
{
    public partial class PPOdsustvoForm : MetroForm
    {
        int idZaposlenog;

        public PPOdsustvoForm()
        {
            InitializeComponent();
        }

        public PPOdsustvoForm(int idZap)
            : this()
        {
            idZaposlenog = idZap;
            RefreshList();
        }

        void RefreshList()
        {
            DateTime datum = DateTime.Now;
            using (OpzContext kontekst = new OpzContext())
            {
                Zaposleni zap = kontekst.ListaZaposlenih.Find(idZaposlenog);
                gridOdsustva.DataSource = (from ods in zap.Odsustva
                                           where ods.Kraj > datum
                                           orderby ods.Pocetak descending
                                           select new
                                           {
                                               Id = ods.Id,
                                               PocetakOdsustva = ods.Pocetak,
                                               KrajOdsustva = ods.Kraj,
                                               BrojUDelovodniku = ods.BrojUDelovodniku,
                                               TipOdsustva = ods.Tip
                                           }).ToList();
            }
            gridOdsustva.Columns[0].Visible = false;
            gridOdsustva.Columns[1].HeaderText = "Početak odsustva";
            gridOdsustva.Columns[2].HeaderText = "Kraj odsustva";
            gridOdsustva.Columns[3].HeaderText = "Broj u delovodniku";
            gridOdsustva.Columns[4].HeaderText = "Tip odsustva";

            foreach (DataGridViewColumn dgvcol in gridOdsustva.Columns)
            {
                if (dgvcol.Index == 0) continue;
                dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void btnPrekini_Click(object sender, EventArgs e)
        {
            using (OpzContext kontekst = new OpzContext())
            {
                Zaposleni zap = kontekst.ListaZaposlenih.Find(idZaposlenog);
                foreach (DataGridViewRow red in gridOdsustva.SelectedRows)
                {
                    int id = (int)red.Cells[0].Value;
                    Odsustvo od = (from odsustvo in zap.Odsustva
                                   where odsustvo.Id == id
                                   select odsustvo).ToList()[0];
                    if (od.Pocetak.Date > DateTime.Today)
                    {
                        kontekst.ListaOdsustva.Remove(od);
                    }
                    else
                    {
                        od.Kraj = DateTime.Today;
                    }
                    kontekst.SaveChanges();
                }
            }
            RefreshList();
            MetroMessageBox.Show(this, "Data odsustva su uspešno prekinuta.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
        }

        private void btnProduzi_Click(object sender, EventArgs e)
        {
            if (gridOdsustva.SelectedRows.Count != 1)
            {
                return;
            }
            int id = (int)gridOdsustva.SelectedRows[0].Cells[0].Value;
            using (OpzContext kontekst = new OpzContext())
            {
                Zaposleni zap = kontekst.ListaZaposlenih.Find(idZaposlenog);
                Odsustvo od = (from odsustvo in zap.Odsustva
                               where odsustvo.Id == id
                               select odsustvo).ToList()[0];
                if (od.Kraj >= metroDateTime1.Value.Date)
                {
                    MetroMessageBox.Show(this, "Odsustvo se ne može produžiti jer novi datum kraja nije validan.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                    return;
                }
                od.Kraj = metroDateTime1.Value.Date;
                kontekst.SaveChanges();
            }
            RefreshList();
            MetroMessageBox.Show(this, "Dato odsustvo je uspešno produženo.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
        }
    }
}
