﻿namespace Prototip_5
{
	partial class MenadzerGlavnaForma
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblMenadzerIme = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.lblRadnoMesto = new MetroFramework.Controls.MetroLabel();
            this.metroGrid1 = new MetroFramework.Controls.MetroGrid();
            this.imgMenadzer = new System.Windows.Forms.PictureBox();
            this.btnOdjava = new MetroFramework.Controls.MetroButton();
            this.btnLozinka = new MetroFramework.Controls.MetroButton();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMenadzer)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMenadzerIme
            // 
            this.lblMenadzerIme.AutoSize = true;
            this.lblMenadzerIme.Location = new System.Drawing.Point(370, 28);
            this.lblMenadzerIme.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMenadzerIme.Name = "lblMenadzerIme";
            this.lblMenadzerIme.Size = new System.Drawing.Size(101, 19);
            this.lblMenadzerIme.TabIndex = 0;
            this.lblMenadzerIme.Text = "Ime menadzera";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(279, 28);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(75, 19);
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Text = "Menadžer: ";
            // 
            // lblRadnoMesto
            // 
            this.lblRadnoMesto.AutoSize = true;
            this.lblRadnoMesto.Location = new System.Drawing.Point(370, 46);
            this.lblRadnoMesto.Name = "lblRadnoMesto";
            this.lblRadnoMesto.Size = new System.Drawing.Size(87, 19);
            this.lblRadnoMesto.TabIndex = 3;
            this.lblRadnoMesto.Text = "Radno mesto";
            // 
            // metroGrid1
            // 
            this.metroGrid1.AllowUserToResizeRows = false;
            this.metroGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.metroGrid1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.metroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(113)))), ((int)(((byte)(189)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(125)))), ((int)(((byte)(196)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(125)))), ((int)(((byte)(196)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGrid1.EnableHeadersVisualStyles = false;
            this.metroGrid1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.Location = new System.Drawing.Point(30, 119);
            this.metroGrid1.Name = "metroGrid1";
            this.metroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(113)))), ((int)(((byte)(189)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(125)))), ((int)(((byte)(196)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1.Size = new System.Drawing.Size(620, 440);
            this.metroGrid1.Style = MetroFramework.MetroColorStyle.Pink;
            this.metroGrid1.TabIndex = 4;
            this.metroGrid1.UseStyleColors = true;
            this.metroGrid1.DoubleClick += new System.EventHandler(this.metroGrid1_DoubleClick);
            // 
            // imgMenadzer
            // 
            this.imgMenadzer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.imgMenadzer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.imgMenadzer.ErrorImage = global::Prototip_5.Properties.Resources.default_person;
            this.imgMenadzer.Image = global::Prototip_5.Properties.Resources.default_person;
            this.imgMenadzer.InitialImage = global::Prototip_5.Properties.Resources.default_person;
            this.imgMenadzer.Location = new System.Drawing.Point(579, 24);
            this.imgMenadzer.Name = "imgMenadzer";
            this.imgMenadzer.Size = new System.Drawing.Size(71, 71);
            this.imgMenadzer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgMenadzer.TabIndex = 2;
            this.imgMenadzer.TabStop = false;
            // 
            // btnOdjava
            // 
            this.btnOdjava.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnOdjava.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnOdjava.Location = new System.Drawing.Point(30, 83);
            this.btnOdjava.Name = "btnOdjava";
            this.btnOdjava.Size = new System.Drawing.Size(80, 23);
            this.btnOdjava.TabIndex = 5;
            this.btnOdjava.Text = "Odjava";
            this.btnOdjava.UseSelectable = true;
            this.btnOdjava.Click += new System.EventHandler(this.btnOdjava_Click);
            // 
            // btnLozinka
            // 
            this.btnLozinka.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnLozinka.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnLozinka.Location = new System.Drawing.Point(116, 83);
            this.btnLozinka.Name = "btnLozinka";
            this.btnLozinka.Size = new System.Drawing.Size(120, 23);
            this.btnLozinka.TabIndex = 6;
            this.btnLozinka.Text = "Promeni lozinku";
            this.btnLozinka.UseSelectable = true;
            this.btnLozinka.Click += new System.EventHandler(this.btnLozinka_Click);
            // 
            // metroButton1
            // 
            this.metroButton1.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton1.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.metroButton1.Location = new System.Drawing.Point(242, 83);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(120, 23);
            this.metroButton1.TabIndex = 7;
            this.metroButton1.Text = "Prikaži odsustva";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // MenadzerGlavnaForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 582);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.btnLozinka);
            this.Controls.Add(this.btnOdjava);
            this.Controls.Add(this.metroGrid1);
            this.Controls.Add(this.lblRadnoMesto);
            this.Controls.Add(this.imgMenadzer);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.lblMenadzerIme);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(680, 582);
            this.Name = "MenadzerGlavnaForma";
            this.Padding = new System.Windows.Forms.Padding(30, 83, 30, 28);
            this.Style = MetroFramework.MetroColorStyle.Pink;
            this.Text = "Pregled radnika";
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMenadzer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private MetroFramework.Controls.MetroLabel lblMenadzerIme;
		private MetroFramework.Controls.MetroLabel metroLabel1;
		private System.Windows.Forms.PictureBox imgMenadzer;
		private MetroFramework.Controls.MetroLabel lblRadnoMesto;
		private MetroFramework.Controls.MetroGrid metroGrid1;
        private MetroFramework.Controls.MetroButton btnOdjava;
        private MetroFramework.Controls.MetroButton btnLozinka;
        private MetroFramework.Controls.MetroButton metroButton1;
    }
}