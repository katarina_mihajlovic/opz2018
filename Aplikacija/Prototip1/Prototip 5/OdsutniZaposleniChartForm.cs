﻿using LiveCharts;
using LiveCharts.Wpf;
using MetroFramework.Forms;
using Persistence.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using MetroFramework;

namespace Prototip_5
{
    public partial class OdsutniZaposleniChartForm : MetroForm
    {
        public OdsutniZaposleniChartForm(int idMenadzera)
        {
            InitializeComponent();
            IdMenadzera = idMenadzera;
        }

        public int IdMenadzera { get; set; }

        private void ProsekPlateChartForm_Load(object sender, EventArgs e)
        {
            DateTime start = new DateTime(2005, 1, 1);
            DateTime end = new DateTime(DateTime.Now.Year, 1, 1);
            List<string> cb1 = new List<string>();
            List<string> cb2 = new List<string>();

            for (DateTime it = start; it <= end; it = it.AddYears(1))
            {
                cb1.Add(it.ToString("yyyy"));
                cb2.Add(it.ToString("yyyy"));
            }

            chart.Series.Clear();
            chart.AxisX.Clear();
            chart.AxisY.Clear();
        }

        private void UpdateChart(List<Tuple<int, string>> lista)
        {
            //mora da se reloaduje ceo, inace ne prikazuje labele za kolone
            chart.Series.Clear();
            chart.AxisX.Clear();
            chart.AxisY.Clear();

            chart.Visible = true;

            chart.Series = new SeriesCollection
            {
                new ColumnSeries
                {
                    DataLabels = true,
                    Title = "Broj odsutnih je",
                    LabelPoint = point => Math.Round(point.Y).ToString(),
                    Values = new ChartValues<int>(from p in lista select p.Item1)                  
                }
            };
           
            
            chart.AxisY.Add(new Axis
            {
                Title = "Broj odsutnih zaposlenih",
                LabelFormatter = value => value.ToString("N"),
                MinValue = 0,
                MaxValue = chart.Series.Count > 7 ? Double.NaN : 10

            });

            chart.AxisX.Add(new Axis
            {
                Title = "Datum",
                Labels = (from p in lista select p.Item2).ToList(),
                Separator = new Separator() { Step = 1, IsEnabled = false },
                LabelsRotation = 20,
            });
        }

        private void btnPrikaziGrafikon_Click(object sender, EventArgs e)
        {
            if (datumOd.Value.Date > datumDo.Value.Date || datumOd.Value.Date <= DateTime.Today)
            {
                MetroMessageBox.Show(this, "Izabrani datumi su nevalidni.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                return;
            }
            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync(new Tuple<DateTime,DateTime,int>(datumOd.Value.Date, datumDo.Value.Date, IdMenadzera));
            }
        }

        #region Spinner
        private void StartSpinning()
        {
            lblSpinner.Visible = true;
            circularProgressBar1.Visible = true;

            datumOd.Enabled = false;
            datumDo.Enabled = false;
            btnPrikaziGrafikon.Enabled = false;
        }

        private void StopSpinning()
        {
            lblSpinner.Visible = false;
            circularProgressBar1.Visible = false;

            datumOd.Enabled = true;
            datumDo.Enabled = true;
            btnPrikaziGrafikon.Enabled = true;
        }
        #endregion

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Invoke(spnstart);

            Tuple<DateTime, DateTime, int> arg = e.Argument as Tuple<DateTime, DateTime, int>;
            List<Tuple<int, string>> brojOdsutnih = new List<Tuple<int, string>>();
            RefreshDel d1 = UpdateChart;

            DateTime startDate = arg.Item1;
            DateTime endDate = arg.Item2;
            int idMenadzera = arg.Item3;

            using (OpzContext kontekst = new OpzContext())
            {
                List<String> smeneTipovi = kontekst.ListaSmena.Select(x => x.TipSmene).Distinct().ToList();
                Dictionary<String, bool> radniDan = new Dictionary<String, bool>();
                

                foreach (String smen in smeneTipovi)
                {
                    for (DateTime dan = startDate; dan <= endDate; dan = dan.AddDays(1))
                    {
                        String danInWeek = "";
                        if (dan.DayOfWeek == DayOfWeek.Monday)
                        {
                            danInWeek = "ponedeljak";
                        }
                        else if (dan.DayOfWeek == DayOfWeek.Tuesday)
                        {
                            danInWeek = "utorak";
                        }
                        else if (dan.DayOfWeek == DayOfWeek.Wednesday)
                        {
                            danInWeek = "sreda";
                        }
                        else if (dan.DayOfWeek == DayOfWeek.Thursday)
                        {
                            danInWeek = "cetvrtak";
                        }
                        else if (dan.DayOfWeek == DayOfWeek.Friday)
                        {
                            danInWeek = "petak";
                        }
                        else if (dan.DayOfWeek == DayOfWeek.Saturday)
                        {
                            danInWeek = "subota";
                        }
                        else
                        {
                            danInWeek = "nedelja";
                        }

                        var datumi = (from x in kontekst.ListaSmena
                                      where x.DatumDonosenja < dan && x.TipSmene == smen && x.DanUNedelji == danInWeek
                                      orderby x.DatumDonosenja descending
                                      select new
                                      {
                                          Pocetak = x.Pocetak,
                                          Kraj = x.Kraj
                                      }).ToList();

                        if (datumi.Count == 0)
                        {
                            radniDan.Add(smen + "/" + dan.ToString(), false);
                        }
                        else
                        {
                            radniDan.Add(smen + "/" + dan.ToString(), true);
                        }
                    }
                }

               

                for (DateTime dan = startDate; dan <= endDate; dan = dan.AddDays(1))
                {
                    int brojche = 0;
                    var a = (from p in kontekst.ListaZaposlenih where p.Nadredjeni != null && p.Nadredjeni.Id == idMenadzera select p).ToList();
                    foreach (Zaposleni zap in a)
                    {
                        var nazivlist = (from x in zap.RadnaVremena
                                         where x.Datum < dan
                                         orderby x.Datum descending
                                         select x.Smena).ToList();

                        if (nazivlist.Count == 0)
                        {
                            continue;
                        }

                        String naziv = nazivlist[0];

                        if (radniDan[naziv + "/" + dan.ToString()])
                        {
                            var ods = (from o in zap.Odsustva
                                       where o.Pocetak <= dan && o.Kraj >= dan
                                       select o).ToList();

                            if (ods.Count != 0)
                            {
                                brojche++;
                            }
                        }
                    }
                    brojOdsutnih.Add(new Tuple<int, string>(brojche, dan.ToString("dd.MM.")));
                }
            }
            
            Invoke(d1, new object[] { brojOdsutnih });
        }

        private delegate void Del2();
        private delegate void RefreshDel(List<Tuple<int, string>> a);
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Del2 spnstop = StopSpinning;
            //zaustavljanje spinnera
            Invoke(spnstop);
        }

        private void OdsutniZaposleniChartForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                e.Cancel = true;
            }
        }
    }
}
