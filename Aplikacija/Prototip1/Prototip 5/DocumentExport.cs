﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using PicoXLSX;
using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Prototip_5
{
    public static class DocumentExport
    {
        #region pdf
        public static void DatagridviewToPdf(DataGridView dgv, string path)
        {
            BaseFont bf = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\Arial.ttf", BaseFont.IDENTITY_H, true);

            //table head
            iTextSharp.text.pdf.PdfPTable table = new iTextSharp.text.pdf.PdfPTable(dgv.Columns.Count);
            int[] widths = new int[dgv.Columns.Count];
            for (int j = 0; j < dgv.Columns.Count; j++)
            {
                int curr = dgv.Columns[j].Name.Length;
                widths[j] = curr > widths[j] ? curr : widths[j];
            }
            for (int x = 0; x < dgv.Columns.Count; x++)
            {
                //treba da se sracuna sirina svake kolone
                //prema borju kolona moze da se podesi velicina fonta kako bi sve stalo na a4


                for (int j = 0; j < dgv.Rows.Count; j++)
                {
                    if (dgv.Rows[j].Cells[x].Value == null) continue;
                    int curr = (int)dgv.Rows[j].Cells[x].Value.ToString().Length;
                    widths[x] = curr > widths[x] ? curr : widths[x];
                }

                string cellText = dgv.Columns[x].Name.ToString();

                //Set Font and Font Color  
                iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.BOLD);
                //font.Color = DeviceNColor.BLACK;
                iTextSharp.text.pdf.PdfPCell cell = new iTextSharp.text.pdf.PdfPCell(new Phrase(10, cellText, font));
                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                cell.Padding = 2;
                //Set Header Row BackGround Color  
                cell.BackgroundColor = DeviceNColor.LIGHT_GRAY;
                table.AddCell(cell);
            }
            table.SetWidths(widths);



            //table body
            for (int i = 0; i < dgv.Rows.Count; i++)
            {

                for (int j = 0; j < dgv.Columns.Count; j++)
                {
                    var cell1 = dgv.Rows[i].Cells[j].Value;
                    string cellText = cell1 == null ? String.Empty : cell1.ToString();

                    //Set Font and Font Color  
                    iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 8, iTextSharp.text.Font.NORMAL);
                    font.Color = DeviceNColor.BLACK;
                    iTextSharp.text.pdf.PdfPCell cell = new iTextSharp.text.pdf.PdfPCell(new Phrase(12, cellText, font));

                    //Set Color of row  
                    if (i % 2 == 0)
                    {
                        //Set Row BackGround Color  
                        cell.BackgroundColor = new BaseColor(234, 234, 234);
                    }

                    table.AddCell(cell);
                }

            }

            //Create the PDF Document  
            Document doc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
            doc.Open(); //open the document in order to write inside.
            doc.Add(table);
            doc.Close();

        }
        #endregion

        #region html

        public static void DatagridviewToHtml(DataGridView dgv, string path)
        {
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(path, false, Encoding.UTF8))
            {
                file.WriteLine(ConvertDataGridViewToHTMLWithFormatting(dgv));
            }
        }

        private static string ConvertDataGridViewToHTMLWithFormatting(DataGridView dgv)
        {
            StringBuilder sb = new StringBuilder();
            //create html & table
            sb.AppendLine(@"<html><head><style> table { font-family: 'Segoe UI', Cambria, Helvetica, Verdana; border-collapse: collapse; width: 100%; } td, #thd { border: 1px solid #ddd; padding: 8px; } tr:nth-child(even){background-color: #f2f2f2;} tr:hover {background-color: #ddd;} #thd { padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: blue; color: white; } </style></head><body><center><table border='1' cellpadding='0' cellspacing='0'>");
            sb.AppendLine("<tr id='thd'>");
            //create table header
            for (int i = 0; i < dgv.Columns.Count; i++)
            {
                sb.Append(DGVHeaderCellToHTMLWithFormatting(dgv, i));
                sb.Append(DGVCellFontAndValueToHTML(dgv.Columns[i].HeaderText, dgv.Columns[i].HeaderCell.Style.Font));
                sb.AppendLine("</td>");
            }
            sb.AppendLine("</tr>");
            //create table body
            for (int rowIndex = 0; rowIndex < dgv.Rows.Count; rowIndex++)
            {
                sb.AppendLine("<tr>");
                foreach (DataGridViewCell dgvc in dgv.Rows[rowIndex].Cells)
                {
                    sb.AppendLine(DGVCellToHTMLWithFormatting(dgv, rowIndex, dgvc.ColumnIndex));
                    string cellValue = dgvc.Value == null ? string.Empty : dgvc.Value.ToString();
                    sb.AppendLine(DGVCellFontAndValueToHTML(cellValue, dgvc.Style.Font));
                    sb.AppendLine("</td>");
                }
                sb.AppendLine("</tr>");
            }
            //table footer & end of html file
            sb.AppendLine("</table></center></body></html>");
            return sb.ToString();
        }

        //TODO: Add more cell styles described here: https://msdn.microsoft.com/en-us/library/1yef90x0(v=vs.110).aspx
        private static string DGVHeaderCellToHTMLWithFormatting(DataGridView dgv, int col)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<td");
            sb.Append(DGVCellColorToHTML(dgv.Columns[col].HeaderCell.Style.ForeColor, dgv.Columns[col].HeaderCell.Style.BackColor));
            sb.Append(DGVCellAlignmentToHTML(dgv.Columns[col].HeaderCell.Style.Alignment));
            sb.Append(">");
            return sb.ToString();
        }

        private static string DGVCellToHTMLWithFormatting(DataGridView dgv, int row, int col)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<td");
            sb.Append(DGVCellColorToHTML(dgv.Rows[row].Cells[col].Style.ForeColor, dgv.Rows[row].Cells[col].Style.BackColor));
            sb.Append(DGVCellAlignmentToHTML(dgv.Rows[row].Cells[col].Style.Alignment));
            sb.Append(">");
            return sb.ToString();
        }

        private static string DGVCellColorToHTML(System.Drawing.Color foreColor, System.Drawing.Color backColor)
        {
            if (foreColor.Name == "0" && backColor.Name == "0") return string.Empty;

            StringBuilder sb = new StringBuilder();
            sb.Append(" style=\"");
            if (foreColor.Name != "0" && backColor.Name != "0")
            {
                sb.Append("color:#");
                sb.Append(foreColor.R.ToString("X2") + foreColor.G.ToString("X2") + foreColor.B.ToString("X2"));
                sb.Append("; background-color:#");
                sb.Append(backColor.R.ToString("X2") + backColor.G.ToString("X2") + backColor.B.ToString("X2"));
            }
            else if (foreColor.Name != "0" && backColor.Name == "0")
            {
                sb.Append("color:#");
                sb.Append(foreColor.R.ToString("X2") + foreColor.G.ToString("X2") + foreColor.B.ToString("X2"));
            }
            else //if (foreColor.Name == "0" &&  backColor.Name != "0")
            {
                sb.Append("background-color:#");
                sb.Append(backColor.R.ToString("X2") + backColor.G.ToString("X2") + backColor.B.ToString("X2"));
            }

            sb.Append(";\"");
            return sb.ToString();
        }

        private static string DGVCellFontAndValueToHTML(string value, System.Drawing.Font font)
        {
            //If no font has been set then assume its the default as someone would be expected in HTML or Excel
            if (font == null/* || font == this.Font && !(font.Bold | font.Italic | font.Underline | font.Strikeout)*/) return value;
            StringBuilder sb = new StringBuilder();
            sb.Append(" ");
            if (font.Bold) sb.Append("<b>");
            if (font.Italic) sb.Append("<i>");
            if (font.Strikeout) sb.Append("<strike>");

            //The <u> element was deprecated in HTML 4.01. The new HTML 5 tag is: text-decoration: underline
            if (font.Underline) sb.Append("<u>");

            string size = string.Empty;
            /*if (font.Size != this.Font.Size) */
            size = "font-size: " + font.Size + "pt;";

            //The <font> tag is not supported in HTML5. Use CSS or a span instead. 
            //if (font.FontFamily.Name != this.Font.Name)
            {
                sb.Append("<span style=\"font-family: ");
                sb.Append(font.FontFamily.Name);
                sb.Append("; ");
                sb.Append(size);
                sb.Append("\">");
            }
            sb.Append(value);
            /*if (font.FontFamily.Name != this.Font.Name)*/
            sb.Append("</span>");

            if (font.Underline) sb.Append("</u>");
            if (font.Strikeout) sb.Append("</strike>");
            if (font.Italic) sb.Append("</i>");
            if (font.Bold) sb.Append("</b>");

            return sb.ToString();
        }

        private static string DGVCellAlignmentToHTML(DataGridViewContentAlignment align)
        {
            if (align == DataGridViewContentAlignment.NotSet) return string.Empty;

            string horizontalAlignment = string.Empty;
            string verticalAlignment = string.Empty;
            CellAlignment(align, ref horizontalAlignment, ref verticalAlignment);
            StringBuilder sb = new StringBuilder();
            sb.Append(" align='");
            sb.Append(horizontalAlignment);
            sb.Append("' valign='");
            sb.Append(verticalAlignment);
            sb.Append("'");
            return sb.ToString();
        }

        private static void CellAlignment(DataGridViewContentAlignment align, ref string horizontalAlignment, ref string verticalAlignment)
        {
            switch (align)
            {
                case DataGridViewContentAlignment.MiddleRight:
                    horizontalAlignment = "right";
                    verticalAlignment = "middle";
                    break;
                case DataGridViewContentAlignment.MiddleLeft:
                    horizontalAlignment = "left";
                    verticalAlignment = "middle";
                    break;
                case DataGridViewContentAlignment.MiddleCenter:
                    horizontalAlignment = "centre";
                    verticalAlignment = "middle";
                    break;
                case DataGridViewContentAlignment.TopCenter:
                    horizontalAlignment = "centre";
                    verticalAlignment = "top";
                    break;
                case DataGridViewContentAlignment.BottomCenter:
                    horizontalAlignment = "centre";
                    verticalAlignment = "bottom";
                    break;
                case DataGridViewContentAlignment.TopLeft:
                    horizontalAlignment = "left";
                    verticalAlignment = "top";
                    break;
                case DataGridViewContentAlignment.BottomLeft:
                    horizontalAlignment = "left";
                    verticalAlignment = "bottom";
                    break;
                case DataGridViewContentAlignment.TopRight:
                    horizontalAlignment = "right";
                    verticalAlignment = "top";
                    break;
                case DataGridViewContentAlignment.BottomRight:
                    horizontalAlignment = "right";
                    verticalAlignment = "bottom";
                    break;

                default: //DataGridViewContentAlignment.NotSet
                    horizontalAlignment = "left";
                    verticalAlignment = "middle";
                    break;
            }
        }

        #endregion

        #region excel
        //moze i uz interop, koji mozda moze da razvlaci kolone

        public static void DatagridviewToExcel(DataGridView dgv, string path)
        {
            /*ovo moze da radi samo ako je excel instaliran
            dgv.RowHeadersVisible = false;
            dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dgv.MultiSelect = true;
            dgv.SelectAll();
            dgv.RowHeadersVisible = true;

            DataObject dataObj = dgv.GetClipboardContent();
            dgv.ClearSelection();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);

            Microsoft.Office.Interop.Excel.Application excelApp;
            Microsoft.Office.Interop.Excel.Workbook excelDoc;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            excelApp = new Microsoft.Office.Interop.Excel.Application();
            excelApp.Visible = true;
            excelDoc = excelApp.Workbooks.Add(misValue);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)excelDoc.Worksheets.get_Item(1);
            Microsoft.Office.Interop.Excel.Range CR = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[1, 1];
            CR.Select();
            xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
            excelDoc.SaveAs(path, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
            false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
            Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            excelDoc.Close();
            */



            Workbook workbook = new Workbook(path, "Sheet1");         // Create new workbook with a worksheet called Sheet1

            foreach (DataGridViewColumn col in dgv.Columns)
            {
                workbook.CurrentWorksheet.AddNextCell(col.Name, Style.BasicStyles.DottedFill_0_125);
            }
            workbook.CurrentWorksheet.GoToNextRow();
            for (int rowIndex = 0; rowIndex < dgv.Rows.Count; rowIndex++)
            {
                foreach (DataGridViewCell dgvc in dgv.Rows[rowIndex].Cells)
                {
                    string cellValue = dgvc.Value == null ? string.Empty : dgvc.Value.ToString();
                    workbook.CurrentWorksheet.AddNextCell(cellValue);
                }
                workbook.CurrentWorksheet.GoToNextRow();
            }



            workbook.Save();
        }
        #endregion

        #region word
        //jedino moze uz interop
        #endregion
    }
}
