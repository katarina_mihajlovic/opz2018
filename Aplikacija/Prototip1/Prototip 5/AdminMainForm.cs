﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Persistence.Entities;
using SharedComponents.Presenters;
using SharedComponents.Forms;
using System.IO;
using static SharedComponents.Forms.Login;
using MetroFramework;

namespace Prototip_5
{
    public partial class AdminMainForm : MetroForm,IUsernameOwner
    {
        #region bazaPodataka
        public string Username { get; set; }
        #endregion

        #region sortiranje
        private class Sort
        {
            public int Kriterijum { get; set; }
            public int Smer { get; set; }
        }
        #endregion
		
        public AdminMainForm(string username)
        {
            InitializeComponent();
            ribbon1.ThemeColor = RibbonTheme.Normal;
            backgroundWorker1.WorkerReportsProgress = true;
            Username = username;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DisableOptions();
            metroComboBox1.DataSource = new List<String>() { "Broj zaposlenog", "Ime", "Srednje slovo", "Prezime", "Radno mesto", "Datum rođenja", "Datum početka rada"};
            metroComboBox2.DataSource = new List<String>() { "Rastuće","Opadajuće"};
            metroComboBox1.SelectedIndexChanged += BtnPretraziClick;//mora ovako
            metroComboBox2.SelectedIndexChanged += BtnPretraziClick;//mora ovako
            metroGrid1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
           // pictureBox1.Image = Properties.Resources.default_image;
            BtnPretraziClick(this, EventArgs.Empty);
            circularProgressBar1.Enabled = false;
        }

        private void BtnPretraziClick(object sender, EventArgs e)
        {
            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync(new Sort{ Kriterijum = metroComboBox1.SelectedIndex, Smer = metroComboBox2.SelectedIndex });
            }
        }

        private void btnDodajOdsustvo_DoubleClick(object sender, EventArgs e)
        {
            if (metroGrid1.SelectedRows.Count == 1)
            {
                //biranje broja zaposlenog, u bazi je to ID
                Int32? i = metroGrid1.SelectedRows[0].Cells[0].Value as Int32?;
                if (i == null)
                {
                    MessageBox.Show("Greska po indeksu.");
                    return;
                }

                OdsustvoForm v = new OdsustvoForm();
                OdsustvoPresenter p = new OdsustvoPresenter(i.Value, v);
                v.ShowDialog();
            }
        }

        //sledeci kod treba da azurira dgv u sporednoj niti, treba i satic u formi
        private delegate void Del1(Object o);
        private delegate void Del2();
        
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Del1 del = UpdateDgv;
            Del2 spnstart = StartSpinning;
            Del2 spnstop = StopSpinning;
            Sort sortVarijanta = (Sort)e.Argument;
            //pokretanje spinnera
            Invoke(spnstart);
            object col = null;
            using (OpzContext kontekst = new OpzContext())
            {
                var src = kontekst.PretraziZaposlene(tbPretrazi.Text);

                if (sortVarijanta.Kriterijum == 1)
                {
                    if (sortVarijanta.Smer == 0)
                    {
                         col = (from zap in src
                                orderby zap.Ime ascending
                                select new
                                {
                                    Broj = zap.Id,
                                    Ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime,
                                    RadnoMesto = zap.RadnoMesto,
                                    DatumRodjenja = zap.DatumRodjenja.ToString("dd.MM.yyyy."),
                                    DatumPocetkaRada = zap.DatumPocetkaRada.ToString("dd.MM.yyyy.")
                                }).ToList();
                    }
                    else
                    {
                        col = (from zap in src
                               orderby zap.Ime descending
                               select new
                               {
                                   Broj = zap.Id,
                                   Ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime,
                                   RadnoMesto = zap.RadnoMesto,
                                   DatumRodjenja = zap.DatumRodjenja.ToString("dd.MM.yyyy."),
                                   DatumPocetkaRada = zap.DatumPocetkaRada.ToString("dd.MM.yyyy.")
                               }).ToList();
                    }
                }
                else if (sortVarijanta.Kriterijum == 2)
                {
                    if (sortVarijanta.Smer == 0)
                    {
                        col = (from zap in src
                               orderby zap.SSlovo ascending
                               select new
                               {
                                   Broj = zap.Id,
                                   Ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime,
                                   RadnoMesto = zap.RadnoMesto,
                                   DatumRodjenja = zap.DatumRodjenja.ToString("dd.MM.yyyy."),
                                   DatumPocetkaRada = zap.DatumPocetkaRada.ToString("dd.MM.yyyy.")
                               }).ToList();
                    }
                    else
                    {
                        col = (from zap in src
                               orderby zap.SSlovo descending
                               select new
                               {
                                   Broj = zap.Id,
                                   Ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime,
                                   RadnoMesto = zap.RadnoMesto,
                                   DatumRodjenja = zap.DatumRodjenja.ToString("dd.MM.yyyy."),
                                   DatumPocetkaRada = zap.DatumPocetkaRada.ToString("dd.MM.yyyy.")
                               }).ToList();
                    }
                }
                else if (sortVarijanta.Kriterijum == 3)
                {
                    if (sortVarijanta.Smer == 0)
                    {
                        col = (from zap in src
                               orderby zap.Prezime ascending
                               select new
                               {
                                   Broj = zap.Id,
                                   Ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime,
                                   RadnoMesto = zap.RadnoMesto,
                                   DatumRodjenja = zap.DatumRodjenja.ToString("dd.MM.yyyy."),
                                   DatumPocetkaRada = zap.DatumPocetkaRada.ToString("dd.MM.yyyy.")
                               }).ToList();
                    }
                    else
                    {
                        col = (from zap in src
                               orderby zap.Prezime descending
                               select new
                               {
                                   Broj = zap.Id,
                                   Ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime,
                                   RadnoMesto = zap.RadnoMesto,
                                   DatumRodjenja = zap.DatumRodjenja.ToString("dd.MM.yyyy."),
                                   DatumPocetkaRada = zap.DatumPocetkaRada.ToString("dd.MM.yyyy.")
                               }).ToList();
                    }
                }
                else if (sortVarijanta.Kriterijum == 4)
                {
                    if (sortVarijanta.Smer == 0)
                    {
                        col = (from zap in src
                               orderby zap.RadnoMesto ascending
                               select new
                               {
                                   Broj = zap.Id,
                                   Ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime,
                                   RadnoMesto = zap.RadnoMesto,
                                   DatumRodjenja = zap.DatumRodjenja.ToString("dd.MM.yyyy."),
                                   DatumPocetkaRada = zap.DatumPocetkaRada.ToString("dd.MM.yyyy.")
                               }).ToList();
                    }
                    else
                    {
                        col = (from zap in src
                               orderby zap.RadnoMesto descending
                               select new
                               {
                                   Broj = zap.Id,
                                   Ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime,
                                   RadnoMesto = zap.RadnoMesto,
                                   DatumRodjenja = zap.DatumRodjenja.ToString("dd.MM.yyyy."),
                                   DatumPocetkaRada = zap.DatumPocetkaRada.ToString("dd.MM.yyyy.")
                               }).ToList();
                    }
                }
                else if (sortVarijanta.Kriterijum == 5)
                {
                    if (sortVarijanta.Smer == 0)
                    {
                        col = (from zap in src
                               orderby zap.DatumRodjenja ascending
                               select new
                               {
                                   Broj = zap.Id,
                                   Ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime,
                                   RadnoMesto = zap.RadnoMesto,
                                   DatumRodjenja = zap.DatumRodjenja.ToString("dd.MM.yyyy."),
                                   DatumPocetkaRada = zap.DatumPocetkaRada.ToString("dd.MM.yyyy.")
                               }).ToList();
                    }
                    else
                    {
                        col = (from zap in src
                               orderby zap.DatumRodjenja descending
                               select new
                               {
                                   Broj = zap.Id,
                                   Ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime,
                                   RadnoMesto = zap.RadnoMesto,
                                   DatumRodjenja = zap.DatumRodjenja.ToString("dd.MM.yyyy."),
                                   DatumPocetkaRada = zap.DatumPocetkaRada.ToString("dd.MM.yyyy.")
                               }).ToList();
                    }
                }
                else if (sortVarijanta.Kriterijum == 6)
                {
                    if (sortVarijanta.Smer == 0)
                    {
                        col = (from zap in src
                               orderby zap.DatumPocetkaRada ascending
                               select new
                               {
                                   Broj = zap.Id,
                                   Ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime,
                                   RadnoMesto = zap.RadnoMesto,
                                   DatumRodjenja = zap.DatumRodjenja.ToString("dd.MM.yyyy."),
                                   DatumPocetkaRada = zap.DatumPocetkaRada.ToString("dd.MM.yyyy.")
                               }).ToList();
                    }
                    else
                    {
                        col = (from zap in src
                               orderby zap.DatumPocetkaRada descending
                               select new
                               {
                                   Broj = zap.Id,
                                   Ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime,
                                   RadnoMesto = zap.RadnoMesto,
                                   DatumRodjenja = zap.DatumRodjenja.ToString("dd.MM.yyyy."),
                                   DatumPocetkaRada = zap.DatumPocetkaRada.ToString("dd.MM.yyyy.")
                               }).ToList();
                    }
                }
                else if (sortVarijanta.Kriterijum == 0)
                {
                    if (sortVarijanta.Smer == 0)
                    {
                        col = (from zap in src
                               orderby zap.Id ascending
                               select new
                               {
                                   Broj = zap.Id,
                                   Ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime,
                                   RadnoMesto = zap.RadnoMesto,
                                   DatumRodjenja = zap.DatumRodjenja.ToString("dd.MM.yyyy."),
                                   DatumPocetkaRada = zap.DatumPocetkaRada.ToString("dd.MM.yyyy.")
                               }).ToList();
                    }
                    else
                    {
                        col = (from zap in src
                               orderby zap.Id descending
                               select new
                               {
                                   Broj = zap.Id,
                                   Ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime,
                                   RadnoMesto = zap.RadnoMesto,
                                   DatumRodjenja = zap.DatumRodjenja.ToString("dd.MM.yyyy."),
                                   DatumPocetkaRada = zap.DatumPocetkaRada.ToString("dd.MM.yyyy.")
                               }).ToList();
                    }
                }
            }
            Invoke(del, new object[] { col });
        }

        private void UpdateDgv(Object col)
        {
            metroGrid1.DataSource = col;
            metroGrid1.ClearSelection();
            pictureBox1.Image = Properties.Resources.default_person;
            metroGrid1.Columns[0].HeaderText = "Broj zaposlenog";
            metroGrid1.Columns[1].HeaderText = "Ime i prezime";
            metroGrid1.Columns[2].HeaderText = "Radno mesto";
            metroGrid1.Columns[3].HeaderText = "Datum rođenja";
            metroGrid1.Columns[4].HeaderText = "Datum početka rada";

            foreach (DataGridViewColumn dgvcol in metroGrid1.Columns)
            {
                if (dgvcol.Index == 0) continue;
                dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Del2 spnstop = StopSpinning;
            //zaustavljanje spinnera
            Invoke(spnstop);
        }

        private void rbtnDodajNovog_Click(object sender, EventArgs e)
        {
            var a = new NoviZaposleniForm();
            var b = new NoviZaposleniPresenter(a);
            if (a.ShowDialog() == DialogResult.OK)
            {
                if (!backgroundWorker1.IsBusy)
                {
                    backgroundWorker1.RunWorkerAsync(new Sort { Kriterijum = metroComboBox1.SelectedIndex, Smer = metroComboBox2.SelectedIndex });
                }
            }
        }

        private void metroGrid1_SelectionChanged(object sender, EventArgs e)
        {
            if (metroGrid1.SelectedRows.Count != 1)
            {
                DisableOptions();
                return;
            }
            Int32? id = metroGrid1.SelectedRows[0].Cells[0].Value as Int32?;
            if (id != null)
            {
                using (OpzContext kontekst = new OpzContext())
                {
                    Zaposleni z = kontekst.ListaZaposlenih.Find(id);
                    if (z.Slike != null && z.Slike.Count > 0)
                    {
                        var lista = z.Slike.OrderByDescending(x => x.Datum).ToList();
                        using (var ms = new MemoryStream(lista[0].Image))
                        {
                            pictureBox1.Image = Image.FromStream(ms);
                        }
                    }
                    else
                    {
                        pictureBox1.Image = Properties.Resources.default_person;
                    }
                }
            }
        }

        #region Spinner
        private void StartSpinning()
        {
            lblSpinner.Visible = true;
            circularProgressBar1.Visible = true;
            DisableOptions();

            btnPretrazi.Enabled = false;
            metroComboBox1.Enabled = false;
            metroComboBox2.Enabled = false;
            tbPretrazi.Enabled = false;

            btnDodajNovog.Enabled = false;
            rbtnArhivirani.Enabled = false;
            rbtnDodavanjeSmene.Enabled = false;
            rbtnIzmenaSmene.Enabled = false;
            rbtnRotacijaSmena.Enabled = false;
            rbtnBrisanjeSmene.Enabled = false;
            rbtnOdjava.Enabled = false;
            rbtnPromenaLozinke.Enabled = false;
        }

        private void StopSpinning()
        {
            lblSpinner.Visible = false;
            circularProgressBar1.Visible = false;

            btnPretrazi.Enabled = true;
            metroComboBox1.Enabled = true;
            metroComboBox2.Enabled = true;
            tbPretrazi.Enabled = true;

            btnDodajNovog.Enabled = true;
            rbtnArhivirani.Enabled = true;
            rbtnDodavanjeSmene.Enabled = true;
            rbtnIzmenaSmene.Enabled = true;
            rbtnRotacijaSmena.Enabled = true;
            rbtnBrisanjeSmene.Enabled = true;
            rbtnOdjava.Enabled = true;
            rbtnPromenaLozinke.Enabled = true;
        }

        #endregion

        private void ribbonButton15_Click(object sender, EventArgs e)
        {
            if (metroGrid1.SelectedRows.Count != 1)
            {
                return;
            }
            Int32? id = metroGrid1.SelectedRows[0].Cells[0].Value as Int32?;
            if (id == null)
            {
                return;
            }
            AzuriranjeZaposlenihForm f = new AzuriranjeZaposlenihForm();
            IzborNadredjenogForm izbor = new IzborNadredjenogForm();
            AzuriranjeZaposlenogPresenter p = new AzuriranjeZaposlenogPresenter(id.Value, f, izbor);
            if (f.ShowDialog() == DialogResult.OK)
            {
                if (!backgroundWorker1.IsBusy)
                {
                    backgroundWorker1.RunWorkerAsync(new Sort { Kriterijum = metroComboBox1.SelectedIndex, Smer = metroComboBox2.SelectedIndex });
                }
            }
        }

        private void btnOtpusti_Click(object sender, EventArgs e)
        {
            if (metroGrid1.SelectedRows.Count != 1)
                return;
            Int32? id = metroGrid1.SelectedRows[0].Cells[0].Value as Int32?;
            if (id == null)
            {
                return;
            }

            using(OpzContext c = new OpzContext())
            {
                var a = (from p in c.ListaZaposlenih where p.Id == id select p.DatumKrajaRada).ToList();
                //ova lista sadrzi ili jedan ili nula, jedan ako je nije arh. i nula ako jeste
                if(a.Count == 0)
                {
                    //arhiviran
                    MetroMessageBox.Show(this, "Izabrani zaposleni je u međuvremenu arhiviran.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
                    return;
                } else if(a[0] != null)
                {
                    MetroMessageBox.Show(this,"Izabrani zaposleni je u međuvremenu otpušten.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
                    return;
                }
            }

            OtpustanjeForm form = new OtpustanjeForm();
            OtpustanjePresenter presenter = new OtpustanjePresenter(id.Value, form);
            if (form.ShowDialog() == DialogResult.OK)
            {
                if (!backgroundWorker1.IsBusy)
                {
                    backgroundWorker1.RunWorkerAsync(new Sort { Kriterijum = metroComboBox1.SelectedIndex, Smer = metroComboBox2.SelectedIndex });
                }
            }
        }

        private void rbtnOdjava_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void rbtnPromenaLozinke_Click(object sender, EventArgs e)
        {
            PromenaLozinkeForm f = new PromenaLozinkeForm();
            if (f.ShowDialog(this) == DialogResult.OK) this.DialogResult = DialogResult.OK;
        }

        private void rbtnDodajOdsustvo_Click(object sender, EventArgs e)
        {
            if (metroGrid1.SelectedRows.Count == 1)
            {
                //biranje broja zaposlenog, u bazi je to ID
                Int32? i = metroGrid1.SelectedRows[0].Cells[0].Value as Int32?;
                if (i == null)
                {
                    MetroMessageBox.Show(this, "Problem sa indeksom.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
                    return;
                }

                OdsustvoForm v = new OdsustvoForm();
                OdsustvoPresenter p = new OdsustvoPresenter(i.Value, v);
                v.ShowDialog();
            }
        }

        private void rbtnPregledOdsustva_Click(object sender, EventArgs e)
        {
            if (metroGrid1.SelectedRows.Count != 1)
                return;
            Int32? id = metroGrid1.SelectedRows[0].Cells[0].Value as Int32?;
            if (id == null)
            {
                return;
            }
            PregledOdsustvaForm pof = new PregledOdsustvaForm(id.Value, true);
            pof.ShowDialog(this);
        }

        private void rbtnPromenaOdsustva_Click(object sender, EventArgs e)
        {
            if (metroGrid1.SelectedRows.Count != 1)
                return;
            Int32? id = metroGrid1.SelectedRows[0].Cells[0].Value as Int32?;
            if (id == null)
            {
                return;
            }

            using (OpzContext kontekst = new OpzContext())
            {
                Zaposleni z = kontekst.ListaZaposlenih.Find(id);
                if (z.DatumKrajaRada != null)
                {
                    MessageBox.Show("Taj lik vise ne radi.");
                    return;
                }
            }

            PPOdsustvoForm ppof = new PPOdsustvoForm(id.Value);
            ppof.ShowDialog();
        }

        private void rbtnDodavanjeSmene_Click(object sender, EventArgs e)
        {
            var a = new DodavanjeSmeneForm();
            a.ShowDialog();
        }

        private void rbtnIzmenaSmene_Click(object sender, EventArgs e)
        {
            var a = new IzmenaSmeneForm();
            a.ShowDialog();
        }

        private void rbtnRotacijaSmena_Click(object sender, EventArgs e)
        {
            var a = new RotacijaSmenaForm();
            a.ShowDialog();
        }

        private void rbtnBrisanjeSmene_Click(object sender, EventArgs e)
        {
            var a = new BrisanjeSmeneForm();
            a.ShowDialog();
        }

        private void rbtnArhivirani_Click(object sender, EventArgs e)
        {
            ArhiviranjeZaposlenihForm f = new ArhiviranjeZaposlenihForm();
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private void DisableOptions()
        {
            btnAzurirajPodatke.Enabled = false;
            btnPrekiniOdnos.Enabled = false;
            rbtnPregledOdsustva.Enabled = false;
            rbtnDodajOdsustvo.Enabled = false;
            rbtnPromenaOdsustva.Enabled = false;
        }

        private void EnableOptions()
        {
            btnAzurirajPodatke.Enabled = true;
            btnPrekiniOdnos.Enabled = true;
            rbtnPregledOdsustva.Enabled = true;
            rbtnDodajOdsustvo.Enabled = true;
            rbtnPromenaOdsustva.Enabled = true;
        }

        private void metroGrid1_Click(object sender, EventArgs e)
        {
            if (metroGrid1.SelectedRows.Count == 1)
            {
                EnableOptions();
            }
            else
            {
                DisableOptions();
            }
        }

        private void AdminMainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                e.Cancel = true;
            }
        }
    }
}
