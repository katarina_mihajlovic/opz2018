﻿namespace Prototip_5
{
    partial class AdminMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminMainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ribbon1 = new System.Windows.Forms.Ribbon();
            this.tabZaposleni = new System.Windows.Forms.RibbonTab();
            this.pnlZaposleni = new System.Windows.Forms.RibbonPanel();
            this.btnDodajNovog = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator13 = new System.Windows.Forms.RibbonSeparator();
            this.btnAzurirajPodatke = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator15 = new System.Windows.Forms.RibbonSeparator();
            this.btnPrekiniOdnos = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator19 = new System.Windows.Forms.RibbonSeparator();
            this.rbtnArhivirani = new System.Windows.Forms.RibbonButton();
            this.tabOdsustva = new System.Windows.Forms.RibbonTab();
            this.pnlOdsustva = new System.Windows.Forms.RibbonPanel();
            this.rbtnDodajOdsustvo = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator5 = new System.Windows.Forms.RibbonSeparator();
            this.rbtnPregledOdsustva = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator16 = new System.Windows.Forms.RibbonSeparator();
            this.rbtnPromenaOdsustva = new System.Windows.Forms.RibbonButton();
            this.tabSmene = new System.Windows.Forms.RibbonTab();
            this.pnlSmene = new System.Windows.Forms.RibbonPanel();
            this.rbtnDodavanjeSmene = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator14 = new System.Windows.Forms.RibbonSeparator();
            this.rbtnIzmenaSmene = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator17 = new System.Windows.Forms.RibbonSeparator();
            this.rbtnRotacijaSmena = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator18 = new System.Windows.Forms.RibbonSeparator();
            this.rbtnBrisanjeSmene = new System.Windows.Forms.RibbonButton();
            this.tabNalog = new System.Windows.Forms.RibbonTab();
            this.pnlNalog = new System.Windows.Forms.RibbonPanel();
            this.rbtnOdjava = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator4 = new System.Windows.Forms.RibbonSeparator();
            this.rbtnPromenaLozinke = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator11 = new System.Windows.Forms.RibbonSeparator();
            this.ribbonSeparator12 = new System.Windows.Forms.RibbonSeparator();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.metroGrid1 = new MetroFramework.Controls.MetroGrid();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnPretrazi = new MetroFramework.Controls.MetroButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tbPretrazi = new MetroFramework.Controls.MetroTextBox();
            this.metroComboBox1 = new MetroFramework.Controls.MetroComboBox();
            this.metroComboBox2 = new MetroFramework.Controls.MetroComboBox();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.lblSpinner = new MetroFramework.Controls.MetroLabel();
            this.circularProgressBar1 = new CircularProgressBar.CircularProgressBar();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDodajOdsustvo = new System.Windows.Forms.RibbonButton();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.metroPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbon1
            // 
            this.ribbon1.CaptionBarVisible = false;
            this.ribbon1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ribbon1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ribbon1.Location = new System.Drawing.Point(20, 60);
            this.ribbon1.Minimized = false;
            this.ribbon1.Name = "ribbon1";
            // 
            // 
            // 
            this.ribbon1.OrbDropDown.BorderRoundness = 8;
            this.ribbon1.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.OrbDropDown.Name = "";
            this.ribbon1.OrbDropDown.Size = new System.Drawing.Size(527, 447);
            this.ribbon1.OrbDropDown.TabIndex = 0;
            this.ribbon1.OrbStyle = System.Windows.Forms.RibbonOrbStyle.Office_2013;
            this.ribbon1.OrbVisible = false;
            this.ribbon1.RibbonTabFont = new System.Drawing.Font("Trebuchet MS", 9F);
            this.ribbon1.Size = new System.Drawing.Size(1260, 123);
            this.ribbon1.TabIndex = 0;
            this.ribbon1.Tabs.Add(this.tabZaposleni);
            this.ribbon1.Tabs.Add(this.tabOdsustva);
            this.ribbon1.Tabs.Add(this.tabSmene);
            this.ribbon1.Tabs.Add(this.tabNalog);
            this.ribbon1.TabsMargin = new System.Windows.Forms.Padding(12, 2, 20, 0);
            this.ribbon1.TabSpacing = 4;
            this.ribbon1.Text = "ribbon1";
            // 
            // tabZaposleni
            // 
            this.tabZaposleni.Name = "tabZaposleni";
            this.tabZaposleni.Panels.Add(this.pnlZaposleni);
            this.tabZaposleni.Text = "Zaposleni";
            // 
            // pnlZaposleni
            // 
            this.pnlZaposleni.ButtonMoreEnabled = false;
            this.pnlZaposleni.ButtonMoreVisible = false;
            this.pnlZaposleni.Items.Add(this.btnDodajNovog);
            this.pnlZaposleni.Items.Add(this.ribbonSeparator13);
            this.pnlZaposleni.Items.Add(this.btnAzurirajPodatke);
            this.pnlZaposleni.Items.Add(this.ribbonSeparator15);
            this.pnlZaposleni.Items.Add(this.btnPrekiniOdnos);
            this.pnlZaposleni.Items.Add(this.ribbonSeparator19);
            this.pnlZaposleni.Items.Add(this.rbtnArhivirani);
            this.pnlZaposleni.Name = "pnlZaposleni";
            this.pnlZaposleni.Text = "";
            // 
            // btnDodajNovog
            // 
            this.btnDodajNovog.Image = global::Prototip_5.Properties.Resources.icon1;
            this.btnDodajNovog.LargeImage = global::Prototip_5.Properties.Resources.icon1;
            this.btnDodajNovog.MinimumSize = new System.Drawing.Size(121, 0);
            this.btnDodajNovog.Name = "btnDodajNovog";
            this.btnDodajNovog.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnDodajNovog.SmallImage")));
            this.btnDodajNovog.Text = "Dodavanje novog zaposlenog";
            this.btnDodajNovog.Click += new System.EventHandler(this.rbtnDodajNovog_Click);
            // 
            // ribbonSeparator13
            // 
            this.ribbonSeparator13.Name = "ribbonSeparator13";
            // 
            // btnAzurirajPodatke
            // 
            this.btnAzurirajPodatke.Image = global::Prototip_5.Properties.Resources.icon2;
            this.btnAzurirajPodatke.LargeImage = global::Prototip_5.Properties.Resources.icon2;
            this.btnAzurirajPodatke.MinimumSize = new System.Drawing.Size(121, 0);
            this.btnAzurirajPodatke.Name = "btnAzurirajPodatke";
            this.btnAzurirajPodatke.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnAzurirajPodatke.SmallImage")));
            this.btnAzurirajPodatke.Text = "Ažuriranje podataka o zaposlenom";
            this.btnAzurirajPodatke.Click += new System.EventHandler(this.ribbonButton15_Click);
            // 
            // ribbonSeparator15
            // 
            this.ribbonSeparator15.Name = "ribbonSeparator15";
            // 
            // btnPrekiniOdnos
            // 
            this.btnPrekiniOdnos.Image = global::Prototip_5.Properties.Resources.removeUser;
            this.btnPrekiniOdnos.LargeImage = global::Prototip_5.Properties.Resources.removeUser;
            this.btnPrekiniOdnos.MinimumSize = new System.Drawing.Size(121, 0);
            this.btnPrekiniOdnos.Name = "btnPrekiniOdnos";
            this.btnPrekiniOdnos.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPrekiniOdnos.SmallImage")));
            this.btnPrekiniOdnos.Text = "Prekid radnog odnosa";
            this.btnPrekiniOdnos.Click += new System.EventHandler(this.btnOtpusti_Click);
            // 
            // ribbonSeparator19
            // 
            this.ribbonSeparator19.Name = "ribbonSeparator19";
            // 
            // rbtnArhivirani
            // 
            this.rbtnArhivirani.Image = global::Prototip_5.Properties.Resources.workersTeam;
            this.rbtnArhivirani.LargeImage = global::Prototip_5.Properties.Resources.workersTeam;
            this.rbtnArhivirani.MinimumSize = new System.Drawing.Size(121, 0);
            this.rbtnArhivirani.Name = "rbtnArhivirani";
            this.rbtnArhivirani.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbtnArhivirani.SmallImage")));
            this.rbtnArhivirani.Text = "Pregled lica koja nisu u radnom odnosu";
            this.rbtnArhivirani.Click += new System.EventHandler(this.rbtnArhivirani_Click);
            // 
            // tabOdsustva
            // 
            this.tabOdsustva.Name = "tabOdsustva";
            this.tabOdsustva.Panels.Add(this.pnlOdsustva);
            this.tabOdsustva.Text = "Odsustva";
            // 
            // pnlOdsustva
            // 
            this.pnlOdsustva.ButtonMoreEnabled = false;
            this.pnlOdsustva.ButtonMoreVisible = false;
            this.pnlOdsustva.Items.Add(this.rbtnDodajOdsustvo);
            this.pnlOdsustva.Items.Add(this.ribbonSeparator5);
            this.pnlOdsustva.Items.Add(this.rbtnPregledOdsustva);
            this.pnlOdsustva.Items.Add(this.ribbonSeparator16);
            this.pnlOdsustva.Items.Add(this.rbtnPromenaOdsustva);
            this.pnlOdsustva.Name = "pnlOdsustva";
            this.pnlOdsustva.Text = "";
            // 
            // rbtnDodajOdsustvo
            // 
            this.rbtnDodajOdsustvo.Image = global::Prototip_5.Properties.Resources.calendar3;
            this.rbtnDodajOdsustvo.LargeImage = global::Prototip_5.Properties.Resources.calendar3;
            this.rbtnDodajOdsustvo.MinimumSize = new System.Drawing.Size(121, 0);
            this.rbtnDodajOdsustvo.Name = "rbtnDodajOdsustvo";
            this.rbtnDodajOdsustvo.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbtnDodajOdsustvo.SmallImage")));
            this.rbtnDodajOdsustvo.Text = "Dodavanje odsustva";
            this.rbtnDodajOdsustvo.Click += new System.EventHandler(this.rbtnDodajOdsustvo_Click);
            // 
            // ribbonSeparator5
            // 
            this.ribbonSeparator5.Name = "ribbonSeparator5";
            // 
            // rbtnPregledOdsustva
            // 
            this.rbtnPregledOdsustva.Image = global::Prototip_5.Properties.Resources.calendar2;
            this.rbtnPregledOdsustva.LargeImage = global::Prototip_5.Properties.Resources.calendar2;
            this.rbtnPregledOdsustva.MinimumSize = new System.Drawing.Size(121, 0);
            this.rbtnPregledOdsustva.Name = "rbtnPregledOdsustva";
            this.rbtnPregledOdsustva.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbtnPregledOdsustva.SmallImage")));
            this.rbtnPregledOdsustva.Text = "Pregled odsustva";
            this.rbtnPregledOdsustva.Click += new System.EventHandler(this.rbtnPregledOdsustva_Click);
            // 
            // ribbonSeparator16
            // 
            this.ribbonSeparator16.Name = "ribbonSeparator16";
            // 
            // rbtnPromenaOdsustva
            // 
            this.rbtnPromenaOdsustva.Image = global::Prototip_5.Properties.Resources.calendar2;
            this.rbtnPromenaOdsustva.LargeImage = global::Prototip_5.Properties.Resources.calendar2;
            this.rbtnPromenaOdsustva.MinimumSize = new System.Drawing.Size(121, 0);
            this.rbtnPromenaOdsustva.Name = "rbtnPromenaOdsustva";
            this.rbtnPromenaOdsustva.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbtnPromenaOdsustva.SmallImage")));
            this.rbtnPromenaOdsustva.Text = "Promena odsustva";
            this.rbtnPromenaOdsustva.Click += new System.EventHandler(this.rbtnPromenaOdsustva_Click);
            // 
            // tabSmene
            // 
            this.tabSmene.Name = "tabSmene";
            this.tabSmene.Panels.Add(this.pnlSmene);
            this.tabSmene.Text = "Smene";
            // 
            // pnlSmene
            // 
            this.pnlSmene.ButtonMoreEnabled = false;
            this.pnlSmene.ButtonMoreVisible = false;
            this.pnlSmene.Items.Add(this.rbtnDodavanjeSmene);
            this.pnlSmene.Items.Add(this.ribbonSeparator14);
            this.pnlSmene.Items.Add(this.rbtnIzmenaSmene);
            this.pnlSmene.Items.Add(this.ribbonSeparator17);
            this.pnlSmene.Items.Add(this.rbtnRotacijaSmena);
            this.pnlSmene.Items.Add(this.ribbonSeparator18);
            this.pnlSmene.Items.Add(this.rbtnBrisanjeSmene);
            this.pnlSmene.Name = "pnlSmene";
            this.pnlSmene.Text = "";
            // 
            // rbtnDodavanjeSmene
            // 
            this.rbtnDodavanjeSmene.Image = global::Prototip_5.Properties.Resources.alarm;
            this.rbtnDodavanjeSmene.LargeImage = global::Prototip_5.Properties.Resources.alarm;
            this.rbtnDodavanjeSmene.MinimumSize = new System.Drawing.Size(121, 0);
            this.rbtnDodavanjeSmene.Name = "rbtnDodavanjeSmene";
            this.rbtnDodavanjeSmene.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbtnDodavanjeSmene.SmallImage")));
            this.rbtnDodavanjeSmene.Text = "Dodavanje smene";
            this.rbtnDodavanjeSmene.Click += new System.EventHandler(this.rbtnDodavanjeSmene_Click);
            // 
            // ribbonSeparator14
            // 
            this.ribbonSeparator14.Name = "ribbonSeparator14";
            // 
            // rbtnIzmenaSmene
            // 
            this.rbtnIzmenaSmene.Image = global::Prototip_5.Properties.Resources.file;
            this.rbtnIzmenaSmene.LargeImage = global::Prototip_5.Properties.Resources.file;
            this.rbtnIzmenaSmene.MinimumSize = new System.Drawing.Size(121, 0);
            this.rbtnIzmenaSmene.Name = "rbtnIzmenaSmene";
            this.rbtnIzmenaSmene.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbtnIzmenaSmene.SmallImage")));
            this.rbtnIzmenaSmene.Text = "Izmena smene";
            this.rbtnIzmenaSmene.Click += new System.EventHandler(this.rbtnIzmenaSmene_Click);
            // 
            // ribbonSeparator17
            // 
            this.ribbonSeparator17.Name = "ribbonSeparator17";
            // 
            // rbtnRotacijaSmena
            // 
            this.rbtnRotacijaSmena.Image = global::Prototip_5.Properties.Resources.change;
            this.rbtnRotacijaSmena.LargeImage = global::Prototip_5.Properties.Resources.change;
            this.rbtnRotacijaSmena.MinimumSize = new System.Drawing.Size(121, 0);
            this.rbtnRotacijaSmena.Name = "rbtnRotacijaSmena";
            this.rbtnRotacijaSmena.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbtnRotacijaSmena.SmallImage")));
            this.rbtnRotacijaSmena.Text = "Rotacija smena";
            this.rbtnRotacijaSmena.Click += new System.EventHandler(this.rbtnRotacijaSmena_Click);
            // 
            // ribbonSeparator18
            // 
            this.ribbonSeparator18.Name = "ribbonSeparator18";
            // 
            // rbtnBrisanjeSmene
            // 
            this.rbtnBrisanjeSmene.Image = global::Prototip_5.Properties.Resources.removeAlarm;
            this.rbtnBrisanjeSmene.LargeImage = global::Prototip_5.Properties.Resources.removeAlarm;
            this.rbtnBrisanjeSmene.MinimumSize = new System.Drawing.Size(121, 0);
            this.rbtnBrisanjeSmene.Name = "rbtnBrisanjeSmene";
            this.rbtnBrisanjeSmene.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbtnBrisanjeSmene.SmallImage")));
            this.rbtnBrisanjeSmene.Text = "Brisanje smene";
            this.rbtnBrisanjeSmene.Click += new System.EventHandler(this.rbtnBrisanjeSmene_Click);
            // 
            // tabNalog
            // 
            this.tabNalog.Name = "tabNalog";
            this.tabNalog.Panels.Add(this.pnlNalog);
            this.tabNalog.Text = "Nalog";
            // 
            // pnlNalog
            // 
            this.pnlNalog.ButtonMoreEnabled = false;
            this.pnlNalog.ButtonMoreVisible = false;
            this.pnlNalog.Items.Add(this.rbtnOdjava);
            this.pnlNalog.Items.Add(this.ribbonSeparator4);
            this.pnlNalog.Items.Add(this.rbtnPromenaLozinke);
            this.pnlNalog.Name = "pnlNalog";
            this.pnlNalog.Text = "";
            // 
            // rbtnOdjava
            // 
            this.rbtnOdjava.Image = global::Prototip_5.Properties.Resources.signOut;
            this.rbtnOdjava.LargeImage = global::Prototip_5.Properties.Resources.signOut;
            this.rbtnOdjava.MinimumSize = new System.Drawing.Size(121, 0);
            this.rbtnOdjava.Name = "rbtnOdjava";
            this.rbtnOdjava.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbtnOdjava.SmallImage")));
            this.rbtnOdjava.Text = "Odjava";
            this.rbtnOdjava.Click += new System.EventHandler(this.rbtnOdjava_Click);
            // 
            // ribbonSeparator4
            // 
            this.ribbonSeparator4.Name = "ribbonSeparator4";
            // 
            // rbtnPromenaLozinke
            // 
            this.rbtnPromenaLozinke.Image = global::Prototip_5.Properties.Resources.padlock;
            this.rbtnPromenaLozinke.LargeImage = global::Prototip_5.Properties.Resources.padlock;
            this.rbtnPromenaLozinke.MinimumSize = new System.Drawing.Size(121, 0);
            this.rbtnPromenaLozinke.Name = "rbtnPromenaLozinke";
            this.rbtnPromenaLozinke.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbtnPromenaLozinke.SmallImage")));
            this.rbtnPromenaLozinke.Text = "Promena lozinke";
            this.rbtnPromenaLozinke.Click += new System.EventHandler(this.rbtnPromenaLozinke_Click);
            // 
            // ribbonSeparator11
            // 
            this.ribbonSeparator11.Name = "ribbonSeparator11";
            // 
            // ribbonSeparator12
            // 
            this.ribbonSeparator12.Name = "ribbonSeparator12";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // metroGrid1
            // 
            this.metroGrid1.AllowUserToDeleteRows = false;
            this.metroGrid1.AllowUserToOrderColumns = true;
            this.metroGrid1.AllowUserToResizeRows = false;
            this.metroGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.metroGrid1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.metroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(65)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(73)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(73)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroGrid1.EnableHeadersVisualStyles = false;
            this.metroGrid1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.Location = new System.Drawing.Point(3, 15);
            this.metroGrid1.Margin = new System.Windows.Forms.Padding(3, 15, 3, 3);
            this.metroGrid1.Name = "metroGrid1";
            this.metroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(65)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(73)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1.Size = new System.Drawing.Size(954, 461);
            this.metroGrid1.Style = MetroFramework.MetroColorStyle.Purple;
            this.metroGrid1.TabIndex = 4;
            this.metroGrid1.UseStyleColors = true;
            this.metroGrid1.SelectionChanged += new System.EventHandler(this.metroGrid1_SelectionChanged);
            this.metroGrid1.Click += new System.EventHandler(this.metroGrid1_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.tableLayoutPanel2.Controls.Add(this.btnPretrazi, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tbPretrazi, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.metroComboBox1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.metroComboBox2, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.metroPanel1, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(963, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(294, 473);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // btnPretrazi
            // 
            this.btnPretrazi.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.SetColumnSpan(this.btnPretrazi, 2);
            this.btnPretrazi.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnPretrazi.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnPretrazi.Location = new System.Drawing.Point(3, 442);
            this.btnPretrazi.Name = "btnPretrazi";
            this.btnPretrazi.Size = new System.Drawing.Size(288, 28);
            this.btnPretrazi.TabIndex = 0;
            this.btnPretrazi.Text = "Pretraži zaposlene";
            this.btnPretrazi.Theme = MetroFramework.MetroThemeStyle.Light;
            this.btnPretrazi.UseSelectable = true;
            this.btnPretrazi.Click += new System.EventHandler(this.BtnPretraziClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel2.SetColumnSpan(this.pictureBox1, 2);
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.ErrorImage = global::Prototip_5.Properties.Resources.default_person;
            this.pictureBox1.Image = global::Prototip_5.Properties.Resources.default_person;
            this.pictureBox1.InitialImage = global::Prototip_5.Properties.Resources.default_person;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(288, 294);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // tbPretrazi
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.tbPretrazi, 2);
            // 
            // 
            // 
            this.tbPretrazi.CustomButton.Image = null;
            this.tbPretrazi.CustomButton.Location = new System.Drawing.Point(262, 2);
            this.tbPretrazi.CustomButton.Name = "";
            this.tbPretrazi.CustomButton.Size = new System.Drawing.Size(23, 23);
            this.tbPretrazi.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbPretrazi.CustomButton.TabIndex = 1;
            this.tbPretrazi.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbPretrazi.CustomButton.UseSelectable = true;
            this.tbPretrazi.CustomButton.Visible = false;
            this.tbPretrazi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPretrazi.Lines = new string[0];
            this.tbPretrazi.Location = new System.Drawing.Point(3, 408);
            this.tbPretrazi.MaxLength = 32767;
            this.tbPretrazi.Name = "tbPretrazi";
            this.tbPretrazi.PasswordChar = '\0';
            this.tbPretrazi.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbPretrazi.SelectedText = "";
            this.tbPretrazi.SelectionLength = 0;
            this.tbPretrazi.SelectionStart = 0;
            this.tbPretrazi.ShortcutsEnabled = true;
            this.tbPretrazi.Size = new System.Drawing.Size(288, 28);
            this.tbPretrazi.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbPretrazi.TabIndex = 1;
            this.tbPretrazi.UseSelectable = true;
            this.tbPretrazi.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbPretrazi.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroComboBox1
            // 
            this.metroComboBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroComboBox1.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.metroComboBox1.FontWeight = MetroFramework.MetroComboBoxWeight.Light;
            this.metroComboBox1.FormattingEnabled = true;
            this.metroComboBox1.ItemHeight = 19;
            this.metroComboBox1.Items.AddRange(new object[] {
            "Broj zaposlenog",
            "Ime",
            "Srednje slovo",
            "Prezime",
            "Radno mesto",
            "Datum rođenja",
            "Datum početka rada"});
            this.metroComboBox1.Location = new System.Drawing.Point(3, 303);
            this.metroComboBox1.Name = "metroComboBox1";
            this.metroComboBox1.Size = new System.Drawing.Size(182, 25);
            this.metroComboBox1.Style = MetroFramework.MetroColorStyle.Purple;
            this.metroComboBox1.TabIndex = 7;
            this.metroComboBox1.UseSelectable = true;
            // 
            // metroComboBox2
            // 
            this.metroComboBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroComboBox2.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.metroComboBox2.FontWeight = MetroFramework.MetroComboBoxWeight.Light;
            this.metroComboBox2.FormattingEnabled = true;
            this.metroComboBox2.ItemHeight = 19;
            this.metroComboBox2.Items.AddRange(new object[] {
            "Rastuće",
            "Opadajuće"});
            this.metroComboBox2.Location = new System.Drawing.Point(191, 303);
            this.metroComboBox2.Name = "metroComboBox2";
            this.metroComboBox2.Size = new System.Drawing.Size(100, 25);
            this.metroComboBox2.Style = MetroFramework.MetroColorStyle.Purple;
            this.metroComboBox2.TabIndex = 9;
            this.metroComboBox2.UseSelectable = true;
            // 
            // metroPanel1
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.metroPanel1, 2);
            this.metroPanel1.Controls.Add(this.lblSpinner);
            this.metroPanel1.Controls.Add(this.circularProgressBar1);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 355);
            this.metroPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(294, 50);
            this.metroPanel1.TabIndex = 10;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // lblSpinner
            // 
            this.lblSpinner.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSpinner.AutoSize = true;
            this.lblSpinner.Location = new System.Drawing.Point(136, 28);
            this.lblSpinner.Name = "lblSpinner";
            this.lblSpinner.Size = new System.Drawing.Size(107, 19);
            this.lblSpinner.TabIndex = 8;
            this.lblSpinner.Text = "Učitavanje traje...";
            this.lblSpinner.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // circularProgressBar1
            // 
            this.circularProgressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.circularProgressBar1.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.circularProgressBar1.AnimationSpeed = 5000;
            this.circularProgressBar1.BackColor = System.Drawing.Color.Transparent;
            this.circularProgressBar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold);
            this.circularProgressBar1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.circularProgressBar1.InnerColor = System.Drawing.SystemColors.ControlLightLight;
            this.circularProgressBar1.InnerMargin = 2;
            this.circularProgressBar1.InnerWidth = -1;
            this.circularProgressBar1.Location = new System.Drawing.Point(249, 10);
            this.circularProgressBar1.MarqueeAnimationSpeed = 500;
            this.circularProgressBar1.Name = "circularProgressBar1";
            this.circularProgressBar1.OuterColor = System.Drawing.SystemColors.ControlLightLight;
            this.circularProgressBar1.OuterMargin = -25;
            this.circularProgressBar1.OuterWidth = 26;
            this.circularProgressBar1.ProgressColor = System.Drawing.Color.Purple;
            this.circularProgressBar1.ProgressWidth = 3;
            this.circularProgressBar1.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.circularProgressBar1.Size = new System.Drawing.Size(42, 35);
            this.circularProgressBar1.StartAngle = 90;
            this.circularProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.circularProgressBar1.SubscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circularProgressBar1.SubscriptMargin = new System.Windows.Forms.Padding(10, -35, 0, 0);
            this.circularProgressBar1.SubscriptText = ".23";
            this.circularProgressBar1.SuperscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circularProgressBar1.SuperscriptMargin = new System.Windows.Forms.Padding(10, 35, 0, 0);
            this.circularProgressBar1.SuperscriptText = "";
            this.circularProgressBar1.TabIndex = 0;
            this.circularProgressBar1.TextMargin = new System.Windows.Forms.Padding(8, 8, 0, 0);
            this.circularProgressBar1.Value = 25;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroGrid1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(20, 183);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1260, 479);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // btnDodajOdsustvo
            // 
            this.btnDodajOdsustvo.Image = global::Prototip_5.Properties.Resources.html;
            this.btnDodajOdsustvo.LargeImage = global::Prototip_5.Properties.Resources.html;
            this.btnDodajOdsustvo.Name = "btnDodajOdsustvo";
            this.btnDodajOdsustvo.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnDodajOdsustvo.SmallImage")));
            this.btnDodajOdsustvo.Text = "Dodaj odsustvo";
            this.btnDodajOdsustvo.TextAlignment = System.Windows.Forms.RibbonItem.RibbonItemTextAlignment.Center;
            this.btnDodajOdsustvo.Click += new System.EventHandler(this.btnDodajOdsustvo_DoubleClick);
            // 
            // AdminMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1300, 682);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.ribbon1);
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(1300, 682);
            this.Name = "AdminMainForm";
            this.Style = MetroFramework.MetroColorStyle.Purple;
            this.Text = "OPZ–2018: Opšta služba";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminMainForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Ribbon ribbon1;
        private System.Windows.Forms.RibbonTab tabSmene;
        private System.Windows.Forms.RibbonTab tabZaposleni;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private MetroFramework.Controls.MetroProgressSpinner metroProgressSpinner1;
        private System.Windows.Forms.RibbonPanel ribbonPanel6;
        private System.Windows.Forms.RibbonPanel ribbonPanel7;
        private System.Windows.Forms.RibbonButton btnDodajOdsustvo;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.RibbonPanel pnlZaposleni;
        private System.Windows.Forms.RibbonButton btnDodajNovog;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator11;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator12;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator13;
        private System.Windows.Forms.RibbonButton btnAzurirajPodatke;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator15;
        private System.Windows.Forms.RibbonButton btnPrekiniOdnos;
        private MetroFramework.Controls.MetroGrid metroGrid1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroFramework.Controls.MetroButton btnPretrazi;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroTextBox tbPretrazi;
        private MetroFramework.Controls.MetroComboBox metroComboBox1;
        private MetroFramework.Controls.MetroLabel lblSpinner;
        private MetroFramework.Controls.MetroComboBox metroComboBox2;
        private CircularProgressBar.CircularProgressBar circularProgressBar1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.RibbonTab tabNalog;
        private System.Windows.Forms.RibbonPanel pnlNalog;
        private System.Windows.Forms.RibbonButton rbtnOdjava;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator4;
        private System.Windows.Forms.RibbonButton rbtnPromenaLozinke;
        private System.Windows.Forms.RibbonTab tabOdsustva;
        private System.Windows.Forms.RibbonPanel pnlOdsustva;
        private System.Windows.Forms.RibbonButton rbtnDodajOdsustvo;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator5;
        private System.Windows.Forms.RibbonButton rbtnPregledOdsustva;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator16;
        private System.Windows.Forms.RibbonButton rbtnPromenaOdsustva;
        private System.Windows.Forms.RibbonPanel pnlSmene;
        private System.Windows.Forms.RibbonButton rbtnDodavanjeSmene;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator14;
        private System.Windows.Forms.RibbonButton rbtnIzmenaSmene;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator17;
        private System.Windows.Forms.RibbonButton rbtnRotacijaSmena;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator18;
        private System.Windows.Forms.RibbonButton rbtnBrisanjeSmene;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator19;
        private System.Windows.Forms.RibbonButton rbtnArhivirani;
        private MetroFramework.Controls.MetroPanel metroPanel1;
    }
}

