﻿namespace Prototip_5
{
    partial class OdsutniZaposleniChartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnPrikaziGrafikon = new MetroFramework.Controls.MetroButton();
            this.lblMesec = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.chart = new LiveCharts.WinForms.CartesianChart();
            this.datumOd = new MetroFramework.Controls.MetroDateTime();
            this.datumDo = new MetroFramework.Controls.MetroDateTime();
            this.circularProgressBar1 = new CircularProgressBar.CircularProgressBar();
            this.lblSpinner = new MetroFramework.Controls.MetroLabel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 192F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 196F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 138F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 151F));
            this.tableLayoutPanel1.Controls.Add(this.btnPrikaziGrafikon, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblMesec, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel1, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.chart, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.datumOd, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.datumDo, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.circularProgressBar1, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblSpinner, 5, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(20, 60);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(899, 574);
            this.tableLayoutPanel1.TabIndex = 12;
            // 
            // btnPrikaziGrafikon
            // 
            this.btnPrikaziGrafikon.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPrikaziGrafikon.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnPrikaziGrafikon.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnPrikaziGrafikon.Location = new System.Drawing.Point(751, 3);
            this.btnPrikaziGrafikon.Name = "btnPrikaziGrafikon";
            this.btnPrikaziGrafikon.Size = new System.Drawing.Size(145, 32);
            this.btnPrikaziGrafikon.TabIndex = 1;
            this.btnPrikaziGrafikon.Text = "Prikaži grafikon";
            this.btnPrikaziGrafikon.UseSelectable = true;
            this.btnPrikaziGrafikon.Click += new System.EventHandler(this.btnPrikaziGrafikon_Click);
            // 
            // lblMesec
            // 
            this.lblMesec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMesec.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblMesec.Location = new System.Drawing.Point(3, 0);
            this.lblMesec.Name = "lblMesec";
            this.lblMesec.Size = new System.Drawing.Size(65, 38);
            this.lblMesec.TabIndex = 0;
            this.lblMesec.Text = "Od";
            this.lblMesec.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroLabel1
            // 
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.Location = new System.Drawing.Point(294, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(63, 36);
            this.metroLabel1.TabIndex = 14;
            this.metroLabel1.Text = "Do";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chart
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.chart, 8);
            this.chart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart.Location = new System.Drawing.Point(3, 41);
            this.chart.Name = "chart";
            this.chart.Size = new System.Drawing.Size(893, 530);
            this.chart.TabIndex = 17;
            this.chart.Visible = false;
            // 
            // datumOd
            // 
            this.datumOd.CustomFormat = "dd.MM.yyyy.";
            this.datumOd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datumOd.Location = new System.Drawing.Point(74, 3);
            this.datumOd.MinimumSize = new System.Drawing.Size(0, 29);
            this.datumOd.Name = "datumOd";
            this.datumOd.Size = new System.Drawing.Size(186, 29);
            this.datumOd.Style = MetroFramework.MetroColorStyle.Pink;
            this.datumOd.TabIndex = 18;
            // 
            // datumDo
            // 
            this.datumDo.CustomFormat = "dd.MM.yyyy.";
            this.datumDo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datumDo.Location = new System.Drawing.Point(363, 3);
            this.datumDo.MinimumSize = new System.Drawing.Size(0, 29);
            this.datumDo.Name = "datumDo";
            this.datumDo.Size = new System.Drawing.Size(190, 29);
            this.datumDo.Style = MetroFramework.MetroColorStyle.Pink;
            this.datumDo.TabIndex = 19;
            // 
            // circularProgressBar1
            // 
            this.circularProgressBar1.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.circularProgressBar1.AnimationSpeed = 5000;
            this.circularProgressBar1.BackColor = System.Drawing.Color.Transparent;
            this.circularProgressBar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold);
            this.circularProgressBar1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.circularProgressBar1.InnerColor = System.Drawing.SystemColors.ControlLightLight;
            this.circularProgressBar1.InnerMargin = 2;
            this.circularProgressBar1.InnerWidth = -1;
            this.circularProgressBar1.Location = new System.Drawing.Point(697, 3);
            this.circularProgressBar1.MarqueeAnimationSpeed = 500;
            this.circularProgressBar1.Name = "circularProgressBar1";
            this.circularProgressBar1.OuterColor = System.Drawing.SystemColors.ControlLightLight;
            this.circularProgressBar1.OuterMargin = -25;
            this.circularProgressBar1.OuterWidth = 26;
            this.circularProgressBar1.ProgressColor = System.Drawing.Color.PaleVioletRed;
            this.circularProgressBar1.ProgressWidth = 4;
            this.circularProgressBar1.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.circularProgressBar1.Size = new System.Drawing.Size(40, 31);
            this.circularProgressBar1.StartAngle = 90;
            this.circularProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.circularProgressBar1.SubscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circularProgressBar1.SubscriptMargin = new System.Windows.Forms.Padding(10, -35, 0, 0);
            this.circularProgressBar1.SubscriptText = ".23";
            this.circularProgressBar1.SuperscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circularProgressBar1.SuperscriptMargin = new System.Windows.Forms.Padding(10, 35, 0, 0);
            this.circularProgressBar1.SuperscriptText = "";
            this.circularProgressBar1.TabIndex = 20;
            this.circularProgressBar1.TextMargin = new System.Windows.Forms.Padding(8, 8, 0, 0);
            this.circularProgressBar1.Value = 25;
            this.circularProgressBar1.Visible = false;
            // 
            // lblSpinner
            // 
            this.lblSpinner.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSpinner.AutoSize = true;
            this.lblSpinner.Location = new System.Drawing.Point(586, 19);
            this.lblSpinner.Name = "lblSpinner";
            this.lblSpinner.Size = new System.Drawing.Size(105, 19);
            this.lblSpinner.TabIndex = 21;
            this.lblSpinner.Text = "Operacija traje...";
            this.lblSpinner.Visible = false;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // OdsutniZaposleniChartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 654);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(939, 654);
            this.Name = "OdsutniZaposleniChartForm";
            this.Style = MetroFramework.MetroColorStyle.Pink;
            this.Text = "Prikaz broja odsutnih zaposlenih";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OdsutniZaposleniChartForm_FormClosing);
            this.Load += new System.EventHandler(this.ProsekPlateChartForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroButton btnPrikaziGrafikon;
        private MetroFramework.Controls.MetroLabel lblMesec;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private LiveCharts.WinForms.CartesianChart chart;
        private MetroFramework.Controls.MetroDateTime datumOd;
        private MetroFramework.Controls.MetroDateTime datumDo;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private CircularProgressBar.CircularProgressBar circularProgressBar1;
        private MetroFramework.Controls.MetroLabel lblSpinner;
    }
}