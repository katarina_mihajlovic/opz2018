﻿using MetroFramework.Forms;
using static SharedComponents.Forms.Login;

namespace Prototip_5
{
    class AdminFormFactory : FormFactory
    {
        public override MetroForm MakeForm(string username)
        {
            return new AdminMainForm(username);
        }
    }
}
