﻿using MetroFramework.Forms;
using System;
using SharedComponents.Views;
using SharedComponents.Presenters;
using System.Windows.Forms;
using MetroFramework;

namespace FinansijskaApp
{
    public partial class NoviKoeficijentiForm : MetroForm, INoviKoeficijentiView
    {
        public NoviKoeficijentiForm()
        {
            InitializeComponent();
        }

        public double CenaRada
        {
            get { return txtCenaRada.Text != String.Empty ? double.Parse(txtCenaRada.Text) : 0; }
            set { txtCenaRada.Text = value.ToString(); }
        }
        public double ProcenatZaExtra
        {
            get { return txtCenaRada.Text != String.Empty ? double.Parse(txtPrekovremeni.Text) : 0; }
            set { txtPrekovremeni.Text = value.ToString(); }
        }
        public double ProcenatZaMinuli
        {
            get { return txtCenaRada.Text != String.Empty ? double.Parse(txtMinuli.Text) : 0; }
            set { txtMinuli.Text = value.ToString(); }
        }
        public double NeoporezivaOsnovica
        {
            get { return txtCenaRada.Text != String.Empty ? double.Parse(txtNeoporeziva.Text) : 0; }
            set { txtNeoporeziva.Text = value.ToString(); }
        }
        public double ProcenatZaPorez
        {
            get { return txtCenaRada.Text != String.Empty ? double.Parse(txtPorez.Text) : 0; }
            set { txtPorez.Text = value.ToString(); }
        }
        public double DopZaposlenogPIO
        {
            get { return txtCenaRada.Text != String.Empty ? double.Parse(txtZapPIO.Text) : 0; }
            set { txtZapPIO.Text = value.ToString(); }
        }
        public double DopZaposlenogZdravstvo
        {
            get { return txtCenaRada.Text != String.Empty ? double.Parse(txtZapZdravstvo.Text) : 0; }
            set { txtZapZdravstvo.Text = value.ToString(); }
        }
        public double DopZaposlenogNezaposlenost
        {
            get { return txtCenaRada.Text != String.Empty ? double.Parse(txtZapNezap.Text) : 0; }
            set { txtZapNezap.Text = value.ToString(); }
        }
        public double DopPoslodavcaPIO
        {
            get { return txtCenaRada.Text != String.Empty ? double.Parse(txtPosPIO.Text) : 0; }
            set { txtPosPIO.Text = value.ToString(); }
        }
        public double DopPoslodavcaZdravstvo
        {
            get { return txtCenaRada.Text != String.Empty ? double.Parse(txtPosZdravstvo.Text) : 0; }
            set { txtPosZdravstvo.Text = value.ToString(); }
        }
        public double DopPoslodavcaNezaposlenost
        {
            get { return txtCenaRada.Text != String.Empty ? double.Parse(txtPosNezap.Text) : 0; }
            set { txtPosNezap.Text = value.ToString(); }
        }
        public NoviKoeficijentiPresenter Presenter
        {
            get; set;
        }

        private void btnIzadji_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSacuvaj_Click(object sender, EventArgs e)
        {
            try
            {
                double.Parse(txtCenaRada.Text);
                double.Parse(txtPrekovremeni.Text);
                double.Parse(txtMinuli.Text);
                double.Parse(txtNeoporeziva.Text);
                double.Parse(txtPorez.Text);
                double.Parse(txtZapPIO.Text);
                double.Parse(txtZapZdravstvo.Text);
                double.Parse(txtZapNezap.Text);
                double.Parse(txtPosPIO.Text);
                double.Parse(txtPosZdravstvo.Text);
                double.Parse(txtPosNezap.Text);

            }
            catch (Exception)
            {
                MetroMessageBox.Show(this, "Neka od vrednosti je unešena u pogrešnom formatu.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
                return;
            }
            Presenter.AzurirajKoeficijente();
            Close();
        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != ',')
                e.Handled = true;
        }
    }
}
