﻿using MetroFramework.Forms;
using static SharedComponents.Forms.Login;

namespace FinansijskaApp
{
    class FinansijskaFormFactory : FormFactory
    {
        public override MetroForm MakeForm(string username)
        {
            return new FinansijskaMainForm(username);
        }
    }
}
