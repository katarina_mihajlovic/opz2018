﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using SharedComponents;
using Persistence.Entities;
using MetroFramework;
using MetroFramework.Controls;

namespace FinansijskaApp
{
    public partial class IzvestajBanka : MetroForm
    {
        private DateTime mesecGodina;

        public IzvestajBanka()
        {
            InitializeComponent();

            DateTime trenutak = DateTime.Today;
            int godina = trenutak.Year;
            int mesec = trenutak.Month;
            if (--mesec == 0)
            {
                mesec = 12;
                godina--;
            }
            cbMesec.SelectedIndex = mesec - 1;
            txtGodina.Text = godina.ToString();
        }

        private void btnExportPDF_Click(object sender, EventArgs e)
        {
            if (dgvObracun.Rows.Count == 0)
            {
                MetroMessageBox.Show(this, "Tabela je prazna i ne može se izvršiti eksport.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                return;
            }

            saveFileDialog1.Filter = "Portable Document Format (*.pdf)|*.pdf";
            saveFileDialog1.DefaultExt = "";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                MetroGrid temp = new MetroGrid();
                temp.Columns.Add("RedniBroj", "Redni broj");
                temp.Columns.Add("Ime", "Ime");
                temp.Columns.Add("BrojTekucegRacuna", "Broj tekućeg računa");
                temp.Columns.Add("Neto", "Neto");
                foreach (DataGridViewRow red in dgvObracun.Rows)
                {
                    DataGridViewRow novi = (DataGridViewRow)red.Clone();
                    int indeks = 0;
                    foreach (DataGridViewCell cell in red.Cells)
                    {
                        novi.Cells[indeks].Value = cell.Value;
                        indeks++;
                    }
                    temp.Rows.Add(novi);
                }

                if (!backgroundWorker1.IsBusy && !backgroundWorker2.IsBusy)
                {
                    backgroundWorker2.RunWorkerAsync(new Tuple<DataGridView,string>(temp, saveFileDialog1.FileName));
                }
                
            }
            saveFileDialog1.FileName = String.Empty;
        }

        private void btnPrikazi_Click(object sender, EventArgs e)
        {
            int mesec = cbMesec.SelectedIndex + 1;
            int godina = int.Parse(txtGodina.Text);
            DateTime startOfMonth = new DateTime(godina, mesec, 1);

            if (startOfMonth.AddMonths(1) >= DateTime.Now)
            {
                MetroMessageBox.Show(this, "Izabrani mesec još uvek nije prošao.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                return;
            }

            mesecGodina = startOfMonth;

            if (!backgroundWorker1.IsBusy && !backgroundWorker2.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync(startOfMonth);
            }
        }

        private void txtGodina_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        #region Spinner
        private void StartSpinning()
        {
            lblSpinner.Visible = true;
            circularProgressBar1.Visible = true;

            btnExportPDF.Enabled = false;
            btnPrikazi.Enabled = false;
            cbMesec.Enabled = false;
            txtGodina.Enabled = false;
        }

        private void StopSpinning()
        {
            lblSpinner.Visible = false;
            circularProgressBar1.Visible = false;

            btnExportPDF.Enabled = true;
            btnPrikazi.Enabled = true;
            cbMesec.Enabled = true;
            txtGodina.Enabled = true;
        }
        #endregion

        private delegate void RefreshDlg(object l);
        private void RefreshDgv(object prikaz)
        {
            dgvObracun.DataSource = prikaz;
            dgvObracun.Columns["RedniBroj"].HeaderText = "Redni broj";
            dgvObracun.Columns["BrojTekucegRacuna"].HeaderText = "Broj tekućeg računa";
            //dgvObracun.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
           // dgvObracun.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            //dgvObracun.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            foreach(DataGridViewColumn dgvcol in dgvObracun.Columns)
            {
                if (dgvcol.Index == 0) continue;
                dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Invoke(spnstart);

            DateTime startOfMonth = (e.Argument as DateTime?).Value;
            object result;
            using (OpzContext kontekst = new OpzContext())
            {
                var obracuni = (from x in kontekst.ListaObracuna
                                where x.MesecGodina == startOfMonth
                                orderby x.Zaposleni.Id
                                select new
                                {
                                    Ime = x.Zaposleni.Ime,
                                    SSlovo = x.Zaposleni.SSlovo,
                                    Prezime = x.Zaposleni.Prezime,
                                    TekuciRacun = x.Zaposleni.TekuciRacun,
                                    Neto = x.Neto
                                }).ToList();
                int rbr = 0;
                var obracuniIndeks = obracuni.Select(x => new
                {
                    RedniBroj = (++rbr).ToString(),
                    Ime = x.Ime + " " + x.SSlovo + ". " + x.Prezime,
                    BrojTekucegRacuna = x.TekuciRacun,
                    Neto = x.Neto
                }).ToList();
                obracuniIndeks.Add(new
                {
                    RedniBroj = "",
                    Ime = "UKUPNO",
                    BrojTekucegRacuna = "",
                    Neto = obracuni.Sum(x => x.Neto)
                });
                result = obracuniIndeks;
            }
            RefreshDlg d1 = RefreshDgv;
            Invoke(d1, new object[] { result });
        }

        private delegate void Del2();
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Del2 spnstop = StopSpinning;
            //zaustavljanje spinnera
            Invoke(spnstop);
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Invoke(spnstart);

            Tuple<DataGridView, string> arg = e.Argument as Tuple<DataGridView, string>;

            DocumentExport.DatagridviewToPdf(arg.Item1, arg.Item2, "Izveštaj za banku za mesec " + mesecGodina.ToString("MM.yyyy."));
        }

        private void backgroundWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Del2 spnstop = StopSpinning;
            //zaustavljanje spinnera
            Invoke(spnstop);
        }

        private void ObracunZaradaPoMesecu_Load(object sender, EventArgs e)
        {
            DateTime start = new DateTime(2005, 1, 1);
            DateTime end = new DateTime(DateTime.Now.Year, 1, 1);           
            List<string> cb = new List<string>();

            for(DateTime it = start;it<=end;it = it.AddYears(1))
            {
                cb.Add(it.ToString("yyyy"));
            }

            txtGodina.DataSource = cb;
            txtGodina.SelectedIndex = cb.Count - 1;
        }

        private void IzvestajBanka_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorker1.IsBusy || backgroundWorker2.IsBusy)
            {
                e.Cancel = true;
            }
        }
    }
}
