﻿namespace FinansijskaApp
{
    partial class FinansijskaMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.btnAzuzirajKoef = new MetroFramework.Controls.MetroButton();
            this.metroButton4 = new MetroFramework.Controls.MetroButton();
            this.btnPoMesecu = new MetroFramework.Controls.MetroButton();
            this.btnPoZaposlenom = new MetroFramework.Controls.MetroButton();
            this.dgvZaposleni = new MetroFramework.Controls.MetroGrid();
            this.btnTesting = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgvZaposleni)).BeginInit();
            this.SuspendLayout();
            // 
            // metroButton1
            // 
            this.metroButton1.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton1.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.metroButton1.Location = new System.Drawing.Point(30, 72);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(80, 23);
            this.metroButton1.TabIndex = 1;
            this.metroButton1.Text = "Odjava";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // btnAzuzirajKoef
            // 
            this.btnAzuzirajKoef.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnAzuzirajKoef.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnAzuzirajKoef.Location = new System.Drawing.Point(242, 72);
            this.btnAzuzirajKoef.Name = "btnAzuzirajKoef";
            this.btnAzuzirajKoef.Size = new System.Drawing.Size(148, 23);
            this.btnAzuzirajKoef.TabIndex = 7;
            this.btnAzuzirajKoef.Text = "Ažuriraj koeficijente";
            this.btnAzuzirajKoef.UseSelectable = true;
            this.btnAzuzirajKoef.Click += new System.EventHandler(this.btnAzuzirajKoef_Click);
            // 
            // metroButton4
            // 
            this.metroButton4.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton4.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.metroButton4.Location = new System.Drawing.Point(116, 72);
            this.metroButton4.Name = "metroButton4";
            this.metroButton4.Size = new System.Drawing.Size(120, 23);
            this.metroButton4.TabIndex = 8;
            this.metroButton4.Text = "Promeni lozinku";
            this.metroButton4.UseSelectable = true;
            this.metroButton4.Click += new System.EventHandler(this.metroButton4_Click);
            // 
            // btnPoMesecu
            // 
            this.btnPoMesecu.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnPoMesecu.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnPoMesecu.Location = new System.Drawing.Point(396, 72);
            this.btnPoMesecu.Name = "btnPoMesecu";
            this.btnPoMesecu.Size = new System.Drawing.Size(219, 23);
            this.btnPoMesecu.TabIndex = 9;
            this.btnPoMesecu.Text = "Pregled izveštaja po mesecu";
            this.btnPoMesecu.UseSelectable = true;
            this.btnPoMesecu.Click += new System.EventHandler(this.btnPoMesecu_Click);
            // 
            // btnPoZaposlenom
            // 
            this.btnPoZaposlenom.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnPoZaposlenom.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnPoZaposlenom.Location = new System.Drawing.Point(622, 72);
            this.btnPoZaposlenom.Name = "btnPoZaposlenom";
            this.btnPoZaposlenom.Size = new System.Drawing.Size(233, 23);
            this.btnPoZaposlenom.TabIndex = 10;
            this.btnPoZaposlenom.Text = "Pregled izveštaja o zaposlenom";
            this.btnPoZaposlenom.UseSelectable = true;
            this.btnPoZaposlenom.Click += new System.EventHandler(this.btnPoZaposlenom_Click);
            // 
            // dgvZaposleni
            // 
            this.dgvZaposleni.AllowUserToResizeRows = false;
            this.dgvZaposleni.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvZaposleni.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgvZaposleni.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvZaposleni.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvZaposleni.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvZaposleni.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvZaposleni.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvZaposleni.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvZaposleni.EnableHeadersVisualStyles = false;
            this.dgvZaposleni.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgvZaposleni.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgvZaposleni.Location = new System.Drawing.Point(30, 119);
            this.dgvZaposleni.Name = "dgvZaposleni";
            this.dgvZaposleni.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvZaposleni.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvZaposleni.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvZaposleni.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvZaposleni.Size = new System.Drawing.Size(825, 566);
            this.dgvZaposleni.TabIndex = 11;
            // 
            // btnTesting
            // 
            this.btnTesting.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnTesting.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnTesting.Location = new System.Drawing.Point(396, 39);
            this.btnTesting.Name = "btnTesting";
            this.btnTesting.Size = new System.Drawing.Size(219, 23);
            this.btnTesting.TabIndex = 12;
            this.btnTesting.Text = "Pregled proseka zarade";
            this.btnTesting.UseSelectable = true;
            this.btnTesting.Click += new System.EventHandler(this.btnTesting_Click_1);
            // 
            // metroButton2
            // 
            this.metroButton2.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton2.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.metroButton2.Location = new System.Drawing.Point(622, 39);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(233, 23);
            this.metroButton2.TabIndex = 13;
            this.metroButton2.Text = "Izveštaji za banku";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click_1);
            // 
            // FinansijskaMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 708);
            this.Controls.Add(this.metroButton2);
            this.Controls.Add(this.btnTesting);
            this.Controls.Add(this.dgvZaposleni);
            this.Controls.Add(this.btnPoZaposlenom);
            this.Controls.Add(this.btnPoMesecu);
            this.Controls.Add(this.metroButton4);
            this.Controls.Add(this.btnAzuzirajKoef);
            this.Controls.Add(this.metroButton1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(885, 10000);
            this.MinimumSize = new System.Drawing.Size(885, 708);
            this.Name = "FinansijskaMainForm";
            this.Text = "OPZ–2018: Finansijska služba";
            ((System.ComponentModel.ISupportInitialize)(this.dgvZaposleni)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroButton btnAzuzirajKoef;
        private MetroFramework.Controls.MetroButton metroButton4;
        private MetroFramework.Controls.MetroButton btnPoMesecu;
        private MetroFramework.Controls.MetroButton btnPoZaposlenom;
        private MetroFramework.Controls.MetroGrid dgvZaposleni;
        private MetroFramework.Controls.MetroButton btnTesting;
        private MetroFramework.Controls.MetroButton metroButton2;
    }
}

