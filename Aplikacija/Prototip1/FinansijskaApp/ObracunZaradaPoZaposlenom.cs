﻿using MetroFramework.Forms;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using SharedComponents;
using Persistence.Entities;

namespace FinansijskaApp
{
    public partial class ObracunZaradaPoZaposlenom : MetroForm
    {
        private int idZaposlenog;
        private String ime;

        public ObracunZaradaPoZaposlenom()
        {
            InitializeComponent();
        }

        public ObracunZaradaPoZaposlenom(int id)
            : this()
        {
            idZaposlenog = id;
            Prikazi();
        }

        private void btnExportPDF_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Portable Document Format (*.pdf)|*.pdf";
            saveFileDialog1.DefaultExt = "";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                DataGridView temp = new DataGridView();
                temp.Columns.Add("MesecGodina", "Mesec");
                temp.Columns.Add("CenaRada", "Cena rada");
                temp.Columns.Add("KoeficijentRadnogMesta", "Koeficijent");
                temp.Columns.Add("RadniCasovi", "Radni časovi");
                temp.Columns.Add("SankcijaStimulacija", "Sank/stim");
                temp.Columns.Add("PrekovremeniRad", "Prekovremeni");
                temp.Columns.Add("MinuliRad", "Minuli");
                temp.Columns.Add("Bruto", "Bruto");
                temp.Columns.Add("Porez", "Porez");
                temp.Columns.Add("DopZapPIO", "DopZapPIO");
                temp.Columns.Add("DopZapNez", "DopZapNez");
                temp.Columns.Add("DopZapZdr", "DopZapZdr");
                temp.Columns.Add("DopPosPIO", "DopPosPIO");
                temp.Columns.Add("DopPosNez", "DopPosNez");
                temp.Columns.Add("DopPosZdr", "DopPosZdr");
                temp.Columns.Add("Neto", "Neto");
                foreach (DataGridViewRow red in dgvObracun.Rows)
                {
                    DataGridViewRow novi = (DataGridViewRow)red.Clone();
                    int indeks = 0;
                    foreach (DataGridViewCell cell in red.Cells)
                    {
                        novi.Cells[indeks].Value = cell.Value;
                        indeks++;
                    }
                    temp.Rows.Add(novi);
                }

                DocumentExport.DatagridviewToPdf(temp, saveFileDialog1.FileName, ime + " (" + idZaposlenog + ") – Istorija obračuna zarada");
            }
            saveFileDialog1.FileName = String.Empty;
        }

        private void Prikazi()
        {
            using (OpzContext kontekst = new OpzContext())
            {
                Zaposleni zap = kontekst.ListaZaposlenih.Find(idZaposlenog);
                ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime;
                lblIme.Text = ime;
                var prikaz = (from obracun in zap.Obracuni
                              orderby obracun.MesecGodina descending
                              select new
                              {
                                  MesecGodina = obracun.MesecGodina.ToString("MM.yyyy."),
                                  CenaRada = obracun.CenaRada,
                                  KoeficijentRadnogMesta = obracun.KoeficijentRadnogMesta,
                                  RadniCasovi = obracun.RadniCasovi,
                                  SankcijaStimulacija = obracun.SankcijaStimulacija,
                                  PrekovremeniRad = obracun.PrekovremeniRad,
                                  MinuliRad = obracun.MinuliRad,
                                  Bruto = obracun.Bruto,
                                  Porez = obracun.Porez,
                                  DopZapPIO = obracun.DopZapPIO,
                                  DopZapNez = obracun.DopZapNez,
                                  DopZapZdr = obracun.DopZapZdr,
                                  DopPosPIO = obracun.DopPosPIO,
                                  DopPosNez = obracun.DopPosNez,
                                  DopPosZdr = obracun.DopPosZdr,
                                  Neto = obracun.Neto
                              }).ToList();

                dgvObracun.DataSource = prikaz;
                dgvObracun.Columns["MesecGodina"].HeaderText = "Mesec";
                dgvObracun.Columns["CenaRada"].HeaderText = "Cena rada";
                dgvObracun.Columns["KoeficijentRadnogMesta"].HeaderText = "Koeficijent radnog mesta";
                dgvObracun.Columns["RadniCasovi"].HeaderText = "Radni časovi za tekući mesec";
                dgvObracun.Columns["SankcijaStimulacija"].HeaderText = "Sankcija/stimulacija";
                dgvObracun.Columns["PrekovremeniRad"].HeaderText = "Prekovremeni rad";
                dgvObracun.Columns["MinuliRad"].HeaderText = "Minuli rad";
                dgvObracun.Columns["Bruto"].HeaderText = "Ukupna bruto zarada i naknada za mesec";
                dgvObracun.Columns["Porez"].HeaderText = "Porez";
                dgvObracun.Columns["DopZapPIO"].HeaderText = "Doprinosi zaposlenog (PIO)";
                dgvObracun.Columns["DopZapNez"].HeaderText = "Doprinosi zaposlenog (Nezaposlenost)";
                dgvObracun.Columns["DopZapZdr"].HeaderText = "Doprinosi zaposlenog (Zdravstvo)";
                dgvObracun.Columns["DopPosPIO"].HeaderText = "Doprinosi poslodavca (PIO)";
                dgvObracun.Columns["DopPosNez"].HeaderText = "Doprinosi poslodavca (Nezaposlenost)";
                dgvObracun.Columns["DopPosZdr"].HeaderText = "Doprinosi poslodavca (Zdravstvo)";
                dgvObracun.Columns["Neto"].HeaderText = "Neto po odbitku";

                foreach (DataGridViewColumn dgvcol in dgvObracun.Columns)
                {
                    dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

        private void txtGodina_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
