﻿using LiveCharts;
using LiveCharts.Wpf;
using MetroFramework.Forms;
using Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace FinansijskaApp
{
    public partial class ProsekPlateChartForm : MetroForm
    {
        public ProsekPlateChartForm()
        {
            InitializeComponent();
        }

        private void ProsekPlateChartForm_Load(object sender, EventArgs e)
        {
            DateTime start = new DateTime(2005, 1, 1);
            DateTime end = new DateTime(DateTime.Now.Year, 1, 1);
            List<string> cb1 = new List<string>();
            List<string> cb2 = new List<string>();

            for (DateTime it = start; it <= end; it = it.AddYears(1))
            {
                cb1.Add(it.ToString("yyyy"));
                cb2.Add(it.ToString("yyyy"));
            }

            godinaOd.DataSource = cb1;
            godinaDo.DataSource = cb2;

            godinaOd.DropDownStyle = ComboBoxStyle.DropDownList;
            godinaDo.DropDownStyle = ComboBoxStyle.DropDownList;

            mesecOd.Text = "Januar";
            mesecDo.Text = "Januar";
            godinaOd.Text = "2018";
            godinaDo.Text = "2018";

            mesecOd.DropDownStyle = ComboBoxStyle.DropDownList;
            mesecDo.DropDownStyle = ComboBoxStyle.DropDownList;

            chart.Series.Clear();
            chart.AxisX.Clear();
            chart.AxisY.Clear();

        }

        #region funkcije
        private void btnTesting_Click_1(object sender, EventArgs e)
        {
            DateTime startDate = new DateTime(2018, 6, 17);
            DateTime endDate = new DateTime(2018, 8, 17);

            if (startDate > endDate || startDate <= DateTime.Today)
            {
                return;
            }

            using (OpzContext kontekst = new OpzContext())
            {
                List<String> smeneTipovi = kontekst.ListaSmena.Select(x => x.TipSmene).Distinct().ToList();
                Dictionary<String, bool> radniDan = new Dictionary<String, bool>();

                foreach (String smen in smeneTipovi)
                {
                    for (DateTime dan = startDate; dan <= endDate; dan = dan.AddDays(1))
                    {
                        String danInWeek = "";
                        if (dan.DayOfWeek == DayOfWeek.Monday)
                        {
                            danInWeek = "ponedeljak";
                        }
                        else if (dan.DayOfWeek == DayOfWeek.Tuesday)
                        {
                            danInWeek = "utorak";
                        }
                        else if (dan.DayOfWeek == DayOfWeek.Wednesday)
                        {
                            danInWeek = "sreda";
                        }
                        else if (dan.DayOfWeek == DayOfWeek.Thursday)
                        {
                            danInWeek = "cetvrtak";
                        }
                        else if (dan.DayOfWeek == DayOfWeek.Friday)
                        {
                            danInWeek = "petak";
                        }
                        else if (dan.DayOfWeek == DayOfWeek.Saturday)
                        {
                            danInWeek = "subota";
                        }
                        else
                        {
                            danInWeek = "nedelja";
                        }

                        var datumi = (from x in kontekst.ListaSmena
                                      where x.DatumDonosenja < dan && x.TipSmene == smen && x.DanUNedelji == danInWeek
                                      orderby x.DatumDonosenja descending
                                      select new
                                      {
                                          Pocetak = x.Pocetak,
                                          Kraj = x.Kraj
                                      }).ToList();

                        if (datumi.Count == 0)
                        {
                            radniDan.Add(smen + "/" + dan.ToString(), false);
                        }
                        else
                        {
                            radniDan.Add(smen + "/" + dan.ToString(), true);
                        }
                    }
                }

                List<int> brojOdsutnih = new List<int>();

                for (DateTime dan = startDate; dan <= endDate; dan = dan.AddDays(1))
                {
                    int brojche = 0;
                    foreach (Zaposleni zap in kontekst.ListaZaposlenih)
                    {
                        var nazivlist = (from x in zap.RadnaVremena
                                         where x.Datum < dan
                                         orderby x.Datum descending
                                         select x.Smena).ToList();

                        if (nazivlist.Count == 0)
                        {
                            continue;
                        }

                        String naziv = nazivlist[0];

                        if (radniDan[naziv + "/" + dan.ToString()])
                        {
                            var ods = (from o in zap.Odsustva
                                       where o.Pocetak <= dan && o.Kraj >= dan
                                       select o).ToList();

                            if (ods.Count != 0)
                            {
                                brojche++;
                            }
                        }
                    }
                    brojOdsutnih.Add(brojche);
                }

                brojOdsutnih.Add(-1);
            }
        }

        private List<Tuple<string, double>> VratiProseke()
        {
            DateTime startMonth = new DateTime(Int32.Parse(godinaOd.Text), mesecOd.SelectedIndex+1, 1);
            DateTime endMonth = new DateTime(Int32.Parse(godinaDo.Text), mesecDo.SelectedIndex + 1, 1);


            List<Tuple<string, double>> proseci = new List<Tuple<string, double>>();
            using (OpzContext kontekst = new OpzContext())
            {
                for (DateTime dan = startMonth; dan <= endMonth; dan = dan.AddMonths(1))
                {
                    List<double> obr = (from x in kontekst.ListaObracuna
                                        where x.MesecGodina == dan
                                        select x.Neto).ToList();

                    if (obr.Count == 0)
                    {
                        proseci.Add(new Tuple<string, double>(dan.ToString("MM.yyyy."), 0));
                    }
                    else
                    {
                        proseci.Add(new Tuple<string, double>(dan.ToString("MM.yyyy."), obr.Average()));
                    }
                }
            }
            return proseci;
        }
        #endregion

        private void btnPrikaziGrafikon_Click(object sender, EventArgs e)
        {
            //mora da se reloaduje ceo, inace ne prikazuje labele za kolone
            chart.Series.Clear();
            chart.AxisX.Clear();
            chart.AxisY.Clear();

            chart.Visible = true;

            chart.Series = new SeriesCollection
            {
                new ColumnSeries
                {
                    DataLabels = true,
                    Title = "Prosečna zarada je",
                    LabelPoint = point => Math.Round(point.Y).ToString(),
                    Values = new ChartValues<double>(from p in VratiProseke() select p.Item2)
                }
            };


            chart.AxisY.Add(new Axis
            {
                Title = "Prosečna zarada",
                LabelFormatter = value => value.ToString("N")
            });

            chart.AxisX.Add(new Axis
            {
                Title = "Datum obračuna",
                Labels = (from p in VratiProseke() select p.Item1).ToList(),
                Separator = new Separator() { Step = 1, IsEnabled = false },
                //LabelsRotation = 20,
            });
        }
    }
}
