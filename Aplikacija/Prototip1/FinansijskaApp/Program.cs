﻿using SharedComponents.Forms;
using System;
using System.Windows.Forms;
using static SharedComponents.Forms.Login;

namespace FinansijskaApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Uloga[] uloge = new Uloga[]
            {
                new Uloga("Knjigovođa", new FinansijskaFormFactory())
            };

            Application.Run(new Login(uloge));
            //Application.Run(new FinansijskaMainForm("ivica10"));
        }
    }
}
