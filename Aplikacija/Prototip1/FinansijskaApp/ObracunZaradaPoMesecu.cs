﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using SharedComponents;
using Persistence.Entities;
using MetroFramework;
using MetroFramework.Controls;

namespace FinansijskaApp
{
    public partial class ObracunZaradaPoMesecu : MetroForm
    {
        private DateTime mesecGodina;

        public ObracunZaradaPoMesecu()
        {
            InitializeComponent();

            DateTime trenutak = DateTime.Today;
            int godina = trenutak.Year;
            int mesec = trenutak.Month;
            if (--mesec == 0)
            {
                mesec = 12;
                godina--;
            }
            cbMesec.SelectedIndex = mesec - 1;
            txtGodina.Text = godina.ToString();
        }

        private void btnExportPDF_Click(object sender, EventArgs e)
        {
            if (dgvObracun.Rows.Count == 0)
            {
                MetroMessageBox.Show(this, "Tabela je prazna i ne može se izvršiti eksport.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                return;
            }

            saveFileDialog1.Filter = "Portable Document Format (*.pdf)|*.pdf";
            saveFileDialog1.DefaultExt = "";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                MetroGrid temp = new MetroGrid();
                temp.Columns.Add("IDZaposlenog", "ID");
                temp.Columns.Add("ImeZaposlenog", "Ime");
                temp.Columns.Add("CenaRada", "Cena rada");
                temp.Columns.Add("KoeficijentRadnogMesta", "Koeficijent");
                temp.Columns.Add("RadniCasovi", "Radni časovi");
                temp.Columns.Add("SankcijaStimulacija", "Sank/stim");
                temp.Columns.Add("PrekovremeniRad", "Prekovremeni");
                temp.Columns.Add("MinuliRad", "Minuli");
                temp.Columns.Add("Bruto", "Bruto");
                temp.Columns.Add("Porez", "Porez");
                temp.Columns.Add("DopZapPIO", "DopZapPIO");
                temp.Columns.Add("DopZapNez", "DopZapNez");
                temp.Columns.Add("DopZapZdr", "DopZapZdr");
                temp.Columns.Add("DopPosPIO", "DopPosPIO");
                temp.Columns.Add("DopPosNez", "DopPosNez");
                temp.Columns.Add("DopPosZdr", "DopPosZdr");
                temp.Columns.Add("Neto", "Neto");
                foreach (DataGridViewRow red in dgvObracun.Rows)
                {
                    DataGridViewRow novi = (DataGridViewRow)red.Clone();
                    int indeks = 0;
                    foreach (DataGridViewCell cell in red.Cells)
                    {
                        novi.Cells[indeks].Value = cell.Value;
                        indeks++;
                    }
                    temp.Rows.Add(novi);
                }

                if (!backgroundWorker1.IsBusy && !backgroundWorker2.IsBusy)
                {
                    backgroundWorker2.RunWorkerAsync(new Tuple<DataGridView,string>(temp,saveFileDialog1.FileName));
                }
                
            }
            saveFileDialog1.FileName = String.Empty;
        }

        private void btnPrikazi_Click(object sender, EventArgs e)
        {
            int mesec = cbMesec.SelectedIndex + 1;
            int godina = int.Parse(txtGodina.Text);
            DateTime startOfMonth = new DateTime(godina, mesec, 1);

            if (startOfMonth.AddMonths(1) >= DateTime.Now)
            {
                MetroMessageBox.Show(this, "Izabrani mesec još uvek nije prošao.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                return;
            }

            mesecGodina = startOfMonth;

            if (!backgroundWorker1.IsBusy && !backgroundWorker2.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync(startOfMonth);
            }

        }

        private void txtGodina_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        #region Spinner
        private void StartSpinning()
        {
            lblSpinner.Visible = true;
            circularProgressBar1.Visible = true;

            btnExportPDF.Enabled = false;
            btnPrikazi.Enabled = false;
            cbMesec.Enabled = false;
            txtGodina.Enabled = false;
        }

        private void StopSpinning()
        {
            lblSpinner.Visible = false;
            circularProgressBar1.Visible = false;

            btnExportPDF.Enabled = true;
            btnPrikazi.Enabled = true;
            cbMesec.Enabled = true;
            txtGodina.Enabled = true;
        }
        #endregion

        private delegate void RefreshDlg(List<Obracun> l);
        private void RefreshDgv(List<Obracun> obracuni)
        {
            var prikaz = obracuni.Select(obracun => new
            {
                IDZaposlenog = obracun.Zaposleni.Id.ToString(),
                ImeZaposlenog = obracun.Zaposleni.Ime + " " + obracun.Zaposleni.SSlovo + ". " + obracun.Zaposleni.Prezime,
                CenaRada = obracun.CenaRada.ToString(),
                KoeficijentRadnogMesta = obracun.KoeficijentRadnogMesta.ToString(),
                RadniCasovi = obracun.RadniCasovi.ToString(),
                SankcijaStimulacija = obracun.SankcijaStimulacija.ToString(),
                PrekovremeniRad = obracun.PrekovremeniRad.ToString(),
                MinuliRad = obracun.MinuliRad.ToString(),
                Bruto = obracun.Bruto,
                Porez = obracun.Porez,
                DopZapPIO = obracun.DopZapPIO,
                DopZapNez = obracun.DopZapNez,
                DopZapZdr = obracun.DopZapZdr,
                DopPosPIO = obracun.DopPosPIO,
                DopPosNez = obracun.DopPosNez,
                DopPosZdr = obracun.DopPosZdr,
                Neto = obracun.Neto
            }).ToList();

            prikaz.Add(new
            {
                IDZaposlenog = "",
                ImeZaposlenog = "UKUPNO",
                CenaRada = "",
                KoeficijentRadnogMesta = "",
                RadniCasovi = "",
                SankcijaStimulacija = "",
                PrekovremeniRad = "",
                MinuliRad = "",
                Bruto = obracuni.Sum(x => x.Bruto),
                Porez = obracuni.Sum(x => x.Porez),
                DopZapPIO = obracuni.Sum(x => x.DopZapPIO),
                DopZapNez = obracuni.Sum(x => x.DopZapNez),
                DopZapZdr = obracuni.Sum(x => x.DopZapZdr),
                DopPosPIO = obracuni.Sum(x => x.DopPosPIO),
                DopPosNez = obracuni.Sum(x => x.DopPosNez),
                DopPosZdr = obracuni.Sum(x => x.DopPosZdr),
                Neto = obracuni.Sum(x => x.Neto)
            });

            dgvObracun.DataSource = prikaz;
            dgvObracun.Columns["IDZaposlenog"].HeaderText = "ID zaposlenog";
            dgvObracun.Columns["ImeZaposlenog"].HeaderText = "Ime zaposlenog";
            dgvObracun.Columns["CenaRada"].HeaderText = "Cena rada";
            dgvObracun.Columns["KoeficijentRadnogMesta"].HeaderText = "Koeficijent radnog mesta";
            dgvObracun.Columns["RadniCasovi"].HeaderText = "Radni časovi za tekući mesec";
            dgvObracun.Columns["SankcijaStimulacija"].HeaderText = "Sankcija/stimulacija";
            dgvObracun.Columns["PrekovremeniRad"].HeaderText = "Prekovremeni rad";
            dgvObracun.Columns["MinuliRad"].HeaderText = "Minuli rad";
            dgvObracun.Columns["Bruto"].HeaderText = "Ukupna bruto zarada i naknada za mesec";
            dgvObracun.Columns["Porez"].HeaderText = "Porez";
            dgvObracun.Columns["DopZapPIO"].HeaderText = "Doprinosi zaposlenog (PIO)";
            dgvObracun.Columns["DopZapNez"].HeaderText = "Doprinosi zaposlenog (Nezaposlenost)";
            dgvObracun.Columns["DopZapZdr"].HeaderText = "Doprinosi zaposlenog (Zdravstvo)";
            dgvObracun.Columns["DopPosPIO"].HeaderText = "Doprinosi poslodavca (PIO)";
            dgvObracun.Columns["DopPosNez"].HeaderText = "Doprinosi poslodavca (Nezaposlenost)";
            dgvObracun.Columns["DopPosZdr"].HeaderText = "Doprinosi poslodavca (Zdravstvo)";
            dgvObracun.Columns["Neto"].HeaderText = "Neto po odbitku";

            foreach (DataGridViewColumn dgvcol in dgvObracun.Columns)
            {
                dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            dgvObracun.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Invoke(spnstart);

            DateTime startOfMonth = (e.Argument as DateTime?).Value; 
            List<Obracun> obracuni = new List<Obracun>();
            
            using (OpzContext kontekst = new OpzContext())
            {
                var koeflist = (from x in kontekst.ListaKoeficijenata
                                orderby x.Vreme descending
                                select x).ToList();

                if (koeflist.Count == 0)
                {
                    MessageBox.Show("Nema koeficijenata");
                    return;
                }

                Koeficijenti koef = koeflist[0];

                List<String> smeneTipovi = kontekst.ListaSmena.Select(x => x.TipSmene).Distinct().ToList();
                Dictionary<String, DateTime> datumPocetka = new Dictionary<String, DateTime>();
                Dictionary<String, DateTime> datumKraja = new Dictionary<String, DateTime>();

                foreach (String smen in smeneTipovi)
                {
                    for (DateTime dan = startOfMonth; dan.Month == startOfMonth.Month; dan = dan.AddDays(1))
                    {
                        String danInWeek = "";
                        if (dan.DayOfWeek == DayOfWeek.Monday)
                        {
                            danInWeek = "ponedeljak";
                        }
                        else if (dan.DayOfWeek == DayOfWeek.Tuesday)
                        {
                            danInWeek = "utorak";
                        }
                        else if (dan.DayOfWeek == DayOfWeek.Wednesday)
                        {
                            danInWeek = "sreda";
                        }
                        else if (dan.DayOfWeek == DayOfWeek.Thursday)
                        {
                            danInWeek = "cetvrtak";
                        }
                        else if (dan.DayOfWeek == DayOfWeek.Friday)
                        {
                            danInWeek = "petak";
                        }
                        else if (dan.DayOfWeek == DayOfWeek.Saturday)
                        {
                            danInWeek = "subota";
                        }
                        else
                        {
                            danInWeek = "nedelja";
                        }

                        var datumi = (from x in kontekst.ListaSmena
                                      where x.DatumDonosenja < dan && x.TipSmene == smen && x.DanUNedelji == danInWeek
                                      orderby x.DatumDonosenja descending
                                      select new
                                      {
                                          Pocetak = x.Pocetak,
                                          Kraj = x.Kraj
                                      }).ToList();

                        if (datumi.Count == 0)
                        {
                            continue;
                        }
                        datumPocetka.Add(smen + "/" + dan.ToString(), datumi[0].Pocetak);
                        datumKraja.Add(smen + "/" + dan.ToString(), datumi[0].Kraj);
                    }
                }

                foreach (Zaposleni zap in kontekst.ListaZaposlenih.OrderBy(x => x.Id))
                {
                    double radniH = 0;
                    double dodatniRadniH = 0;
                    double efektivniHOdmora = 0;
                    double maxH = 0;

                    Obracun ob = zap.Obracuni.Find(x => x.MesecGodina == startOfMonth);
                    if (ob != null)
                    {
                        obracuni.Add(ob);
                        continue;
                    }

                    bool preskocen = false;

                    for (DateTime dan = startOfMonth; dan.Month == startOfMonth.Month; dan = dan.AddDays(1))
                    {
                        var nazivlist = (from x in zap.RadnaVremena
                                         where x.Datum < dan
                                         orderby x.Datum descending
                                         select x.Smena).ToList();

                        if (nazivlist.Count == 0)
                        {
                            preskocen = true;
                            break;
                        }

                        String naziv = nazivlist[0];

                        DateTime y = new DateTime();
                        if (!datumPocetka.TryGetValue(naziv + "/" + dan.ToString(), out y))
                        {
                            continue;
                        }

                        var smena = new
                        {
                            Pocetak = datumPocetka[naziv + "/" + dan.ToString()],
                            Kraj = datumKraja[naziv + "/" + dan.ToString()]
                        };

                        TimeSpan trebaDaRadi = smena.Kraj.TimeOfDay - smena.Pocetak.TimeOfDay;
                        if (smena.Pocetak.TimeOfDay > smena.Kraj.TimeOfDay)
                        {
                            trebaDaRadi += new TimeSpan(24, 0, 0);
                        }
                        DateTime trebaPocetak = new DateTime(dan.Year, dan.Month, dan.Day, smena.Pocetak.Hour, smena.Pocetak.Minute, smena.Pocetak.Second);
                        DateTime trebaKraj = trebaPocetak + trebaDaRadi;

                        var odsustva = (from x in zap.Odsustva
                                        where x.Pocetak >= dan && x.Kraj <= dan
                                        select x).ToList();

                        maxH += trebaDaRadi.TotalHours;

                        if (odsustva.Count != 0)
                        {
                            if (odsustva[0].Tip == "Godišnji odmor" || odsustva[0].Tip == "Plaćeno")
                            {
                                efektivniHOdmora += trebaDaRadi.TotalHours;
                            }
                            else if (odsustva[0].Tip == "Bolovanje")
                            {
                                efektivniHOdmora += trebaDaRadi.TotalHours * 0.6;
                            }
                        }
                        else
                        {
                            radniH += trebaDaRadi.TotalHours;

                            var prisustva = (from x in zap.Prisustva
                                             where x.VremeDolaska < trebaKraj && x.VremeOdlaska > trebaPocetak
                                             select x).ToList();

                            TimeSpan radio = new TimeSpan(0);

                            foreach (var x in prisustva)
                            {
                                if (x.VremeOdlaska == null)
                                {
                                    radio += DateTime.Now - x.VremeDolaska;
                                }
                                else
                                {
                                    radio += x.VremeOdlaska.Value - x.VremeDolaska;
                                }
                            }

                            double dodatniH = 0;
                            if (radio > trebaDaRadi)
                            {
                                dodatniH = Math.Truncate((radio - trebaDaRadi).TotalHours);
                            }
                            dodatniRadniH += dodatniH;
                        }
                    }

                    if (preskocen)
                    {
                        continue;
                    }

                    double pareOdRada = zap.KoeficijentRadnogMesta.Value * koef.CenaRada / 176 * radniH;
                    double pareOdOdmora = zap.KoeficijentRadnogMesta.Value * koef.CenaRada / 176 * efektivniHOdmora;
                    double pareOdDodatnog = zap.KoeficijentRadnogMesta.Value * koef.CenaRada / 176 * dodatniRadniH * koef.ProcenatZaExtra / 100;

                    var ss = (from x in zap.SankcijeStimulacije
                              where x.DatumIzricanja >= startOfMonth && x.DatumIzricanja < startOfMonth.AddMonths(1)
                              select x).ToList();

                    double sankstim = 0;
                    foreach (var s in ss)
                    {
                        sankstim += s.Vrednost;
                    }

                    int meseciStaza = 0;
                    while (zap.DatumPocetkaRada.AddMonths(meseciStaza) < startOfMonth)
                    {
                        meseciStaza++;
                    }
                    meseciStaza += (int)zap.PocetniMeseciStaza;
                    int godineStaza = ((meseciStaza - 1) / 12) + 1;

                    double bruto = pareOdRada * ((1 + sankstim / 100) * (1 + koef.ProcenatZaMinuli / 100 * godineStaza)) + pareOdDodatnog
                        + pareOdOdmora * (1 + koef.ProcenatZaMinuli / 100 * godineStaza);

                    double neto = bruto * (1 - koef.ProcenatZaPorez / 100 - (koef.DopZaposlenogZdravstvo + koef.DopZaposlenogPIO
                        + koef.DopZaposlenogNezaposlenost) / 100) + koef.NeoporezivaOsnovica * koef.ProcenatZaPorez / 100;

                    double djavolche = bruto * (koef.DopPoslodavcaNezaposlenost + koef.DopPoslodavcaPIO + koef.DopPoslodavcaZdravstvo) / 100;

                    Obracun novi = new Obracun()
                    {
                        Zaposleni = zap,
                        MesecGodina = startOfMonth,
                        CenaRada = Math.Truncate(koef.CenaRada * 100) / 100,
                        KoeficijentRadnogMesta = Math.Truncate(zap.KoeficijentRadnogMesta.Value * 100) / 100,
                        RadniCasovi = Math.Truncate(maxH),
                        SankcijaStimulacija = Math.Truncate(100 * sankstim) / 100,
                        PrekovremeniRad = Math.Truncate(pareOdDodatnog),
                        MinuliRad = Math.Truncate((pareOdRada * (1 + sankstim) + pareOdOdmora) * godineStaza * koef.ProcenatZaMinuli / 100),
                        Bruto = Math.Truncate(bruto),
                        Porez = Math.Truncate((bruto - koef.NeoporezivaOsnovica) * koef.ProcenatZaPorez / 100),
                        DopZapPIO = Math.Truncate(bruto * koef.DopZaposlenogPIO / 100),
                        DopZapNez = Math.Truncate(bruto * koef.DopZaposlenogNezaposlenost / 100),
                        DopZapZdr = Math.Truncate(bruto * koef.DopZaposlenogZdravstvo / 100),
                        DopPosPIO = Math.Truncate(bruto * koef.DopPoslodavcaPIO / 100),
                        DopPosNez = Math.Truncate(bruto * koef.DopPoslodavcaNezaposlenost / 100),
                        DopPosZdr = Math.Truncate(bruto * koef.DopPoslodavcaZdravstvo / 100),
                        Neto = Math.Truncate(neto)
                    };

                    obracuni.Add(novi);
                    zap.Obracuni.Add(novi);
                }
                kontekst.SaveChanges();
            }
            RefreshDlg d1 = RefreshDgv;
            Invoke(d1, new object[] { obracuni });
        }

        private delegate void Del2();
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Del2 spnstop = StopSpinning;
            //zaustavljanje spinnera
            Invoke(spnstop);
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Invoke(spnstart);

            Tuple<DataGridView, string> arg = e.Argument as Tuple<DataGridView, string>;

            DocumentExport.DatagridviewToPdf(arg.Item1, arg.Item2, "Obračun zarada za sve zaposlene za mesec " + mesecGodina.ToString("MM.yyyy."));
        }

        private void backgroundWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Del2 spnstop = StopSpinning;
            //zaustavljanje spinnera
            Invoke(spnstop);
        }

        private void ObracunZaradaPoMesecu_Load(object sender, EventArgs e)
        {
            DateTime start = new DateTime(2005, 1, 1);
            DateTime end = new DateTime(DateTime.Now.Year, 1, 1);           
            List<string> cb = new List<string>();

            for(DateTime it = start;it<=end;it = it.AddYears(1))
            {
                cb.Add(it.ToString("yyyy"));
            }

            txtGodina.DataSource = cb;
            txtGodina.SelectedIndex = cb.Count - 1;
        }

        private void ObracunZaradaPoMesecu_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorker1.IsBusy || backgroundWorker2.IsBusy)
            {
                e.Cancel = true;
            }
        }
    }
}
