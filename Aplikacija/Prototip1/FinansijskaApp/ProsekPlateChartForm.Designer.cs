﻿namespace FinansijskaApp
{
    partial class ProsekPlateChartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnPrikaziGrafikon = new MetroFramework.Controls.MetroButton();
            this.lblMesec = new MetroFramework.Controls.MetroLabel();
            this.mesecOd = new MetroFramework.Controls.MetroComboBox();
            this.godinaOd = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.mesecDo = new MetroFramework.Controls.MetroComboBox();
            this.godinaDo = new MetroFramework.Controls.MetroComboBox();
            this.chart = new LiveCharts.WinForms.CartesianChart();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 116F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 116F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 109F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel1.Controls.Add(this.btnPrikaziGrafikon, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblMesec, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.mesecOd, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.godinaOd, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel1, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.mesecDo, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.godinaDo, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.chart, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(20, 60);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(899, 574);
            this.tableLayoutPanel1.TabIndex = 12;
            // 
            // btnPrikaziGrafikon
            // 
            this.btnPrikaziGrafikon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrikaziGrafikon.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnPrikaziGrafikon.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnPrikaziGrafikon.Location = new System.Drawing.Point(762, 3);
            this.btnPrikaziGrafikon.Name = "btnPrikaziGrafikon";
            this.btnPrikaziGrafikon.Size = new System.Drawing.Size(134, 32);
            this.btnPrikaziGrafikon.TabIndex = 1;
            this.btnPrikaziGrafikon.Text = "Prikaži grafikon";
            this.btnPrikaziGrafikon.UseSelectable = true;
            this.btnPrikaziGrafikon.Click += new System.EventHandler(this.btnPrikaziGrafikon_Click);
            // 
            // lblMesec
            // 
            this.lblMesec.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblMesec.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblMesec.Location = new System.Drawing.Point(3, 0);
            this.lblMesec.Name = "lblMesec";
            this.lblMesec.Size = new System.Drawing.Size(65, 38);
            this.lblMesec.TabIndex = 0;
            this.lblMesec.Text = "Od";
            this.lblMesec.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // mesecOd
            // 
            this.mesecOd.FormattingEnabled = true;
            this.mesecOd.ItemHeight = 23;
            this.mesecOd.Items.AddRange(new object[] {
            "Januar",
            "Februar",
            "Mart",
            "April",
            "Maj",
            "Jun",
            "Jul",
            "Avgust",
            "Septembar",
            "Oktobar",
            "Novembar",
            "Decembar"});
            this.mesecOd.Location = new System.Drawing.Point(74, 3);
            this.mesecOd.Name = "mesecOd";
            this.mesecOd.Size = new System.Drawing.Size(110, 29);
            this.mesecOd.TabIndex = 7;
            this.mesecOd.UseSelectable = true;
            // 
            // godinaOd
            // 
            this.godinaOd.FormattingEnabled = true;
            this.godinaOd.ItemHeight = 23;
            this.godinaOd.Location = new System.Drawing.Point(190, 3);
            this.godinaOd.Name = "godinaOd";
            this.godinaOd.Size = new System.Drawing.Size(98, 29);
            this.godinaOd.TabIndex = 13;
            this.godinaOd.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.Location = new System.Drawing.Point(294, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(63, 36);
            this.metroLabel1.TabIndex = 14;
            this.metroLabel1.Text = "Do";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // mesecDo
            // 
            this.mesecDo.FormattingEnabled = true;
            this.mesecDo.ItemHeight = 23;
            this.mesecDo.Items.AddRange(new object[] {
            "Januar",
            "Februar",
            "Mart",
            "April",
            "Maj",
            "Jun",
            "Jul",
            "Avgust",
            "Septembar",
            "Oktobar",
            "Novembar",
            "Decembar"});
            this.mesecDo.Location = new System.Drawing.Point(363, 3);
            this.mesecDo.Name = "mesecDo";
            this.mesecDo.Size = new System.Drawing.Size(110, 29);
            this.mesecDo.TabIndex = 15;
            this.mesecDo.UseSelectable = true;
            // 
            // godinaDo
            // 
            this.godinaDo.FormattingEnabled = true;
            this.godinaDo.ItemHeight = 23;
            this.godinaDo.Location = new System.Drawing.Point(479, 3);
            this.godinaDo.Name = "godinaDo";
            this.godinaDo.Size = new System.Drawing.Size(103, 29);
            this.godinaDo.TabIndex = 16;
            this.godinaDo.UseSelectable = true;
            // 
            // chart
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.chart, 8);
            this.chart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart.Location = new System.Drawing.Point(3, 41);
            this.chart.Name = "chart";
            this.chart.Size = new System.Drawing.Size(893, 530);
            this.chart.TabIndex = 17;
            this.chart.Visible = false;
            // 
            // ProsekPlateChartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 654);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(939, 654);
            this.Name = "ProsekPlateChartForm";
            this.Text = "Prikaz proseka zarade zaposlenih";
            this.Load += new System.EventHandler(this.ProsekPlateChartForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroComboBox godinaDo;
        private MetroFramework.Controls.MetroButton btnPrikaziGrafikon;
        private MetroFramework.Controls.MetroLabel lblMesec;
        private MetroFramework.Controls.MetroComboBox mesecOd;
        private MetroFramework.Controls.MetroComboBox godinaOd;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroComboBox mesecDo;
        private LiveCharts.WinForms.CartesianChart chart;
    }
}