﻿namespace FinansijskaApp
{
    partial class NoviKoeficijentiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCenaRada = new MetroFramework.Controls.MetroLabel();
            this.lblProcenatExtra = new MetroFramework.Controls.MetroLabel();
            this.lblProcenatMinuli = new MetroFramework.Controls.MetroLabel();
            this.lblNeoporeziva = new MetroFramework.Controls.MetroLabel();
            this.lblProcenatPorez = new MetroFramework.Controls.MetroLabel();
            this.lblDopZap = new MetroFramework.Controls.MetroLabel();
            this.lblZapPIO = new MetroFramework.Controls.MetroLabel();
            this.lblZapZdravstvo = new MetroFramework.Controls.MetroLabel();
            this.lblZapNezap = new MetroFramework.Controls.MetroLabel();
            this.lblDopPos = new MetroFramework.Controls.MetroLabel();
            this.lblPosPIO = new MetroFramework.Controls.MetroLabel();
            this.lblPosZdravstvo = new MetroFramework.Controls.MetroLabel();
            this.lblPosNezap = new MetroFramework.Controls.MetroLabel();
            this.txtCenaRada = new MetroFramework.Controls.MetroTextBox();
            this.txtPrekovremeni = new MetroFramework.Controls.MetroTextBox();
            this.txtMinuli = new MetroFramework.Controls.MetroTextBox();
            this.txtNeoporeziva = new MetroFramework.Controls.MetroTextBox();
            this.txtPorez = new MetroFramework.Controls.MetroTextBox();
            this.txtZapPIO = new MetroFramework.Controls.MetroTextBox();
            this.txtZapZdravstvo = new MetroFramework.Controls.MetroTextBox();
            this.txtZapNezap = new MetroFramework.Controls.MetroTextBox();
            this.txtPosNezap = new MetroFramework.Controls.MetroTextBox();
            this.txtPosZdravstvo = new MetroFramework.Controls.MetroTextBox();
            this.txtPosPIO = new MetroFramework.Controls.MetroTextBox();
            this.btnSacuvaj = new MetroFramework.Controls.MetroButton();
            this.btnIzadji = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // lblCenaRada
            // 
            this.lblCenaRada.AutoSize = true;
            this.lblCenaRada.Location = new System.Drawing.Point(23, 79);
            this.lblCenaRada.Name = "lblCenaRada";
            this.lblCenaRada.Size = new System.Drawing.Size(70, 19);
            this.lblCenaRada.TabIndex = 0;
            this.lblCenaRada.Text = "Cena rada";
            // 
            // lblProcenatExtra
            // 
            this.lblProcenatExtra.AutoSize = true;
            this.lblProcenatExtra.Location = new System.Drawing.Point(23, 110);
            this.lblProcenatExtra.Name = "lblProcenatExtra";
            this.lblProcenatExtra.Size = new System.Drawing.Size(187, 19);
            this.lblProcenatExtra.TabIndex = 2;
            this.lblProcenatExtra.Text = "Procenat za prekovremeni rad";
            // 
            // lblProcenatMinuli
            // 
            this.lblProcenatMinuli.AutoSize = true;
            this.lblProcenatMinuli.Location = new System.Drawing.Point(23, 141);
            this.lblProcenatMinuli.Name = "lblProcenatMinuli";
            this.lblProcenatMinuli.Size = new System.Drawing.Size(141, 19);
            this.lblProcenatMinuli.TabIndex = 3;
            this.lblProcenatMinuli.Text = "Procenat za minuli rad";
            // 
            // lblNeoporeziva
            // 
            this.lblNeoporeziva.AutoSize = true;
            this.lblNeoporeziva.Location = new System.Drawing.Point(23, 172);
            this.lblNeoporeziva.Name = "lblNeoporeziva";
            this.lblNeoporeziva.Size = new System.Drawing.Size(138, 19);
            this.lblNeoporeziva.TabIndex = 4;
            this.lblNeoporeziva.Text = "Neoporeziva osnovica";
            // 
            // lblProcenatPorez
            // 
            this.lblProcenatPorez.AutoSize = true;
            this.lblProcenatPorez.Location = new System.Drawing.Point(23, 203);
            this.lblProcenatPorez.Name = "lblProcenatPorez";
            this.lblProcenatPorez.Size = new System.Drawing.Size(116, 19);
            this.lblProcenatPorez.TabIndex = 5;
            this.lblProcenatPorez.Text = "Procenat za porez";
            // 
            // lblDopZap
            // 
            this.lblDopZap.AutoSize = true;
            this.lblDopZap.Location = new System.Drawing.Point(23, 234);
            this.lblDopZap.Name = "lblDopZap";
            this.lblDopZap.Size = new System.Drawing.Size(136, 19);
            this.lblDopZap.TabIndex = 6;
            this.lblDopZap.Text = "Doprinosi zaposlenog";
            // 
            // lblZapPIO
            // 
            this.lblZapPIO.AutoSize = true;
            this.lblZapPIO.Location = new System.Drawing.Point(76, 265);
            this.lblZapPIO.Name = "lblZapPIO";
            this.lblZapPIO.Size = new System.Drawing.Size(31, 19);
            this.lblZapPIO.TabIndex = 7;
            this.lblZapPIO.Text = "PIO";
            // 
            // lblZapZdravstvo
            // 
            this.lblZapZdravstvo.AutoSize = true;
            this.lblZapZdravstvo.Location = new System.Drawing.Point(76, 296);
            this.lblZapZdravstvo.Name = "lblZapZdravstvo";
            this.lblZapZdravstvo.Size = new System.Drawing.Size(66, 19);
            this.lblZapZdravstvo.TabIndex = 8;
            this.lblZapZdravstvo.Text = "Zdravstvo";
            // 
            // lblZapNezap
            // 
            this.lblZapNezap.AutoSize = true;
            this.lblZapNezap.Location = new System.Drawing.Point(76, 327);
            this.lblZapNezap.Name = "lblZapNezap";
            this.lblZapNezap.Size = new System.Drawing.Size(94, 19);
            this.lblZapNezap.TabIndex = 9;
            this.lblZapNezap.Text = "Nezaposlenost";
            // 
            // lblDopPos
            // 
            this.lblDopPos.AutoSize = true;
            this.lblDopPos.Location = new System.Drawing.Point(23, 358);
            this.lblDopPos.Name = "lblDopPos";
            this.lblDopPos.Size = new System.Drawing.Size(135, 19);
            this.lblDopPos.TabIndex = 10;
            this.lblDopPos.Text = "Doprinosi poslodavca";
            // 
            // lblPosPIO
            // 
            this.lblPosPIO.AutoSize = true;
            this.lblPosPIO.Location = new System.Drawing.Point(76, 389);
            this.lblPosPIO.Name = "lblPosPIO";
            this.lblPosPIO.Size = new System.Drawing.Size(31, 19);
            this.lblPosPIO.TabIndex = 11;
            this.lblPosPIO.Text = "PIO";
            // 
            // lblPosZdravstvo
            // 
            this.lblPosZdravstvo.AutoSize = true;
            this.lblPosZdravstvo.Location = new System.Drawing.Point(76, 420);
            this.lblPosZdravstvo.Name = "lblPosZdravstvo";
            this.lblPosZdravstvo.Size = new System.Drawing.Size(66, 19);
            this.lblPosZdravstvo.TabIndex = 12;
            this.lblPosZdravstvo.Text = "Zdravstvo";
            // 
            // lblPosNezap
            // 
            this.lblPosNezap.AutoSize = true;
            this.lblPosNezap.Location = new System.Drawing.Point(76, 451);
            this.lblPosNezap.Name = "lblPosNezap";
            this.lblPosNezap.Size = new System.Drawing.Size(94, 19);
            this.lblPosNezap.TabIndex = 13;
            this.lblPosNezap.Text = "Nezaposlenost";
            // 
            // txtCenaRada
            // 
            // 
            // 
            // 
            this.txtCenaRada.CustomButton.Image = null;
            this.txtCenaRada.CustomButton.Location = new System.Drawing.Point(94, 1);
            this.txtCenaRada.CustomButton.Name = "";
            this.txtCenaRada.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtCenaRada.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCenaRada.CustomButton.TabIndex = 1;
            this.txtCenaRada.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCenaRada.CustomButton.UseSelectable = true;
            this.txtCenaRada.CustomButton.Visible = false;
            this.txtCenaRada.Lines = new string[0];
            this.txtCenaRada.Location = new System.Drawing.Point(291, 77);
            this.txtCenaRada.MaxLength = 32767;
            this.txtCenaRada.Name = "txtCenaRada";
            this.txtCenaRada.PasswordChar = '\0';
            this.txtCenaRada.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCenaRada.SelectedText = "";
            this.txtCenaRada.SelectionLength = 0;
            this.txtCenaRada.SelectionStart = 0;
            this.txtCenaRada.ShortcutsEnabled = true;
            this.txtCenaRada.Size = new System.Drawing.Size(116, 23);
            this.txtCenaRada.TabIndex = 14;
            this.txtCenaRada.UseSelectable = true;
            this.txtCenaRada.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCenaRada.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtCenaRada.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtPrekovremeni
            // 
            // 
            // 
            // 
            this.txtPrekovremeni.CustomButton.Image = null;
            this.txtPrekovremeni.CustomButton.Location = new System.Drawing.Point(94, 1);
            this.txtPrekovremeni.CustomButton.Name = "";
            this.txtPrekovremeni.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPrekovremeni.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPrekovremeni.CustomButton.TabIndex = 1;
            this.txtPrekovremeni.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPrekovremeni.CustomButton.UseSelectable = true;
            this.txtPrekovremeni.CustomButton.Visible = false;
            this.txtPrekovremeni.Lines = new string[0];
            this.txtPrekovremeni.Location = new System.Drawing.Point(291, 108);
            this.txtPrekovremeni.MaxLength = 32767;
            this.txtPrekovremeni.Name = "txtPrekovremeni";
            this.txtPrekovremeni.PasswordChar = '\0';
            this.txtPrekovremeni.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPrekovremeni.SelectedText = "";
            this.txtPrekovremeni.SelectionLength = 0;
            this.txtPrekovremeni.SelectionStart = 0;
            this.txtPrekovremeni.ShortcutsEnabled = true;
            this.txtPrekovremeni.Size = new System.Drawing.Size(116, 23);
            this.txtPrekovremeni.TabIndex = 15;
            this.txtPrekovremeni.UseSelectable = true;
            this.txtPrekovremeni.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPrekovremeni.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtPrekovremeni.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtMinuli
            // 
            // 
            // 
            // 
            this.txtMinuli.CustomButton.Image = null;
            this.txtMinuli.CustomButton.Location = new System.Drawing.Point(94, 1);
            this.txtMinuli.CustomButton.Name = "";
            this.txtMinuli.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtMinuli.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtMinuli.CustomButton.TabIndex = 1;
            this.txtMinuli.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtMinuli.CustomButton.UseSelectable = true;
            this.txtMinuli.CustomButton.Visible = false;
            this.txtMinuli.Lines = new string[0];
            this.txtMinuli.Location = new System.Drawing.Point(291, 139);
            this.txtMinuli.MaxLength = 32767;
            this.txtMinuli.Name = "txtMinuli";
            this.txtMinuli.PasswordChar = '\0';
            this.txtMinuli.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtMinuli.SelectedText = "";
            this.txtMinuli.SelectionLength = 0;
            this.txtMinuli.SelectionStart = 0;
            this.txtMinuli.ShortcutsEnabled = true;
            this.txtMinuli.Size = new System.Drawing.Size(116, 23);
            this.txtMinuli.TabIndex = 16;
            this.txtMinuli.UseSelectable = true;
            this.txtMinuli.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtMinuli.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtMinuli.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtNeoporeziva
            // 
            // 
            // 
            // 
            this.txtNeoporeziva.CustomButton.Image = null;
            this.txtNeoporeziva.CustomButton.Location = new System.Drawing.Point(94, 1);
            this.txtNeoporeziva.CustomButton.Name = "";
            this.txtNeoporeziva.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNeoporeziva.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNeoporeziva.CustomButton.TabIndex = 1;
            this.txtNeoporeziva.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNeoporeziva.CustomButton.UseSelectable = true;
            this.txtNeoporeziva.CustomButton.Visible = false;
            this.txtNeoporeziva.Lines = new string[0];
            this.txtNeoporeziva.Location = new System.Drawing.Point(291, 170);
            this.txtNeoporeziva.MaxLength = 32767;
            this.txtNeoporeziva.Name = "txtNeoporeziva";
            this.txtNeoporeziva.PasswordChar = '\0';
            this.txtNeoporeziva.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNeoporeziva.SelectedText = "";
            this.txtNeoporeziva.SelectionLength = 0;
            this.txtNeoporeziva.SelectionStart = 0;
            this.txtNeoporeziva.ShortcutsEnabled = true;
            this.txtNeoporeziva.Size = new System.Drawing.Size(116, 23);
            this.txtNeoporeziva.TabIndex = 17;
            this.txtNeoporeziva.UseSelectable = true;
            this.txtNeoporeziva.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNeoporeziva.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNeoporeziva.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtPorez
            // 
            // 
            // 
            // 
            this.txtPorez.CustomButton.Image = null;
            this.txtPorez.CustomButton.Location = new System.Drawing.Point(94, 1);
            this.txtPorez.CustomButton.Name = "";
            this.txtPorez.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPorez.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPorez.CustomButton.TabIndex = 1;
            this.txtPorez.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPorez.CustomButton.UseSelectable = true;
            this.txtPorez.CustomButton.Visible = false;
            this.txtPorez.Lines = new string[0];
            this.txtPorez.Location = new System.Drawing.Point(291, 201);
            this.txtPorez.MaxLength = 32767;
            this.txtPorez.Name = "txtPorez";
            this.txtPorez.PasswordChar = '\0';
            this.txtPorez.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPorez.SelectedText = "";
            this.txtPorez.SelectionLength = 0;
            this.txtPorez.SelectionStart = 0;
            this.txtPorez.ShortcutsEnabled = true;
            this.txtPorez.Size = new System.Drawing.Size(116, 23);
            this.txtPorez.TabIndex = 18;
            this.txtPorez.UseSelectable = true;
            this.txtPorez.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPorez.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtPorez.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtZapPIO
            // 
            // 
            // 
            // 
            this.txtZapPIO.CustomButton.Image = null;
            this.txtZapPIO.CustomButton.Location = new System.Drawing.Point(94, 1);
            this.txtZapPIO.CustomButton.Name = "";
            this.txtZapPIO.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtZapPIO.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtZapPIO.CustomButton.TabIndex = 1;
            this.txtZapPIO.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtZapPIO.CustomButton.UseSelectable = true;
            this.txtZapPIO.CustomButton.Visible = false;
            this.txtZapPIO.Lines = new string[0];
            this.txtZapPIO.Location = new System.Drawing.Point(291, 263);
            this.txtZapPIO.MaxLength = 32767;
            this.txtZapPIO.Name = "txtZapPIO";
            this.txtZapPIO.PasswordChar = '\0';
            this.txtZapPIO.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtZapPIO.SelectedText = "";
            this.txtZapPIO.SelectionLength = 0;
            this.txtZapPIO.SelectionStart = 0;
            this.txtZapPIO.ShortcutsEnabled = true;
            this.txtZapPIO.Size = new System.Drawing.Size(116, 23);
            this.txtZapPIO.TabIndex = 19;
            this.txtZapPIO.UseSelectable = true;
            this.txtZapPIO.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtZapPIO.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtZapPIO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtZapZdravstvo
            // 
            // 
            // 
            // 
            this.txtZapZdravstvo.CustomButton.Image = null;
            this.txtZapZdravstvo.CustomButton.Location = new System.Drawing.Point(94, 1);
            this.txtZapZdravstvo.CustomButton.Name = "";
            this.txtZapZdravstvo.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtZapZdravstvo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtZapZdravstvo.CustomButton.TabIndex = 1;
            this.txtZapZdravstvo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtZapZdravstvo.CustomButton.UseSelectable = true;
            this.txtZapZdravstvo.CustomButton.Visible = false;
            this.txtZapZdravstvo.Lines = new string[0];
            this.txtZapZdravstvo.Location = new System.Drawing.Point(291, 294);
            this.txtZapZdravstvo.MaxLength = 32767;
            this.txtZapZdravstvo.Name = "txtZapZdravstvo";
            this.txtZapZdravstvo.PasswordChar = '\0';
            this.txtZapZdravstvo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtZapZdravstvo.SelectedText = "";
            this.txtZapZdravstvo.SelectionLength = 0;
            this.txtZapZdravstvo.SelectionStart = 0;
            this.txtZapZdravstvo.ShortcutsEnabled = true;
            this.txtZapZdravstvo.Size = new System.Drawing.Size(116, 23);
            this.txtZapZdravstvo.TabIndex = 20;
            this.txtZapZdravstvo.UseSelectable = true;
            this.txtZapZdravstvo.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtZapZdravstvo.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtZapZdravstvo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtZapNezap
            // 
            // 
            // 
            // 
            this.txtZapNezap.CustomButton.Image = null;
            this.txtZapNezap.CustomButton.Location = new System.Drawing.Point(94, 1);
            this.txtZapNezap.CustomButton.Name = "";
            this.txtZapNezap.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtZapNezap.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtZapNezap.CustomButton.TabIndex = 1;
            this.txtZapNezap.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtZapNezap.CustomButton.UseSelectable = true;
            this.txtZapNezap.CustomButton.Visible = false;
            this.txtZapNezap.Lines = new string[0];
            this.txtZapNezap.Location = new System.Drawing.Point(291, 325);
            this.txtZapNezap.MaxLength = 32767;
            this.txtZapNezap.Name = "txtZapNezap";
            this.txtZapNezap.PasswordChar = '\0';
            this.txtZapNezap.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtZapNezap.SelectedText = "";
            this.txtZapNezap.SelectionLength = 0;
            this.txtZapNezap.SelectionStart = 0;
            this.txtZapNezap.ShortcutsEnabled = true;
            this.txtZapNezap.Size = new System.Drawing.Size(116, 23);
            this.txtZapNezap.TabIndex = 21;
            this.txtZapNezap.UseSelectable = true;
            this.txtZapNezap.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtZapNezap.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtZapNezap.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtPosNezap
            // 
            // 
            // 
            // 
            this.txtPosNezap.CustomButton.Image = null;
            this.txtPosNezap.CustomButton.Location = new System.Drawing.Point(94, 1);
            this.txtPosNezap.CustomButton.Name = "";
            this.txtPosNezap.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPosNezap.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPosNezap.CustomButton.TabIndex = 1;
            this.txtPosNezap.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPosNezap.CustomButton.UseSelectable = true;
            this.txtPosNezap.CustomButton.Visible = false;
            this.txtPosNezap.Lines = new string[0];
            this.txtPosNezap.Location = new System.Drawing.Point(291, 449);
            this.txtPosNezap.MaxLength = 32767;
            this.txtPosNezap.Name = "txtPosNezap";
            this.txtPosNezap.PasswordChar = '\0';
            this.txtPosNezap.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPosNezap.SelectedText = "";
            this.txtPosNezap.SelectionLength = 0;
            this.txtPosNezap.SelectionStart = 0;
            this.txtPosNezap.ShortcutsEnabled = true;
            this.txtPosNezap.Size = new System.Drawing.Size(116, 23);
            this.txtPosNezap.TabIndex = 24;
            this.txtPosNezap.UseSelectable = true;
            this.txtPosNezap.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPosNezap.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtPosNezap.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtPosZdravstvo
            // 
            // 
            // 
            // 
            this.txtPosZdravstvo.CustomButton.Image = null;
            this.txtPosZdravstvo.CustomButton.Location = new System.Drawing.Point(94, 1);
            this.txtPosZdravstvo.CustomButton.Name = "";
            this.txtPosZdravstvo.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPosZdravstvo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPosZdravstvo.CustomButton.TabIndex = 1;
            this.txtPosZdravstvo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPosZdravstvo.CustomButton.UseSelectable = true;
            this.txtPosZdravstvo.CustomButton.Visible = false;
            this.txtPosZdravstvo.Lines = new string[0];
            this.txtPosZdravstvo.Location = new System.Drawing.Point(291, 418);
            this.txtPosZdravstvo.MaxLength = 32767;
            this.txtPosZdravstvo.Name = "txtPosZdravstvo";
            this.txtPosZdravstvo.PasswordChar = '\0';
            this.txtPosZdravstvo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPosZdravstvo.SelectedText = "";
            this.txtPosZdravstvo.SelectionLength = 0;
            this.txtPosZdravstvo.SelectionStart = 0;
            this.txtPosZdravstvo.ShortcutsEnabled = true;
            this.txtPosZdravstvo.Size = new System.Drawing.Size(116, 23);
            this.txtPosZdravstvo.TabIndex = 23;
            this.txtPosZdravstvo.UseSelectable = true;
            this.txtPosZdravstvo.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPosZdravstvo.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtPosZdravstvo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // txtPosPIO
            // 
            // 
            // 
            // 
            this.txtPosPIO.CustomButton.Image = null;
            this.txtPosPIO.CustomButton.Location = new System.Drawing.Point(94, 1);
            this.txtPosPIO.CustomButton.Name = "";
            this.txtPosPIO.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtPosPIO.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPosPIO.CustomButton.TabIndex = 1;
            this.txtPosPIO.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPosPIO.CustomButton.UseSelectable = true;
            this.txtPosPIO.CustomButton.Visible = false;
            this.txtPosPIO.Lines = new string[0];
            this.txtPosPIO.Location = new System.Drawing.Point(291, 387);
            this.txtPosPIO.MaxLength = 32767;
            this.txtPosPIO.Name = "txtPosPIO";
            this.txtPosPIO.PasswordChar = '\0';
            this.txtPosPIO.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPosPIO.SelectedText = "";
            this.txtPosPIO.SelectionLength = 0;
            this.txtPosPIO.SelectionStart = 0;
            this.txtPosPIO.ShortcutsEnabled = true;
            this.txtPosPIO.Size = new System.Drawing.Size(116, 23);
            this.txtPosPIO.TabIndex = 22;
            this.txtPosPIO.UseSelectable = true;
            this.txtPosPIO.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPosPIO.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtPosPIO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // btnSacuvaj
            // 
            this.btnSacuvaj.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnSacuvaj.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnSacuvaj.Location = new System.Drawing.Point(26, 500);
            this.btnSacuvaj.Name = "btnSacuvaj";
            this.btnSacuvaj.Size = new System.Drawing.Size(144, 23);
            this.btnSacuvaj.TabIndex = 25;
            this.btnSacuvaj.Text = "Sačuvaj promene";
            this.btnSacuvaj.UseSelectable = true;
            this.btnSacuvaj.Click += new System.EventHandler(this.btnSacuvaj_Click);
            // 
            // btnIzadji
            // 
            this.btnIzadji.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnIzadji.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnIzadji.Location = new System.Drawing.Point(291, 500);
            this.btnIzadji.Name = "btnIzadji";
            this.btnIzadji.Size = new System.Drawing.Size(116, 23);
            this.btnIzadji.TabIndex = 26;
            this.btnIzadji.Text = "Izađi";
            this.btnIzadji.UseSelectable = true;
            this.btnIzadji.Click += new System.EventHandler(this.btnIzadji_Click);
            // 
            // NoviKoeficijentiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 541);
            this.Controls.Add(this.btnIzadji);
            this.Controls.Add(this.btnSacuvaj);
            this.Controls.Add(this.txtPosNezap);
            this.Controls.Add(this.txtPosZdravstvo);
            this.Controls.Add(this.txtPosPIO);
            this.Controls.Add(this.txtZapNezap);
            this.Controls.Add(this.txtZapZdravstvo);
            this.Controls.Add(this.txtZapPIO);
            this.Controls.Add(this.txtPorez);
            this.Controls.Add(this.txtNeoporeziva);
            this.Controls.Add(this.txtMinuli);
            this.Controls.Add(this.txtPrekovremeni);
            this.Controls.Add(this.txtCenaRada);
            this.Controls.Add(this.lblPosNezap);
            this.Controls.Add(this.lblPosZdravstvo);
            this.Controls.Add(this.lblPosPIO);
            this.Controls.Add(this.lblDopPos);
            this.Controls.Add(this.lblZapNezap);
            this.Controls.Add(this.lblZapZdravstvo);
            this.Controls.Add(this.lblZapPIO);
            this.Controls.Add(this.lblDopZap);
            this.Controls.Add(this.lblProcenatPorez);
            this.Controls.Add(this.lblNeoporeziva);
            this.Controls.Add(this.lblProcenatMinuli);
            this.Controls.Add(this.lblProcenatExtra);
            this.Controls.Add(this.lblCenaRada);
            this.MaximizeBox = false;
            this.Name = "NoviKoeficijentiForm";
            this.Resizable = false;
            this.Text = "Vrednosti koje utiču na obračun plate";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblCenaRada;
        private MetroFramework.Controls.MetroLabel lblProcenatExtra;
        private MetroFramework.Controls.MetroLabel lblProcenatMinuli;
        private MetroFramework.Controls.MetroLabel lblNeoporeziva;
        private MetroFramework.Controls.MetroLabel lblProcenatPorez;
        private MetroFramework.Controls.MetroLabel lblDopZap;
        private MetroFramework.Controls.MetroLabel lblZapPIO;
        private MetroFramework.Controls.MetroLabel lblZapZdravstvo;
        private MetroFramework.Controls.MetroLabel lblZapNezap;
        private MetroFramework.Controls.MetroLabel lblDopPos;
        private MetroFramework.Controls.MetroLabel lblPosPIO;
        private MetroFramework.Controls.MetroLabel lblPosZdravstvo;
        private MetroFramework.Controls.MetroLabel lblPosNezap;
        private MetroFramework.Controls.MetroTextBox txtCenaRada;
        private MetroFramework.Controls.MetroTextBox txtPrekovremeni;
        private MetroFramework.Controls.MetroTextBox txtMinuli;
        private MetroFramework.Controls.MetroTextBox txtNeoporeziva;
        private MetroFramework.Controls.MetroTextBox txtPorez;
        private MetroFramework.Controls.MetroTextBox txtZapPIO;
        private MetroFramework.Controls.MetroTextBox txtZapZdravstvo;
        private MetroFramework.Controls.MetroTextBox txtZapNezap;
        private MetroFramework.Controls.MetroTextBox txtPosNezap;
        private MetroFramework.Controls.MetroTextBox txtPosZdravstvo;
        private MetroFramework.Controls.MetroTextBox txtPosPIO;
        private MetroFramework.Controls.MetroButton btnSacuvaj;
        private MetroFramework.Controls.MetroButton btnIzadji;
    }
}