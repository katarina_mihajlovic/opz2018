﻿using MetroFramework.Forms;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Persistence.Entities;
using SharedComponents.Presenters;
using static SharedComponents.Forms.Login;
using SharedComponents.Forms;

namespace FinansijskaApp
{
    public partial class FinansijskaMainForm : MetroForm, IUsernameOwner
    {
        public FinansijskaMainForm(string username)
        {
            InitializeComponent();
            Username = username;

            RefreshDGV();
        }

        public string Username { get; set; }

        private void btnTesting_Click(object sender, EventArgs e)
        {
            NoviKoeficijentiForm izbor = new NoviKoeficijentiForm();
            NoviKoeficijentiPresenter presenter = new NoviKoeficijentiPresenter(izbor);
            presenter.PopuniVrednosti();
            izbor.ShowDialog();
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            NoviKoeficijentiForm f = new NoviKoeficijentiForm();
            f.ShowDialog();
        }

        private void metroButton4_Click(object sender, EventArgs e)
        {
            PromenaLozinkeForm f = new PromenaLozinkeForm();
            if (f.ShowDialog(this) == DialogResult.OK) this.DialogResult = DialogResult.OK;
        }

        private void btnAzuzirajKoef_Click(object sender, EventArgs e)
        {
            NoviKoeficijentiForm izbor = new NoviKoeficijentiForm();
            NoviKoeficijentiPresenter presenter = new NoviKoeficijentiPresenter(izbor);
            presenter.PopuniVrednosti();
            izbor.ShowDialog();
        }

        private void btnPoMesecu_Click(object sender, EventArgs e)
        {
            ObracunZaradaPoMesecu ozpm = new ObracunZaradaPoMesecu();
            ozpm.ShowDialog();
        }

        private void btnPoZaposlenom_Click(object sender, EventArgs e)
        {
            if (dgvZaposleni.SelectedRows.Count == 0)
            {
                return;
            }
            Int32? i = dgvZaposleni.SelectedRows[0].Cells[0].Value as Int32?;
            if (i == null)
            {
                return;
            }
            ObracunZaradaPoZaposlenom ozpz = new ObracunZaradaPoZaposlenom(i.Value);
            ozpz.ShowDialog();
        }

        private void RefreshDGV()
        {
            using (OpzContext kontekst = new OpzContext())
            {
                dgvZaposleni.DataSource = (from zap in kontekst.ListaZaposlenih
                                           orderby zap.Id
                                           select new
                                           {
                                               BrojZaposlenog = zap.Id,
                                               Ime = zap.Ime,
                                               SrednjeSlovo = zap.SSlovo,
                                               Prezime = zap.Prezime,
                                               RadnoMesto = zap.RadnoMesto

                                           }).ToList();
            }
            dgvZaposleni.Columns[0].HeaderText = "Broj zaposlenog";
            dgvZaposleni.Columns[0].HeaderText = "Broj zaposlenog";
            dgvZaposleni.Columns[2].HeaderText = "Srednje slovo";
            dgvZaposleni.Columns[2].HeaderText = "Srednje slovo";
            dgvZaposleni.Columns[4].HeaderText = "Radno mesto";
            dgvZaposleni.Columns[4].HeaderText = "Radno mesto";

            dgvZaposleni.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvZaposleni.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvZaposleni.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            dgvZaposleni.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            /*foreach (DataGridViewColumn dgvcol in dgvZaposleni.Columns)
            {
                if (dgvcol.Index == 0) continue;
                dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }*/
        }

        private void btnTesting_Click_1(object sender, EventArgs e)
        {
            var a = new ProsekPlateChartForm();
            a.ShowDialog();
        }

        private void metroButton2_Click_1(object sender, EventArgs e)
        {
            IzvestajBanka ib = new IzvestajBanka();
            ib.ShowDialog();
        }
    }
}
