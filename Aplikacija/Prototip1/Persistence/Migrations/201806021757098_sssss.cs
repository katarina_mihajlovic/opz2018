namespace Persistence.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class sssss : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "S15755.Obracuns",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        MesecGodina = c.DateTime(nullable: false),
                        CenaRada = c.Double(nullable: false),
                        KoeficijentRadnogMesta = c.Double(nullable: false),
                        RadniCasovi = c.Double(nullable: false),
                        SankcijaStimulacija = c.Double(nullable: false),
                        PrekovremeniRad = c.Double(nullable: false),
                        MinuliRad = c.Double(nullable: false),
                        Bruto = c.Double(nullable: false),
                        Porez = c.Double(nullable: false),
                        DopZapPIO = c.Double(nullable: false),
                        DopZapZdr = c.Double(nullable: false),
                        DopZapNez = c.Double(nullable: false),
                        DopPosPIO = c.Double(nullable: false),
                        DopPosZdr = c.Double(nullable: false),
                        DopPosNez = c.Double(nullable: false),
                        Neto = c.Double(nullable: false),
                        Zaposleni_Id = c.Decimal(precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("S15755.Zaposleni", t => t.Zaposleni_Id)
                .Index(t => t.Zaposleni_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("S15755.Obracuns", "Zaposleni_Id", "S15755.Zaposleni");
            DropIndex("S15755.Obracuns", new[] { "Zaposleni_Id" });
            DropTable("S15755.Obracuns");
        }
    }
}
