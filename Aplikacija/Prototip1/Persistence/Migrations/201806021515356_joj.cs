namespace Persistence.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class joj : DbMigration
    {
        public override void Up()
        {
            AddColumn("S15755.Zaposleni", "PocetniMeseciStaza", c => c.Double(defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("S15755.Zaposleni", "PocetniMeseciStaza");
        }
    }
}
