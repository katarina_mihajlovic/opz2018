namespace Persistence.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class DodatNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("S15755.Prisustvo", "VremeOdlaska", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("S15755.Prisustvo", "VremeOdlaska", c => c.DateTime(nullable: false));
        }
    }
}
