namespace Persistence.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Inicijalna : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "S15755.Zaposleni",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        KorisnickoIme = c.String(maxLength: 15, unicode: false),
                        LozinkaHash = c.String(maxLength: 200, unicode: false),
                        Salt = c.String(maxLength: 200, unicode: false),
                        Ime = c.String(maxLength: 100, unicode: false),
                        SSlovo = c.String(maxLength: 2, unicode: false),
                        Prezime = c.String(maxLength: 100, unicode: false),
                        DatumRodjenja = c.DateTime(nullable: false),
                        Adresa = c.String(maxLength: 100, unicode: false),
                        MestoRodjenja = c.String(maxLength: 100, unicode: false),
                        BrojTelefona = c.String(maxLength: 100, unicode: false),
                        RadnoMesto = c.String(maxLength: 100, unicode: false),
                        TekuciRacun = c.String(maxLength: 100, unicode: false),
                        DatumPocetkaRada = c.DateTime(nullable: false),
                        DatumKrajaRada = c.DateTime(nullable: false),
                        NormaRada = c.Decimal(nullable: false, precision: 10, scale: 0),
                        Nadredjeni_Id = c.Decimal(precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("S15755.Zaposleni", t => t.Nadredjeni_Id)
                .Index(t => t.Nadredjeni_Id);
            
            CreateTable(
                "S15755.Prisustvo",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        VremeDolaska = c.DateTime(nullable: false),
                        VremeOdlaska = c.DateTime(nullable: false),
                        Zaposleni_Id = c.Decimal(precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("S15755.Zaposleni", t => t.Zaposleni_Id)
                .Index(t => t.Zaposleni_Id);
            
            CreateTable(
                "S15755.SlikaZaposlenog",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Image = c.Binary(),
                        Datum = c.DateTime(nullable: false),
                        Zaposleni_Id = c.Decimal(precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("S15755.Zaposleni", t => t.Zaposleni_Id)
                .Index(t => t.Zaposleni_Id);
            
            CreateTable(
                "S15755.RadnoVreme",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Datum = c.DateTime(nullable: false),
                        Smena = c.String(maxLength: 10, unicode: false),
                        Zaposleni_Id = c.Decimal(precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("S15755.Zaposleni", t => t.Zaposleni_Id)
                .Index(t => t.Zaposleni_Id);
            
            CreateTable(
                "S15755.SankcijaStimulacija",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        DatumIzricanja = c.DateTime(nullable: false),
                        Vrednost = c.Single(nullable: false),
                        Opis = c.String(maxLength: 300, unicode: false),
                        Zaposleni_Id = c.Decimal(precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("S15755.Zaposleni", t => t.Zaposleni_Id)
                .Index(t => t.Zaposleni_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("S15755.SankcijaStimulacija", "Zaposleni_Id", "S15755.Zaposleni");
            DropForeignKey("S15755.RadnoVreme", "Zaposleni_Id", "S15755.Zaposleni");
            DropForeignKey("S15755.SlikaZaposlenog", "Zaposleni_Id", "S15755.Zaposleni");
            DropForeignKey("S15755.Prisustvo", "Zaposleni_Id", "S15755.Zaposleni");
            DropForeignKey("S15755.Zaposleni", "Nadredjeni_Id", "S15755.Zaposleni");
            DropIndex("S15755.SankcijaStimulacija", new[] { "Zaposleni_Id" });
            DropIndex("S15755.RadnoVreme", new[] { "Zaposleni_Id" });
            DropIndex("S15755.SlikaZaposlenog", new[] { "Zaposleni_Id" });
            DropIndex("S15755.Prisustvo", new[] { "Zaposleni_Id" });
            DropIndex("S15755.Zaposleni", new[] { "Nadredjeni_Id" });
            DropTable("S15755.SankcijaStimulacija");
            DropTable("S15755.RadnoVreme");
            DropTable("S15755.SlikaZaposlenog");
            DropTable("S15755.Prisustvo");
            DropTable("S15755.Zaposleni");
        }
    }
}
