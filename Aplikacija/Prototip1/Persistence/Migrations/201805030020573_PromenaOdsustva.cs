namespace Persistence.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PromenaOdsustva : DbMigration
    {
        public override void Up()
        {
            AlterColumn("S15755.Odsustvo", "BrojUDelovodniku", c => c.String(maxLength: 50, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("S15755.Odsustvo", "BrojUDelovodniku", c => c.Decimal(nullable: false, precision: 10, scale: 0));
        }
    }
}
