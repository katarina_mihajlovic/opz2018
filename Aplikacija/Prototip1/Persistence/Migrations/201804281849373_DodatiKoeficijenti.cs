namespace Persistence.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class DodatiKoeficijenti : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "S15755.Koeficijenti",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Vreme = c.DateTime(nullable: false),
                        CenaRada = c.Double(nullable: false),
                        ProcenatZaExtra = c.Double(nullable: false),
                        ProcenatZaMinuli = c.Double(nullable: false),
                        NeoporezivaOsnovica = c.Double(nullable: false),
                        ProcenatZaPorez = c.Double(nullable: false),
                        DopZaposlenogPIO = c.Double(nullable: false),
                        DopZaposlenogZdravstvo = c.Double(nullable: false),
                        DopZaposlenogNezaposlenost = c.Double(nullable: false),
                        DopPoslodavcaPIO = c.Double(nullable: false),
                        DopPoslodavcaZdravstvo = c.Double(nullable: false),
                        DopPoslodavcaNezaposlenost = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("S15755.Koeficijenti");
        }
    }
}
