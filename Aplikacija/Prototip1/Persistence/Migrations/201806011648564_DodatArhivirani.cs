namespace Persistence.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class DodatArhivirani : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "S15755.Arhivirani",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Ime = c.String(maxLength: 15, unicode: false),
                        SSlovo = c.String(maxLength: 2, unicode: false),
                        Prezime = c.String(maxLength: 40, unicode: false),
                        DatumRodjenja = c.DateTime(nullable: false),
                        Adresa = c.String(maxLength: 60, unicode: false),
                        MestoRodjenja = c.String(maxLength: 40, unicode: false),
                        RadnoMesto = c.String(maxLength: 40, unicode: false),
                        DatumPocetkaRada = c.DateTime(nullable: false),
                        DatumKrajaRada = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("S15755.Arhivirani");
        }
    }
}
