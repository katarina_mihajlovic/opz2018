namespace Persistence.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class oooo : DbMigration
    {
        public override void Up()
        {
            AddColumn("S15755.Zaposleni", "KoeficijentRadnogMesta", c => c.Double());
        }
        
        public override void Down()
        {
            DropColumn("S15755.Zaposleni", "KoeficijentRadnogMesta");
        }
    }
}
