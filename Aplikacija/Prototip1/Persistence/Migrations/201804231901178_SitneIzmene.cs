namespace Persistence.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class SitneIzmene : DbMigration
    {
        public override void Up()
        {
            AddColumn("S15755.Zaposleni", "UlogaKorisnickogNaloga", c => c.String(maxLength: 20, unicode: false));
            AlterColumn("S15755.Zaposleni", "DatumKrajaRada", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("S15755.Zaposleni", "DatumKrajaRada", c => c.DateTime(nullable: false));
            DropColumn("S15755.Zaposleni", "UlogaKorisnickogNaloga");
        }
    }
}
