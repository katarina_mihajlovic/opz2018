namespace Persistence.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class DodataSmenaIOdsustvo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "S15755.Odsustvo",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        Pocetak = c.DateTime(nullable: false),
                        Kraj = c.DateTime(nullable: false),
                        BrojUDelovodniku = c.Decimal(nullable: false, precision: 10, scale: 0),
                        Tip = c.String(maxLength: 300, unicode: false),
                        Zaposleni_Id = c.Decimal(precision: 10, scale: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("S15755.Zaposleni", t => t.Zaposleni_Id)
                .Index(t => t.Zaposleni_Id);
            
            CreateTable(
                "S15755.Smena",
                c => new
                    {
                        Id = c.Decimal(nullable: false, precision: 10, scale: 0, identity: true),
                        DatumDonosenja = c.DateTime(nullable: false),
                        TipSmene = c.String(maxLength: 300, unicode: false),
                        Kraj = c.DateTime(nullable: false),
                        Pocetak = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("S15755.Odsustvo", "Zaposleni_Id", "S15755.Zaposleni");
            DropIndex("S15755.Odsustvo", new[] { "Zaposleni_Id" });
            DropTable("S15755.Smena");
            DropTable("S15755.Odsustvo");
        }
    }
}
