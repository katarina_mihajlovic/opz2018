namespace Persistence.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PromenaSmene : DbMigration
    {
        public override void Up()
        {
            AddColumn("S15755.Smena", "DanUNedelji", c => c.String(maxLength: 20, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("S15755.Smena", "DanUNedelji");
        }
    }
}
