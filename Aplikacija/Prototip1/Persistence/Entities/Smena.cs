﻿using System;

namespace Persistence.Entities
{
    public class Smena
    {
        private int id;
        private DateTime datumDonosenja;
        private string tipSmene;
        private DateTime pocetak;
        private DateTime kraj;
        private string danUNedelji;

        public int Id { get => id; set => id = value; }
        public DateTime DatumDonosenja { get => datumDonosenja; set => datumDonosenja = value; }
        public string TipSmene { get => tipSmene; set => tipSmene = value; }
        public DateTime Kraj { get => kraj; set => kraj = value; }
        public DateTime Pocetak { get => pocetak; set => pocetak = value; }
        public string DanUNedelji { get => danUNedelji; set => danUNedelji = value; }
    }
}
