﻿using System;

namespace Persistence.Entities
{
    public class Odsustvo
    {
        private int id;
        private Zaposleni zaposleni;
        private DateTime pocetak;
        private DateTime kraj;
        private string brojUDelovodniku;
        private string tip;//odredjuje kako utice na platu

        public int Id { get => id; set => id = value; }
        public Zaposleni Zaposleni { get => zaposleni; set => zaposleni = value; }
        public DateTime Pocetak { get => pocetak; set => pocetak = value; }
        public DateTime Kraj { get => kraj; set => kraj = value; }
        public string BrojUDelovodniku { get => brojUDelovodniku; set => brojUDelovodniku = value; }
        public string Tip { get => tip; set => tip = value; }
    }
}
