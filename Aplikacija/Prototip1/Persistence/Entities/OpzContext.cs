﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Persistence.Entities
{
    public class OpzContext : DbContext
    {
        virtual public DbSet<Zaposleni> ListaZaposlenih { get; set; }
        virtual public DbSet<SankcijaStimulacija> ListaSankcijaStimulacija { get; set; }
        virtual public DbSet<RadnoVreme> ListaRadnihVremena { get; set; }
        virtual public DbSet<Prisustvo> ListaPrisustva { get; set; }
        virtual public DbSet<Smena> ListaSmena { get; set; }
        virtual public DbSet<Koeficijenti> ListaKoeficijenata { get; set; }
        virtual public DbSet<Odsustvo> ListaOdsustva { get; set; }
        virtual public DbSet<Arhivirani> ListaArhiviranih { get; set; }
        virtual public DbSet<SlikaZaposlenog> ListaSlika { get; set; }
        virtual public DbSet<Obracun> ListaObracuna { get; set; }

        public OpzContext() : base()
        {

        }

        public List<Zaposleni> PretraziZaposlene(string filt)
        {
            String filter = filt.ToUpper();
            List<Zaposleni> ret = (from zap in ListaZaposlenih
                                   where zap.DatumKrajaRada == null && (zap.Id.ToString().ToUpper().Contains(filter) || zap.Ime.ToUpper().Contains(filter) || zap.Prezime.ToUpper().Contains(filter))
                                   select zap).ToList();

            return ret;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("S15755");
            //fluent API
            modelBuilder.Entity<Zaposleni>().Property(z => z.Ime).HasColumnType("VARCHAR2").HasMaxLength(100);
            modelBuilder.Entity<Zaposleni>().Property(z => z.Adresa).HasColumnType("VARCHAR2").HasMaxLength(100);
            modelBuilder.Entity<Zaposleni>().Property(z => z.MestoRodjenja).HasColumnType("VARCHAR2").HasMaxLength(100);
            modelBuilder.Entity<Zaposleni>().Property(z => z.Prezime).HasColumnType("VARCHAR2").HasMaxLength(100);
            modelBuilder.Entity<Zaposleni>().Property(z => z.BrojTelefona).HasColumnType("VARCHAR2").HasMaxLength(100);
            modelBuilder.Entity<Zaposleni>().Property(z => z.RadnoMesto).HasColumnType("VARCHAR2").HasMaxLength(100);
            modelBuilder.Entity<Zaposleni>().Property(z => z.TekuciRacun).HasColumnType("VARCHAR2").HasMaxLength(100);
            modelBuilder.Entity<Zaposleni>().Property(z => z.SSlovo).HasColumnType("VARCHAR2").HasMaxLength(2);
            modelBuilder.Entity<Zaposleni>().Property(z => z.KorisnickoIme).HasColumnType("VARCHAR2").HasMaxLength(15);
            modelBuilder.Entity<Zaposleni>().Property(z => z.Salt).HasColumnType("VARCHAR2").HasMaxLength(200);
            modelBuilder.Entity<Zaposleni>().Property(z => z.LozinkaHash).HasColumnType("VARCHAR2").HasMaxLength(200);
            modelBuilder.Entity<Zaposleni>().Property(z => z.UlogaKorisnickogNaloga).HasColumnType("VARCHAR2").HasMaxLength(20);
            modelBuilder.Entity<Zaposleni>().Property(z => z.DatumKrajaRada).IsOptional();
            modelBuilder.Entity<RadnoVreme>().Property(z => z.Smena).HasColumnType("VARCHAR2").HasMaxLength(10);
            modelBuilder.Entity<SankcijaStimulacija>().Property(z => z.Opis).HasColumnType("VARCHAR2").HasMaxLength(300);
            modelBuilder.Entity<Smena>().Property(z => z.TipSmene).HasColumnType("VARCHAR2").HasMaxLength(300);
            modelBuilder.Entity<Smena>().Property(z => z.DanUNedelji).HasColumnType("VARCHAR2").HasMaxLength(20);
            modelBuilder.Entity<Odsustvo>().Property(z => z.Tip).HasColumnType("VARCHAR2").HasMaxLength(300);
            modelBuilder.Entity<Odsustvo>().Property(z => z.BrojUDelovodniku).HasColumnType("VARCHAR2").HasMaxLength(50);
            modelBuilder.Entity<Arhivirani>().Property(z => z.Ime).HasColumnType("VARCHAR2").HasMaxLength(15);
            modelBuilder.Entity<Arhivirani>().Property(z => z.Prezime).HasColumnType("VARCHAR2").HasMaxLength(40);
            modelBuilder.Entity<Arhivirani>().Property(z => z.MestoRodjenja).HasColumnType("VARCHAR2").HasMaxLength(40);
            modelBuilder.Entity<Arhivirani>().Property(z => z.RadnoMesto).HasColumnType("VARCHAR2").HasMaxLength(40);
            modelBuilder.Entity<Arhivirani>().Property(z => z.SSlovo).HasColumnType("VARCHAR2").HasMaxLength(2);
            modelBuilder.Entity<Arhivirani>().Property(z => z.Adresa).HasColumnType("VARCHAR2").HasMaxLength(60);
            modelBuilder.Entity<Zaposleni>().ToTable("Zaposleni");
            modelBuilder.Entity<Prisustvo>().ToTable("Prisustvo");
            modelBuilder.Entity<RadnoVreme>().ToTable("RadnoVreme");
            modelBuilder.Entity<SankcijaStimulacija>().ToTable("SankcijaStimulacija");
            modelBuilder.Entity<SlikaZaposlenog>().ToTable("SlikaZaposlenog");
            modelBuilder.Entity<Smena>().ToTable("Smena");
            modelBuilder.Entity<Odsustvo>().ToTable("Odsustvo");
            modelBuilder.Entity<Koeficijenti>().ToTable("Koeficijenti");
            modelBuilder.Entity<Arhivirani>().ToTable("Arhivirani");

        }
    }
}
