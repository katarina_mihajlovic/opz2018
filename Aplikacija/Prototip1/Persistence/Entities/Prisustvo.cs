﻿using System;

namespace Persistence.Entities
{
    public class Prisustvo
    {
        private int id;
        private Zaposleni zaposleni;
        private DateTime vremeDolaska;
        private DateTime? vremeOdlaska;

        public int Id { get => id; set => id = value; }
        public virtual Zaposleni Zaposleni { get => zaposleni; set => zaposleni = value; }
        public DateTime VremeDolaska { get => vremeDolaska; set => vremeDolaska = value; }
        public DateTime? VremeOdlaska { get => vremeOdlaska; set => vremeOdlaska = value; }
    }
}
