﻿using System;

namespace Persistence.Entities
{
    public class SlikaZaposlenog
    {
        int id;
        Zaposleni zaposleni;
        private DateTime datum;//datum kad je unesena slika
        private byte[] image;

        public int Id { get => id; set => id = value; }
        public Zaposleni Zaposleni { get => zaposleni; set => zaposleni = value; }
        public byte[] Image { get => image; set => image = value; }
        public DateTime Datum { get => datum; set => datum = value; }
    }
}
