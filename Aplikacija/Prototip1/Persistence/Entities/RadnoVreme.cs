﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Persistence.Entities
{
    public class RadnoVreme
    {
        private int id;
        private Zaposleni zaposleni;
        private DateTime datum;//datum donosenja odluke o radnom vremenu
        [MaxLength(10)]
        private string smena;

        public int Id { get => id; set => id = value; }
        public virtual Zaposleni Zaposleni { get => zaposleni; set => zaposleni = value; }
        public DateTime Datum { get => datum; set => datum = value; }
        public string Smena { get => smena; set => smena = value; }
    }
}
