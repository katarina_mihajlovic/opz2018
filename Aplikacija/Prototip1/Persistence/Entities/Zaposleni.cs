﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Persistence.Entities
{
    public class Zaposleni
    { 
        private int id;
        [MaxLength(15)]
        private string ime;
        [MaxLength(2)]
        private string sSlovo;
        [MaxLength(40)]
        private string prezime;
        private double? pocetniMeseciStaza;
        private DateTime datumRodjenja;
        [MaxLength(60)]
        private string adresa;
        [MaxLength(40)]
        private string mestoRodjenja;
        [MaxLength(20)]
        private string brojTelefona;
        [MaxLength(40)]
        private string radnoMesto;
        [MaxLength(30)]
        private string tekuciRacun;
        DateTime datumPocetkaRada;
        DateTime? datumKrajaRada;
        int normaRada;
        double? koeficijentRadnogMesta;
        private List<Prisustvo> prisustva;
        //slika u posebnu tabelu da bi se postigao lazy loading, pamti se kao byte[]
        //posledica ovoga je da radnik moze da ima vise slika
        //ako to treba da se zabrani onda gubim vreme sa nametanjem ogranicenja one-to-one za razl. tabele
        private List<SlikaZaposlenog> slike;
        private List<SankcijaStimulacija> sankcijeStimulacije;
        private List<RadnoVreme> radnaVremena;
        private List<Odsustvo> odsustva;
        private List<Obracun> obracuni;
        private Zaposleni nadredjeni;

        /*Korisnicki nalog*/
        /*Sve ovo moze da bude null ako se radi o zaposlenom bez naloga*/
        [MaxLength(15)]
        private string korisnickoIme;
        [MaxLength(200)]
        private string lozinkaHash;
        [MaxLength(200)]
        private string salt;
        [MaxLength(20)]
        private string ulogaKorisnickogNaloga;
        /*Vrsi se uporedjivanje lozinkaHash sa HASHFUNCTION(concatenate(unesenaLozinka,salt))*/
        /*Salt se generise pri kreiranju korisnickog naloga, prakticno svaki username ima svoj salt*/

        public string KorisnickoIme { get => korisnickoIme; set => korisnickoIme = value; }
        public string LozinkaHash { get => lozinkaHash; set => lozinkaHash = value; }
        public string Salt { get => salt; set => salt = value; }
        public string UlogaKorisnickogNaloga { get => ulogaKorisnickogNaloga; set => ulogaKorisnickogNaloga = value; }
        /*Korisnicki nalog*/


        public int Id { get => id; set => id = value; }
        public string Ime { get => ime; set => ime = value; }
        public string SSlovo { get => sSlovo; set => sSlovo = value; }
        public string Prezime { get => prezime; set => prezime = value; }
        public double? PocetniMeseciStaza { get => pocetniMeseciStaza; set => pocetniMeseciStaza = value; }
        public DateTime DatumRodjenja { get => datumRodjenja; set => datumRodjenja = value; }
        public string Adresa { get => adresa; set => adresa = value; }
        public string MestoRodjenja { get => mestoRodjenja; set => mestoRodjenja = value; }
        public string BrojTelefona { get => brojTelefona; set => brojTelefona = value; }
        public string RadnoMesto { get => radnoMesto; set => radnoMesto = value; }
        public string TekuciRacun { get => tekuciRacun; set => tekuciRacun = value; }
        public DateTime DatumPocetkaRada { get => datumPocetkaRada; set => datumPocetkaRada = value; }
        public DateTime? DatumKrajaRada { get => datumKrajaRada; set => datumKrajaRada = value; }
        public int NormaRada { get => normaRada; set => normaRada = value; }
        public double? KoeficijentRadnogMesta { get => koeficijentRadnogMesta; set => koeficijentRadnogMesta = value; }
        public virtual List<Prisustvo> Prisustva { get => prisustva; set => prisustva = value; }
        public virtual Zaposleni Nadredjeni { get => nadredjeni; set => nadredjeni = value; }
        public virtual List<SlikaZaposlenog> Slike { get => slike; set => slike = value; }
        public virtual List<SankcijaStimulacija> SankcijeStimulacije { get => sankcijeStimulacije; set => sankcijeStimulacije = value; }
        public virtual List<RadnoVreme> RadnaVremena { get => radnaVremena; set => radnaVremena = value; }
        public virtual List<Odsustvo> Odsustva { get => odsustva; set => odsustva = value; }
        public virtual List<Obracun> Obracuni { get => obracuni; set => obracuni = value; }
    }
}
