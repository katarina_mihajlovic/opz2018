﻿using System;

namespace Persistence.Entities
{
    public class Obracun
    {
        private int id;
        private Zaposleni zaposleni;
        private DateTime mesecGodina;

        private double cenaRada;
        private double koeficijentRadnogMesta;
        private double radniCasovi;
        private double sankcijaStimulacija;
        private double prekovremeniRad;
        private double minuliRad;
        private double bruto;
        private double porez;
        private double dopZapPIO;
        private double dopZapZdr;
        private double dopZapNez;
        private double dopPosPIO;
        private double dopPosZdr;
        private double dopPosNez;
        private double neto;

        public int Id { get => id; set => id = value; }
        public Zaposleni Zaposleni { get => zaposleni; set => zaposleni = value; }
        public DateTime MesecGodina { get => mesecGodina; set => mesecGodina = value; }

        public double CenaRada { get => cenaRada; set => cenaRada = value; }
        public double KoeficijentRadnogMesta { get => koeficijentRadnogMesta; set => koeficijentRadnogMesta = value; }
        public double RadniCasovi { get => radniCasovi; set => radniCasovi = value; }
        public double SankcijaStimulacija { get => sankcijaStimulacija; set => sankcijaStimulacija = value; }
        public double PrekovremeniRad { get => prekovremeniRad; set => prekovremeniRad = value; }
        public double MinuliRad { get => minuliRad; set => minuliRad = value; }
        public double Bruto { get => bruto; set => bruto = value; }
        public double Porez { get => porez; set => porez = value; }
        public double DopZapPIO { get => dopZapPIO; set => dopZapPIO = value; }
        public double DopZapZdr { get => dopZapZdr; set => dopZapZdr = value; }
        public double DopZapNez { get => dopZapNez; set => dopZapNez = value; }
        public double DopPosPIO { get => dopPosPIO; set => dopPosPIO = value; }
        public double DopPosZdr { get => dopPosZdr; set => dopPosZdr = value; }
        public double DopPosNez { get => dopPosNez; set => dopPosNez = value; }
        public double Neto { get => neto; set => neto = value; }
    }
}
