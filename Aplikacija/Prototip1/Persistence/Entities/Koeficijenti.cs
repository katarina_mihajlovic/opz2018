﻿using System;

namespace Persistence.Entities
{
    public class Koeficijenti
    {
        private int id;
        private DateTime vreme;
        private double cenaRada;
        private double procenatZaExtra;
        private double procenatZaMinuli;
        private double neoporezivaOsnovica;
        private double procenatZaPorez;

        private double dopZaposlenogPIO;
        private double dopZaposlenogZdravstvo;
        private double dopZaposlenogNezaposlenost;

        private double dopPoslodavcaPIO;
        private double dopPoslodavcaZdravstvo;
        private double dopPoslodavcaNezaposlenost;

        public int Id { get => id; set => id = value; }
        public DateTime Vreme { get => vreme; set => vreme = value; }
        public double CenaRada { get => cenaRada; set => cenaRada = value; }
        public double ProcenatZaExtra { get => procenatZaExtra; set => procenatZaExtra = value; }
        public double ProcenatZaMinuli { get => procenatZaMinuli; set => procenatZaMinuli = value; }
        public double NeoporezivaOsnovica { get => neoporezivaOsnovica; set => neoporezivaOsnovica = value; }
        public double ProcenatZaPorez { get => procenatZaPorez; set => procenatZaPorez = value; }
        public double DopZaposlenogPIO { get => dopZaposlenogPIO; set => dopZaposlenogPIO = value; }
        public double DopZaposlenogZdravstvo { get => dopZaposlenogZdravstvo; set => dopZaposlenogZdravstvo = value; }
        public double DopZaposlenogNezaposlenost { get => dopZaposlenogNezaposlenost; set => dopZaposlenogNezaposlenost = value; }
        public double DopPoslodavcaPIO { get => dopPoslodavcaPIO; set => dopPoslodavcaPIO = value; }
        public double DopPoslodavcaZdravstvo { get => dopPoslodavcaZdravstvo; set => dopPoslodavcaZdravstvo = value; }
        public double DopPoslodavcaNezaposlenost { get => dopPoslodavcaNezaposlenost; set => dopPoslodavcaNezaposlenost = value; }
    }
}
