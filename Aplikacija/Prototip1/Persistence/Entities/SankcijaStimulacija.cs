﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Persistence.Entities
{
    public class SankcijaStimulacija
    {
        private int id;
        private Zaposleni zaposleni;
        private DateTime datumIzricanja;
        private float vrednost;
        [MaxLength(500)]
        private string opis;

        public virtual Zaposleni Zaposleni { get => zaposleni; set => zaposleni = value; }
        public DateTime DatumIzricanja { get => datumIzricanja; set => datumIzricanja = value; }
        public float Vrednost { get => vrednost; set => vrednost = value; }
        public string Opis { get => opis; set => opis = value; }
        public int Id { get => id; set => id = value; }
    }
}
