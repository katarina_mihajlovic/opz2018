﻿using System;

namespace Persistence.Entities
{
    public class Arhivirani
    {
        private int id;
        //[MaxLength(15)]
        private string ime;
      //  [MaxLength(2)]
        private string sSlovo;
       // [MaxLength(40)]
        private string prezime;
        private DateTime datumRodjenja;
        //[MaxLength(60)]
        private string adresa;
      //  [MaxLength(40)]
        private string mestoRodjenja;
      //  [MaxLength(40)]
        private string radnoMesto;
        DateTime datumPocetkaRada;
        DateTime datumKrajaRada;

        public int Id { get => id; set => id = value; }
        public string Ime { get => ime; set => ime = value; }
        public string SSlovo { get => sSlovo; set => sSlovo = value; }
        public string Prezime { get => prezime; set => prezime = value; }
        public DateTime DatumRodjenja { get => datumRodjenja; set => datumRodjenja = value; }
        public string Adresa { get => adresa; set => adresa = value; }
        public string MestoRodjenja { get => mestoRodjenja; set => mestoRodjenja = value; }
        public string RadnoMesto { get => radnoMesto; set => radnoMesto = value; }
        public DateTime DatumPocetkaRada { get => datumPocetkaRada; set => datumPocetkaRada = value; }
        public DateTime DatumKrajaRada { get => datumKrajaRada; set => datumKrajaRada = value; }
    }
}
