﻿namespace SharedComponents.Forms
{
    partial class AzuriranjeZaposlenihForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIme = new MetroFramework.Controls.MetroLabel();
            this.lblSrednjeSlovo = new MetroFramework.Controls.MetroLabel();
            this.lblPrezime = new MetroFramework.Controls.MetroLabel();
            this.lblDatumRodjenja = new MetroFramework.Controls.MetroLabel();
            this.lblAdresa = new MetroFramework.Controls.MetroLabel();
            this.lblMestoRodjenja = new MetroFramework.Controls.MetroLabel();
            this.lblBrojTelefona = new MetroFramework.Controls.MetroLabel();
            this.lblRadnoMesto = new MetroFramework.Controls.MetroLabel();
            this.lblTekuciRacun = new MetroFramework.Controls.MetroLabel();
            this.lblNormaRada = new MetroFramework.Controls.MetroLabel();
            this.txtIme = new MetroFramework.Controls.MetroTextBox();
            this.txtSrednjeSlovo = new MetroFramework.Controls.MetroTextBox();
            this.txtPrezime = new MetroFramework.Controls.MetroTextBox();
            this.txtAdresa = new MetroFramework.Controls.MetroTextBox();
            this.txtMestoRodjenja = new MetroFramework.Controls.MetroTextBox();
            this.txtBrojTelefona = new MetroFramework.Controls.MetroTextBox();
            this.txtNazivRadnogMesta = new MetroFramework.Controls.MetroTextBox();
            this.txtBrojTekucegRacuna = new MetroFramework.Controls.MetroTextBox();
            this.txtNormaRada = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.lblErrKoef = new MetroFramework.Controls.MetroLabel();
            this.txtKoeficijent = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.lblErrStaz = new MetroFramework.Controls.MetroLabel();
            this.txtStaz = new MetroFramework.Controls.MetroTextBox();
            this.dtpDatumRodjenja = new MetroFramework.Controls.MetroDateTime();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.lblErrNorma = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.lblErrRacun = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.lblErrRadnoMesto = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.LblErrBrojTelefona = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.lblErrMestoRodjenja = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.lblErrPrezime = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.lblErrIme = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.lblErrSSlovo = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.lblErrAdresa = new MetroFramework.Controls.MetroLabel();
            this.lblStaz = new MetroFramework.Controls.MetroLabel();
            this.lblKoefRadMesta = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnIzaberiSliku = new MetroFramework.Controls.MetroButton();
            this.btnObrisiNadredjenog = new MetroFramework.Controls.MetroButton();
            this.btnIzaberiNadredjenog = new MetroFramework.Controls.MetroButton();
            this.btnSacuvaj = new MetroFramework.Controls.MetroButton();
            this.btnIzaberiRadnoVreme = new MetroFramework.Controls.MetroButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.lblSmena = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.txtKorisnickoIme = new MetroFramework.Controls.MetroTextBox();
            this.cmbUloga = new MetroFramework.Controls.MetroComboBox();
            this.lblKorisnikSaNalogom = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.radioNe = new MetroFramework.Controls.MetroRadioButton();
            this.radioDa = new MetroFramework.Controls.MetroRadioButton();
            this.lblKorisnickoIme = new MetroFramework.Controls.MetroLabel();
            this.lblUlogaKorNaloga = new MetroFramework.Controls.MetroLabel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblIme
            // 
            this.lblIme.AutoSize = true;
            this.lblIme.Location = new System.Drawing.Point(3, 0);
            this.lblIme.Name = "lblIme";
            this.lblIme.Size = new System.Drawing.Size(34, 19);
            this.lblIme.TabIndex = 0;
            this.lblIme.Text = "Ime:";
            // 
            // lblSrednjeSlovo
            // 
            this.lblSrednjeSlovo.AutoSize = true;
            this.lblSrednjeSlovo.Location = new System.Drawing.Point(3, 52);
            this.lblSrednjeSlovo.Name = "lblSrednjeSlovo";
            this.lblSrednjeSlovo.Size = new System.Drawing.Size(90, 19);
            this.lblSrednjeSlovo.TabIndex = 1;
            this.lblSrednjeSlovo.Text = "Srednje slovo:";
            // 
            // lblPrezime
            // 
            this.lblPrezime.AutoSize = true;
            this.lblPrezime.Location = new System.Drawing.Point(3, 103);
            this.lblPrezime.Name = "lblPrezime";
            this.lblPrezime.Size = new System.Drawing.Size(60, 19);
            this.lblPrezime.TabIndex = 1;
            this.lblPrezime.Text = "Prezime:";
            // 
            // lblDatumRodjenja
            // 
            this.lblDatumRodjenja.AutoSize = true;
            this.lblDatumRodjenja.Location = new System.Drawing.Point(3, 155);
            this.lblDatumRodjenja.Name = "lblDatumRodjenja";
            this.lblDatumRodjenja.Size = new System.Drawing.Size(100, 19);
            this.lblDatumRodjenja.TabIndex = 1;
            this.lblDatumRodjenja.Text = "Datum rođenja:";
            // 
            // lblAdresa
            // 
            this.lblAdresa.AutoSize = true;
            this.lblAdresa.Location = new System.Drawing.Point(3, 207);
            this.lblAdresa.Name = "lblAdresa";
            this.lblAdresa.Size = new System.Drawing.Size(53, 19);
            this.lblAdresa.TabIndex = 1;
            this.lblAdresa.Text = "Adresa:";
            // 
            // lblMestoRodjenja
            // 
            this.lblMestoRodjenja.AutoSize = true;
            this.lblMestoRodjenja.Location = new System.Drawing.Point(3, 259);
            this.lblMestoRodjenja.Name = "lblMestoRodjenja";
            this.lblMestoRodjenja.Size = new System.Drawing.Size(97, 19);
            this.lblMestoRodjenja.TabIndex = 1;
            this.lblMestoRodjenja.Text = "Mesto rođenja:";
            // 
            // lblBrojTelefona
            // 
            this.lblBrojTelefona.AutoSize = true;
            this.lblBrojTelefona.Location = new System.Drawing.Point(3, 311);
            this.lblBrojTelefona.Name = "lblBrojTelefona";
            this.lblBrojTelefona.Size = new System.Drawing.Size(87, 19);
            this.lblBrojTelefona.TabIndex = 1;
            this.lblBrojTelefona.Text = "Broj telefona:";
            // 
            // lblRadnoMesto
            // 
            this.lblRadnoMesto.AutoSize = true;
            this.lblRadnoMesto.Location = new System.Drawing.Point(3, 363);
            this.lblRadnoMesto.Name = "lblRadnoMesto";
            this.lblRadnoMesto.Size = new System.Drawing.Size(130, 19);
            this.lblRadnoMesto.TabIndex = 2;
            this.lblRadnoMesto.Text = "Naziv radnog mesta:";
            // 
            // lblTekuciRacun
            // 
            this.lblTekuciRacun.AutoSize = true;
            this.lblTekuciRacun.Location = new System.Drawing.Point(3, 415);
            this.lblTekuciRacun.Name = "lblTekuciRacun";
            this.lblTekuciRacun.Size = new System.Drawing.Size(128, 19);
            this.lblTekuciRacun.TabIndex = 2;
            this.lblTekuciRacun.Text = "Broj tekućeg računa:";
            // 
            // lblNormaRada
            // 
            this.lblNormaRada.AutoSize = true;
            this.lblNormaRada.Location = new System.Drawing.Point(3, 467);
            this.lblNormaRada.Name = "lblNormaRada";
            this.lblNormaRada.Size = new System.Drawing.Size(85, 19);
            this.lblNormaRada.TabIndex = 2;
            this.lblNormaRada.Text = "Norma rada:";
            // 
            // txtIme
            // 
            // 
            // 
            // 
            this.txtIme.CustomButton.Image = null;
            this.txtIme.CustomButton.Location = new System.Drawing.Point(240, 1);
            this.txtIme.CustomButton.Name = "";
            this.txtIme.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtIme.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtIme.CustomButton.TabIndex = 1;
            this.txtIme.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtIme.CustomButton.UseSelectable = true;
            this.txtIme.CustomButton.Visible = false;
            this.txtIme.Lines = new string[0];
            this.txtIme.Location = new System.Drawing.Point(3, 3);
            this.txtIme.MaxLength = 32767;
            this.txtIme.Name = "txtIme";
            this.txtIme.PasswordChar = '\0';
            this.txtIme.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIme.SelectedText = "";
            this.txtIme.SelectionLength = 0;
            this.txtIme.SelectionStart = 0;
            this.txtIme.ShortcutsEnabled = true;
            this.txtIme.Size = new System.Drawing.Size(266, 27);
            this.txtIme.Style = MetroFramework.MetroColorStyle.Purple;
            this.txtIme.TabIndex = 0;
            this.txtIme.UseSelectable = true;
            this.txtIme.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtIme.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtIme.Click += new System.EventHandler(this.txtIme_Click);
            this.txtIme.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIme_KeyPress);
            // 
            // txtSrednjeSlovo
            // 
            // 
            // 
            // 
            this.txtSrednjeSlovo.CustomButton.Image = null;
            this.txtSrednjeSlovo.CustomButton.Location = new System.Drawing.Point(242, 2);
            this.txtSrednjeSlovo.CustomButton.Name = "";
            this.txtSrednjeSlovo.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtSrednjeSlovo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSrednjeSlovo.CustomButton.TabIndex = 1;
            this.txtSrednjeSlovo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSrednjeSlovo.CustomButton.UseSelectable = true;
            this.txtSrednjeSlovo.CustomButton.Visible = false;
            this.txtSrednjeSlovo.Lines = new string[0];
            this.txtSrednjeSlovo.Location = new System.Drawing.Point(3, 3);
            this.txtSrednjeSlovo.MaxLength = 32767;
            this.txtSrednjeSlovo.Name = "txtSrednjeSlovo";
            this.txtSrednjeSlovo.PasswordChar = '\0';
            this.txtSrednjeSlovo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSrednjeSlovo.SelectedText = "";
            this.txtSrednjeSlovo.SelectionLength = 0;
            this.txtSrednjeSlovo.SelectionStart = 0;
            this.txtSrednjeSlovo.ShortcutsEnabled = true;
            this.txtSrednjeSlovo.Size = new System.Drawing.Size(266, 26);
            this.txtSrednjeSlovo.Style = MetroFramework.MetroColorStyle.Purple;
            this.txtSrednjeSlovo.TabIndex = 1;
            this.txtSrednjeSlovo.UseSelectable = true;
            this.txtSrednjeSlovo.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSrednjeSlovo.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtSrednjeSlovo.Click += new System.EventHandler(this.txtSrednjeSlovo_Click);
            this.txtSrednjeSlovo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSrednjeSlovo_KeyPress);
            // 
            // txtPrezime
            // 
            // 
            // 
            // 
            this.txtPrezime.CustomButton.Image = null;
            this.txtPrezime.CustomButton.Location = new System.Drawing.Point(240, 1);
            this.txtPrezime.CustomButton.Name = "";
            this.txtPrezime.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtPrezime.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPrezime.CustomButton.TabIndex = 1;
            this.txtPrezime.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPrezime.CustomButton.UseSelectable = true;
            this.txtPrezime.CustomButton.Visible = false;
            this.txtPrezime.Lines = new string[0];
            this.txtPrezime.Location = new System.Drawing.Point(3, 3);
            this.txtPrezime.MaxLength = 32767;
            this.txtPrezime.Name = "txtPrezime";
            this.txtPrezime.PasswordChar = '\0';
            this.txtPrezime.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPrezime.SelectedText = "";
            this.txtPrezime.SelectionLength = 0;
            this.txtPrezime.SelectionStart = 0;
            this.txtPrezime.ShortcutsEnabled = true;
            this.txtPrezime.Size = new System.Drawing.Size(266, 27);
            this.txtPrezime.Style = MetroFramework.MetroColorStyle.Purple;
            this.txtPrezime.TabIndex = 2;
            this.txtPrezime.UseSelectable = true;
            this.txtPrezime.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPrezime.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtPrezime.Click += new System.EventHandler(this.txtPrezime_Click);
            this.txtPrezime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrezime_KeyPress);
            // 
            // txtAdresa
            // 
            // 
            // 
            // 
            this.txtAdresa.CustomButton.Image = null;
            this.txtAdresa.CustomButton.Location = new System.Drawing.Point(240, 1);
            this.txtAdresa.CustomButton.Name = "";
            this.txtAdresa.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtAdresa.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAdresa.CustomButton.TabIndex = 1;
            this.txtAdresa.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAdresa.CustomButton.UseSelectable = true;
            this.txtAdresa.CustomButton.Visible = false;
            this.txtAdresa.Lines = new string[0];
            this.txtAdresa.Location = new System.Drawing.Point(3, 3);
            this.txtAdresa.MaxLength = 32767;
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.PasswordChar = '\0';
            this.txtAdresa.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAdresa.SelectedText = "";
            this.txtAdresa.SelectionLength = 0;
            this.txtAdresa.SelectionStart = 0;
            this.txtAdresa.ShortcutsEnabled = true;
            this.txtAdresa.Size = new System.Drawing.Size(266, 27);
            this.txtAdresa.Style = MetroFramework.MetroColorStyle.Purple;
            this.txtAdresa.TabIndex = 4;
            this.txtAdresa.UseSelectable = true;
            this.txtAdresa.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAdresa.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtAdresa.Click += new System.EventHandler(this.txtAdresa_Click);
            // 
            // txtMestoRodjenja
            // 
            // 
            // 
            // 
            this.txtMestoRodjenja.CustomButton.Image = null;
            this.txtMestoRodjenja.CustomButton.Location = new System.Drawing.Point(240, 1);
            this.txtMestoRodjenja.CustomButton.Name = "";
            this.txtMestoRodjenja.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtMestoRodjenja.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtMestoRodjenja.CustomButton.TabIndex = 1;
            this.txtMestoRodjenja.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtMestoRodjenja.CustomButton.UseSelectable = true;
            this.txtMestoRodjenja.CustomButton.Visible = false;
            this.txtMestoRodjenja.Lines = new string[0];
            this.txtMestoRodjenja.Location = new System.Drawing.Point(3, 3);
            this.txtMestoRodjenja.MaxLength = 32767;
            this.txtMestoRodjenja.Name = "txtMestoRodjenja";
            this.txtMestoRodjenja.PasswordChar = '\0';
            this.txtMestoRodjenja.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtMestoRodjenja.SelectedText = "";
            this.txtMestoRodjenja.SelectionLength = 0;
            this.txtMestoRodjenja.SelectionStart = 0;
            this.txtMestoRodjenja.ShortcutsEnabled = true;
            this.txtMestoRodjenja.Size = new System.Drawing.Size(266, 27);
            this.txtMestoRodjenja.Style = MetroFramework.MetroColorStyle.Purple;
            this.txtMestoRodjenja.TabIndex = 5;
            this.txtMestoRodjenja.UseSelectable = true;
            this.txtMestoRodjenja.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtMestoRodjenja.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtMestoRodjenja.Click += new System.EventHandler(this.txtMestoRodjenja_Click);
            this.txtMestoRodjenja.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMestoRodjenja_KeyPress);
            // 
            // txtBrojTelefona
            // 
            // 
            // 
            // 
            this.txtBrojTelefona.CustomButton.Image = null;
            this.txtBrojTelefona.CustomButton.Location = new System.Drawing.Point(240, 1);
            this.txtBrojTelefona.CustomButton.Name = "";
            this.txtBrojTelefona.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtBrojTelefona.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBrojTelefona.CustomButton.TabIndex = 1;
            this.txtBrojTelefona.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBrojTelefona.CustomButton.UseSelectable = true;
            this.txtBrojTelefona.CustomButton.Visible = false;
            this.txtBrojTelefona.Lines = new string[0];
            this.txtBrojTelefona.Location = new System.Drawing.Point(3, 3);
            this.txtBrojTelefona.MaxLength = 32767;
            this.txtBrojTelefona.Name = "txtBrojTelefona";
            this.txtBrojTelefona.PasswordChar = '\0';
            this.txtBrojTelefona.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBrojTelefona.SelectedText = "";
            this.txtBrojTelefona.SelectionLength = 0;
            this.txtBrojTelefona.SelectionStart = 0;
            this.txtBrojTelefona.ShortcutsEnabled = true;
            this.txtBrojTelefona.Size = new System.Drawing.Size(266, 27);
            this.txtBrojTelefona.Style = MetroFramework.MetroColorStyle.Purple;
            this.txtBrojTelefona.TabIndex = 6;
            this.txtBrojTelefona.UseSelectable = true;
            this.txtBrojTelefona.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBrojTelefona.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBrojTelefona.Click += new System.EventHandler(this.txtBrojTelefona_Click);
            this.txtBrojTelefona.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBrojTelefona_KeyPress);
            // 
            // txtNazivRadnogMesta
            // 
            // 
            // 
            // 
            this.txtNazivRadnogMesta.CustomButton.Image = null;
            this.txtNazivRadnogMesta.CustomButton.Location = new System.Drawing.Point(240, 1);
            this.txtNazivRadnogMesta.CustomButton.Name = "";
            this.txtNazivRadnogMesta.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtNazivRadnogMesta.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNazivRadnogMesta.CustomButton.TabIndex = 1;
            this.txtNazivRadnogMesta.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNazivRadnogMesta.CustomButton.UseSelectable = true;
            this.txtNazivRadnogMesta.CustomButton.Visible = false;
            this.txtNazivRadnogMesta.Lines = new string[0];
            this.txtNazivRadnogMesta.Location = new System.Drawing.Point(3, 3);
            this.txtNazivRadnogMesta.MaxLength = 32767;
            this.txtNazivRadnogMesta.Name = "txtNazivRadnogMesta";
            this.txtNazivRadnogMesta.PasswordChar = '\0';
            this.txtNazivRadnogMesta.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNazivRadnogMesta.SelectedText = "";
            this.txtNazivRadnogMesta.SelectionLength = 0;
            this.txtNazivRadnogMesta.SelectionStart = 0;
            this.txtNazivRadnogMesta.ShortcutsEnabled = true;
            this.txtNazivRadnogMesta.Size = new System.Drawing.Size(266, 27);
            this.txtNazivRadnogMesta.Style = MetroFramework.MetroColorStyle.Purple;
            this.txtNazivRadnogMesta.TabIndex = 7;
            this.txtNazivRadnogMesta.UseSelectable = true;
            this.txtNazivRadnogMesta.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNazivRadnogMesta.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNazivRadnogMesta.Click += new System.EventHandler(this.txtNazivRadnogMesta_Click);
            this.txtNazivRadnogMesta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRadnoMesto_KeyPress);
            // 
            // txtBrojTekucegRacuna
            // 
            // 
            // 
            // 
            this.txtBrojTekucegRacuna.CustomButton.Image = null;
            this.txtBrojTekucegRacuna.CustomButton.Location = new System.Drawing.Point(240, 1);
            this.txtBrojTekucegRacuna.CustomButton.Name = "";
            this.txtBrojTekucegRacuna.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtBrojTekucegRacuna.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBrojTekucegRacuna.CustomButton.TabIndex = 1;
            this.txtBrojTekucegRacuna.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBrojTekucegRacuna.CustomButton.UseSelectable = true;
            this.txtBrojTekucegRacuna.CustomButton.Visible = false;
            this.txtBrojTekucegRacuna.Lines = new string[0];
            this.txtBrojTekucegRacuna.Location = new System.Drawing.Point(3, 3);
            this.txtBrojTekucegRacuna.MaxLength = 32767;
            this.txtBrojTekucegRacuna.Name = "txtBrojTekucegRacuna";
            this.txtBrojTekucegRacuna.PasswordChar = '\0';
            this.txtBrojTekucegRacuna.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBrojTekucegRacuna.SelectedText = "";
            this.txtBrojTekucegRacuna.SelectionLength = 0;
            this.txtBrojTekucegRacuna.SelectionStart = 0;
            this.txtBrojTekucegRacuna.ShortcutsEnabled = true;
            this.txtBrojTekucegRacuna.Size = new System.Drawing.Size(266, 27);
            this.txtBrojTekucegRacuna.Style = MetroFramework.MetroColorStyle.Purple;
            this.txtBrojTekucegRacuna.TabIndex = 8;
            this.txtBrojTekucegRacuna.UseSelectable = true;
            this.txtBrojTekucegRacuna.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBrojTekucegRacuna.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBrojTekucegRacuna.Click += new System.EventHandler(this.txtBrojTekucegRacuna_Click);
            this.txtBrojTekucegRacuna.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTekuciRacun_KeyPress);
            // 
            // txtNormaRada
            // 
            // 
            // 
            // 
            this.txtNormaRada.CustomButton.Image = null;
            this.txtNormaRada.CustomButton.Location = new System.Drawing.Point(240, 1);
            this.txtNormaRada.CustomButton.Name = "";
            this.txtNormaRada.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtNormaRada.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNormaRada.CustomButton.TabIndex = 1;
            this.txtNormaRada.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNormaRada.CustomButton.UseSelectable = true;
            this.txtNormaRada.CustomButton.Visible = false;
            this.txtNormaRada.Lines = new string[0];
            this.txtNormaRada.Location = new System.Drawing.Point(3, 3);
            this.txtNormaRada.MaxLength = 32767;
            this.txtNormaRada.Name = "txtNormaRada";
            this.txtNormaRada.PasswordChar = '\0';
            this.txtNormaRada.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNormaRada.SelectedText = "";
            this.txtNormaRada.SelectionLength = 0;
            this.txtNormaRada.SelectionStart = 0;
            this.txtNormaRada.ShortcutsEnabled = true;
            this.txtNormaRada.Size = new System.Drawing.Size(266, 27);
            this.txtNormaRada.Style = MetroFramework.MetroColorStyle.Purple;
            this.txtNormaRada.TabIndex = 9;
            this.txtNormaRada.UseSelectable = true;
            this.txtNormaRada.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNormaRada.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNormaRada.Click += new System.EventHandler(this.txtNormaRada_Click);
            this.txtNormaRada.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNormaRada_KeyPress);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.80761F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.19239F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel21, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel20, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.lblIme, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dtpDatumRodjenja, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel16, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel15, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel14, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel13, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel12, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel10, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel8, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblSrednjeSlovo, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblPrezime, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblDatumRodjenja, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblAdresa, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblMestoRodjenja, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblNormaRada, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.lblBrojTelefona, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblRadnoMesto, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblTekuciRacun, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel9, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel11, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblStaz, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.lblKoefRadMesta, 0, 11);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 12;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.81967F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.18033F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(447, 624);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 1;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.Controls.Add(this.lblErrKoef, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.txtKoeficijent, 0, 0);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(172, 574);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 2;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 69.04762F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30.95238F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(272, 47);
            this.tableLayoutPanel21.TabIndex = 19;
            // 
            // lblErrKoef
            // 
            this.lblErrKoef.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblErrKoef.AutoSize = true;
            this.lblErrKoef.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblErrKoef.Location = new System.Drawing.Point(187, 32);
            this.lblErrKoef.Margin = new System.Windows.Forms.Padding(0);
            this.lblErrKoef.Name = "lblErrKoef";
            this.lblErrKoef.Size = new System.Drawing.Size(85, 15);
            this.lblErrKoef.Style = MetroFramework.MetroColorStyle.Red;
            this.lblErrKoef.TabIndex = 6;
            this.lblErrKoef.Text = "Obavezan unos";
            this.lblErrKoef.UseStyleColors = true;
            this.lblErrKoef.Visible = false;
            // 
            // txtKoeficijent
            // 
            // 
            // 
            // 
            this.txtKoeficijent.CustomButton.Image = null;
            this.txtKoeficijent.CustomButton.Location = new System.Drawing.Point(242, 2);
            this.txtKoeficijent.CustomButton.Name = "";
            this.txtKoeficijent.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtKoeficijent.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtKoeficijent.CustomButton.TabIndex = 1;
            this.txtKoeficijent.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtKoeficijent.CustomButton.UseSelectable = true;
            this.txtKoeficijent.CustomButton.Visible = false;
            this.txtKoeficijent.Lines = new string[0];
            this.txtKoeficijent.Location = new System.Drawing.Point(3, 3);
            this.txtKoeficijent.MaxLength = 32767;
            this.txtKoeficijent.Name = "txtKoeficijent";
            this.txtKoeficijent.PasswordChar = '\0';
            this.txtKoeficijent.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtKoeficijent.SelectedText = "";
            this.txtKoeficijent.SelectionLength = 0;
            this.txtKoeficijent.SelectionStart = 0;
            this.txtKoeficijent.ShortcutsEnabled = true;
            this.txtKoeficijent.Size = new System.Drawing.Size(266, 26);
            this.txtKoeficijent.Style = MetroFramework.MetroColorStyle.Purple;
            this.txtKoeficijent.TabIndex = 11;
            this.txtKoeficijent.UseSelectable = true;
            this.txtKoeficijent.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtKoeficijent.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtKoeficijent.Click += new System.EventHandler(this.txtKoeficijent_Click);
            this.txtKoeficijent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKoeficijentRadnogMesta_KeyPress);
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 1;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.Controls.Add(this.lblErrStaz, 0, 1);
            this.tableLayoutPanel20.Controls.Add(this.txtStaz, 0, 0);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(172, 522);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 2;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 73.91304F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.08696F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(272, 46);
            this.tableLayoutPanel20.TabIndex = 17;
            // 
            // lblErrStaz
            // 
            this.lblErrStaz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblErrStaz.AutoSize = true;
            this.lblErrStaz.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblErrStaz.Location = new System.Drawing.Point(187, 33);
            this.lblErrStaz.Margin = new System.Windows.Forms.Padding(0);
            this.lblErrStaz.Name = "lblErrStaz";
            this.lblErrStaz.Size = new System.Drawing.Size(85, 13);
            this.lblErrStaz.Style = MetroFramework.MetroColorStyle.Red;
            this.lblErrStaz.TabIndex = 6;
            this.lblErrStaz.Text = "Obavezan unos";
            this.lblErrStaz.UseStyleColors = true;
            this.lblErrStaz.Visible = false;
            // 
            // txtStaz
            // 
            // 
            // 
            // 
            this.txtStaz.CustomButton.Image = null;
            this.txtStaz.CustomButton.Location = new System.Drawing.Point(240, 1);
            this.txtStaz.CustomButton.Name = "";
            this.txtStaz.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.txtStaz.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtStaz.CustomButton.TabIndex = 1;
            this.txtStaz.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtStaz.CustomButton.UseSelectable = true;
            this.txtStaz.CustomButton.Visible = false;
            this.txtStaz.Lines = new string[0];
            this.txtStaz.Location = new System.Drawing.Point(3, 3);
            this.txtStaz.MaxLength = 32767;
            this.txtStaz.Name = "txtStaz";
            this.txtStaz.PasswordChar = '\0';
            this.txtStaz.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtStaz.SelectedText = "";
            this.txtStaz.SelectionLength = 0;
            this.txtStaz.SelectionStart = 0;
            this.txtStaz.ShortcutsEnabled = true;
            this.txtStaz.Size = new System.Drawing.Size(266, 27);
            this.txtStaz.Style = MetroFramework.MetroColorStyle.Purple;
            this.txtStaz.TabIndex = 10;
            this.txtStaz.UseSelectable = true;
            this.txtStaz.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtStaz.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtStaz.Click += new System.EventHandler(this.txtStaz_Click);
            this.txtStaz.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtStaz_KeyPress);
            // 
            // dtpDatumRodjenja
            // 
            this.dtpDatumRodjenja.CustomFormat = "dd.MM.yyyy.";
            this.dtpDatumRodjenja.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDatumRodjenja.Location = new System.Drawing.Point(172, 158);
            this.dtpDatumRodjenja.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtpDatumRodjenja.Name = "dtpDatumRodjenja";
            this.dtpDatumRodjenja.Size = new System.Drawing.Size(269, 29);
            this.dtpDatumRodjenja.Style = MetroFramework.MetroColorStyle.Purple;
            this.dtpDatumRodjenja.TabIndex = 3;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Controls.Add(this.txtNormaRada, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.lblErrNorma, 0, 1);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(172, 470);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 2;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 72.72727F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.27273F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(272, 46);
            this.tableLayoutPanel16.TabIndex = 15;
            // 
            // lblErrNorma
            // 
            this.lblErrNorma.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblErrNorma.AutoSize = true;
            this.lblErrNorma.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblErrNorma.Location = new System.Drawing.Point(187, 33);
            this.lblErrNorma.Margin = new System.Windows.Forms.Padding(0);
            this.lblErrNorma.Name = "lblErrNorma";
            this.lblErrNorma.Size = new System.Drawing.Size(85, 13);
            this.lblErrNorma.Style = MetroFramework.MetroColorStyle.Red;
            this.lblErrNorma.TabIndex = 6;
            this.lblErrNorma.Text = "Obavezan unos";
            this.lblErrNorma.UseStyleColors = true;
            this.lblErrNorma.Visible = false;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 1;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Controls.Add(this.lblErrRacun, 0, 1);
            this.tableLayoutPanel15.Controls.Add(this.txtBrojTekucegRacuna, 0, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(172, 418);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 2;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 73.91304F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.08696F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(272, 46);
            this.tableLayoutPanel15.TabIndex = 15;
            // 
            // lblErrRacun
            // 
            this.lblErrRacun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblErrRacun.AutoSize = true;
            this.lblErrRacun.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblErrRacun.Location = new System.Drawing.Point(187, 33);
            this.lblErrRacun.Margin = new System.Windows.Forms.Padding(0);
            this.lblErrRacun.Name = "lblErrRacun";
            this.lblErrRacun.Size = new System.Drawing.Size(85, 13);
            this.lblErrRacun.Style = MetroFramework.MetroColorStyle.Red;
            this.lblErrRacun.TabIndex = 6;
            this.lblErrRacun.Text = "Obavezan unos";
            this.lblErrRacun.UseStyleColors = true;
            this.lblErrRacun.Visible = false;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 1;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Controls.Add(this.lblErrRadnoMesto, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.txtNazivRadnogMesta, 0, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(172, 366);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 2;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 73.91304F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.08696F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(272, 46);
            this.tableLayoutPanel14.TabIndex = 15;
            // 
            // lblErrRadnoMesto
            // 
            this.lblErrRadnoMesto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblErrRadnoMesto.AutoSize = true;
            this.lblErrRadnoMesto.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblErrRadnoMesto.Location = new System.Drawing.Point(187, 33);
            this.lblErrRadnoMesto.Margin = new System.Windows.Forms.Padding(0);
            this.lblErrRadnoMesto.Name = "lblErrRadnoMesto";
            this.lblErrRadnoMesto.Size = new System.Drawing.Size(85, 13);
            this.lblErrRadnoMesto.Style = MetroFramework.MetroColorStyle.Red;
            this.lblErrRadnoMesto.TabIndex = 6;
            this.lblErrRadnoMesto.Text = "Obavezan unos";
            this.lblErrRadnoMesto.UseStyleColors = true;
            this.lblErrRadnoMesto.Visible = false;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Controls.Add(this.LblErrBrojTelefona, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.txtBrojTelefona, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(172, 314);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 2;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 73.91304F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.08696F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(272, 46);
            this.tableLayoutPanel13.TabIndex = 15;
            // 
            // LblErrBrojTelefona
            // 
            this.LblErrBrojTelefona.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblErrBrojTelefona.AutoSize = true;
            this.LblErrBrojTelefona.FontSize = MetroFramework.MetroLabelSize.Small;
            this.LblErrBrojTelefona.Location = new System.Drawing.Point(187, 33);
            this.LblErrBrojTelefona.Margin = new System.Windows.Forms.Padding(0);
            this.LblErrBrojTelefona.Name = "LblErrBrojTelefona";
            this.LblErrBrojTelefona.Size = new System.Drawing.Size(85, 13);
            this.LblErrBrojTelefona.Style = MetroFramework.MetroColorStyle.Red;
            this.LblErrBrojTelefona.TabIndex = 6;
            this.LblErrBrojTelefona.Text = "Obavezan unos";
            this.LblErrBrojTelefona.UseStyleColors = true;
            this.LblErrBrojTelefona.Visible = false;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 1;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Controls.Add(this.lblErrMestoRodjenja, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.txtMestoRodjenja, 0, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(172, 262);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 2;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 73.91304F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.08696F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(272, 46);
            this.tableLayoutPanel12.TabIndex = 15;
            // 
            // lblErrMestoRodjenja
            // 
            this.lblErrMestoRodjenja.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblErrMestoRodjenja.AutoSize = true;
            this.lblErrMestoRodjenja.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblErrMestoRodjenja.Location = new System.Drawing.Point(187, 33);
            this.lblErrMestoRodjenja.Margin = new System.Windows.Forms.Padding(0);
            this.lblErrMestoRodjenja.Name = "lblErrMestoRodjenja";
            this.lblErrMestoRodjenja.Size = new System.Drawing.Size(85, 13);
            this.lblErrMestoRodjenja.Style = MetroFramework.MetroColorStyle.Red;
            this.lblErrMestoRodjenja.TabIndex = 6;
            this.lblErrMestoRodjenja.Text = "Obavezan unos";
            this.lblErrMestoRodjenja.UseStyleColors = true;
            this.lblErrMestoRodjenja.Visible = false;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Controls.Add(this.lblErrPrezime, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.txtPrezime, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(172, 106);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 2;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 73.91304F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.08696F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(272, 46);
            this.tableLayoutPanel10.TabIndex = 14;
            // 
            // lblErrPrezime
            // 
            this.lblErrPrezime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblErrPrezime.AutoSize = true;
            this.lblErrPrezime.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblErrPrezime.Location = new System.Drawing.Point(187, 33);
            this.lblErrPrezime.Margin = new System.Windows.Forms.Padding(0);
            this.lblErrPrezime.Name = "lblErrPrezime";
            this.lblErrPrezime.Size = new System.Drawing.Size(85, 13);
            this.lblErrPrezime.Style = MetroFramework.MetroColorStyle.Red;
            this.lblErrPrezime.TabIndex = 6;
            this.lblErrPrezime.Text = "Obavezan unos";
            this.lblErrPrezime.UseStyleColors = true;
            this.lblErrPrezime.Visible = false;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.lblErrIme, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.txtIme, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(172, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 73.91304F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.08696F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(272, 46);
            this.tableLayoutPanel8.TabIndex = 14;
            // 
            // lblErrIme
            // 
            this.lblErrIme.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblErrIme.AutoSize = true;
            this.lblErrIme.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblErrIme.Location = new System.Drawing.Point(187, 33);
            this.lblErrIme.Margin = new System.Windows.Forms.Padding(0);
            this.lblErrIme.Name = "lblErrIme";
            this.lblErrIme.Size = new System.Drawing.Size(85, 13);
            this.lblErrIme.Style = MetroFramework.MetroColorStyle.Red;
            this.lblErrIme.TabIndex = 5;
            this.lblErrIme.Text = "Obavezan unos";
            this.lblErrIme.UseStyleColors = true;
            this.lblErrIme.Visible = false;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.lblErrSSlovo, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.txtSrednjeSlovo, 0, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(172, 55);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 73.33334F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.66667F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(272, 45);
            this.tableLayoutPanel9.TabIndex = 15;
            // 
            // lblErrSSlovo
            // 
            this.lblErrSSlovo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblErrSSlovo.AutoSize = true;
            this.lblErrSSlovo.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblErrSSlovo.Location = new System.Drawing.Point(187, 32);
            this.lblErrSSlovo.Margin = new System.Windows.Forms.Padding(0);
            this.lblErrSSlovo.Name = "lblErrSSlovo";
            this.lblErrSSlovo.Size = new System.Drawing.Size(85, 13);
            this.lblErrSSlovo.Style = MetroFramework.MetroColorStyle.Red;
            this.lblErrSSlovo.TabIndex = 5;
            this.lblErrSSlovo.Text = "Obavezan unos";
            this.lblErrSSlovo.UseStyleColors = true;
            this.lblErrSSlovo.Visible = false;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Controls.Add(this.lblErrAdresa, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.txtAdresa, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(172, 210);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 73.91304F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.08696F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(272, 46);
            this.tableLayoutPanel11.TabIndex = 15;
            // 
            // lblErrAdresa
            // 
            this.lblErrAdresa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblErrAdresa.AutoSize = true;
            this.lblErrAdresa.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblErrAdresa.Location = new System.Drawing.Point(187, 33);
            this.lblErrAdresa.Margin = new System.Windows.Forms.Padding(0);
            this.lblErrAdresa.Name = "lblErrAdresa";
            this.lblErrAdresa.Size = new System.Drawing.Size(85, 13);
            this.lblErrAdresa.Style = MetroFramework.MetroColorStyle.Red;
            this.lblErrAdresa.TabIndex = 6;
            this.lblErrAdresa.Text = "Obavezan unos";
            this.lblErrAdresa.UseStyleColors = true;
            this.lblErrAdresa.Visible = false;
            // 
            // lblStaz
            // 
            this.lblStaz.AutoSize = true;
            this.lblStaz.Location = new System.Drawing.Point(3, 519);
            this.lblStaz.Name = "lblStaz";
            this.lblStaz.Size = new System.Drawing.Size(78, 19);
            this.lblStaz.TabIndex = 16;
            this.lblStaz.Text = "Početni staž";
            // 
            // lblKoefRadMesta
            // 
            this.lblKoefRadMesta.AutoSize = true;
            this.lblKoefRadMesta.Location = new System.Drawing.Point(3, 571);
            this.lblKoefRadMesta.Name = "lblKoefRadMesta";
            this.lblKoefRadMesta.Size = new System.Drawing.Size(157, 19);
            this.lblKoefRadMesta.TabIndex = 18;
            this.lblKoefRadMesta.Text = "Koeficijent radnog mesta:";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel6, 1, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(20, 60);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(907, 630);
            this.tableLayoutPanel7.TabIndex = 13;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(456, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(448, 624);
            this.tableLayoutPanel6.TabIndex = 12;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.54751F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.45249F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.pictureBox1, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(442, 274);
            this.tableLayoutPanel3.TabIndex = 7;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.btnIzaberiSliku, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.btnObrisiNadredjenog, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnIzaberiNadredjenog, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnSacuvaj, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnIzaberiRadnoVreme, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(212, 268);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // btnIzaberiSliku
            // 
            this.btnIzaberiSliku.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnIzaberiSliku.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnIzaberiSliku.Location = new System.Drawing.Point(3, 162);
            this.btnIzaberiSliku.Name = "btnIzaberiSliku";
            this.btnIzaberiSliku.Size = new System.Drawing.Size(205, 42);
            this.btnIzaberiSliku.TabIndex = 15;
            this.btnIzaberiSliku.Text = "Izaberi sliku";
            this.btnIzaberiSliku.UseSelectable = true;
            this.btnIzaberiSliku.Click += new System.EventHandler(this.btnIzaberiSliku_Click);
            // 
            // btnObrisiNadredjenog
            // 
            this.btnObrisiNadredjenog.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnObrisiNadredjenog.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnObrisiNadredjenog.Location = new System.Drawing.Point(3, 56);
            this.btnObrisiNadredjenog.Name = "btnObrisiNadredjenog";
            this.btnObrisiNadredjenog.Size = new System.Drawing.Size(205, 42);
            this.btnObrisiNadredjenog.TabIndex = 13;
            this.btnObrisiNadredjenog.Text = "Obriši nadređenog";
            this.btnObrisiNadredjenog.UseSelectable = true;
            this.btnObrisiNadredjenog.Click += new System.EventHandler(this.btnObrisiNadredjenog_Click);
            // 
            // btnIzaberiNadredjenog
            // 
            this.btnIzaberiNadredjenog.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnIzaberiNadredjenog.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnIzaberiNadredjenog.Location = new System.Drawing.Point(3, 3);
            this.btnIzaberiNadredjenog.Name = "btnIzaberiNadredjenog";
            this.btnIzaberiNadredjenog.Size = new System.Drawing.Size(205, 42);
            this.btnIzaberiNadredjenog.TabIndex = 12;
            this.btnIzaberiNadredjenog.Text = "Izaberi nadređenog";
            this.btnIzaberiNadredjenog.UseSelectable = true;
            this.btnIzaberiNadredjenog.Click += new System.EventHandler(this.btnIzaberiNadredjenog_Click);
            // 
            // btnSacuvaj
            // 
            this.btnSacuvaj.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnSacuvaj.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnSacuvaj.Location = new System.Drawing.Point(3, 215);
            this.btnSacuvaj.Name = "btnSacuvaj";
            this.btnSacuvaj.Size = new System.Drawing.Size(205, 42);
            this.btnSacuvaj.TabIndex = 16;
            this.btnSacuvaj.Text = "Sačuvaj izmene";
            this.btnSacuvaj.UseSelectable = true;
            this.btnSacuvaj.Click += new System.EventHandler(this.btnSacuvaj_Click);
            // 
            // btnIzaberiRadnoVreme
            // 
            this.btnIzaberiRadnoVreme.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnIzaberiRadnoVreme.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnIzaberiRadnoVreme.Location = new System.Drawing.Point(3, 109);
            this.btnIzaberiRadnoVreme.Name = "btnIzaberiRadnoVreme";
            this.btnIzaberiRadnoVreme.Size = new System.Drawing.Size(205, 42);
            this.btnIzaberiRadnoVreme.TabIndex = 14;
            this.btnIzaberiRadnoVreme.Text = "Izaberi smenu";
            this.btnIzaberiRadnoVreme.UseSelectable = true;
            this.btnIzaberiRadnoVreme.Click += new System.EventHandler(this.btnIzaberiRadnoVreme_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = global::SharedComponents.Properties.Resources.default_person;
            this.pictureBox1.Image = global::SharedComponents.Properties.Resources.default_person;
            this.pictureBox1.InitialImage = global::SharedComponents.Properties.Resources.default_person;
            this.pictureBox1.Location = new System.Drawing.Point(228, 3);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(10, 3, 3, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(208, 233);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.11338F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.88662F));
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel19, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel2, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel18, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel1, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel17, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.cmbUloga, 1, 4);
            this.tableLayoutPanel5.Controls.Add(this.lblKorisnikSaNalogom, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel4, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.lblKorisnickoIme, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.lblUlogaKorNaloga, 0, 4);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 283);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 5;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(442, 338);
            this.tableLayoutPanel5.TabIndex = 11;
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 1;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Controls.Add(this.metroLabel3, 0, 0);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(224, 204);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 2;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(215, 61);
            this.tableLayoutPanel19.TabIndex = 15;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(3, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(107, 19);
            this.metroLabel3.TabIndex = 3;
            this.metroLabel3.Text = "ime nadređenog";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(3, 201);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(74, 19);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "Nadređeni:";
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 1;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Controls.Add(this.lblSmena, 0, 0);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(224, 137);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 2;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(215, 61);
            this.tableLayoutPanel18.TabIndex = 15;
            // 
            // lblSmena
            // 
            this.lblSmena.AutoSize = true;
            this.lblSmena.Location = new System.Drawing.Point(3, 0);
            this.lblSmena.Name = "lblSmena";
            this.lblSmena.Size = new System.Drawing.Size(73, 19);
            this.lblSmena.TabIndex = 1;
            this.lblSmena.Text = "ime smene";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(3, 134);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(52, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Smena:";
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 1;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.Controls.Add(this.txtKorisnickoIme, 0, 0);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(224, 70);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 2;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(215, 61);
            this.tableLayoutPanel17.TabIndex = 15;
            // 
            // txtKorisnickoIme
            // 
            // 
            // 
            // 
            this.txtKorisnickoIme.CustomButton.Image = null;
            this.txtKorisnickoIme.CustomButton.Location = new System.Drawing.Point(188, 1);
            this.txtKorisnickoIme.CustomButton.Name = "";
            this.txtKorisnickoIme.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.txtKorisnickoIme.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtKorisnickoIme.CustomButton.TabIndex = 1;
            this.txtKorisnickoIme.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtKorisnickoIme.CustomButton.UseSelectable = true;
            this.txtKorisnickoIme.CustomButton.Visible = false;
            this.txtKorisnickoIme.Enabled = false;
            this.txtKorisnickoIme.Lines = new string[0];
            this.txtKorisnickoIme.Location = new System.Drawing.Point(3, 3);
            this.txtKorisnickoIme.MaxLength = 32767;
            this.txtKorisnickoIme.Name = "txtKorisnickoIme";
            this.txtKorisnickoIme.PasswordChar = '\0';
            this.txtKorisnickoIme.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtKorisnickoIme.SelectedText = "";
            this.txtKorisnickoIme.SelectionLength = 0;
            this.txtKorisnickoIme.SelectionStart = 0;
            this.txtKorisnickoIme.ShortcutsEnabled = true;
            this.txtKorisnickoIme.Size = new System.Drawing.Size(208, 21);
            this.txtKorisnickoIme.Style = MetroFramework.MetroColorStyle.Purple;
            this.txtKorisnickoIme.TabIndex = 3;
            this.txtKorisnickoIme.UseSelectable = true;
            this.txtKorisnickoIme.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtKorisnickoIme.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // cmbUloga
            // 
            this.cmbUloga.Enabled = false;
            this.cmbUloga.FormattingEnabled = true;
            this.cmbUloga.ItemHeight = 23;
            this.cmbUloga.Items.AddRange(new object[] {
            "Administrator",
            "Menadžer",
            "Knjigovođa",
            "Portir"});
            this.cmbUloga.Location = new System.Drawing.Point(224, 271);
            this.cmbUloga.Name = "cmbUloga";
            this.cmbUloga.Size = new System.Drawing.Size(214, 29);
            this.cmbUloga.Style = MetroFramework.MetroColorStyle.Purple;
            this.cmbUloga.TabIndex = 11;
            this.cmbUloga.UseSelectable = true;
            // 
            // lblKorisnikSaNalogom
            // 
            this.lblKorisnikSaNalogom.AutoSize = true;
            this.lblKorisnikSaNalogom.Location = new System.Drawing.Point(3, 0);
            this.lblKorisnikSaNalogom.Name = "lblKorisnikSaNalogom";
            this.lblKorisnikSaNalogom.Size = new System.Drawing.Size(126, 19);
            this.lblKorisnikSaNalogom.TabIndex = 2;
            this.lblKorisnikSaNalogom.Text = "Korisnik sa nalogom";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.radioNe, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.radioDa, 0, 0);
            this.tableLayoutPanel4.Enabled = false;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(224, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(214, 41);
            this.tableLayoutPanel4.TabIndex = 10;
            // 
            // radioNe
            // 
            this.radioNe.AutoSize = true;
            this.radioNe.Location = new System.Drawing.Point(110, 3);
            this.radioNe.Name = "radioNe";
            this.radioNe.Size = new System.Drawing.Size(38, 15);
            this.radioNe.Style = MetroFramework.MetroColorStyle.Purple;
            this.radioNe.TabIndex = 9;
            this.radioNe.Text = "Ne";
            this.radioNe.UseSelectable = true;
            // 
            // radioDa
            // 
            this.radioDa.AutoSize = true;
            this.radioDa.Location = new System.Drawing.Point(3, 3);
            this.radioDa.Name = "radioDa";
            this.radioDa.Size = new System.Drawing.Size(37, 15);
            this.radioDa.Style = MetroFramework.MetroColorStyle.Purple;
            this.radioDa.TabIndex = 8;
            this.radioDa.Text = "Da";
            this.radioDa.UseSelectable = true;
            // 
            // lblKorisnickoIme
            // 
            this.lblKorisnickoIme.AutoSize = true;
            this.lblKorisnickoIme.Location = new System.Drawing.Point(3, 67);
            this.lblKorisnickoIme.Name = "lblKorisnickoIme";
            this.lblKorisnickoIme.Size = new System.Drawing.Size(96, 19);
            this.lblKorisnickoIme.TabIndex = 2;
            this.lblKorisnickoIme.Text = "Korisničko ime:";
            // 
            // lblUlogaKorNaloga
            // 
            this.lblUlogaKorNaloga.AutoSize = true;
            this.lblUlogaKorNaloga.Location = new System.Drawing.Point(3, 268);
            this.lblUlogaKorNaloga.Name = "lblUlogaKorNaloga";
            this.lblUlogaKorNaloga.Size = new System.Drawing.Size(160, 19);
            this.lblUlogaKorNaloga.TabIndex = 2;
            this.lblUlogaKorNaloga.Text = "Uloga korisničkog naloga:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // AzuriranjeZaposlenihForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 710);
            this.Controls.Add(this.tableLayoutPanel7);
            this.MaximizeBox = false;
            this.Name = "AzuriranjeZaposlenihForm";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Purple;
            this.Text = "Ažuriranje podataka";
            this.Load += new System.EventHandler(this.AzuriranjeZaposlenihForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel21.PerformLayout();
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel20.PerformLayout();
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel19.PerformLayout();
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblIme;
        private MetroFramework.Controls.MetroLabel lblSrednjeSlovo;
        private MetroFramework.Controls.MetroLabel lblPrezime;
        private MetroFramework.Controls.MetroLabel lblDatumRodjenja;
        private MetroFramework.Controls.MetroLabel lblAdresa;
        private MetroFramework.Controls.MetroLabel lblMestoRodjenja;
        private MetroFramework.Controls.MetroLabel lblBrojTelefona;
        private MetroFramework.Controls.MetroLabel lblRadnoMesto;
        private MetroFramework.Controls.MetroLabel lblTekuciRacun;
        private MetroFramework.Controls.MetroLabel lblNormaRada;
        private MetroFramework.Controls.MetroTextBox txtIme;
        private MetroFramework.Controls.MetroTextBox txtSrednjeSlovo;
        private MetroFramework.Controls.MetroTextBox txtPrezime;
        private MetroFramework.Controls.MetroTextBox txtAdresa;
        private MetroFramework.Controls.MetroTextBox txtMestoRodjenja;
        private MetroFramework.Controls.MetroTextBox txtBrojTelefona;
        private MetroFramework.Controls.MetroTextBox txtNazivRadnogMesta;
        private MetroFramework.Controls.MetroTextBox txtBrojTekucegRacuna;
        private MetroFramework.Controls.MetroTextBox txtNormaRada;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroDateTime dtpDatumRodjenja;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private MetroFramework.Controls.MetroLabel lblErrMestoRodjenja;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private MetroFramework.Controls.MetroLabel lblErrAdresa;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private MetroFramework.Controls.MetroLabel lblErrPrezime;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private MetroFramework.Controls.MetroLabel lblErrIme;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private MetroFramework.Controls.MetroLabel lblErrSSlovo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private MetroFramework.Controls.MetroLabel lblErrRadnoMesto;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private MetroFramework.Controls.MetroLabel LblErrBrojTelefona;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private MetroFramework.Controls.MetroLabel lblErrNorma;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private MetroFramework.Controls.MetroLabel lblErrRacun;
        private MetroFramework.Controls.MetroLabel lblStaz;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private MetroFramework.Controls.MetroLabel lblErrStaz;
        private MetroFramework.Controls.MetroTextBox txtStaz;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private MetroFramework.Controls.MetroLabel lblErrKoef;
        private MetroFramework.Controls.MetroTextBox txtKoeficijent;
        private MetroFramework.Controls.MetroLabel lblKoefRadMesta;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private MetroFramework.Controls.MetroTextBox txtKorisnickoIme;
        private MetroFramework.Controls.MetroComboBox cmbUloga;
        private MetroFramework.Controls.MetroLabel lblKorisnikSaNalogom;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private MetroFramework.Controls.MetroRadioButton radioNe;
        private MetroFramework.Controls.MetroRadioButton radioDa;
        private MetroFramework.Controls.MetroLabel lblKorisnickoIme;
        private MetroFramework.Controls.MetroLabel lblUlogaKorNaloga;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel lblSmena;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroFramework.Controls.MetroButton btnSacuvaj;
        private MetroFramework.Controls.MetroButton btnIzaberiNadredjenog;
        private MetroFramework.Controls.MetroButton btnIzaberiSliku;
        private MetroFramework.Controls.MetroButton btnObrisiNadredjenog;
        private MetroFramework.Controls.MetroButton btnIzaberiRadnoVreme;
    }
}