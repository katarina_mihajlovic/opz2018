﻿namespace SharedComponents.Forms
{
    partial class DodavanjeSmeneForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.tbPonedeljak1 = new MetroFramework.Controls.MetroTextBox();
            this.tbPonedeljak2 = new MetroFramework.Controls.MetroTextBox();
            this.tbUtorak1 = new MetroFramework.Controls.MetroTextBox();
            this.tbUtorak2 = new MetroFramework.Controls.MetroTextBox();
            this.tbSreda1 = new MetroFramework.Controls.MetroTextBox();
            this.tbSreda2 = new MetroFramework.Controls.MetroTextBox();
            this.tbCetvrtak1 = new MetroFramework.Controls.MetroTextBox();
            this.tbCetvrtak2 = new MetroFramework.Controls.MetroTextBox();
            this.tbPetak1 = new MetroFramework.Controls.MetroTextBox();
            this.tbPetak2 = new MetroFramework.Controls.MetroTextBox();
            this.tbSubota1 = new MetroFramework.Controls.MetroTextBox();
            this.tbSubota2 = new MetroFramework.Controls.MetroTextBox();
            this.tbNedelja1 = new MetroFramework.Controls.MetroTextBox();
            this.tbNedelja2 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.tbNazivSmene = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(3, 40);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(73, 19);
            this.metroLabel3.TabIndex = 2;
            this.metroLabel3.Text = "Ponedeljak";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(3, 80);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(48, 19);
            this.metroLabel4.TabIndex = 3;
            this.metroLabel4.Text = "Utorak";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(3, 120);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(43, 19);
            this.metroLabel5.TabIndex = 4;
            this.metroLabel5.Text = "Sreda";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(3, 160);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(57, 19);
            this.metroLabel6.TabIndex = 5;
            this.metroLabel6.Text = "Četvrtak";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(3, 200);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(41, 19);
            this.metroLabel7.TabIndex = 6;
            this.metroLabel7.Text = "Petak";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(3, 240);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(50, 19);
            this.metroLabel8.TabIndex = 7;
            this.metroLabel8.Text = "Subota";
            // 
            // tbPonedeljak1
            // 
            // 
            // 
            // 
            this.tbPonedeljak1.CustomButton.Image = null;
            this.tbPonedeljak1.CustomButton.Location = new System.Drawing.Point(83, 1);
            this.tbPonedeljak1.CustomButton.Name = "";
            this.tbPonedeljak1.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbPonedeljak1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbPonedeljak1.CustomButton.TabIndex = 1;
            this.tbPonedeljak1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbPonedeljak1.CustomButton.UseSelectable = true;
            this.tbPonedeljak1.CustomButton.Visible = false;
            this.tbPonedeljak1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbPonedeljak1.Lines = new string[0];
            this.tbPonedeljak1.Location = new System.Drawing.Point(114, 43);
            this.tbPonedeljak1.MaxLength = 32767;
            this.tbPonedeljak1.Name = "tbPonedeljak1";
            this.tbPonedeljak1.PasswordChar = '\0';
            this.tbPonedeljak1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbPonedeljak1.SelectedText = "";
            this.tbPonedeljak1.SelectionLength = 0;
            this.tbPonedeljak1.SelectionStart = 0;
            this.tbPonedeljak1.ShortcutsEnabled = true;
            this.tbPonedeljak1.Size = new System.Drawing.Size(105, 23);
            this.tbPonedeljak1.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbPonedeljak1.TabIndex = 0;
            this.tbPonedeljak1.UseSelectable = true;
            this.tbPonedeljak1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbPonedeljak1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbPonedeljak1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Vreme_KeyPress);
            // 
            // tbPonedeljak2
            // 
            // 
            // 
            // 
            this.tbPonedeljak2.CustomButton.Image = null;
            this.tbPonedeljak2.CustomButton.Location = new System.Drawing.Point(83, 1);
            this.tbPonedeljak2.CustomButton.Name = "";
            this.tbPonedeljak2.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbPonedeljak2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbPonedeljak2.CustomButton.TabIndex = 1;
            this.tbPonedeljak2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbPonedeljak2.CustomButton.UseSelectable = true;
            this.tbPonedeljak2.CustomButton.Visible = false;
            this.tbPonedeljak2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbPonedeljak2.Lines = new string[0];
            this.tbPonedeljak2.Location = new System.Drawing.Point(225, 43);
            this.tbPonedeljak2.MaxLength = 32767;
            this.tbPonedeljak2.Name = "tbPonedeljak2";
            this.tbPonedeljak2.PasswordChar = '\0';
            this.tbPonedeljak2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbPonedeljak2.SelectedText = "";
            this.tbPonedeljak2.SelectionLength = 0;
            this.tbPonedeljak2.SelectionStart = 0;
            this.tbPonedeljak2.ShortcutsEnabled = true;
            this.tbPonedeljak2.Size = new System.Drawing.Size(105, 23);
            this.tbPonedeljak2.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbPonedeljak2.TabIndex = 1;
            this.tbPonedeljak2.UseSelectable = true;
            this.tbPonedeljak2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbPonedeljak2.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbPonedeljak2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Vreme_KeyPress);
            // 
            // tbUtorak1
            // 
            // 
            // 
            // 
            this.tbUtorak1.CustomButton.Image = null;
            this.tbUtorak1.CustomButton.Location = new System.Drawing.Point(83, 1);
            this.tbUtorak1.CustomButton.Name = "";
            this.tbUtorak1.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbUtorak1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbUtorak1.CustomButton.TabIndex = 1;
            this.tbUtorak1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbUtorak1.CustomButton.UseSelectable = true;
            this.tbUtorak1.CustomButton.Visible = false;
            this.tbUtorak1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbUtorak1.Lines = new string[0];
            this.tbUtorak1.Location = new System.Drawing.Point(114, 83);
            this.tbUtorak1.MaxLength = 32767;
            this.tbUtorak1.Name = "tbUtorak1";
            this.tbUtorak1.PasswordChar = '\0';
            this.tbUtorak1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbUtorak1.SelectedText = "";
            this.tbUtorak1.SelectionLength = 0;
            this.tbUtorak1.SelectionStart = 0;
            this.tbUtorak1.ShortcutsEnabled = true;
            this.tbUtorak1.Size = new System.Drawing.Size(105, 23);
            this.tbUtorak1.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbUtorak1.TabIndex = 2;
            this.tbUtorak1.UseSelectable = true;
            this.tbUtorak1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbUtorak1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbUtorak1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Vreme_KeyPress);
            // 
            // tbUtorak2
            // 
            // 
            // 
            // 
            this.tbUtorak2.CustomButton.Image = null;
            this.tbUtorak2.CustomButton.Location = new System.Drawing.Point(83, 1);
            this.tbUtorak2.CustomButton.Name = "";
            this.tbUtorak2.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbUtorak2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbUtorak2.CustomButton.TabIndex = 1;
            this.tbUtorak2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbUtorak2.CustomButton.UseSelectable = true;
            this.tbUtorak2.CustomButton.Visible = false;
            this.tbUtorak2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbUtorak2.Lines = new string[0];
            this.tbUtorak2.Location = new System.Drawing.Point(225, 83);
            this.tbUtorak2.MaxLength = 32767;
            this.tbUtorak2.Name = "tbUtorak2";
            this.tbUtorak2.PasswordChar = '\0';
            this.tbUtorak2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbUtorak2.SelectedText = "";
            this.tbUtorak2.SelectionLength = 0;
            this.tbUtorak2.SelectionStart = 0;
            this.tbUtorak2.ShortcutsEnabled = true;
            this.tbUtorak2.Size = new System.Drawing.Size(105, 23);
            this.tbUtorak2.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbUtorak2.TabIndex = 3;
            this.tbUtorak2.UseSelectable = true;
            this.tbUtorak2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbUtorak2.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbUtorak2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Vreme_KeyPress);
            // 
            // tbSreda1
            // 
            // 
            // 
            // 
            this.tbSreda1.CustomButton.Image = null;
            this.tbSreda1.CustomButton.Location = new System.Drawing.Point(83, 1);
            this.tbSreda1.CustomButton.Name = "";
            this.tbSreda1.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbSreda1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbSreda1.CustomButton.TabIndex = 1;
            this.tbSreda1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbSreda1.CustomButton.UseSelectable = true;
            this.tbSreda1.CustomButton.Visible = false;
            this.tbSreda1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbSreda1.Lines = new string[0];
            this.tbSreda1.Location = new System.Drawing.Point(114, 123);
            this.tbSreda1.MaxLength = 32767;
            this.tbSreda1.Name = "tbSreda1";
            this.tbSreda1.PasswordChar = '\0';
            this.tbSreda1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbSreda1.SelectedText = "";
            this.tbSreda1.SelectionLength = 0;
            this.tbSreda1.SelectionStart = 0;
            this.tbSreda1.ShortcutsEnabled = true;
            this.tbSreda1.Size = new System.Drawing.Size(105, 23);
            this.tbSreda1.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbSreda1.TabIndex = 4;
            this.tbSreda1.UseSelectable = true;
            this.tbSreda1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbSreda1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbSreda1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Vreme_KeyPress);
            // 
            // tbSreda2
            // 
            // 
            // 
            // 
            this.tbSreda2.CustomButton.Image = null;
            this.tbSreda2.CustomButton.Location = new System.Drawing.Point(83, 1);
            this.tbSreda2.CustomButton.Name = "";
            this.tbSreda2.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbSreda2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbSreda2.CustomButton.TabIndex = 1;
            this.tbSreda2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbSreda2.CustomButton.UseSelectable = true;
            this.tbSreda2.CustomButton.Visible = false;
            this.tbSreda2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbSreda2.Lines = new string[0];
            this.tbSreda2.Location = new System.Drawing.Point(225, 123);
            this.tbSreda2.MaxLength = 32767;
            this.tbSreda2.Name = "tbSreda2";
            this.tbSreda2.PasswordChar = '\0';
            this.tbSreda2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbSreda2.SelectedText = "";
            this.tbSreda2.SelectionLength = 0;
            this.tbSreda2.SelectionStart = 0;
            this.tbSreda2.ShortcutsEnabled = true;
            this.tbSreda2.Size = new System.Drawing.Size(105, 23);
            this.tbSreda2.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbSreda2.TabIndex = 5;
            this.tbSreda2.UseSelectable = true;
            this.tbSreda2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbSreda2.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbSreda2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Vreme_KeyPress);
            // 
            // tbCetvrtak1
            // 
            // 
            // 
            // 
            this.tbCetvrtak1.CustomButton.Image = null;
            this.tbCetvrtak1.CustomButton.Location = new System.Drawing.Point(83, 1);
            this.tbCetvrtak1.CustomButton.Name = "";
            this.tbCetvrtak1.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbCetvrtak1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbCetvrtak1.CustomButton.TabIndex = 1;
            this.tbCetvrtak1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbCetvrtak1.CustomButton.UseSelectable = true;
            this.tbCetvrtak1.CustomButton.Visible = false;
            this.tbCetvrtak1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbCetvrtak1.Lines = new string[0];
            this.tbCetvrtak1.Location = new System.Drawing.Point(114, 163);
            this.tbCetvrtak1.MaxLength = 32767;
            this.tbCetvrtak1.Name = "tbCetvrtak1";
            this.tbCetvrtak1.PasswordChar = '\0';
            this.tbCetvrtak1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbCetvrtak1.SelectedText = "";
            this.tbCetvrtak1.SelectionLength = 0;
            this.tbCetvrtak1.SelectionStart = 0;
            this.tbCetvrtak1.ShortcutsEnabled = true;
            this.tbCetvrtak1.Size = new System.Drawing.Size(105, 23);
            this.tbCetvrtak1.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbCetvrtak1.TabIndex = 6;
            this.tbCetvrtak1.UseSelectable = true;
            this.tbCetvrtak1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbCetvrtak1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbCetvrtak1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Vreme_KeyPress);
            // 
            // tbCetvrtak2
            // 
            // 
            // 
            // 
            this.tbCetvrtak2.CustomButton.Image = null;
            this.tbCetvrtak2.CustomButton.Location = new System.Drawing.Point(83, 1);
            this.tbCetvrtak2.CustomButton.Name = "";
            this.tbCetvrtak2.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbCetvrtak2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbCetvrtak2.CustomButton.TabIndex = 1;
            this.tbCetvrtak2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbCetvrtak2.CustomButton.UseSelectable = true;
            this.tbCetvrtak2.CustomButton.Visible = false;
            this.tbCetvrtak2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbCetvrtak2.Lines = new string[0];
            this.tbCetvrtak2.Location = new System.Drawing.Point(225, 163);
            this.tbCetvrtak2.MaxLength = 32767;
            this.tbCetvrtak2.Name = "tbCetvrtak2";
            this.tbCetvrtak2.PasswordChar = '\0';
            this.tbCetvrtak2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbCetvrtak2.SelectedText = "";
            this.tbCetvrtak2.SelectionLength = 0;
            this.tbCetvrtak2.SelectionStart = 0;
            this.tbCetvrtak2.ShortcutsEnabled = true;
            this.tbCetvrtak2.Size = new System.Drawing.Size(105, 23);
            this.tbCetvrtak2.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbCetvrtak2.TabIndex = 7;
            this.tbCetvrtak2.UseSelectable = true;
            this.tbCetvrtak2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbCetvrtak2.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbCetvrtak2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Vreme_KeyPress);
            // 
            // tbPetak1
            // 
            // 
            // 
            // 
            this.tbPetak1.CustomButton.Image = null;
            this.tbPetak1.CustomButton.Location = new System.Drawing.Point(83, 1);
            this.tbPetak1.CustomButton.Name = "";
            this.tbPetak1.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbPetak1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbPetak1.CustomButton.TabIndex = 1;
            this.tbPetak1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbPetak1.CustomButton.UseSelectable = true;
            this.tbPetak1.CustomButton.Visible = false;
            this.tbPetak1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbPetak1.Lines = new string[0];
            this.tbPetak1.Location = new System.Drawing.Point(114, 203);
            this.tbPetak1.MaxLength = 32767;
            this.tbPetak1.Name = "tbPetak1";
            this.tbPetak1.PasswordChar = '\0';
            this.tbPetak1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbPetak1.SelectedText = "";
            this.tbPetak1.SelectionLength = 0;
            this.tbPetak1.SelectionStart = 0;
            this.tbPetak1.ShortcutsEnabled = true;
            this.tbPetak1.Size = new System.Drawing.Size(105, 23);
            this.tbPetak1.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbPetak1.TabIndex = 8;
            this.tbPetak1.UseSelectable = true;
            this.tbPetak1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbPetak1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbPetak1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Vreme_KeyPress);
            // 
            // tbPetak2
            // 
            // 
            // 
            // 
            this.tbPetak2.CustomButton.Image = null;
            this.tbPetak2.CustomButton.Location = new System.Drawing.Point(83, 1);
            this.tbPetak2.CustomButton.Name = "";
            this.tbPetak2.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbPetak2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbPetak2.CustomButton.TabIndex = 1;
            this.tbPetak2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbPetak2.CustomButton.UseSelectable = true;
            this.tbPetak2.CustomButton.Visible = false;
            this.tbPetak2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbPetak2.Lines = new string[0];
            this.tbPetak2.Location = new System.Drawing.Point(225, 203);
            this.tbPetak2.MaxLength = 32767;
            this.tbPetak2.Name = "tbPetak2";
            this.tbPetak2.PasswordChar = '\0';
            this.tbPetak2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbPetak2.SelectedText = "";
            this.tbPetak2.SelectionLength = 0;
            this.tbPetak2.SelectionStart = 0;
            this.tbPetak2.ShortcutsEnabled = true;
            this.tbPetak2.Size = new System.Drawing.Size(105, 23);
            this.tbPetak2.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbPetak2.TabIndex = 9;
            this.tbPetak2.UseSelectable = true;
            this.tbPetak2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbPetak2.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbPetak2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Vreme_KeyPress);
            // 
            // tbSubota1
            // 
            // 
            // 
            // 
            this.tbSubota1.CustomButton.Image = null;
            this.tbSubota1.CustomButton.Location = new System.Drawing.Point(83, 1);
            this.tbSubota1.CustomButton.Name = "";
            this.tbSubota1.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbSubota1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbSubota1.CustomButton.TabIndex = 1;
            this.tbSubota1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbSubota1.CustomButton.UseSelectable = true;
            this.tbSubota1.CustomButton.Visible = false;
            this.tbSubota1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbSubota1.Lines = new string[0];
            this.tbSubota1.Location = new System.Drawing.Point(114, 243);
            this.tbSubota1.MaxLength = 32767;
            this.tbSubota1.Name = "tbSubota1";
            this.tbSubota1.PasswordChar = '\0';
            this.tbSubota1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbSubota1.SelectedText = "";
            this.tbSubota1.SelectionLength = 0;
            this.tbSubota1.SelectionStart = 0;
            this.tbSubota1.ShortcutsEnabled = true;
            this.tbSubota1.Size = new System.Drawing.Size(105, 23);
            this.tbSubota1.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbSubota1.TabIndex = 10;
            this.tbSubota1.UseSelectable = true;
            this.tbSubota1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbSubota1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbSubota1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Vreme_KeyPress);
            // 
            // tbSubota2
            // 
            // 
            // 
            // 
            this.tbSubota2.CustomButton.Image = null;
            this.tbSubota2.CustomButton.Location = new System.Drawing.Point(83, 1);
            this.tbSubota2.CustomButton.Name = "";
            this.tbSubota2.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbSubota2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbSubota2.CustomButton.TabIndex = 1;
            this.tbSubota2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbSubota2.CustomButton.UseSelectable = true;
            this.tbSubota2.CustomButton.Visible = false;
            this.tbSubota2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbSubota2.Lines = new string[0];
            this.tbSubota2.Location = new System.Drawing.Point(225, 243);
            this.tbSubota2.MaxLength = 32767;
            this.tbSubota2.Name = "tbSubota2";
            this.tbSubota2.PasswordChar = '\0';
            this.tbSubota2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbSubota2.SelectedText = "";
            this.tbSubota2.SelectionLength = 0;
            this.tbSubota2.SelectionStart = 0;
            this.tbSubota2.ShortcutsEnabled = true;
            this.tbSubota2.Size = new System.Drawing.Size(105, 23);
            this.tbSubota2.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbSubota2.TabIndex = 11;
            this.tbSubota2.UseSelectable = true;
            this.tbSubota2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbSubota2.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbSubota2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Vreme_KeyPress);
            // 
            // tbNedelja1
            // 
            // 
            // 
            // 
            this.tbNedelja1.CustomButton.Image = null;
            this.tbNedelja1.CustomButton.Location = new System.Drawing.Point(83, 1);
            this.tbNedelja1.CustomButton.Name = "";
            this.tbNedelja1.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbNedelja1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbNedelja1.CustomButton.TabIndex = 1;
            this.tbNedelja1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbNedelja1.CustomButton.UseSelectable = true;
            this.tbNedelja1.CustomButton.Visible = false;
            this.tbNedelja1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbNedelja1.Lines = new string[0];
            this.tbNedelja1.Location = new System.Drawing.Point(114, 283);
            this.tbNedelja1.MaxLength = 32767;
            this.tbNedelja1.Name = "tbNedelja1";
            this.tbNedelja1.PasswordChar = '\0';
            this.tbNedelja1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbNedelja1.SelectedText = "";
            this.tbNedelja1.SelectionLength = 0;
            this.tbNedelja1.SelectionStart = 0;
            this.tbNedelja1.ShortcutsEnabled = true;
            this.tbNedelja1.Size = new System.Drawing.Size(105, 23);
            this.tbNedelja1.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbNedelja1.TabIndex = 12;
            this.tbNedelja1.UseSelectable = true;
            this.tbNedelja1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbNedelja1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbNedelja1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Vreme_KeyPress);
            // 
            // tbNedelja2
            // 
            // 
            // 
            // 
            this.tbNedelja2.CustomButton.Image = null;
            this.tbNedelja2.CustomButton.Location = new System.Drawing.Point(83, 1);
            this.tbNedelja2.CustomButton.Name = "";
            this.tbNedelja2.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbNedelja2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbNedelja2.CustomButton.TabIndex = 1;
            this.tbNedelja2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbNedelja2.CustomButton.UseSelectable = true;
            this.tbNedelja2.CustomButton.Visible = false;
            this.tbNedelja2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbNedelja2.Lines = new string[0];
            this.tbNedelja2.Location = new System.Drawing.Point(225, 283);
            this.tbNedelja2.MaxLength = 32767;
            this.tbNedelja2.Name = "tbNedelja2";
            this.tbNedelja2.PasswordChar = '\0';
            this.tbNedelja2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbNedelja2.SelectedText = "";
            this.tbNedelja2.SelectionLength = 0;
            this.tbNedelja2.SelectionStart = 0;
            this.tbNedelja2.ShortcutsEnabled = true;
            this.tbNedelja2.Size = new System.Drawing.Size(105, 23);
            this.tbNedelja2.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbNedelja2.TabIndex = 13;
            this.tbNedelja2.UseSelectable = true;
            this.tbNedelja2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbNedelja2.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbNedelja2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Vreme_KeyPress);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(3, 280);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(54, 19);
            this.metroLabel9.TabIndex = 21;
            this.metroLabel9.Text = "Nedelja";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(18, 60);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(249, 19);
            this.metroLabel10.TabIndex = 1;
            this.metroLabel10.Text = "Vremena treba unositi u formatu HH:MM";
            // 
            // metroButton2
            // 
            this.metroButton2.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton2.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.metroButton2.Location = new System.Drawing.Point(207, 486);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(149, 26);
            this.metroButton2.TabIndex = 15;
            this.metroButton2.Text = "Dodaj smenu";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(21, 452);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(86, 19);
            this.metroLabel11.TabIndex = 8;
            this.metroLabel11.Text = "Naziv smene:";
            // 
            // tbNazivSmene
            // 
            // 
            // 
            // 
            this.tbNazivSmene.CustomButton.Image = null;
            this.tbNazivSmene.CustomButton.Location = new System.Drawing.Point(198, 1);
            this.tbNazivSmene.CustomButton.Name = "";
            this.tbNazivSmene.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbNazivSmene.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbNazivSmene.CustomButton.TabIndex = 1;
            this.tbNazivSmene.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbNazivSmene.CustomButton.UseSelectable = true;
            this.tbNazivSmene.CustomButton.Visible = false;
            this.tbNazivSmene.Lines = new string[0];
            this.tbNazivSmene.Location = new System.Drawing.Point(136, 448);
            this.tbNazivSmene.MaxLength = 32767;
            this.tbNazivSmene.Name = "tbNazivSmene";
            this.tbNazivSmene.PasswordChar = '\0';
            this.tbNazivSmene.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbNazivSmene.SelectedText = "";
            this.tbNazivSmene.SelectionLength = 0;
            this.tbNazivSmene.SelectionStart = 0;
            this.tbNazivSmene.ShortcutsEnabled = true;
            this.tbNazivSmene.Size = new System.Drawing.Size(220, 23);
            this.tbNazivSmene.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbNazivSmene.TabIndex = 14;
            this.tbNazivSmene.UseSelectable = true;
            this.tbNazivSmene.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbNazivSmene.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(18, 79);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(223, 19);
            this.metroLabel12.TabIndex = 10;
            this.metroLabel12.Text = "Za neradan dan polja ostaviti prazna";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.tbNedelja2, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this.tbSubota2, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.tbPetak2, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.tbSreda2, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.tbUtorak2, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.tbPonedeljak2, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.tbCetvrtak2, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel4, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel5, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.tbPonedeljak1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.tbUtorak1, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.tbSreda1, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.tbPetak1, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.tbSubota1, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.tbNedelja1, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.tbCetvrtak1, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel8, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel7, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel6, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel9, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel2, 2, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(23, 112);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(333, 317);
            this.tableLayoutPanel2.TabIndex = 11;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(114, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(97, 19);
            this.metroLabel1.TabIndex = 22;
            this.metroLabel1.Text = "Početak smene";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(225, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(73, 19);
            this.metroLabel2.TabIndex = 23;
            this.metroLabel2.Text = "Kraj smene";
            // 
            // DodavanjeSmeneForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 520);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.metroLabel12);
            this.Controls.Add(this.tbNazivSmene);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.metroButton2);
            this.Controls.Add(this.metroLabel10);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(373, 520);
            this.MinimumSize = new System.Drawing.Size(373, 520);
            this.Name = "DodavanjeSmeneForm";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Purple;
            this.Text = "Dodavanje smene";
            this.Load += new System.EventHandler(this.DodavanjeSmeneForm_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox tbCetvrtak2;
        private MetroFramework.Controls.MetroTextBox tbPonedeljak1;
        private MetroFramework.Controls.MetroTextBox tbPonedeljak2;
        private MetroFramework.Controls.MetroTextBox tbUtorak1;
        private MetroFramework.Controls.MetroTextBox tbUtorak2;
        private MetroFramework.Controls.MetroTextBox tbSreda1;
        private MetroFramework.Controls.MetroTextBox tbSreda2;
        private MetroFramework.Controls.MetroTextBox tbCetvrtak1;
        private MetroFramework.Controls.MetroTextBox tbPetak1;
        private MetroFramework.Controls.MetroTextBox tbPetak2;
        private MetroFramework.Controls.MetroTextBox tbSubota1;
        private MetroFramework.Controls.MetroTextBox tbSubota2;
        private MetroFramework.Controls.MetroTextBox tbNedelja1;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroTextBox tbNedelja2;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroTextBox tbNazivSmene;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
    }
}