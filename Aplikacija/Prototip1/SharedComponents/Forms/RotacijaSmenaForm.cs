﻿using MetroFramework;
using MetroFramework.Forms;
using Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SharedComponents.Forms
{
    public partial class RotacijaSmenaForm : MetroForm
    {
        public RotacijaSmenaForm()
        {
            InitializeComponent();
        }

        private void RotacijaSmenaForm_Load(object sender, EventArgs e)
        {
            dgv1.AutoGenerateColumns = false;
            dgv2.AutoGenerateColumns = false;

            using (OpzContext kontekst = new OpzContext())
            {

                var i = (from p in kontekst.ListaSmena select p.TipSmene).Distinct().OrderBy(x => x.ToUpper()).ToList();


                var col = i.ToList();


                List<SmenaPrint> smene = new List<SmenaPrint>();

                foreach (string smena in col)
                {
                    DateTime maxDatum = (from p in kontekst.ListaSmena where p.TipSmene == smena select p.DatumDonosenja).Max();
                    List<Smena> poDanima = (from p in kontekst.ListaSmena where p.DatumDonosenja == maxDatum && p.TipSmene == smena select p).ToList();

                    SmenaPrint current = new SmenaPrint();
                    current.NazivSmene = smena;
                    foreach (Smena a in poDanima)
                    {
                        if (a.DanUNedelji == "ponedeljak")
                        {
                            current.Ponedeljak = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "utorak")
                        {
                            current.Utorak = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "sreda")
                        {
                            current.Sreda = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "cetvrtak")
                        {
                            current.Cetvrtak = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "petak")
                        {
                            current.Petak = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "subota")
                        {
                            current.Subota = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "nedelja")
                        {
                            current.Nedelja = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                    }
                    //sad za neradne dane...
                    if (current.Ponedeljak == null || current.Ponedeljak.Equals(""))
                    {
                        current.Ponedeljak = "Neradan dan";
                    }
                    if (current.Utorak == null || current.Ponedeljak.Equals(""))
                    {
                        current.Utorak = "Neradan dan";
                    }
                    if (current.Sreda == null || current.Ponedeljak.Equals(""))
                    {
                        current.Sreda = "Neradan dan";
                    }
                    if (current.Cetvrtak == null || current.Ponedeljak.Equals(""))
                    {
                        current.Cetvrtak = "Neradan dan";
                    }
                    if (current.Petak == null || current.Ponedeljak.Equals(""))
                    {
                        current.Petak = "Neradan dan";
                    }
                    if (current.Subota == null || current.Ponedeljak.Equals(""))
                    {
                        current.Subota = "Neradan dan";
                    }
                    if (current.Nedelja == null || current.Ponedeljak.Equals(""))
                    {
                        current.Nedelja = "Neradan dan";
                    }

                    smene.Add(current);
                }

                foreach (SmenaPrint p in smene)
                {
                    dgv1.Rows.Add(p.NazivSmene, p.Ponedeljak, p.Utorak, p.Sreda, p.Cetvrtak, p.Petak, p.Subota, p.Nedelja);
                    dgv2.Rows.Add(p.NazivSmene, p.Ponedeljak, p.Utorak, p.Sreda, p.Cetvrtak, p.Petak, p.Subota, p.Nedelja);
                }

                foreach (DataGridViewColumn dgvcol in dgv1.Columns)
                {
                    dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
                foreach (DataGridViewColumn dgvcol in dgv2.Columns)
                {
                    dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

        private class SmenaPrint
        {
            public string NazivSmene { get; set; }
            public string Ponedeljak { get; set; }
            public string Utorak { get; set; }
            public string Sreda { get; set; }
            public string Cetvrtak { get; set; }
            public string Petak { get; set; }
            public string Subota { get; set; }
            public string Nedelja { get; set; }

            public SmenaPrint()
            {

            }
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            //zamena smena
            string smena1 = dgv1.SelectedRows[0].Cells[0].Value.ToString();
            string smena2 = dgv2.SelectedRows[0].Cells[0].Value.ToString();
            DateTime trenutni = DateTime.Now;

            if(smena1 == smena2)
            {
                MetroMessageBox.Show(this, "Treba izabrati različite smene.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
                return;
            }

            using (OpzContext kontekst = new OpzContext())
            {

                DateTime maxDatum1 = (from p in kontekst.ListaSmena where p.TipSmene == smena1 select p.DatumDonosenja).Max();
                List<Smena> poDanima1 = (from p in kontekst.ListaSmena where p.DatumDonosenja == maxDatum1 && p.TipSmene == smena1 select p).ToList();

                DateTime maxDatum2 = (from p in kontekst.ListaSmena where p.TipSmene == smena2 select p.DatumDonosenja).Max();
                List<Smena> poDanima2 = (from p in kontekst.ListaSmena where p.DatumDonosenja == maxDatum2 && p.TipSmene == smena2 select p).ToList();

                foreach(var a in poDanima1)
                {
                    Smena s = new Smena();
                    s.DanUNedelji = a.DanUNedelji;
                    s.DatumDonosenja = trenutni;
                    s.Kraj = a.Kraj;
                    s.Pocetak = a.Pocetak;
                    s.TipSmene = smena2;
                    kontekst.ListaSmena.Add(s);
                }

                foreach (var a in poDanima2)
                {
                    Smena s = new Smena();
                    s.DanUNedelji = a.DanUNedelji;
                    s.DatumDonosenja = trenutni;
                    s.Kraj = a.Kraj;
                    s.Pocetak = a.Pocetak;
                    s.TipSmene = smena1;
                    kontekst.ListaSmena.Add(s);
                }

                kontekst.SaveChanges();

                MetroMessageBox.Show(this, "Smene su uspešno zamenjene.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
            }
            string nazivPrve = dgv1.SelectedRows[0].Cells[0].Value.ToString();
            string nazivDruge = dgv2.SelectedRows[0].Cells[0].Value.ToString();

            
            foreach (DataGridViewRow r in dgv1.Rows)
            {
                if (r.Cells[0].Value.ToString() == nazivDruge)
                {
                    r.Cells[0].Value = nazivPrve;
                    continue;
                }
            }
            dgv1.SelectedRows[0].Cells[0].Value = nazivDruge;
            
            foreach (DataGridViewRow r in dgv2.Rows)
            {
                if (r.Cells[0].Value.ToString() == nazivPrve)
                {
                    r.Cells[0].Value = nazivDruge;
                    continue;
                }
            }
            dgv2.SelectedRows[0].Cells[0].Value = nazivPrve;
        }
    }
}

