﻿namespace SharedComponents.Forms
{
    partial class NoviZaposleniForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.neodKoeficijentRadnogMesta = new MetroFramework.Controls.MetroLabel();
            this.tbKoeficijentRadnogMesta = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.neodPocetniStaz = new MetroFramework.Controls.MetroLabel();
            this.tbPocetniStaz = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.neodNormaRada = new MetroFramework.Controls.MetroLabel();
            this.tbNormaRada = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.neodTekuciRacun = new MetroFramework.Controls.MetroLabel();
            this.tbTekuciRacun = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.NeodRadnoMesto = new MetroFramework.Controls.MetroLabel();
            this.tbRadnoMesto = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.neodBrojTelefona = new MetroFramework.Controls.MetroLabel();
            this.tbBrojTelefona = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.neodMestoRodjenja = new MetroFramework.Controls.MetroLabel();
            this.tbMestoRodjenja = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.neodAdresa = new MetroFramework.Controls.MetroLabel();
            this.tbAdresa = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.neodPrezime = new MetroFramework.Controls.MetroLabel();
            this.tbPrezime = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.neodSrednjeSlovo = new MetroFramework.Controls.MetroLabel();
            this.tbSrednjeSlovo = new MetroFramework.Controls.MetroTextBox();
            this.lblIme = new MetroFramework.Controls.MetroLabel();
            this.lblSrednjeSlovo = new MetroFramework.Controls.MetroLabel();
            this.lblPrezime = new MetroFramework.Controls.MetroLabel();
            this.lblDatumRodjenja = new MetroFramework.Controls.MetroLabel();
            this.lblMestoRodjenja = new MetroFramework.Controls.MetroLabel();
            this.lblBrojTelefona = new MetroFramework.Controls.MetroLabel();
            this.lblRadnoMesto = new MetroFramework.Controls.MetroLabel();
            this.lblTekuciRacun = new MetroFramework.Controls.MetroLabel();
            this.lblAdresa = new MetroFramework.Controls.MetroLabel();
            this.dtpDatumRodjenja = new MetroFramework.Controls.MetroDateTime();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.neodIme = new MetroFramework.Controls.MetroLabel();
            this.tbIme = new MetroFramework.Controls.MetroTextBox();
            this.lblNormaRada = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.metroButton5 = new MetroFramework.Controls.MetroButton();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.lblNadredjeni = new MetroFramework.Controls.MetroLabel();
            this.Nadređeni = new MetroFramework.Controls.MetroLabel();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.lblSmena = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroButton4 = new MetroFramework.Controls.MetroButton();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.lbKorisnickoIme = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.rbNalogDa = new MetroFramework.Controls.MetroRadioButton();
            this.rbNalogNe = new MetroFramework.Controls.MetroRadioButton();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.cbUlogaKorisnickogNaloga = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.tbLozinka = new MetroFramework.Controls.MetroTextBox();
            this.neodgovarajuciUnosLozinka = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.neodgovarajuciUnosPonovljenaLozinka = new MetroFramework.Controls.MetroLabel();
            this.tbPonovljenaLozinka = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.neodgovarajuciUnosKorisnickoIme = new MetroFramework.Controls.MetroLabel();
            this.tbKorisnickoIme = new MetroFramework.Controls.MetroTextBox();
            this.ribbonTextBox1 = new System.Windows.Forms.RibbonTextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(20, 60);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(915, 526);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel23, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel22, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel21, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel20, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel19, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel18, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel17, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel16, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel15, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel14, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel13, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel12, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblIme, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblSrednjeSlovo, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblPrezime, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblDatumRodjenja, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblMestoRodjenja, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblBrojTelefona, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblRadnoMesto, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblTekuciRacun, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.lblAdresa, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.dtpDatumRodjenja, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel11, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblNormaRada, 0, 9);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 12;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(451, 520);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 1;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel23.Controls.Add(this.metroLabel7, 0, 0);
            this.tableLayoutPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel23.Location = new System.Drawing.Point(0, 473);
            this.tableLayoutPanel23.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 2;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(157, 47);
            this.tableLayoutPanel23.TabIndex = 35;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(3, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(127, 19);
            this.metroLabel7.TabIndex = 11;
            this.metroLabel7.Text = "Koef. radnog mesta:";
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 1;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel22.Controls.Add(this.neodKoeficijentRadnogMesta, 0, 1);
            this.tableLayoutPanel22.Controls.Add(this.tbKoeficijentRadnogMesta, 0, 0);
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(157, 473);
            this.tableLayoutPanel22.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 2;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(294, 47);
            this.tableLayoutPanel22.TabIndex = 34;
            // 
            // neodKoeficijentRadnogMesta
            // 
            this.neodKoeficijentRadnogMesta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.neodKoeficijentRadnogMesta.AutoSize = true;
            this.neodKoeficijentRadnogMesta.FontSize = MetroFramework.MetroLabelSize.Small;
            this.neodKoeficijentRadnogMesta.Location = new System.Drawing.Point(209, 32);
            this.neodKoeficijentRadnogMesta.Margin = new System.Windows.Forms.Padding(0);
            this.neodKoeficijentRadnogMesta.Name = "neodKoeficijentRadnogMesta";
            this.neodKoeficijentRadnogMesta.Size = new System.Drawing.Size(85, 15);
            this.neodKoeficijentRadnogMesta.Style = MetroFramework.MetroColorStyle.Red;
            this.neodKoeficijentRadnogMesta.TabIndex = 1;
            this.neodKoeficijentRadnogMesta.Text = "Obavezan unos";
            this.neodKoeficijentRadnogMesta.UseStyleColors = true;
            this.neodKoeficijentRadnogMesta.Visible = false;
            // 
            // tbKoeficijentRadnogMesta
            // 
            // 
            // 
            // 
            this.tbKoeficijentRadnogMesta.CustomButton.Image = null;
            this.tbKoeficijentRadnogMesta.CustomButton.Location = new System.Drawing.Point(264, 2);
            this.tbKoeficijentRadnogMesta.CustomButton.Name = "";
            this.tbKoeficijentRadnogMesta.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbKoeficijentRadnogMesta.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbKoeficijentRadnogMesta.CustomButton.TabIndex = 1;
            this.tbKoeficijentRadnogMesta.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbKoeficijentRadnogMesta.CustomButton.UseSelectable = true;
            this.tbKoeficijentRadnogMesta.CustomButton.Visible = false;
            this.tbKoeficijentRadnogMesta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbKoeficijentRadnogMesta.Lines = new string[0];
            this.tbKoeficijentRadnogMesta.Location = new System.Drawing.Point(3, 3);
            this.tbKoeficijentRadnogMesta.MaxLength = 32767;
            this.tbKoeficijentRadnogMesta.Name = "tbKoeficijentRadnogMesta";
            this.tbKoeficijentRadnogMesta.PasswordChar = '\0';
            this.tbKoeficijentRadnogMesta.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbKoeficijentRadnogMesta.SelectedText = "";
            this.tbKoeficijentRadnogMesta.SelectionLength = 0;
            this.tbKoeficijentRadnogMesta.SelectionStart = 0;
            this.tbKoeficijentRadnogMesta.ShortcutsEnabled = true;
            this.tbKoeficijentRadnogMesta.Size = new System.Drawing.Size(288, 26);
            this.tbKoeficijentRadnogMesta.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbKoeficijentRadnogMesta.TabIndex = 11;
            this.tbKoeficijentRadnogMesta.UseSelectable = true;
            this.tbKoeficijentRadnogMesta.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbKoeficijentRadnogMesta.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbKoeficijentRadnogMesta.Click += new System.EventHandler(this.tbKoeficijentRadnogMesta_Click);
            this.tbKoeficijentRadnogMesta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbKoeficijentRadnogMesta_KeyPress);
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 1;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.Controls.Add(this.metroLabel5, 0, 0);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(0, 430);
            this.tableLayoutPanel21.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 2;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(157, 43);
            this.tableLayoutPanel21.TabIndex = 33;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(3, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(81, 19);
            this.metroLabel5.TabIndex = 10;
            this.metroLabel5.Text = "Početni staž:";
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 1;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.Controls.Add(this.neodPocetniStaz, 0, 1);
            this.tableLayoutPanel20.Controls.Add(this.tbPocetniStaz, 0, 0);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(157, 430);
            this.tableLayoutPanel20.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 2;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(294, 43);
            this.tableLayoutPanel20.TabIndex = 32;
            // 
            // neodPocetniStaz
            // 
            this.neodPocetniStaz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.neodPocetniStaz.AutoSize = true;
            this.neodPocetniStaz.FontSize = MetroFramework.MetroLabelSize.Small;
            this.neodPocetniStaz.Location = new System.Drawing.Point(209, 30);
            this.neodPocetniStaz.Margin = new System.Windows.Forms.Padding(0);
            this.neodPocetniStaz.Name = "neodPocetniStaz";
            this.neodPocetniStaz.Size = new System.Drawing.Size(85, 13);
            this.neodPocetniStaz.Style = MetroFramework.MetroColorStyle.Red;
            this.neodPocetniStaz.TabIndex = 1;
            this.neodPocetniStaz.Text = "Obavezan unos";
            this.neodPocetniStaz.UseStyleColors = true;
            this.neodPocetniStaz.Visible = false;
            // 
            // tbPocetniStaz
            // 
            // 
            // 
            // 
            this.tbPocetniStaz.CustomButton.Image = null;
            this.tbPocetniStaz.CustomButton.Location = new System.Drawing.Point(266, 2);
            this.tbPocetniStaz.CustomButton.Name = "";
            this.tbPocetniStaz.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.tbPocetniStaz.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbPocetniStaz.CustomButton.TabIndex = 1;
            this.tbPocetniStaz.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbPocetniStaz.CustomButton.UseSelectable = true;
            this.tbPocetniStaz.CustomButton.Visible = false;
            this.tbPocetniStaz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPocetniStaz.Lines = new string[0];
            this.tbPocetniStaz.Location = new System.Drawing.Point(3, 3);
            this.tbPocetniStaz.MaxLength = 32767;
            this.tbPocetniStaz.Name = "tbPocetniStaz";
            this.tbPocetniStaz.PasswordChar = '\0';
            this.tbPocetniStaz.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbPocetniStaz.SelectedText = "";
            this.tbPocetniStaz.SelectionLength = 0;
            this.tbPocetniStaz.SelectionStart = 0;
            this.tbPocetniStaz.ShortcutsEnabled = true;
            this.tbPocetniStaz.Size = new System.Drawing.Size(288, 24);
            this.tbPocetniStaz.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbPocetniStaz.TabIndex = 10;
            this.tbPocetniStaz.UseSelectable = true;
            this.tbPocetniStaz.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbPocetniStaz.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbPocetniStaz.Click += new System.EventHandler(this.tbPocetniStaz_Click);
            this.tbPocetniStaz.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPocetniStaz_KeyPress);
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 1;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Controls.Add(this.neodNormaRada, 0, 1);
            this.tableLayoutPanel19.Controls.Add(this.tbNormaRada, 0, 0);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(157, 387);
            this.tableLayoutPanel19.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 2;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(294, 43);
            this.tableLayoutPanel19.TabIndex = 30;
            // 
            // neodNormaRada
            // 
            this.neodNormaRada.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.neodNormaRada.AutoSize = true;
            this.neodNormaRada.FontSize = MetroFramework.MetroLabelSize.Small;
            this.neodNormaRada.Location = new System.Drawing.Point(209, 30);
            this.neodNormaRada.Margin = new System.Windows.Forms.Padding(0);
            this.neodNormaRada.Name = "neodNormaRada";
            this.neodNormaRada.Size = new System.Drawing.Size(85, 13);
            this.neodNormaRada.Style = MetroFramework.MetroColorStyle.Red;
            this.neodNormaRada.TabIndex = 1;
            this.neodNormaRada.Text = "Obavezan unos";
            this.neodNormaRada.UseStyleColors = true;
            this.neodNormaRada.Visible = false;
            // 
            // tbNormaRada
            // 
            // 
            // 
            // 
            this.tbNormaRada.CustomButton.Image = null;
            this.tbNormaRada.CustomButton.Location = new System.Drawing.Point(266, 2);
            this.tbNormaRada.CustomButton.Name = "";
            this.tbNormaRada.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.tbNormaRada.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbNormaRada.CustomButton.TabIndex = 1;
            this.tbNormaRada.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbNormaRada.CustomButton.UseSelectable = true;
            this.tbNormaRada.CustomButton.Visible = false;
            this.tbNormaRada.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbNormaRada.Lines = new string[0];
            this.tbNormaRada.Location = new System.Drawing.Point(3, 3);
            this.tbNormaRada.MaxLength = 32767;
            this.tbNormaRada.Name = "tbNormaRada";
            this.tbNormaRada.PasswordChar = '\0';
            this.tbNormaRada.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbNormaRada.SelectedText = "";
            this.tbNormaRada.SelectionLength = 0;
            this.tbNormaRada.SelectionStart = 0;
            this.tbNormaRada.ShortcutsEnabled = true;
            this.tbNormaRada.Size = new System.Drawing.Size(288, 24);
            this.tbNormaRada.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbNormaRada.TabIndex = 9;
            this.tbNormaRada.UseSelectable = true;
            this.tbNormaRada.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbNormaRada.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbNormaRada.Click += new System.EventHandler(this.tbNormaRada_Click);
            this.tbNormaRada.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNormaRada_KeyPress);
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 1;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Controls.Add(this.neodTekuciRacun, 0, 1);
            this.tableLayoutPanel18.Controls.Add(this.tbTekuciRacun, 0, 0);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(157, 344);
            this.tableLayoutPanel18.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 2;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(294, 43);
            this.tableLayoutPanel18.TabIndex = 29;
            // 
            // neodTekuciRacun
            // 
            this.neodTekuciRacun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.neodTekuciRacun.AutoSize = true;
            this.neodTekuciRacun.FontSize = MetroFramework.MetroLabelSize.Small;
            this.neodTekuciRacun.Location = new System.Drawing.Point(209, 30);
            this.neodTekuciRacun.Margin = new System.Windows.Forms.Padding(0);
            this.neodTekuciRacun.Name = "neodTekuciRacun";
            this.neodTekuciRacun.Size = new System.Drawing.Size(85, 13);
            this.neodTekuciRacun.Style = MetroFramework.MetroColorStyle.Red;
            this.neodTekuciRacun.TabIndex = 1;
            this.neodTekuciRacun.Text = "Obavezan unos";
            this.neodTekuciRacun.UseStyleColors = true;
            this.neodTekuciRacun.Visible = false;
            // 
            // tbTekuciRacun
            // 
            // 
            // 
            // 
            this.tbTekuciRacun.CustomButton.Image = null;
            this.tbTekuciRacun.CustomButton.Location = new System.Drawing.Point(266, 2);
            this.tbTekuciRacun.CustomButton.Name = "";
            this.tbTekuciRacun.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.tbTekuciRacun.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbTekuciRacun.CustomButton.TabIndex = 1;
            this.tbTekuciRacun.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbTekuciRacun.CustomButton.UseSelectable = true;
            this.tbTekuciRacun.CustomButton.Visible = false;
            this.tbTekuciRacun.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbTekuciRacun.Lines = new string[0];
            this.tbTekuciRacun.Location = new System.Drawing.Point(3, 3);
            this.tbTekuciRacun.MaxLength = 32767;
            this.tbTekuciRacun.Name = "tbTekuciRacun";
            this.tbTekuciRacun.PasswordChar = '\0';
            this.tbTekuciRacun.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbTekuciRacun.SelectedText = "";
            this.tbTekuciRacun.SelectionLength = 0;
            this.tbTekuciRacun.SelectionStart = 0;
            this.tbTekuciRacun.ShortcutsEnabled = true;
            this.tbTekuciRacun.Size = new System.Drawing.Size(288, 24);
            this.tbTekuciRacun.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbTekuciRacun.TabIndex = 8;
            this.tbTekuciRacun.UseSelectable = true;
            this.tbTekuciRacun.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbTekuciRacun.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbTekuciRacun.Click += new System.EventHandler(this.tbTekuciRacun_Click);
            this.tbTekuciRacun.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTekuciRacun_KeyPress);
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 1;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Controls.Add(this.NeodRadnoMesto, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.tbRadnoMesto, 0, 0);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(157, 301);
            this.tableLayoutPanel17.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 2;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(294, 43);
            this.tableLayoutPanel17.TabIndex = 28;
            // 
            // NeodRadnoMesto
            // 
            this.NeodRadnoMesto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NeodRadnoMesto.AutoSize = true;
            this.NeodRadnoMesto.FontSize = MetroFramework.MetroLabelSize.Small;
            this.NeodRadnoMesto.Location = new System.Drawing.Point(209, 30);
            this.NeodRadnoMesto.Margin = new System.Windows.Forms.Padding(0);
            this.NeodRadnoMesto.Name = "NeodRadnoMesto";
            this.NeodRadnoMesto.Size = new System.Drawing.Size(85, 13);
            this.NeodRadnoMesto.Style = MetroFramework.MetroColorStyle.Red;
            this.NeodRadnoMesto.TabIndex = 1;
            this.NeodRadnoMesto.Text = "Obavezan unos";
            this.NeodRadnoMesto.UseStyleColors = true;
            this.NeodRadnoMesto.Visible = false;
            // 
            // tbRadnoMesto
            // 
            // 
            // 
            // 
            this.tbRadnoMesto.CustomButton.Image = null;
            this.tbRadnoMesto.CustomButton.Location = new System.Drawing.Point(266, 2);
            this.tbRadnoMesto.CustomButton.Name = "";
            this.tbRadnoMesto.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.tbRadnoMesto.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbRadnoMesto.CustomButton.TabIndex = 1;
            this.tbRadnoMesto.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbRadnoMesto.CustomButton.UseSelectable = true;
            this.tbRadnoMesto.CustomButton.Visible = false;
            this.tbRadnoMesto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbRadnoMesto.Lines = new string[0];
            this.tbRadnoMesto.Location = new System.Drawing.Point(3, 3);
            this.tbRadnoMesto.MaxLength = 32767;
            this.tbRadnoMesto.Name = "tbRadnoMesto";
            this.tbRadnoMesto.PasswordChar = '\0';
            this.tbRadnoMesto.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbRadnoMesto.SelectedText = "";
            this.tbRadnoMesto.SelectionLength = 0;
            this.tbRadnoMesto.SelectionStart = 0;
            this.tbRadnoMesto.ShortcutsEnabled = true;
            this.tbRadnoMesto.Size = new System.Drawing.Size(288, 24);
            this.tbRadnoMesto.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbRadnoMesto.TabIndex = 7;
            this.tbRadnoMesto.UseSelectable = true;
            this.tbRadnoMesto.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbRadnoMesto.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbRadnoMesto.Click += new System.EventHandler(this.tbRadnoMesto_Click);
            this.tbRadnoMesto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbRadnoMesto_KeyPress);
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Controls.Add(this.neodBrojTelefona, 0, 1);
            this.tableLayoutPanel16.Controls.Add(this.tbBrojTelefona, 0, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(157, 258);
            this.tableLayoutPanel16.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 2;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(294, 43);
            this.tableLayoutPanel16.TabIndex = 27;
            // 
            // neodBrojTelefona
            // 
            this.neodBrojTelefona.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.neodBrojTelefona.AutoSize = true;
            this.neodBrojTelefona.FontSize = MetroFramework.MetroLabelSize.Small;
            this.neodBrojTelefona.Location = new System.Drawing.Point(209, 30);
            this.neodBrojTelefona.Margin = new System.Windows.Forms.Padding(0);
            this.neodBrojTelefona.Name = "neodBrojTelefona";
            this.neodBrojTelefona.Size = new System.Drawing.Size(85, 13);
            this.neodBrojTelefona.Style = MetroFramework.MetroColorStyle.Red;
            this.neodBrojTelefona.TabIndex = 1;
            this.neodBrojTelefona.Text = "Obavezan unos";
            this.neodBrojTelefona.UseStyleColors = true;
            this.neodBrojTelefona.Visible = false;
            // 
            // tbBrojTelefona
            // 
            // 
            // 
            // 
            this.tbBrojTelefona.CustomButton.Image = null;
            this.tbBrojTelefona.CustomButton.Location = new System.Drawing.Point(266, 2);
            this.tbBrojTelefona.CustomButton.Name = "";
            this.tbBrojTelefona.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.tbBrojTelefona.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbBrojTelefona.CustomButton.TabIndex = 1;
            this.tbBrojTelefona.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbBrojTelefona.CustomButton.UseSelectable = true;
            this.tbBrojTelefona.CustomButton.Visible = false;
            this.tbBrojTelefona.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbBrojTelefona.Lines = new string[0];
            this.tbBrojTelefona.Location = new System.Drawing.Point(3, 3);
            this.tbBrojTelefona.MaxLength = 32767;
            this.tbBrojTelefona.Name = "tbBrojTelefona";
            this.tbBrojTelefona.PasswordChar = '\0';
            this.tbBrojTelefona.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbBrojTelefona.SelectedText = "";
            this.tbBrojTelefona.SelectionLength = 0;
            this.tbBrojTelefona.SelectionStart = 0;
            this.tbBrojTelefona.ShortcutsEnabled = true;
            this.tbBrojTelefona.Size = new System.Drawing.Size(288, 24);
            this.tbBrojTelefona.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbBrojTelefona.TabIndex = 6;
            this.tbBrojTelefona.UseSelectable = true;
            this.tbBrojTelefona.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbBrojTelefona.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbBrojTelefona.Click += new System.EventHandler(this.tbBrojTelefona_Click);
            this.tbBrojTelefona.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBrojTelefona_KeyPress);
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 1;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Controls.Add(this.neodMestoRodjenja, 0, 1);
            this.tableLayoutPanel15.Controls.Add(this.tbMestoRodjenja, 0, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(157, 215);
            this.tableLayoutPanel15.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 2;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(294, 43);
            this.tableLayoutPanel15.TabIndex = 26;
            // 
            // neodMestoRodjenja
            // 
            this.neodMestoRodjenja.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.neodMestoRodjenja.AutoSize = true;
            this.neodMestoRodjenja.FontSize = MetroFramework.MetroLabelSize.Small;
            this.neodMestoRodjenja.Location = new System.Drawing.Point(209, 30);
            this.neodMestoRodjenja.Margin = new System.Windows.Forms.Padding(0);
            this.neodMestoRodjenja.Name = "neodMestoRodjenja";
            this.neodMestoRodjenja.Size = new System.Drawing.Size(85, 13);
            this.neodMestoRodjenja.Style = MetroFramework.MetroColorStyle.Red;
            this.neodMestoRodjenja.TabIndex = 1;
            this.neodMestoRodjenja.Text = "Obavezan unos";
            this.neodMestoRodjenja.UseStyleColors = true;
            this.neodMestoRodjenja.Visible = false;
            // 
            // tbMestoRodjenja
            // 
            // 
            // 
            // 
            this.tbMestoRodjenja.CustomButton.Image = null;
            this.tbMestoRodjenja.CustomButton.Location = new System.Drawing.Point(266, 2);
            this.tbMestoRodjenja.CustomButton.Name = "";
            this.tbMestoRodjenja.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.tbMestoRodjenja.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbMestoRodjenja.CustomButton.TabIndex = 1;
            this.tbMestoRodjenja.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbMestoRodjenja.CustomButton.UseSelectable = true;
            this.tbMestoRodjenja.CustomButton.Visible = false;
            this.tbMestoRodjenja.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMestoRodjenja.Lines = new string[0];
            this.tbMestoRodjenja.Location = new System.Drawing.Point(3, 3);
            this.tbMestoRodjenja.MaxLength = 32767;
            this.tbMestoRodjenja.Name = "tbMestoRodjenja";
            this.tbMestoRodjenja.PasswordChar = '\0';
            this.tbMestoRodjenja.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbMestoRodjenja.SelectedText = "";
            this.tbMestoRodjenja.SelectionLength = 0;
            this.tbMestoRodjenja.SelectionStart = 0;
            this.tbMestoRodjenja.ShortcutsEnabled = true;
            this.tbMestoRodjenja.Size = new System.Drawing.Size(288, 24);
            this.tbMestoRodjenja.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbMestoRodjenja.TabIndex = 5;
            this.tbMestoRodjenja.UseSelectable = true;
            this.tbMestoRodjenja.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbMestoRodjenja.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbMestoRodjenja.Click += new System.EventHandler(this.tbMestoRodjenja_Click);
            this.tbMestoRodjenja.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbMestoRodjenja_KeyPress);
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 1;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Controls.Add(this.neodAdresa, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.tbAdresa, 0, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(157, 172);
            this.tableLayoutPanel14.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 2;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(294, 43);
            this.tableLayoutPanel14.TabIndex = 25;
            // 
            // neodAdresa
            // 
            this.neodAdresa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.neodAdresa.AutoSize = true;
            this.neodAdresa.FontSize = MetroFramework.MetroLabelSize.Small;
            this.neodAdresa.Location = new System.Drawing.Point(209, 30);
            this.neodAdresa.Margin = new System.Windows.Forms.Padding(0);
            this.neodAdresa.Name = "neodAdresa";
            this.neodAdresa.Size = new System.Drawing.Size(85, 13);
            this.neodAdresa.Style = MetroFramework.MetroColorStyle.Red;
            this.neodAdresa.TabIndex = 1;
            this.neodAdresa.Text = "Obavezan unos";
            this.neodAdresa.UseStyleColors = true;
            this.neodAdresa.Visible = false;
            // 
            // tbAdresa
            // 
            // 
            // 
            // 
            this.tbAdresa.CustomButton.Image = null;
            this.tbAdresa.CustomButton.Location = new System.Drawing.Point(266, 2);
            this.tbAdresa.CustomButton.Name = "";
            this.tbAdresa.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.tbAdresa.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbAdresa.CustomButton.TabIndex = 1;
            this.tbAdresa.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbAdresa.CustomButton.UseSelectable = true;
            this.tbAdresa.CustomButton.Visible = false;
            this.tbAdresa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbAdresa.Lines = new string[0];
            this.tbAdresa.Location = new System.Drawing.Point(3, 3);
            this.tbAdresa.MaxLength = 32767;
            this.tbAdresa.Name = "tbAdresa";
            this.tbAdresa.PasswordChar = '\0';
            this.tbAdresa.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbAdresa.SelectedText = "";
            this.tbAdresa.SelectionLength = 0;
            this.tbAdresa.SelectionStart = 0;
            this.tbAdresa.ShortcutsEnabled = true;
            this.tbAdresa.Size = new System.Drawing.Size(288, 24);
            this.tbAdresa.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbAdresa.TabIndex = 4;
            this.tbAdresa.UseSelectable = true;
            this.tbAdresa.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbAdresa.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbAdresa.Click += new System.EventHandler(this.tbAdresa_Click);
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Controls.Add(this.neodPrezime, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.tbPrezime, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(157, 86);
            this.tableLayoutPanel13.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 2;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(294, 43);
            this.tableLayoutPanel13.TabIndex = 24;
            // 
            // neodPrezime
            // 
            this.neodPrezime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.neodPrezime.AutoSize = true;
            this.neodPrezime.FontSize = MetroFramework.MetroLabelSize.Small;
            this.neodPrezime.Location = new System.Drawing.Point(209, 30);
            this.neodPrezime.Margin = new System.Windows.Forms.Padding(0);
            this.neodPrezime.Name = "neodPrezime";
            this.neodPrezime.Size = new System.Drawing.Size(85, 13);
            this.neodPrezime.Style = MetroFramework.MetroColorStyle.Red;
            this.neodPrezime.TabIndex = 1;
            this.neodPrezime.Text = "Obavezan unos";
            this.neodPrezime.UseStyleColors = true;
            this.neodPrezime.Visible = false;
            // 
            // tbPrezime
            // 
            // 
            // 
            // 
            this.tbPrezime.CustomButton.Image = null;
            this.tbPrezime.CustomButton.Location = new System.Drawing.Point(266, 2);
            this.tbPrezime.CustomButton.Name = "";
            this.tbPrezime.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.tbPrezime.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbPrezime.CustomButton.TabIndex = 1;
            this.tbPrezime.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbPrezime.CustomButton.UseSelectable = true;
            this.tbPrezime.CustomButton.Visible = false;
            this.tbPrezime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPrezime.Lines = new string[0];
            this.tbPrezime.Location = new System.Drawing.Point(3, 3);
            this.tbPrezime.MaxLength = 32767;
            this.tbPrezime.Name = "tbPrezime";
            this.tbPrezime.PasswordChar = '\0';
            this.tbPrezime.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbPrezime.SelectedText = "";
            this.tbPrezime.SelectionLength = 0;
            this.tbPrezime.SelectionStart = 0;
            this.tbPrezime.ShortcutsEnabled = true;
            this.tbPrezime.Size = new System.Drawing.Size(288, 24);
            this.tbPrezime.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbPrezime.TabIndex = 2;
            this.tbPrezime.UseSelectable = true;
            this.tbPrezime.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbPrezime.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbPrezime.Click += new System.EventHandler(this.tbPrezime_Click);
            this.tbPrezime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPrezime_KeyPress);
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 1;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Controls.Add(this.neodSrednjeSlovo, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.tbSrednjeSlovo, 0, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(157, 43);
            this.tableLayoutPanel12.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 2;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(294, 43);
            this.tableLayoutPanel12.TabIndex = 23;
            // 
            // neodSrednjeSlovo
            // 
            this.neodSrednjeSlovo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.neodSrednjeSlovo.AutoSize = true;
            this.neodSrednjeSlovo.FontSize = MetroFramework.MetroLabelSize.Small;
            this.neodSrednjeSlovo.Location = new System.Drawing.Point(209, 30);
            this.neodSrednjeSlovo.Margin = new System.Windows.Forms.Padding(0);
            this.neodSrednjeSlovo.Name = "neodSrednjeSlovo";
            this.neodSrednjeSlovo.Size = new System.Drawing.Size(85, 13);
            this.neodSrednjeSlovo.Style = MetroFramework.MetroColorStyle.Red;
            this.neodSrednjeSlovo.TabIndex = 1;
            this.neodSrednjeSlovo.Text = "Obavezan unos";
            this.neodSrednjeSlovo.UseStyleColors = true;
            this.neodSrednjeSlovo.Visible = false;
            // 
            // tbSrednjeSlovo
            // 
            // 
            // 
            // 
            this.tbSrednjeSlovo.CustomButton.Image = null;
            this.tbSrednjeSlovo.CustomButton.Location = new System.Drawing.Point(266, 2);
            this.tbSrednjeSlovo.CustomButton.Name = "";
            this.tbSrednjeSlovo.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.tbSrednjeSlovo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbSrednjeSlovo.CustomButton.TabIndex = 1;
            this.tbSrednjeSlovo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbSrednjeSlovo.CustomButton.UseSelectable = true;
            this.tbSrednjeSlovo.CustomButton.Visible = false;
            this.tbSrednjeSlovo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbSrednjeSlovo.Lines = new string[0];
            this.tbSrednjeSlovo.Location = new System.Drawing.Point(3, 3);
            this.tbSrednjeSlovo.MaxLength = 32767;
            this.tbSrednjeSlovo.Name = "tbSrednjeSlovo";
            this.tbSrednjeSlovo.PasswordChar = '\0';
            this.tbSrednjeSlovo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbSrednjeSlovo.SelectedText = "";
            this.tbSrednjeSlovo.SelectionLength = 0;
            this.tbSrednjeSlovo.SelectionStart = 0;
            this.tbSrednjeSlovo.ShortcutsEnabled = true;
            this.tbSrednjeSlovo.Size = new System.Drawing.Size(288, 24);
            this.tbSrednjeSlovo.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbSrednjeSlovo.TabIndex = 1;
            this.tbSrednjeSlovo.UseSelectable = true;
            this.tbSrednjeSlovo.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbSrednjeSlovo.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbSrednjeSlovo.Click += new System.EventHandler(this.tbSrednjeSlovo_Click);
            this.tbSrednjeSlovo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSrednjeSlovo_KeyPress);
            // 
            // lblIme
            // 
            this.lblIme.AutoSize = true;
            this.lblIme.Location = new System.Drawing.Point(3, 0);
            this.lblIme.Name = "lblIme";
            this.lblIme.Size = new System.Drawing.Size(34, 19);
            this.lblIme.TabIndex = 0;
            this.lblIme.Text = "Ime:";
            // 
            // lblSrednjeSlovo
            // 
            this.lblSrednjeSlovo.AutoSize = true;
            this.lblSrednjeSlovo.Location = new System.Drawing.Point(3, 43);
            this.lblSrednjeSlovo.Name = "lblSrednjeSlovo";
            this.lblSrednjeSlovo.Size = new System.Drawing.Size(90, 19);
            this.lblSrednjeSlovo.TabIndex = 1;
            this.lblSrednjeSlovo.Text = "Srednje slovo:";
            // 
            // lblPrezime
            // 
            this.lblPrezime.AutoSize = true;
            this.lblPrezime.Location = new System.Drawing.Point(3, 86);
            this.lblPrezime.Name = "lblPrezime";
            this.lblPrezime.Size = new System.Drawing.Size(60, 19);
            this.lblPrezime.TabIndex = 2;
            this.lblPrezime.Text = "Prezime:";
            // 
            // lblDatumRodjenja
            // 
            this.lblDatumRodjenja.AutoSize = true;
            this.lblDatumRodjenja.Location = new System.Drawing.Point(3, 129);
            this.lblDatumRodjenja.Name = "lblDatumRodjenja";
            this.lblDatumRodjenja.Size = new System.Drawing.Size(100, 19);
            this.lblDatumRodjenja.TabIndex = 3;
            this.lblDatumRodjenja.Text = "Datum rođenja:";
            // 
            // lblMestoRodjenja
            // 
            this.lblMestoRodjenja.AutoSize = true;
            this.lblMestoRodjenja.Location = new System.Drawing.Point(3, 215);
            this.lblMestoRodjenja.Name = "lblMestoRodjenja";
            this.lblMestoRodjenja.Size = new System.Drawing.Size(97, 19);
            this.lblMestoRodjenja.TabIndex = 5;
            this.lblMestoRodjenja.Text = "Mesto rođenja:";
            // 
            // lblBrojTelefona
            // 
            this.lblBrojTelefona.AutoSize = true;
            this.lblBrojTelefona.Location = new System.Drawing.Point(3, 258);
            this.lblBrojTelefona.Name = "lblBrojTelefona";
            this.lblBrojTelefona.Size = new System.Drawing.Size(87, 19);
            this.lblBrojTelefona.TabIndex = 6;
            this.lblBrojTelefona.Text = "Broj telefona:";
            // 
            // lblRadnoMesto
            // 
            this.lblRadnoMesto.AutoSize = true;
            this.lblRadnoMesto.Location = new System.Drawing.Point(3, 301);
            this.lblRadnoMesto.Name = "lblRadnoMesto";
            this.lblRadnoMesto.Size = new System.Drawing.Size(90, 19);
            this.lblRadnoMesto.TabIndex = 7;
            this.lblRadnoMesto.Text = "Radno mesto:";
            // 
            // lblTekuciRacun
            // 
            this.lblTekuciRacun.AutoSize = true;
            this.lblTekuciRacun.Location = new System.Drawing.Point(3, 344);
            this.lblTekuciRacun.Name = "lblTekuciRacun";
            this.lblTekuciRacun.Size = new System.Drawing.Size(84, 19);
            this.lblTekuciRacun.TabIndex = 8;
            this.lblTekuciRacun.Text = "Tekući račun:";
            // 
            // lblAdresa
            // 
            this.lblAdresa.AutoSize = true;
            this.lblAdresa.Location = new System.Drawing.Point(3, 172);
            this.lblAdresa.Name = "lblAdresa";
            this.lblAdresa.Size = new System.Drawing.Size(53, 19);
            this.lblAdresa.TabIndex = 10;
            this.lblAdresa.Text = "Adresa:";
            // 
            // dtpDatumRodjenja
            // 
            this.dtpDatumRodjenja.CustomFormat = "dd.MM.yyyy.";
            this.dtpDatumRodjenja.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDatumRodjenja.Location = new System.Drawing.Point(160, 132);
            this.dtpDatumRodjenja.MinDate = new System.DateTime(1900, 2, 22, 0, 0, 0, 0);
            this.dtpDatumRodjenja.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtpDatumRodjenja.Name = "dtpDatumRodjenja";
            this.dtpDatumRodjenja.Size = new System.Drawing.Size(285, 29);
            this.dtpDatumRodjenja.Style = MetroFramework.MetroColorStyle.Purple;
            this.dtpDatumRodjenja.TabIndex = 3;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this.neodIme, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.tbIme, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(157, 0);
            this.tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(294, 43);
            this.tableLayoutPanel11.TabIndex = 22;
            // 
            // neodIme
            // 
            this.neodIme.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.neodIme.AutoSize = true;
            this.neodIme.FontSize = MetroFramework.MetroLabelSize.Small;
            this.neodIme.Location = new System.Drawing.Point(209, 30);
            this.neodIme.Margin = new System.Windows.Forms.Padding(0);
            this.neodIme.Name = "neodIme";
            this.neodIme.Size = new System.Drawing.Size(85, 13);
            this.neodIme.Style = MetroFramework.MetroColorStyle.Red;
            this.neodIme.TabIndex = 1;
            this.neodIme.Text = "Obavezan unos";
            this.neodIme.UseStyleColors = true;
            this.neodIme.Visible = false;
            // 
            // tbIme
            // 
            this.tbIme.AllowDrop = true;
            // 
            // 
            // 
            this.tbIme.CustomButton.Image = null;
            this.tbIme.CustomButton.Location = new System.Drawing.Point(266, 2);
            this.tbIme.CustomButton.Name = "";
            this.tbIme.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.tbIme.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbIme.CustomButton.TabIndex = 1;
            this.tbIme.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbIme.CustomButton.UseSelectable = true;
            this.tbIme.CustomButton.Visible = false;
            this.tbIme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbIme.Lines = new string[0];
            this.tbIme.Location = new System.Drawing.Point(3, 3);
            this.tbIme.MaxLength = 32767;
            this.tbIme.Name = "tbIme";
            this.tbIme.PasswordChar = '\0';
            this.tbIme.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbIme.SelectedText = "";
            this.tbIme.SelectionLength = 0;
            this.tbIme.SelectionStart = 0;
            this.tbIme.ShortcutsEnabled = true;
            this.tbIme.Size = new System.Drawing.Size(288, 24);
            this.tbIme.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbIme.TabIndex = 0;
            this.tbIme.UseSelectable = true;
            this.tbIme.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbIme.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbIme.Click += new System.EventHandler(this.tbIme_Click);
            this.tbIme.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbIme_KeyPress);
            // 
            // lblNormaRada
            // 
            this.lblNormaRada.AutoSize = true;
            this.lblNormaRada.Location = new System.Drawing.Point(3, 387);
            this.lblNormaRada.Name = "lblNormaRada";
            this.lblNormaRada.Size = new System.Drawing.Size(85, 19);
            this.lblNormaRada.TabIndex = 9;
            this.lblNormaRada.Text = "Norma rada:";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(460, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(452, 520);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.pictureBox1, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel7, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(446, 254);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.metroButton5, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.metroPanel2, 0, 6);
            this.tableLayoutPanel7.Controls.Add(this.metroButton1, 0, 3);
            this.tableLayoutPanel7.Controls.Add(this.metroButton2, 0, 4);
            this.tableLayoutPanel7.Controls.Add(this.metroButton3, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.metroPanel1, 0, 5);
            this.tableLayoutPanel7.Controls.Add(this.metroButton4, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 7;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.32022F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.31622F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.31622F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.31622F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.31622F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.70744F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.70744F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(217, 248);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // metroButton5
            // 
            this.metroButton5.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton5.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.metroButton5.Location = new System.Drawing.Point(5, 38);
            this.metroButton5.Margin = new System.Windows.Forms.Padding(5);
            this.metroButton5.Name = "metroButton5";
            this.metroButton5.Size = new System.Drawing.Size(207, 23);
            this.metroButton5.TabIndex = 14;
            this.metroButton5.Text = "Obriši nadređenog";
            this.metroButton5.UseSelectable = true;
            this.metroButton5.Click += new System.EventHandler(this.metroButton5_Click);
            // 
            // metroPanel2
            // 
            this.metroPanel2.Controls.Add(this.lblNadredjeni);
            this.metroPanel2.Controls.Add(this.Nadređeni);
            this.metroPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(0, 206);
            this.metroPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(217, 42);
            this.metroPanel2.TabIndex = 5;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // lblNadredjeni
            // 
            this.lblNadredjeni.Location = new System.Drawing.Point(5, 19);
            this.lblNadredjeni.Name = "lblNadredjeni";
            this.lblNadredjeni.Size = new System.Drawing.Size(209, 19);
            this.lblNadredjeni.TabIndex = 3;
            this.lblNadredjeni.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // Nadređeni
            // 
            this.Nadređeni.AutoSize = true;
            this.Nadređeni.Location = new System.Drawing.Point(0, 0);
            this.Nadređeni.Name = "Nadređeni";
            this.Nadređeni.Size = new System.Drawing.Size(74, 19);
            this.Nadređeni.TabIndex = 2;
            this.Nadređeni.Text = "Nadređeni:";
            // 
            // metroButton1
            // 
            this.metroButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroButton1.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton1.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.metroButton1.Location = new System.Drawing.Point(5, 104);
            this.metroButton1.Margin = new System.Windows.Forms.Padding(5);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(207, 23);
            this.metroButton1.TabIndex = 16;
            this.metroButton1.Text = "Izaberi sliku";
            this.metroButton1.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // metroButton2
            // 
            this.metroButton2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroButton2.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton2.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.metroButton2.Location = new System.Drawing.Point(5, 137);
            this.metroButton2.Margin = new System.Windows.Forms.Padding(5);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(207, 23);
            this.metroButton2.TabIndex = 17;
            this.metroButton2.Text = "Dodaj zaposlenog";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // metroButton3
            // 
            this.metroButton3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroButton3.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton3.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.metroButton3.Location = new System.Drawing.Point(5, 71);
            this.metroButton3.Margin = new System.Windows.Forms.Padding(5);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(207, 23);
            this.metroButton3.TabIndex = 15;
            this.metroButton3.Text = "Izaberi smenu";
            this.metroButton3.UseSelectable = true;
            this.metroButton3.Click += new System.EventHandler(this.metroButton3_Click);
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.lblSmena);
            this.metroPanel1.Controls.Add(this.metroLabel6);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 165);
            this.metroPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(217, 41);
            this.metroPanel1.TabIndex = 4;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // lblSmena
            // 
            this.lblSmena.Location = new System.Drawing.Point(0, 16);
            this.lblSmena.Name = "lblSmena";
            this.lblSmena.Size = new System.Drawing.Size(214, 18);
            this.lblSmena.TabIndex = 3;
            this.lblSmena.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(5, -3);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(52, 19);
            this.metroLabel6.TabIndex = 2;
            this.metroLabel6.Text = "Smena:";
            // 
            // metroButton4
            // 
            this.metroButton4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroButton4.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton4.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.metroButton4.Location = new System.Drawing.Point(5, 5);
            this.metroButton4.Margin = new System.Windows.Forms.Padding(5);
            this.metroButton4.Name = "metroButton4";
            this.metroButton4.Size = new System.Drawing.Size(207, 23);
            this.metroButton4.TabIndex = 13;
            this.metroButton4.Text = "Izaberi nadređenog";
            this.metroButton4.UseSelectable = true;
            this.metroButton4.Click += new System.EventHandler(this.metroButton4_Click);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.lbKorisnickoIme, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel1, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel3, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel2, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.cbUlogaKorisnickogNaloga, 1, 4);
            this.tableLayoutPanel5.Controls.Add(this.metroLabel4, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel8, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel9, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel10, 1, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 263);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 5;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(446, 254);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // lbKorisnickoIme
            // 
            this.lbKorisnickoIme.AutoSize = true;
            this.lbKorisnickoIme.Location = new System.Drawing.Point(3, 50);
            this.lbKorisnickoIme.Name = "lbKorisnickoIme";
            this.lbKorisnickoIme.Size = new System.Drawing.Size(96, 19);
            this.lbKorisnickoIme.TabIndex = 1;
            this.lbKorisnickoIme.Text = "Korisničko ime:";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(3, 100);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(55, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Lozinka:";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(3, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(129, 19);
            this.metroLabel3.TabIndex = 0;
            this.metroLabel3.Text = "Korisnik sa nalogom:";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.rbNalogDa, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.rbNalogNe, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(226, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(217, 44);
            this.tableLayoutPanel6.TabIndex = 7;
            // 
            // rbNalogDa
            // 
            this.rbNalogDa.AutoSize = true;
            this.rbNalogDa.Location = new System.Drawing.Point(5, 5);
            this.rbNalogDa.Margin = new System.Windows.Forms.Padding(5);
            this.rbNalogDa.Name = "rbNalogDa";
            this.rbNalogDa.Size = new System.Drawing.Size(37, 15);
            this.rbNalogDa.Style = MetroFramework.MetroColorStyle.Purple;
            this.rbNalogDa.TabIndex = 12;
            this.rbNalogDa.TabStop = true;
            this.rbNalogDa.Text = "Da";
            this.rbNalogDa.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.rbNalogDa.UseSelectable = true;
            this.rbNalogDa.CheckedChanged += new System.EventHandler(this.rbNalogDa_CheckedChanged);
            // 
            // rbNalogNe
            // 
            this.rbNalogNe.AutoSize = true;
            this.rbNalogNe.Checked = true;
            this.rbNalogNe.Location = new System.Drawing.Point(113, 5);
            this.rbNalogNe.Margin = new System.Windows.Forms.Padding(5);
            this.rbNalogNe.Name = "rbNalogNe";
            this.rbNalogNe.Size = new System.Drawing.Size(38, 15);
            this.rbNalogNe.Style = MetroFramework.MetroColorStyle.Purple;
            this.rbNalogNe.TabIndex = 12;
            this.rbNalogNe.TabStop = true;
            this.rbNalogNe.Text = "Ne";
            this.rbNalogNe.UseSelectable = true;
            this.rbNalogNe.CheckedChanged += new System.EventHandler(this.rbNalogDa_CheckedChanged);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(3, 200);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(160, 19);
            this.metroLabel2.TabIndex = 8;
            this.metroLabel2.Text = "Uloga korisničkog naloga:";
            // 
            // cbUlogaKorisnickogNaloga
            // 
            this.cbUlogaKorisnickogNaloga.Enabled = false;
            this.cbUlogaKorisnickogNaloga.FormattingEnabled = true;
            this.cbUlogaKorisnickogNaloga.ItemHeight = 23;
            this.cbUlogaKorisnickogNaloga.Items.AddRange(new object[] {
            "Administrator",
            "Menadžer",
            "Knjigovođa",
            "Portir"});
            this.cbUlogaKorisnickogNaloga.Location = new System.Drawing.Point(226, 203);
            this.cbUlogaKorisnickogNaloga.Name = "cbUlogaKorisnickogNaloga";
            this.cbUlogaKorisnickogNaloga.Size = new System.Drawing.Size(215, 29);
            this.cbUlogaKorisnickogNaloga.Style = MetroFramework.MetroColorStyle.Purple;
            this.cbUlogaKorisnickogNaloga.TabIndex = 16;
            this.cbUlogaKorisnickogNaloga.UseSelectable = true;
            this.cbUlogaKorisnickogNaloga.Click += new System.EventHandler(this.tbLozinka_Click);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(3, 150);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(123, 19);
            this.metroLabel4.TabIndex = 5;
            this.metroLabel4.Text = "Ponovljenja lozinka:";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.tbLozinka, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.neodgovarajuciUnosLozinka, 0, 1);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(223, 100);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(223, 50);
            this.tableLayoutPanel8.TabIndex = 11;
            // 
            // tbLozinka
            // 
            // 
            // 
            // 
            this.tbLozinka.CustomButton.Image = null;
            this.tbLozinka.CustomButton.Location = new System.Drawing.Point(189, 1);
            this.tbLozinka.CustomButton.Name = "";
            this.tbLozinka.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.tbLozinka.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbLozinka.CustomButton.TabIndex = 1;
            this.tbLozinka.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbLozinka.CustomButton.UseSelectable = true;
            this.tbLozinka.CustomButton.Visible = false;
            this.tbLozinka.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbLozinka.Lines = new string[0];
            this.tbLozinka.Location = new System.Drawing.Point(3, 3);
            this.tbLozinka.MaxLength = 32767;
            this.tbLozinka.Name = "tbLozinka";
            this.tbLozinka.PasswordChar = '●';
            this.tbLozinka.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbLozinka.SelectedText = "";
            this.tbLozinka.SelectionLength = 0;
            this.tbLozinka.SelectionStart = 0;
            this.tbLozinka.ShortcutsEnabled = true;
            this.tbLozinka.Size = new System.Drawing.Size(217, 29);
            this.tbLozinka.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbLozinka.TabIndex = 14;
            this.tbLozinka.UseSelectable = true;
            this.tbLozinka.UseSystemPasswordChar = true;
            this.tbLozinka.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbLozinka.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbLozinka.Click += new System.EventHandler(this.tbLozinka_Click);
            // 
            // neodgovarajuciUnosLozinka
            // 
            this.neodgovarajuciUnosLozinka.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.neodgovarajuciUnosLozinka.AutoSize = true;
            this.neodgovarajuciUnosLozinka.FontSize = MetroFramework.MetroLabelSize.Small;
            this.neodgovarajuciUnosLozinka.Location = new System.Drawing.Point(137, 35);
            this.neodgovarajuciUnosLozinka.Margin = new System.Windows.Forms.Padding(0);
            this.neodgovarajuciUnosLozinka.Name = "neodgovarajuciUnosLozinka";
            this.neodgovarajuciUnosLozinka.Size = new System.Drawing.Size(86, 15);
            this.neodgovarajuciUnosLozinka.Style = MetroFramework.MetroColorStyle.Red;
            this.neodgovarajuciUnosLozinka.TabIndex = 0;
            this.neodgovarajuciUnosLozinka.Text = "Lozinke nisu iste";
            this.neodgovarajuciUnosLozinka.UseStyleColors = true;
            this.neodgovarajuciUnosLozinka.Visible = false;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.neodgovarajuciUnosPonovljenaLozinka, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.tbPonovljenaLozinka, 0, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(223, 150);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(223, 50);
            this.tableLayoutPanel9.TabIndex = 12;
            // 
            // neodgovarajuciUnosPonovljenaLozinka
            // 
            this.neodgovarajuciUnosPonovljenaLozinka.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.neodgovarajuciUnosPonovljenaLozinka.AutoSize = true;
            this.neodgovarajuciUnosPonovljenaLozinka.FontSize = MetroFramework.MetroLabelSize.Small;
            this.neodgovarajuciUnosPonovljenaLozinka.Location = new System.Drawing.Point(137, 35);
            this.neodgovarajuciUnosPonovljenaLozinka.Margin = new System.Windows.Forms.Padding(0);
            this.neodgovarajuciUnosPonovljenaLozinka.Name = "neodgovarajuciUnosPonovljenaLozinka";
            this.neodgovarajuciUnosPonovljenaLozinka.Size = new System.Drawing.Size(86, 15);
            this.neodgovarajuciUnosPonovljenaLozinka.Style = MetroFramework.MetroColorStyle.Red;
            this.neodgovarajuciUnosPonovljenaLozinka.TabIndex = 2;
            this.neodgovarajuciUnosPonovljenaLozinka.Text = "Lozinke nisu iste";
            this.neodgovarajuciUnosPonovljenaLozinka.UseStyleColors = true;
            this.neodgovarajuciUnosPonovljenaLozinka.Visible = false;
            // 
            // tbPonovljenaLozinka
            // 
            // 
            // 
            // 
            this.tbPonovljenaLozinka.CustomButton.Image = null;
            this.tbPonovljenaLozinka.CustomButton.Location = new System.Drawing.Point(189, 1);
            this.tbPonovljenaLozinka.CustomButton.Name = "";
            this.tbPonovljenaLozinka.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.tbPonovljenaLozinka.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbPonovljenaLozinka.CustomButton.TabIndex = 1;
            this.tbPonovljenaLozinka.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbPonovljenaLozinka.CustomButton.UseSelectable = true;
            this.tbPonovljenaLozinka.CustomButton.Visible = false;
            this.tbPonovljenaLozinka.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPonovljenaLozinka.Lines = new string[0];
            this.tbPonovljenaLozinka.Location = new System.Drawing.Point(3, 3);
            this.tbPonovljenaLozinka.MaxLength = 32767;
            this.tbPonovljenaLozinka.Name = "tbPonovljenaLozinka";
            this.tbPonovljenaLozinka.PasswordChar = '●';
            this.tbPonovljenaLozinka.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbPonovljenaLozinka.SelectedText = "";
            this.tbPonovljenaLozinka.SelectionLength = 0;
            this.tbPonovljenaLozinka.SelectionStart = 0;
            this.tbPonovljenaLozinka.ShortcutsEnabled = true;
            this.tbPonovljenaLozinka.Size = new System.Drawing.Size(217, 29);
            this.tbPonovljenaLozinka.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbPonovljenaLozinka.TabIndex = 15;
            this.tbPonovljenaLozinka.UseSelectable = true;
            this.tbPonovljenaLozinka.UseSystemPasswordChar = true;
            this.tbPonovljenaLozinka.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbPonovljenaLozinka.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.neodgovarajuciUnosKorisnickoIme, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.tbKorisnickoIme, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(223, 50);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 2;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(223, 50);
            this.tableLayoutPanel10.TabIndex = 13;
            // 
            // neodgovarajuciUnosKorisnickoIme
            // 
            this.neodgovarajuciUnosKorisnickoIme.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.neodgovarajuciUnosKorisnickoIme.AutoSize = true;
            this.neodgovarajuciUnosKorisnickoIme.FontSize = MetroFramework.MetroLabelSize.Small;
            this.neodgovarajuciUnosKorisnickoIme.Location = new System.Drawing.Point(138, 35);
            this.neodgovarajuciUnosKorisnickoIme.Margin = new System.Windows.Forms.Padding(0);
            this.neodgovarajuciUnosKorisnickoIme.Name = "neodgovarajuciUnosKorisnickoIme";
            this.neodgovarajuciUnosKorisnickoIme.Size = new System.Drawing.Size(85, 15);
            this.neodgovarajuciUnosKorisnickoIme.Style = MetroFramework.MetroColorStyle.Red;
            this.neodgovarajuciUnosKorisnickoIme.TabIndex = 1;
            this.neodgovarajuciUnosKorisnickoIme.Text = "Obavezan unos";
            this.neodgovarajuciUnosKorisnickoIme.UseStyleColors = true;
            this.neodgovarajuciUnosKorisnickoIme.Visible = false;
            // 
            // tbKorisnickoIme
            // 
            // 
            // 
            // 
            this.tbKorisnickoIme.CustomButton.Image = null;
            this.tbKorisnickoIme.CustomButton.Location = new System.Drawing.Point(189, 1);
            this.tbKorisnickoIme.CustomButton.Name = "";
            this.tbKorisnickoIme.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.tbKorisnickoIme.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbKorisnickoIme.CustomButton.TabIndex = 1;
            this.tbKorisnickoIme.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbKorisnickoIme.CustomButton.UseSelectable = true;
            this.tbKorisnickoIme.CustomButton.Visible = false;
            this.tbKorisnickoIme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbKorisnickoIme.Enabled = false;
            this.tbKorisnickoIme.Lines = new string[0];
            this.tbKorisnickoIme.Location = new System.Drawing.Point(3, 3);
            this.tbKorisnickoIme.MaxLength = 32767;
            this.tbKorisnickoIme.Name = "tbKorisnickoIme";
            this.tbKorisnickoIme.PasswordChar = '\0';
            this.tbKorisnickoIme.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbKorisnickoIme.SelectedText = "";
            this.tbKorisnickoIme.SelectionLength = 0;
            this.tbKorisnickoIme.SelectionStart = 0;
            this.tbKorisnickoIme.ShortcutsEnabled = true;
            this.tbKorisnickoIme.Size = new System.Drawing.Size(217, 29);
            this.tbKorisnickoIme.Style = MetroFramework.MetroColorStyle.Purple;
            this.tbKorisnickoIme.TabIndex = 13;
            this.tbKorisnickoIme.UseSelectable = true;
            this.tbKorisnickoIme.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbKorisnickoIme.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbKorisnickoIme.Click += new System.EventHandler(this.tbKorisnickoIme_Click);
            // 
            // ribbonTextBox1
            // 
            this.ribbonTextBox1.Name = "ribbonTextBox1";
            this.ribbonTextBox1.TextBoxText = "";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.ErrorImage = global::SharedComponents.Properties.Resources.default_person;
            this.pictureBox1.Image = global::SharedComponents.Properties.Resources.default_person;
            this.pictureBox1.InitialImage = global::SharedComponents.Properties.Resources.default_person;
            this.pictureBox1.Location = new System.Drawing.Point(226, 3);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(217, 236);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // NoviZaposleniForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 606);
            this.Controls.Add(this.tableLayoutPanel2);
            this.MaximizeBox = false;
            this.Name = "NoviZaposleniForm";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Purple;
            this.Text = "Novi zaposleni";
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel23.ResumeLayout(false);
            this.tableLayoutPanel23.PerformLayout();
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel22.PerformLayout();
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel21.PerformLayout();
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel20.PerformLayout();
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel19.PerformLayout();
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroLabel lblIme;
        private MetroFramework.Controls.MetroLabel lblSrednjeSlovo;
        private MetroFramework.Controls.MetroLabel lblPrezime;
        private MetroFramework.Controls.MetroLabel lblDatumRodjenja;
        private MetroFramework.Controls.MetroLabel lblMestoRodjenja;
        private MetroFramework.Controls.MetroLabel lblBrojTelefona;
        private MetroFramework.Controls.MetroLabel lblRadnoMesto;
        private MetroFramework.Controls.MetroLabel lblTekuciRacun;
        private MetroFramework.Controls.MetroLabel lblNormaRada;
        private MetroFramework.Controls.MetroLabel lblAdresa;
        private MetroFramework.Controls.MetroTextBox tbSrednjeSlovo;
        private MetroFramework.Controls.MetroTextBox tbPrezime;
        private MetroFramework.Controls.MetroTextBox tbAdresa;
        private MetroFramework.Controls.MetroTextBox tbMestoRodjenja;
        private MetroFramework.Controls.MetroTextBox tbRadnoMesto;
        private MetroFramework.Controls.MetroTextBox tbTekuciRacun;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.RibbonTextBox ribbonTextBox1;
        private MetroFramework.Controls.MetroComboBox cbUlogaKorisnickogNaloga;
        private MetroFramework.Controls.MetroLabel lbKorisnickoIme;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private MetroFramework.Controls.MetroRadioButton rbNalogDa;
        private MetroFramework.Controls.MetroRadioButton rbNalogNe;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private MetroFramework.Controls.MetroButton metroButton1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroDateTime dtpDatumRodjenja;
        private MetroFramework.Controls.MetroButton metroButton3;
        private MetroFramework.Controls.MetroButton metroButton4;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private MetroFramework.Controls.MetroTextBox tbLozinka;
        private MetroFramework.Controls.MetroLabel neodgovarajuciUnosLozinka;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private MetroFramework.Controls.MetroLabel neodgovarajuciUnosPonovljenaLozinka;
        private MetroFramework.Controls.MetroTextBox tbPonovljenaLozinka;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private MetroFramework.Controls.MetroLabel neodgovarajuciUnosKorisnickoIme;
        private MetroFramework.Controls.MetroTextBox tbKorisnickoIme;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private MetroFramework.Controls.MetroTextBox tbIme;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private MetroFramework.Controls.MetroLabel neodNormaRada;
        private MetroFramework.Controls.MetroTextBox tbNormaRada;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private MetroFramework.Controls.MetroLabel neodTekuciRacun;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private MetroFramework.Controls.MetroLabel NeodRadnoMesto;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private MetroFramework.Controls.MetroLabel neodBrojTelefona;
        private MetroFramework.Controls.MetroTextBox tbBrojTelefona;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private MetroFramework.Controls.MetroLabel neodMestoRodjenja;
        private MetroFramework.Controls.MetroLabel neodAdresa;
        private MetroFramework.Controls.MetroLabel neodPrezime;
        private MetroFramework.Controls.MetroLabel neodSrednjeSlovo;
        private MetroFramework.Controls.MetroLabel neodIme;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private MetroFramework.Controls.MetroLabel neodPocetniStaz;
        private MetroFramework.Controls.MetroTextBox tbPocetniStaz;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private MetroFramework.Controls.MetroLabel neodKoeficijentRadnogMesta;
        private MetroFramework.Controls.MetroTextBox tbKoeficijentRadnogMesta;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroLabel lblNadredjeni;
        private MetroFramework.Controls.MetroLabel Nadređeni;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel lblSmena;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroButton metroButton5;
    }
}