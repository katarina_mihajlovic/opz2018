﻿namespace SharedComponents.Forms
{
	partial class SankcijaStimulacijaForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.LOpis = new MetroFramework.Controls.MetroLabel();
            this.txtOpis = new MetroFramework.Controls.MetroTextBox();
            this.LSankcijaStimulacija = new MetroFramework.Controls.MetroLabel();
            this.txtSankcijaStimulacija = new MetroFramework.Controls.MetroTextBox();
            this.Lradnik = new MetroFramework.Controls.MetroLabel();
            this.Lime = new MetroFramework.Controls.MetroLabel();
            this.btnOK = new MetroFramework.Controls.MetroButton();
            this.btnCancel = new MetroFramework.Controls.MetroButton();
            this.lblError = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // LOpis
            // 
            this.LOpis.AutoSize = true;
            this.LOpis.Location = new System.Drawing.Point(21, 200);
            this.LOpis.Name = "LOpis";
            this.LOpis.Size = new System.Drawing.Size(39, 19);
            this.LOpis.TabIndex = 0;
            this.LOpis.Text = "Opis:";
            // 
            // txtOpis
            // 
            // 
            // 
            // 
            this.txtOpis.CustomButton.Image = null;
            this.txtOpis.CustomButton.Location = new System.Drawing.Point(117, 2);
            this.txtOpis.CustomButton.Name = "";
            this.txtOpis.CustomButton.Size = new System.Drawing.Size(127, 127);
            this.txtOpis.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtOpis.CustomButton.TabIndex = 1;
            this.txtOpis.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtOpis.CustomButton.UseSelectable = true;
            this.txtOpis.CustomButton.Visible = false;
            this.txtOpis.Lines = new string[0];
            this.txtOpis.Location = new System.Drawing.Point(108, 197);
            this.txtOpis.MaxLength = 32767;
            this.txtOpis.Multiline = true;
            this.txtOpis.Name = "txtOpis";
            this.txtOpis.PasswordChar = '\0';
            this.txtOpis.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtOpis.SelectedText = "";
            this.txtOpis.SelectionLength = 0;
            this.txtOpis.SelectionStart = 0;
            this.txtOpis.ShortcutsEnabled = true;
            this.txtOpis.Size = new System.Drawing.Size(247, 132);
            this.txtOpis.Style = MetroFramework.MetroColorStyle.Pink;
            this.txtOpis.TabIndex = 1;
            this.txtOpis.UseSelectable = true;
            this.txtOpis.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtOpis.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // LSankcijaStimulacija
            // 
            this.LSankcijaStimulacija.AutoSize = true;
            this.LSankcijaStimulacija.Location = new System.Drawing.Point(21, 141);
            this.LSankcijaStimulacija.Name = "LSankcijaStimulacija";
            this.LSankcijaStimulacija.Size = new System.Drawing.Size(64, 19);
            this.LSankcijaStimulacija.TabIndex = 4;
            this.LSankcijaStimulacija.Text = "Vrednost:";
            // 
            // txtSankcijaStimulacija
            // 
            // 
            // 
            // 
            this.txtSankcijaStimulacija.CustomButton.Image = null;
            this.txtSankcijaStimulacija.CustomButton.Location = new System.Drawing.Point(77, 1);
            this.txtSankcijaStimulacija.CustomButton.Name = "";
            this.txtSankcijaStimulacija.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtSankcijaStimulacija.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSankcijaStimulacija.CustomButton.TabIndex = 1;
            this.txtSankcijaStimulacija.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSankcijaStimulacija.CustomButton.UseSelectable = true;
            this.txtSankcijaStimulacija.CustomButton.Visible = false;
            this.txtSankcijaStimulacija.Lines = new string[0];
            this.txtSankcijaStimulacija.Location = new System.Drawing.Point(108, 139);
            this.txtSankcijaStimulacija.MaxLength = 32767;
            this.txtSankcijaStimulacija.Name = "txtSankcijaStimulacija";
            this.txtSankcijaStimulacija.PasswordChar = '\0';
            this.txtSankcijaStimulacija.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSankcijaStimulacija.SelectedText = "";
            this.txtSankcijaStimulacija.SelectionLength = 0;
            this.txtSankcijaStimulacija.SelectionStart = 0;
            this.txtSankcijaStimulacija.ShortcutsEnabled = true;
            this.txtSankcijaStimulacija.Size = new System.Drawing.Size(99, 23);
            this.txtSankcijaStimulacija.Style = MetroFramework.MetroColorStyle.Pink;
            this.txtSankcijaStimulacija.TabIndex = 0;
            this.txtSankcijaStimulacija.UseSelectable = true;
            this.txtSankcijaStimulacija.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSankcijaStimulacija.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtSankcijaStimulacija.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSankcijaStimulacija_KeyPress);
            this.txtSankcijaStimulacija.Leave += new System.EventHandler(this.txtSankcijaStimulacija_Leave);
            // 
            // Lradnik
            // 
            this.Lradnik.AutoSize = true;
            this.Lradnik.Location = new System.Drawing.Point(21, 82);
            this.Lradnik.Name = "Lradnik";
            this.Lradnik.Size = new System.Drawing.Size(52, 19);
            this.Lradnik.TabIndex = 6;
            this.Lradnik.Text = "Radnik ";
            // 
            // Lime
            // 
            this.Lime.AutoSize = true;
            this.Lime.Location = new System.Drawing.Point(108, 82);
            this.Lime.Name = "Lime";
            this.Lime.Size = new System.Drawing.Size(95, 19);
            this.Lime.TabIndex = 7;
            this.Lime.Text = "Kaja Mihajlovic";
            // 
            // btnOK
            // 
            this.btnOK.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnOK.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnOK.Location = new System.Drawing.Point(108, 340);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(87, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "Sačuvaj";
            this.btnOK.UseSelectable = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnCancel.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnCancel.Location = new System.Drawing.Point(268, 340);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Odustani";
            this.btnCancel.UseSelectable = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblError.ForeColor = System.Drawing.Color.DarkRed;
            this.lblError.Location = new System.Drawing.Point(108, 117);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(44, 19);
            this.lblError.TabIndex = 11;
            this.lblError.Text = "error";
            this.lblError.UseCustomForeColor = true;
            // 
            // SankcijaStimulacijaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 377);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.Lime);
            this.Controls.Add(this.Lradnik);
            this.Controls.Add(this.txtSankcijaStimulacija);
            this.Controls.Add(this.LSankcijaStimulacija);
            this.Controls.Add(this.txtOpis);
            this.Controls.Add(this.LOpis);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.Name = "SankcijaStimulacijaForm";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Pink;
            this.Text = "SankStim";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.SankcijaStimulacijaForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private MetroFramework.Controls.MetroLabel LOpis;
		private MetroFramework.Controls.MetroTextBox txtOpis;
		private MetroFramework.Controls.MetroLabel LSankcijaStimulacija;
		private MetroFramework.Controls.MetroTextBox txtSankcijaStimulacija;
		private MetroFramework.Controls.MetroLabel Lradnik;
		private MetroFramework.Controls.MetroLabel Lime;
		private MetroFramework.Controls.MetroButton btnOK;
		private MetroFramework.Controls.MetroButton btnCancel;
		private MetroFramework.Controls.MetroLabel lblError;
    }
}