﻿namespace SharedComponents.Forms
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.tbKorisnickoIme = new MetroFramework.Controls.MetroTextBox();
            this.tbLozinka = new MetroFramework.Controls.MetroTextBox();
            this.lblKorisnickoIme = new MetroFramework.Controls.MetroLabel();
            this.lblPassword = new MetroFramework.Controls.MetroLabel();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.circularProgressBar1 = new CircularProgressBar.CircularProgressBar();
            this.lblSpinner = new MetroFramework.Controls.MetroLabel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblBazaPodataka = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.tableLayoutPanel1.Controls.Add(this.metroLabel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tbKorisnickoIme, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbLozinka, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblKorisnickoIme, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblPassword, 1, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(137, 80);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(308, 101);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(3, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(96, 19);
            this.metroLabel1.TabIndex = 5;
            this.metroLabel1.Text = "Korisničko ime:";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(3, 50);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(55, 19);
            this.metroLabel2.TabIndex = 6;
            this.metroLabel2.Text = "Lozinka:";
            // 
            // tbKorisnickoIme
            // 
            // 
            // 
            // 
            this.tbKorisnickoIme.CustomButton.Image = null;
            this.tbKorisnickoIme.CustomButton.Location = new System.Drawing.Point(178, 2);
            this.tbKorisnickoIme.CustomButton.Name = "";
            this.tbKorisnickoIme.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.tbKorisnickoIme.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbKorisnickoIme.CustomButton.TabIndex = 1;
            this.tbKorisnickoIme.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbKorisnickoIme.CustomButton.UseSelectable = true;
            this.tbKorisnickoIme.CustomButton.Visible = false;
            this.tbKorisnickoIme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbKorisnickoIme.Lines = new string[0];
            this.tbKorisnickoIme.Location = new System.Drawing.Point(105, 3);
            this.tbKorisnickoIme.MaxLength = 32767;
            this.tbKorisnickoIme.Name = "tbKorisnickoIme";
            this.tbKorisnickoIme.PasswordChar = '\0';
            this.tbKorisnickoIme.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbKorisnickoIme.SelectedText = "";
            this.tbKorisnickoIme.SelectionLength = 0;
            this.tbKorisnickoIme.SelectionStart = 0;
            this.tbKorisnickoIme.ShortcutsEnabled = true;
            this.tbKorisnickoIme.Size = new System.Drawing.Size(200, 24);
            this.tbKorisnickoIme.TabIndex = 0;
            this.tbKorisnickoIme.UseSelectable = true;
            this.tbKorisnickoIme.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbKorisnickoIme.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbKorisnickoIme.Click += new System.EventHandler(this.tbKorisnickoIme_Click);
            // 
            // tbLozinka
            // 
            // 
            // 
            // 
            this.tbLozinka.CustomButton.Image = null;
            this.tbLozinka.CustomButton.Location = new System.Drawing.Point(178, 2);
            this.tbLozinka.CustomButton.Name = "";
            this.tbLozinka.CustomButton.Size = new System.Drawing.Size(19, 19);
            this.tbLozinka.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbLozinka.CustomButton.TabIndex = 1;
            this.tbLozinka.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbLozinka.CustomButton.UseSelectable = true;
            this.tbLozinka.CustomButton.Visible = false;
            this.tbLozinka.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbLozinka.Lines = new string[0];
            this.tbLozinka.Location = new System.Drawing.Point(105, 53);
            this.tbLozinka.MaxLength = 32767;
            this.tbLozinka.Name = "tbLozinka";
            this.tbLozinka.PasswordChar = '●';
            this.tbLozinka.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbLozinka.SelectedText = "";
            this.tbLozinka.SelectionLength = 0;
            this.tbLozinka.SelectionStart = 0;
            this.tbLozinka.ShortcutsEnabled = true;
            this.tbLozinka.Size = new System.Drawing.Size(200, 24);
            this.tbLozinka.TabIndex = 1;
            this.tbLozinka.UseSelectable = true;
            this.tbLozinka.UseSystemPasswordChar = true;
            this.tbLozinka.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbLozinka.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbLozinka.Click += new System.EventHandler(this.tbLozinka_Click);
            this.tbLozinka.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbLozinka_KeyPress);
            // 
            // lblKorisnickoIme
            // 
            this.lblKorisnickoIme.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblKorisnickoIme.AutoSize = true;
            this.lblKorisnickoIme.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblKorisnickoIme.ForeColor = System.Drawing.Color.Red;
            this.lblKorisnickoIme.Location = new System.Drawing.Point(215, 30);
            this.lblKorisnickoIme.Name = "lblKorisnickoIme";
            this.lblKorisnickoIme.Size = new System.Drawing.Size(90, 15);
            this.lblKorisnickoIme.TabIndex = 4;
            this.lblKorisnickoIme.Text = "Neispravan unos";
            this.lblKorisnickoIme.UseCustomForeColor = true;
            this.lblKorisnickoIme.UseStyleColors = true;
            this.lblKorisnickoIme.Visible = false;
            // 
            // lblPassword
            // 
            this.lblPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPassword.AutoSize = true;
            this.lblPassword.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblPassword.ForeColor = System.Drawing.Color.Red;
            this.lblPassword.Location = new System.Drawing.Point(215, 80);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(90, 15);
            this.lblPassword.TabIndex = 5;
            this.lblPassword.Text = "Neispravan unos";
            this.lblPassword.UseCustomForeColor = true;
            this.lblPassword.UseStyleColors = true;
            this.lblPassword.Visible = false;
            // 
            // metroButton1
            // 
            this.metroButton1.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.metroButton1.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.metroButton1.Location = new System.Drawing.Point(275, 199);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(167, 26);
            this.metroButton1.TabIndex = 0;
            this.metroButton1.Text = "Prijavi se";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // circularProgressBar1
            // 
            this.circularProgressBar1.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.circularProgressBar1.AnimationSpeed = 5000;
            this.circularProgressBar1.BackColor = System.Drawing.Color.Transparent;
            this.circularProgressBar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold);
            this.circularProgressBar1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.circularProgressBar1.InnerColor = System.Drawing.SystemColors.ControlLightLight;
            this.circularProgressBar1.InnerMargin = 2;
            this.circularProgressBar1.InnerWidth = -1;
            this.circularProgressBar1.Location = new System.Drawing.Point(401, 29);
            this.circularProgressBar1.MarqueeAnimationSpeed = 500;
            this.circularProgressBar1.Name = "circularProgressBar1";
            this.circularProgressBar1.OuterColor = System.Drawing.SystemColors.ControlLightLight;
            this.circularProgressBar1.OuterMargin = -25;
            this.circularProgressBar1.OuterWidth = 26;
            this.circularProgressBar1.ProgressColor = System.Drawing.SystemColors.MenuHighlight;
            this.circularProgressBar1.ProgressWidth = 5;
            this.circularProgressBar1.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.circularProgressBar1.Size = new System.Drawing.Size(45, 40);
            this.circularProgressBar1.StartAngle = 90;
            this.circularProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.circularProgressBar1.SubscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circularProgressBar1.SubscriptMargin = new System.Windows.Forms.Padding(10, -35, 0, 0);
            this.circularProgressBar1.SubscriptText = ".23";
            this.circularProgressBar1.SuperscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circularProgressBar1.SuperscriptMargin = new System.Windows.Forms.Padding(10, 35, 0, 0);
            this.circularProgressBar1.SuperscriptText = "";
            this.circularProgressBar1.TabIndex = 2;
            this.circularProgressBar1.TextMargin = new System.Windows.Forms.Padding(8, 8, 0, 0);
            this.circularProgressBar1.Value = 20;
            // 
            // lblSpinner
            // 
            this.lblSpinner.AutoSize = true;
            this.lblSpinner.Location = new System.Drawing.Point(323, 50);
            this.lblSpinner.Name = "lblSpinner";
            this.lblSpinner.Size = new System.Drawing.Size(77, 19);
            this.lblSpinner.TabIndex = 3;
            this.lblSpinner.Text = "Učitavanje...";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::SharedComponents.Properties.Resources.novi_login;
            this.pictureBox1.Location = new System.Drawing.Point(23, 77);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(108, 104);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lblBazaPodataka
            // 
            this.lblBazaPodataka.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBazaPodataka.AutoSize = true;
            this.lblBazaPodataka.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblBazaPodataka.ForeColor = System.Drawing.Color.Red;
            this.lblBazaPodataka.Location = new System.Drawing.Point(21, 210);
            this.lblBazaPodataka.Name = "lblBazaPodataka";
            this.lblBazaPodataka.Size = new System.Drawing.Size(248, 15);
            this.lblBazaPodataka.TabIndex = 5;
            this.lblBazaPodataka.Text = "Postoji problem prilikom pristupa bazi podataka";
            this.lblBazaPodataka.UseCustomForeColor = true;
            this.lblBazaPodataka.UseStyleColors = true;
            this.lblBazaPodataka.Visible = false;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 233);
            this.Controls.Add(this.lblBazaPodataka);
            this.Controls.Add(this.lblSpinner);
            this.Controls.Add(this.circularProgressBar1);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.pictureBox1);
            this.MaximizeBox = false;
            this.Name = "Login";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Lime;
            this.Text = "OPZ–2018: Login";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Login_FormClosing);
            this.Load += new System.EventHandler(this.Login_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox tbKorisnickoIme;
        private MetroFramework.Controls.MetroTextBox tbLozinka;
        private MetroFramework.Controls.MetroLabel lblKorisnickoIme;
        private MetroFramework.Controls.MetroLabel lblPassword;
        private MetroFramework.Controls.MetroButton metroButton1;
        private CircularProgressBar.CircularProgressBar circularProgressBar1;
        private MetroFramework.Controls.MetroLabel lblSpinner;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private MetroFramework.Controls.MetroLabel lblBazaPodataka;
    }
}