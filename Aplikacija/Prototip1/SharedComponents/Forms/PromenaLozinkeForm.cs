﻿using MetroFramework.Forms;
using Persistence.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using MetroFramework;
using static SharedComponents.Forms.Login;

namespace SharedComponents.Forms
{
    public partial class PromenaLozinkeForm : MetroForm
    {
        public PromenaLozinkeForm()
        {
            InitializeComponent();
            lblSpinner.Visible = false;
            circularProgressBar1.Visible = false;
        }

        private string username;
        private DialogResult dlgrslt;

        private void PromenaLozinkeForm_Load(object sender, EventArgs e)
        {
            IUsernameOwner p = this.Owner as IUsernameOwner;
            if (p == null) return;
            username = p.Username;
            lblUsername.Text = username;

            Personalize();
        }
        private void Personalize()
        {
            MetroColorStyle stil = (Owner as MetroForm).Style;
            this.Style = stil;
            tbLozinka.Style = stil;
            tbNovaLozinka.Style = stil;
            tbNovaLozinkaPonovo.Style = stil;

            if (stil == MetroColorStyle.Green)
                circularProgressBar1.ProgressColor = Color.ForestGreen;
            else if (stil == MetroColorStyle.Purple)
                circularProgressBar1.ProgressColor = Color.Purple;
            else if (stil == MetroColorStyle.Pink)
                circularProgressBar1.ProgressColor = Color.PaleVioletRed;

        }

        private void metroTextBox2_Click(object sender, EventArgs e)
        {
            lblKorisnickoIme.Visible = false;
            lblPassword.Visible = false;
            lblPasswordPonovo.Visible = false;
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            if(tbNovaLozinka.Text.Trim() == String.Empty)
            {
                lblPassword.Visible = true;
                return;
            }
            if(tbNovaLozinka.Text != tbNovaLozinkaPonovo.Text)
            {
                lblPasswordPonovo.Visible = true;
                return;
            }
            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync(new Tuple<string,string,string>(tbLozinka.Text,tbNovaLozinka.Text,lblUsername.Text));
            }
        }

        private delegate void Del2();
        private delegate void Del1(List<Zaposleni> lista);
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Del2 spnstop = StopSpinning;
            Tuple<string, string, string> arg = e.Argument as Tuple<string, string, string>;
            Invoke(spnstart);
            List<Zaposleni> lista = null;
            using (OpzContext kontekst = new OpzContext())
            {
                lista = kontekst.ListaZaposlenih.Where(zap1 => zap1.KorisnickoIme == arg.Item3).ToList();
                if (lista.Count == 0)
                {
                    Del2 dd = ShowError;
                    Invoke(dd);
                    dlgrslt = DialogResult.Cancel;
                    return;
                }
                Zaposleni zap = lista[0];
                String pass = arg.Item1 + zap.Salt;

                System.Text.Encoding enc = System.Text.Encoding.ASCII;
                SHA256Managed alg = new SHA256Managed();
                byte[] passwordAndSaltHashed = alg.ComputeHash(new MemoryStream(Encoding.ASCII.GetBytes(pass)));
                string forCompare = enc.GetString(passwordAndSaltHashed);

                if (zap.LozinkaHash != forCompare)
                {
                    Del2 dd = ShowError;
                    Invoke(dd);
                    dlgrslt = DialogResult.Cancel;
                    return;
                }
                else
                {
                    //promena lozinke i povratak u login form
                    RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();
                    byte[] salt1 = new byte[100];
                    rngCsp.GetBytes(salt1, 0, 100);
                    //System.Text.Encoding enc = System.Text.Encoding.ASCII;
                    string salt = enc.GetString(salt1);
                    string forHash = arg.Item2 + salt;


                    zap.Salt = salt;
                    //SHA256Managed alg = new SHA256Managed();
                    byte[] passwordAndSaltHashed1 = alg.ComputeHash(new MemoryStream(enc.GetBytes(forHash)));
                    zap.LozinkaHash = enc.GetString(passwordAndSaltHashed1);
                    kontekst.SaveChanges();
                    dlgrslt = DialogResult.OK;
                }
            }
            //Del1 del = CheckLogin;
            // Thread.Sleep(2000);
            //Invoke(del, new object[] { lista });

        }

        
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Del2 spnstop = StopSpinning;
            //zaustavljanje spinnera
            Invoke(spnstop);
            if (dlgrslt == DialogResult.OK) DialogResult = DialogResult.OK;
        }

        private void ShowError()
        {
            //lblPassword.Visible = true;
            lblKorisnickoIme.Visible = true;
        }

        private void NoMatch()
        {
            lblPasswordPonovo.Visible = true;
        }

        private void StartSpinning()
        {
            lblSpinner.Visible = true;
            circularProgressBar1.Visible = true;

            tbLozinka.Enabled = false;
            tbNovaLozinka.Enabled = false;
            tbNovaLozinkaPonovo.Enabled = false;
        }

        private void StopSpinning()
        {
            lblSpinner.Visible = false;
            circularProgressBar1.Visible = false;

            tbLozinka.Enabled = true;
            tbNovaLozinka.Enabled = true;
            tbNovaLozinkaPonovo.Enabled = true;
        }

        private void PromenaLozinkeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                e.Cancel = true;
            }
        }
    }
}
