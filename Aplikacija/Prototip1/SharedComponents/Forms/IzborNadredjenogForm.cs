﻿using MetroFramework.Forms;
using Persistence.Entities;
using SharedComponents.Presenters;
using SharedComponents.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace SharedComponents.Forms
{
    public partial class IzborNadredjenogForm : MetroForm, IIzborNadredjenogView
    {
        private OpzContext kontekst;

        public IzborNadredjenogForm()
        {
            InitializeComponent();
        }

        public int IndeksNadredjenog { get { if (metroGrid1.SelectedRows.Count == 0) return -1; else return (int)metroGrid1.SelectedRows[0].Cells[0].Value; } }

        public IZaposleniPresenter Presenter { get; set; }

        private void GlavnaForma_Load(object sender, EventArgs e)
        {
            kontekst = new OpzContext();
            backgroundWorker1.RunWorkerAsync(String.Empty);
        }

        private void ListRefresh(object col)
        {
            metroGrid1.DataSource = col;

            metroGrid1.Columns[0].HeaderText = "Broj zaposlenog";
            metroGrid1.Columns[1].HeaderText = "Ime";
            metroGrid1.Columns[2].HeaderText = "Srednje slovo";
            metroGrid1.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            metroGrid1.Columns[3].HeaderText = "Prezime";
            metroGrid1.Columns[4].HeaderText = "Radno mesto";
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync(txtSearch.Text);
            }
        }

        delegate void Del1(object a);
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Del2 spnstop = StopSpinning;
            String filter = (String)e.Argument;
            Invoke(spnstart);
            List<Zaposleni> col1 = kontekst.PretraziZaposlene(txtSearch.Text).Where(z=>z.UlogaKorisnickogNaloga== "Menadžer").ToList();

            var col = (from z in col1
                       orderby z.Id
                       select new
                       {
                           BrojZaposlenog = z.Id,
                           Ime = z.Ime,
                           SrednjeSlovo = z.SSlovo,
                           Prezime = z.Prezime,
                           RadnoMesto = z.RadnoMesto
                       }).ToList();

            Del1 del = ListRefresh;
            Invoke(del, new object[] { col });
        }

        private delegate void Del2();
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Del2 spnstop = StopSpinning;
            //zaustavljanje spinnera
            Invoke(spnstop);
        }

        #region Spinner
        private void StartSpinning()
        {
            lblSpinner.Visible = true;
            circularProgressBar1.Visible = true;

            metroButton1.Enabled = false;
            metroButton2.Enabled = false;
            txtSearch.Enabled = false;
        }

        private void StopSpinning()
        {
            lblSpinner.Visible = false;
            circularProgressBar1.Visible = false;

            metroButton1.Enabled = true;
            metroButton2.Enabled = true;
            txtSearch.Enabled = true;
        }
        #endregion

        private void metroButton2_Click(object sender, EventArgs e)
        {
            Presenter.UzmiIndeksNadredjenog();
            Hide();
        }

        private void IzborNadredjenogForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                e.Cancel = true;
            }
        }
    }
}
