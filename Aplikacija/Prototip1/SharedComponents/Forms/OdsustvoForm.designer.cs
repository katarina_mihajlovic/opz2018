﻿namespace SharedComponents.Forms
{
    partial class OdsustvoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPocetak = new MetroFramework.Controls.MetroLabel();
            this.lblKraj = new MetroFramework.Controls.MetroLabel();
            this.lblTip = new MetroFramework.Controls.MetroLabel();
            this.cbxOdsustva = new MetroFramework.Controls.MetroComboBox();
            this.dtmPocetak = new MetroFramework.Controls.MetroDateTime();
            this.dtmKraj = new MetroFramework.Controls.MetroDateTime();
            this.lblDelovodnik = new MetroFramework.Controls.MetroLabel();
            this.txtDelovodnik = new MetroFramework.Controls.MetroTextBox();
            this.btnDodaj = new MetroFramework.Controls.MetroButton();
            this.btnIzadji = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // lblPocetak
            // 
            this.lblPocetak.AutoSize = true;
            this.lblPocetak.Location = new System.Drawing.Point(31, 92);
            this.lblPocetak.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPocetak.Name = "lblPocetak";
            this.lblPocetak.Size = new System.Drawing.Size(109, 19);
            this.lblPocetak.TabIndex = 0;
            this.lblPocetak.Text = "Početak odsustva";
            // 
            // lblKraj
            // 
            this.lblKraj.AutoSize = true;
            this.lblKraj.Location = new System.Drawing.Point(31, 125);
            this.lblKraj.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKraj.Name = "lblKraj";
            this.lblKraj.Size = new System.Drawing.Size(85, 19);
            this.lblKraj.TabIndex = 1;
            this.lblKraj.Text = "Kraj odsustva";
            // 
            // lblTip
            // 
            this.lblTip.AutoSize = true;
            this.lblTip.Location = new System.Drawing.Point(31, 158);
            this.lblTip.Name = "lblTip";
            this.lblTip.Size = new System.Drawing.Size(81, 19);
            this.lblTip.TabIndex = 2;
            this.lblTip.Text = "Tip odsustva";
            // 
            // cbxOdsustva
            // 
            this.cbxOdsustva.FormattingEnabled = true;
            this.cbxOdsustva.ItemHeight = 23;
            this.cbxOdsustva.Items.AddRange(new object[] {
            "Godišnji odmor",
            "Plaćeno",
            "Neplaćeno",
            "Bolovanje"});
            this.cbxOdsustva.Location = new System.Drawing.Point(196, 153);
            this.cbxOdsustva.Name = "cbxOdsustva";
            this.cbxOdsustva.Size = new System.Drawing.Size(131, 29);
            this.cbxOdsustva.Style = MetroFramework.MetroColorStyle.Purple;
            this.cbxOdsustva.TabIndex = 3;
            this.cbxOdsustva.UseSelectable = true;
            // 
            // dtmPocetak
            // 
            this.dtmPocetak.CustomFormat = "dd.MM.yyyy.";
            this.dtmPocetak.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtmPocetak.Location = new System.Drawing.Point(196, 87);
            this.dtmPocetak.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtmPocetak.Name = "dtmPocetak";
            this.dtmPocetak.Size = new System.Drawing.Size(131, 29);
            this.dtmPocetak.Style = MetroFramework.MetroColorStyle.Purple;
            this.dtmPocetak.TabIndex = 4;
            // 
            // dtmKraj
            // 
            this.dtmKraj.CustomFormat = "dd.MM.yyyy.";
            this.dtmKraj.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtmKraj.Location = new System.Drawing.Point(196, 120);
            this.dtmKraj.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtmKraj.Name = "dtmKraj";
            this.dtmKraj.Size = new System.Drawing.Size(131, 29);
            this.dtmKraj.Style = MetroFramework.MetroColorStyle.Purple;
            this.dtmKraj.TabIndex = 5;
            // 
            // lblDelovodnik
            // 
            this.lblDelovodnik.AutoSize = true;
            this.lblDelovodnik.Location = new System.Drawing.Point(31, 188);
            this.lblDelovodnik.Name = "lblDelovodnik";
            this.lblDelovodnik.Size = new System.Drawing.Size(119, 19);
            this.lblDelovodnik.TabIndex = 6;
            this.lblDelovodnik.Text = "Broj u delovodniku";
            // 
            // txtDelovodnik
            // 
            // 
            // 
            // 
            this.txtDelovodnik.CustomButton.Image = null;
            this.txtDelovodnik.CustomButton.Location = new System.Drawing.Point(109, 1);
            this.txtDelovodnik.CustomButton.Name = "";
            this.txtDelovodnik.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtDelovodnik.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtDelovodnik.CustomButton.TabIndex = 1;
            this.txtDelovodnik.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtDelovodnik.CustomButton.UseSelectable = true;
            this.txtDelovodnik.CustomButton.Visible = false;
            this.txtDelovodnik.Lines = new string[0];
            this.txtDelovodnik.Location = new System.Drawing.Point(196, 186);
            this.txtDelovodnik.MaxLength = 32767;
            this.txtDelovodnik.Name = "txtDelovodnik";
            this.txtDelovodnik.PasswordChar = '\0';
            this.txtDelovodnik.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDelovodnik.SelectedText = "";
            this.txtDelovodnik.SelectionLength = 0;
            this.txtDelovodnik.SelectionStart = 0;
            this.txtDelovodnik.ShortcutsEnabled = true;
            this.txtDelovodnik.Size = new System.Drawing.Size(131, 23);
            this.txtDelovodnik.Style = MetroFramework.MetroColorStyle.Purple;
            this.txtDelovodnik.TabIndex = 7;
            this.txtDelovodnik.UseSelectable = true;
            this.txtDelovodnik.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtDelovodnik.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnDodaj
            // 
            this.btnDodaj.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnDodaj.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnDodaj.Location = new System.Drawing.Point(31, 236);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(119, 23);
            this.btnDodaj.TabIndex = 8;
            this.btnDodaj.Text = "Dodaj odsustvo";
            this.btnDodaj.UseSelectable = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnIzadji
            // 
            this.btnIzadji.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnIzadji.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnIzadji.Location = new System.Drawing.Point(219, 236);
            this.btnIzadji.Name = "btnIzadji";
            this.btnIzadji.Size = new System.Drawing.Size(108, 23);
            this.btnIzadji.TabIndex = 9;
            this.btnIzadji.Text = "Izađi";
            this.btnIzadji.UseSelectable = true;
            this.btnIzadji.Click += new System.EventHandler(this.btnIzadji_Click);
            // 
            // OdsustvoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 279);
            this.Controls.Add(this.btnIzadji);
            this.Controls.Add(this.btnDodaj);
            this.Controls.Add(this.txtDelovodnik);
            this.Controls.Add(this.lblDelovodnik);
            this.Controls.Add(this.dtmKraj);
            this.Controls.Add(this.dtmPocetak);
            this.Controls.Add(this.cbxOdsustva);
            this.Controls.Add(this.lblTip);
            this.Controls.Add(this.lblKraj);
            this.Controls.Add(this.lblPocetak);
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "OdsustvoForm";
            this.Padding = new System.Windows.Forms.Padding(27, 92, 27, 31);
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Purple;
            this.Text = "Unos novog odsustva";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblPocetak;
        private MetroFramework.Controls.MetroLabel lblKraj;
        private MetroFramework.Controls.MetroLabel lblTip;
        private MetroFramework.Controls.MetroComboBox cbxOdsustva;
        private MetroFramework.Controls.MetroDateTime dtmPocetak;
        private MetroFramework.Controls.MetroDateTime dtmKraj;
        private MetroFramework.Controls.MetroLabel lblDelovodnik;
        private MetroFramework.Controls.MetroTextBox txtDelovodnik;
        private MetroFramework.Controls.MetroButton btnDodaj;
        private MetroFramework.Controls.MetroButton btnIzadji;
    }
}