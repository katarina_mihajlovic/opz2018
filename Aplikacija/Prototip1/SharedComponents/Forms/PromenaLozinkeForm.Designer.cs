﻿namespace SharedComponents.Forms
{
    partial class PromenaLozinkeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.lblKorisnickoIme = new MetroFramework.Controls.MetroLabel();
            this.lblPassword = new MetroFramework.Controls.MetroLabel();
            this.tbNovaLozinka = new MetroFramework.Controls.MetroTextBox();
            this.tbLozinka = new MetroFramework.Controls.MetroTextBox();
            this.lblUsername = new MetroFramework.Controls.MetroLabel();
            this.lblPasswordPonovo = new MetroFramework.Controls.MetroLabel();
            this.tbNovaLozinkaPonovo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.circularProgressBar1 = new CircularProgressBar.CircularProgressBar();
            this.lblSpinner = new MetroFramework.Controls.MetroLabel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.85714F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.14286F));
            this.tableLayoutPanel1.Controls.Add(this.metroLabel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblKorisnickoIme, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblPassword, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.tbNovaLozinka, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tbLozinka, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblUsername, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblPasswordPonovo, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.tbNovaLozinkaPonovo, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel5, 0, 5);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(23, 87);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(356, 210);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(3, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(96, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Korisničko ime:";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(3, 50);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(86, 19);
            this.metroLabel2.TabIndex = 1;
            this.metroLabel2.Text = "Stara lozinka:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(3, 100);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(87, 19);
            this.metroLabel3.TabIndex = 2;
            this.metroLabel3.Text = "Nova lozinka:";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKorisnickoIme
            // 
            this.lblKorisnickoIme.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblKorisnickoIme.AutoSize = true;
            this.lblKorisnickoIme.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblKorisnickoIme.ForeColor = System.Drawing.Color.Red;
            this.lblKorisnickoIme.Location = new System.Drawing.Point(263, 80);
            this.lblKorisnickoIme.Name = "lblKorisnickoIme";
            this.lblKorisnickoIme.Size = new System.Drawing.Size(90, 15);
            this.lblKorisnickoIme.TabIndex = 3;
            this.lblKorisnickoIme.Text = "Neispravan unos";
            this.lblKorisnickoIme.UseCustomForeColor = true;
            this.lblKorisnickoIme.UseStyleColors = true;
            this.lblKorisnickoIme.Visible = false;
            // 
            // lblPassword
            // 
            this.lblPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPassword.AutoSize = true;
            this.lblPassword.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblPassword.ForeColor = System.Drawing.Color.Red;
            this.lblPassword.Location = new System.Drawing.Point(263, 130);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(90, 15);
            this.lblPassword.TabIndex = 4;
            this.lblPassword.Text = "Neispravan unos";
            this.lblPassword.UseCustomForeColor = true;
            this.lblPassword.UseStyleColors = true;
            this.lblPassword.Visible = false;
            // 
            // tbNovaLozinka
            // 
            // 
            // 
            // 
            this.tbNovaLozinka.CustomButton.Image = null;
            this.tbNovaLozinka.CustomButton.Location = new System.Drawing.Point(176, 1);
            this.tbNovaLozinka.CustomButton.Name = "";
            this.tbNovaLozinka.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbNovaLozinka.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbNovaLozinka.CustomButton.TabIndex = 1;
            this.tbNovaLozinka.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbNovaLozinka.CustomButton.UseSelectable = true;
            this.tbNovaLozinka.CustomButton.Visible = false;
            this.tbNovaLozinka.Lines = new string[0];
            this.tbNovaLozinka.Location = new System.Drawing.Point(155, 103);
            this.tbNovaLozinka.MaxLength = 32767;
            this.tbNovaLozinka.Name = "tbNovaLozinka";
            this.tbNovaLozinka.PasswordChar = '●';
            this.tbNovaLozinka.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbNovaLozinka.SelectedText = "";
            this.tbNovaLozinka.SelectionLength = 0;
            this.tbNovaLozinka.SelectionStart = 0;
            this.tbNovaLozinka.ShortcutsEnabled = true;
            this.tbNovaLozinka.Size = new System.Drawing.Size(198, 23);
            this.tbNovaLozinka.TabIndex = 1;
            this.tbNovaLozinka.UseSelectable = true;
            this.tbNovaLozinka.UseSystemPasswordChar = true;
            this.tbNovaLozinka.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbNovaLozinka.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbNovaLozinka.Click += new System.EventHandler(this.metroTextBox2_Click);
            // 
            // tbLozinka
            // 
            // 
            // 
            // 
            this.tbLozinka.CustomButton.Image = null;
            this.tbLozinka.CustomButton.Location = new System.Drawing.Point(176, 1);
            this.tbLozinka.CustomButton.Name = "";
            this.tbLozinka.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbLozinka.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbLozinka.CustomButton.TabIndex = 1;
            this.tbLozinka.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbLozinka.CustomButton.UseSelectable = true;
            this.tbLozinka.CustomButton.Visible = false;
            this.tbLozinka.Lines = new string[0];
            this.tbLozinka.Location = new System.Drawing.Point(155, 53);
            this.tbLozinka.MaxLength = 32767;
            this.tbLozinka.Name = "tbLozinka";
            this.tbLozinka.PasswordChar = '●';
            this.tbLozinka.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbLozinka.SelectedText = "";
            this.tbLozinka.SelectionLength = 0;
            this.tbLozinka.SelectionStart = 0;
            this.tbLozinka.ShortcutsEnabled = true;
            this.tbLozinka.Size = new System.Drawing.Size(198, 23);
            this.tbLozinka.TabIndex = 0;
            this.tbLozinka.UseSelectable = true;
            this.tbLozinka.UseSystemPasswordChar = true;
            this.tbLozinka.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbLozinka.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.tbLozinka.Click += new System.EventHandler(this.metroTextBox2_Click);
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblUsername.Location = new System.Drawing.Point(155, 0);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(85, 19);
            this.lblUsername.TabIndex = 7;
            this.lblUsername.Text = "lblUsername";
            // 
            // lblPasswordPonovo
            // 
            this.lblPasswordPonovo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPasswordPonovo.AutoSize = true;
            this.lblPasswordPonovo.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblPasswordPonovo.ForeColor = System.Drawing.Color.Red;
            this.lblPasswordPonovo.Location = new System.Drawing.Point(229, 180);
            this.lblPasswordPonovo.Name = "lblPasswordPonovo";
            this.lblPasswordPonovo.Size = new System.Drawing.Size(124, 15);
            this.lblPasswordPonovo.TabIndex = 8;
            this.lblPasswordPonovo.Text = "Lozinke se ne poklapaju";
            this.lblPasswordPonovo.UseCustomForeColor = true;
            this.lblPasswordPonovo.UseStyleColors = true;
            this.lblPasswordPonovo.Visible = false;
            // 
            // tbNovaLozinkaPonovo
            // 
            // 
            // 
            // 
            this.tbNovaLozinkaPonovo.CustomButton.Image = null;
            this.tbNovaLozinkaPonovo.CustomButton.Location = new System.Drawing.Point(176, 1);
            this.tbNovaLozinkaPonovo.CustomButton.Name = "";
            this.tbNovaLozinkaPonovo.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbNovaLozinkaPonovo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbNovaLozinkaPonovo.CustomButton.TabIndex = 1;
            this.tbNovaLozinkaPonovo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbNovaLozinkaPonovo.CustomButton.UseSelectable = true;
            this.tbNovaLozinkaPonovo.CustomButton.Visible = false;
            this.tbNovaLozinkaPonovo.Lines = new string[0];
            this.tbNovaLozinkaPonovo.Location = new System.Drawing.Point(155, 153);
            this.tbNovaLozinkaPonovo.MaxLength = 32767;
            this.tbNovaLozinkaPonovo.Name = "tbNovaLozinkaPonovo";
            this.tbNovaLozinkaPonovo.PasswordChar = '●';
            this.tbNovaLozinkaPonovo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbNovaLozinkaPonovo.SelectedText = "";
            this.tbNovaLozinkaPonovo.SelectionLength = 0;
            this.tbNovaLozinkaPonovo.SelectionStart = 0;
            this.tbNovaLozinkaPonovo.ShortcutsEnabled = true;
            this.tbNovaLozinkaPonovo.Size = new System.Drawing.Size(198, 23);
            this.tbNovaLozinkaPonovo.TabIndex = 2;
            this.tbNovaLozinkaPonovo.UseSelectable = true;
            this.tbNovaLozinkaPonovo.UseSystemPasswordChar = true;
            this.tbNovaLozinkaPonovo.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbNovaLozinkaPonovo.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(3, 150);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(144, 19);
            this.metroLabel5.TabIndex = 10;
            this.metroLabel5.Text = "Nova lozinka (ponovo):";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroButton1
            // 
            this.metroButton1.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton1.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.metroButton1.Location = new System.Drawing.Point(209, 304);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(167, 23);
            this.metroButton1.TabIndex = 3;
            this.metroButton1.Text = "Promeni lozinku";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // circularProgressBar1
            // 
            this.circularProgressBar1.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.Liner;
            this.circularProgressBar1.AnimationSpeed = 5000;
            this.circularProgressBar1.BackColor = System.Drawing.Color.Transparent;
            this.circularProgressBar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Bold);
            this.circularProgressBar1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.circularProgressBar1.InnerColor = System.Drawing.SystemColors.ControlLightLight;
            this.circularProgressBar1.InnerMargin = 2;
            this.circularProgressBar1.InnerWidth = -1;
            this.circularProgressBar1.Location = new System.Drawing.Point(334, 41);
            this.circularProgressBar1.MarqueeAnimationSpeed = 500;
            this.circularProgressBar1.Name = "circularProgressBar1";
            this.circularProgressBar1.OuterColor = System.Drawing.SystemColors.ControlLightLight;
            this.circularProgressBar1.OuterMargin = -25;
            this.circularProgressBar1.OuterWidth = 26;
            this.circularProgressBar1.ProgressColor = System.Drawing.SystemColors.MenuHighlight;
            this.circularProgressBar1.ProgressWidth = 5;
            this.circularProgressBar1.SecondaryFont = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.circularProgressBar1.Size = new System.Drawing.Size(45, 40);
            this.circularProgressBar1.StartAngle = 90;
            this.circularProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.circularProgressBar1.SubscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circularProgressBar1.SubscriptMargin = new System.Windows.Forms.Padding(10, -35, 0, 0);
            this.circularProgressBar1.SubscriptText = ".23";
            this.circularProgressBar1.SuperscriptColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.circularProgressBar1.SuperscriptMargin = new System.Windows.Forms.Padding(10, 35, 0, 0);
            this.circularProgressBar1.SuperscriptText = "";
            this.circularProgressBar1.TabIndex = 3;
            this.circularProgressBar1.TextMargin = new System.Windows.Forms.Padding(8, 8, 0, 0);
            this.circularProgressBar1.Value = 20;
            // 
            // lblSpinner
            // 
            this.lblSpinner.AutoSize = true;
            this.lblSpinner.Location = new System.Drawing.Point(251, 60);
            this.lblSpinner.Name = "lblSpinner";
            this.lblSpinner.Size = new System.Drawing.Size(77, 19);
            this.lblSpinner.TabIndex = 4;
            this.lblSpinner.Text = "Učitavanje...";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // PromenaLozinkeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(402, 347);
            this.Controls.Add(this.lblSpinner);
            this.Controls.Add(this.circularProgressBar1);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.Name = "PromenaLozinkeForm";
            this.Resizable = false;
            this.Text = "Promena lozinke";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PromenaLozinkeForm_FormClosing);
            this.Load += new System.EventHandler(this.PromenaLozinkeForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel lblKorisnickoIme;
        private MetroFramework.Controls.MetroLabel lblPassword;
        private MetroFramework.Controls.MetroTextBox tbNovaLozinka;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroTextBox tbLozinka;
        private MetroFramework.Controls.MetroLabel lblUsername;
        private CircularProgressBar.CircularProgressBar circularProgressBar1;
        private MetroFramework.Controls.MetroLabel lblSpinner;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private MetroFramework.Controls.MetroLabel lblPasswordPonovo;
        private MetroFramework.Controls.MetroTextBox tbNovaLozinkaPonovo;
        private MetroFramework.Controls.MetroLabel metroLabel5;
    }
}