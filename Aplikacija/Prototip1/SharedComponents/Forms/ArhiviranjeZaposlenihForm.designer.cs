﻿namespace SharedComponents.Forms
{
    partial class ArhiviranjeZaposlenihForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gridLevi = new MetroFramework.Controls.MetroGrid();
            this.gridDesni = new MetroFramework.Controls.MetroGrid();
            this.lblSkorasnjiOtpustena = new MetroFramework.Controls.MetroLabel();
            this.lblArhiva = new MetroFramework.Controls.MetroLabel();
            this.btnArhivirajOznacene = new MetroFramework.Controls.MetroButton();
            this.lblNisuURadnomOdnosu = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.gridLevi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDesni)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridLevi
            // 
            this.gridLevi.AllowUserToAddRows = false;
            this.gridLevi.AllowUserToDeleteRows = false;
            this.gridLevi.AllowUserToOrderColumns = true;
            this.gridLevi.AllowUserToResizeRows = false;
            this.gridLevi.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridLevi.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridLevi.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gridLevi.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(65)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(73)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridLevi.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridLevi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(73)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridLevi.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridLevi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridLevi.EnableHeadersVisualStyles = false;
            this.gridLevi.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gridLevi.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridLevi.Location = new System.Drawing.Point(0, 40);
            this.gridLevi.Margin = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.gridLevi.Name = "gridLevi";
            this.gridLevi.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(65)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(73)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridLevi.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gridLevi.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridLevi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridLevi.Size = new System.Drawing.Size(606, 346);
            this.gridLevi.Style = MetroFramework.MetroColorStyle.Purple;
            this.gridLevi.TabIndex = 0;
            // 
            // gridDesni
            // 
            this.gridDesni.AllowUserToAddRows = false;
            this.gridDesni.AllowUserToDeleteRows = false;
            this.gridDesni.AllowUserToOrderColumns = true;
            this.gridDesni.AllowUserToResizeRows = false;
            this.gridDesni.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridDesni.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridDesni.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gridDesni.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(65)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(73)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDesni.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.gridDesni.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(73)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridDesni.DefaultCellStyle = dataGridViewCellStyle5;
            this.gridDesni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDesni.EnableHeadersVisualStyles = false;
            this.gridDesni.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gridDesni.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridDesni.Location = new System.Drawing.Point(616, 40);
            this.gridDesni.Margin = new System.Windows.Forms.Padding(5, 5, 0, 5);
            this.gridDesni.Name = "gridDesni";
            this.gridDesni.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(65)))), ((int)(((byte)(153)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(73)))), ((int)(((byte)(173)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDesni.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.gridDesni.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridDesni.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridDesni.Size = new System.Drawing.Size(527, 346);
            this.gridDesni.Style = MetroFramework.MetroColorStyle.Purple;
            this.gridDesni.TabIndex = 1;
            // 
            // lblSkorasnjiOtpustena
            // 
            this.lblSkorasnjiOtpustena.AutoSize = true;
            this.lblSkorasnjiOtpustena.Location = new System.Drawing.Point(66, 84);
            this.lblSkorasnjiOtpustena.Name = "lblSkorasnjiOtpustena";
            this.lblSkorasnjiOtpustena.Size = new System.Drawing.Size(0, 0);
            this.lblSkorasnjiOtpustena.TabIndex = 2;
            // 
            // lblArhiva
            // 
            this.lblArhiva.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblArhiva.Location = new System.Drawing.Point(614, 0);
            this.lblArhiva.Name = "lblArhiva";
            this.lblArhiva.Size = new System.Drawing.Size(526, 35);
            this.lblArhiva.TabIndex = 3;
            this.lblArhiva.Text = "Arhiva radnika";
            this.lblArhiva.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnArhivirajOznacene
            // 
            this.btnArhivirajOznacene.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnArhivirajOznacene.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnArhivirajOznacene.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnArhivirajOznacene.Location = new System.Drawing.Point(3, 394);
            this.btnArhivirajOznacene.Name = "btnArhivirajOznacene";
            this.btnArhivirajOznacene.Size = new System.Drawing.Size(218, 24);
            this.btnArhivirajOznacene.TabIndex = 4;
            this.btnArhivirajOznacene.Text = "Arhiviraj označene radnike";
            this.btnArhivirajOznacene.UseSelectable = true;
            this.btnArhivirajOznacene.Click += new System.EventHandler(this.btnArhivirajOznacene_Click);
            // 
            // lblNisuURadnomOdnosu
            // 
            this.lblNisuURadnomOdnosu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNisuURadnomOdnosu.Location = new System.Drawing.Point(3, 0);
            this.lblNisuURadnomOdnosu.Name = "lblNisuURadnomOdnosu";
            this.lblNisuURadnomOdnosu.Size = new System.Drawing.Size(605, 35);
            this.lblNisuURadnomOdnosu.TabIndex = 5;
            this.lblNisuURadnomOdnosu.Text = "Nisu u tekućem radnom odnosu";
            this.lblNisuURadnomOdnosu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.5F));
            this.tableLayoutPanel1.Controls.Add(this.lblNisuURadnomOdnosu, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnArhivirajOznacene, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblArhiva, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.gridLevi, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.gridDesni, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(20, 60);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1143, 421);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // ArhiviranjeZaposlenihForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1183, 501);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.lblSkorasnjiOtpustena);
            this.MinimumSize = new System.Drawing.Size(1183, 501);
            this.Name = "ArhiviranjeZaposlenihForm";
            this.Style = MetroFramework.MetroColorStyle.Purple;
            this.Text = "Arhiva radnika koji nisu u radnom odnosu";
            this.Load += new System.EventHandler(this.ArhiviranjeZaposlenih_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridLevi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDesni)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroGrid gridLevi;
        private MetroFramework.Controls.MetroGrid gridDesni;
        private MetroFramework.Controls.MetroLabel lblSkorasnjiOtpustena;
        private MetroFramework.Controls.MetroLabel lblArhiva;
        private MetroFramework.Controls.MetroButton btnArhivirajOznacene;
        private MetroFramework.Controls.MetroLabel lblNisuURadnomOdnosu;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}