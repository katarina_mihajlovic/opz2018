﻿using System;
using System.Drawing;
using System.Windows.Forms;
using MetroFramework.Forms;
using SharedComponents.Presenters;
using SharedComponents.Views;
using MetroFramework;

namespace SharedComponents.Forms
{
    public partial class AzuriranjeZaposlenihForm : MetroForm, IAzuriranjeZaposlenogView
    {
        #region isChanged Fields

        bool isChangedIdNadredjenog;
        bool isChangedSlika;
        bool isChangedNalog;
        bool isChangedPassword;
        bool isChangedSmena;

        #endregion

        #region isChangedProperties

        public bool IsChangedIdNadredjenog { get => isChangedIdNadredjenog; set => isChangedIdNadredjenog = value; }
        public bool IsChangedSlika { get => isChangedSlika; set => isChangedSlika = value; }
        public bool IsChangedNalog { get => isChangedNalog; set => isChangedNalog = value; }
        public bool IsChangedPassword { get => isChangedPassword; set => isChangedPassword = value; }
        public bool IsChangedSmena { get => isChangedSmena; set => isChangedSmena=value; }
      
        #endregion

        public AzuriranjeZaposlenihForm()
        {
            InitializeComponent();
            Slika = null;
        }

        private void AzuriranjeZaposlenihForm_Load(object sender, EventArgs e)
        {
            Presenter.MyOnLoad();
        }

        #region Properties

        public string Ime { get => txtIme.Text; set => txtIme.Text = value; }
        public string SrednjeSlovo { get => txtSrednjeSlovo.Text; set => txtSrednjeSlovo.Text = value; }
        public string Prezime { get => txtPrezime.Text; set => txtPrezime.Text = value; }

        public bool SaKorisnickimNalogom
        {
            get => radioDa.Checked;
            set { radioDa.Checked = value;  radioNe.Checked = !value;/* txtPonovljenaLozinka.Text = txtLozinka.Text = "*****";*/ }
        }

        public DateTime DatumRodjenja { get => dtpDatumRodjenja.Value; set => dtpDatumRodjenja.Value = value; }
        public string Adresa { get => txtAdresa.Text; set => txtAdresa.Text = value; }
        public string MestoRodjenja { get => txtMestoRodjenja.Text; set => txtMestoRodjenja.Text = value; }
        public string BrojTelefona { get => txtBrojTelefona.Text; set => txtBrojTelefona.Text = value; }
        public string RadnoMesto { get => txtNazivRadnogMesta.Text; set => txtNazivRadnogMesta.Text = value; }
        public string TekuciRacun { get => txtBrojTekucegRacuna.Text; set => txtBrojTekucegRacuna.Text = value; }

        public string PutanjaDoSlike { get { return openFileDialog1.FileName; } set { } }

        public int NormaRada
        {
            get
            {
                int x;
                if (Int32.TryParse(txtNormaRada.Text, out x))
                {
                    return x;
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                txtNormaRada.Text = value.ToString();
            }
        }
        public string KorisnickoIme { get => txtKorisnickoIme.Text; set => txtKorisnickoIme.Text = value; }
        //public string Lozinka { get => txtLozinka.Text; set => txtLozinka.Text = value; }
        //public string Lozinka2 { get => txtPonovljenaLozinka.Text; set => txtPonovljenaLozinka.Text = value; }
        public string Uloga { get => cmbUloga.Text; set => cmbUloga.Text = value; }
        public AzuriranjeZaposlenogPresenter Presenter { get; set; }

        public Image Slika
        {
            get { return null; }
            set { if (value != null) pictureBox1.Image = value; }
        }

        public int PocetniStaz
        {
            get
            {
                int x;
                if (Int32.TryParse(txtStaz.Text, out x))
                {
                    return x;
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                txtStaz.Text = value.ToString();
            }
        }

        public double KoeficijentRadnogMesta
        {
            get
            {
                double x;
                if (double.TryParse(txtKoeficijent.Text, out x))
                {
                    return x;
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                txtKoeficijent.Text = value.ToString();
            }
        }

        public string Smena { set => lblSmena.Text = value; }
        public string Nadredjeni { set => metroLabel3.Text = value; }

        #endregion

        #region TextBox_Click

        private void txtIme_Click(object sender, EventArgs e)
        {
            lblErrIme.Visible = false;
        }

        private void txtSrednjeSlovo_Click(object sender, EventArgs e)
        {
            lblErrSSlovo.Visible = false;
        }

        private void txtPrezime_Click(object sender, EventArgs e)
        {
            lblErrPrezime.Visible = false;
        }

        private void txtAdresa_Click(object sender, EventArgs e)
        {
            lblErrAdresa.Visible = false;
        }

        private void txtMestoRodjenja_Click(object sender, EventArgs e)
        {
            lblErrMestoRodjenja.Visible = false;
        }

        private void txtBrojTelefona_Click(object sender, EventArgs e)
        {
            LblErrBrojTelefona.Visible = false;
        }

        private void txtNazivRadnogMesta_Click(object sender, EventArgs e)
        {
            lblErrRadnoMesto.Visible = false;
        }

        private void txtBrojTekucegRacuna_Click(object sender, EventArgs e)
        {
            lblErrRacun.Visible = false;
        }

        private void txtNormaRada_Click(object sender, EventArgs e)
        {
            lblErrNorma.Visible = false;
        }


        private void txtStaz_Click(object sender, EventArgs e)
        {
            lblErrStaz.Visible = false;
        }

        private void txtKoeficijent_Click(object sender, EventArgs e)
        {
            lblErrKoef.Visible = false;
        }

        #endregion

        #region ButtonClick

        private void btnIzaberiNadredjenog_Click(object sender, EventArgs e)
        {
            Presenter.IzaberiNadredjenog();
        }

        private void btnObrisiNadredjenog_Click(object sender, EventArgs e)
        {
            Presenter.ObrisiNadredjenog();
        }

        private void btnIzaberiSliku_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.ImageLocation = openFileDialog1.FileName;
                isChangedSlika = true;
            }
        }

        private void btnIzaberiRadnoVreme_Click(object sender, EventArgs e)
        {
            Presenter.IzaberiSmenu();
        }

        private void btnSacuvaj_Click(object sender, EventArgs e)
        {
            if (CheckIfEmpty())
            {
                if (Presenter.SacuvajZaposlenog())
                {
                    DialogResult = DialogResult.OK;
                    Close();
                }
                else
                {
                    MetroMessageBox.Show(this, "Dati zaposleni je u međuvremenu izmenjen. Njegovi podaci su ponovo učitani.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                }
            }
        }

        #endregion

        private bool CheckIfEmpty()
        {
            if (txtIme.Text == String.Empty)
            {
                lblErrIme.Visible = true;
                return false;
            }
            else if (txtSrednjeSlovo.Text == String.Empty)
            {
                lblErrSSlovo.Visible = true;
                return false;
            }
            else if (txtPrezime.Text == String.Empty)
            {
                lblErrPrezime.Visible = true;
                return false;
            }
            else if (txtAdresa.Text == String.Empty)
            {
                lblErrAdresa.Visible = true;
                return false;
            }
            else if (txtMestoRodjenja.Text == String.Empty)
            {
                lblErrMestoRodjenja.Visible = true;
                return false;
            }
            else if (txtBrojTelefona.Text == String.Empty)
            {
                LblErrBrojTelefona.Visible = true;
                return false;
            }
            else if (txtNazivRadnogMesta.Text == String.Empty)
            {
                lblErrRadnoMesto.Visible = true;
                return false;
            }
            else if (txtBrojTekucegRacuna.Text == String.Empty)
            {
                lblErrRacun.Visible = true;
                return false;
            }
            else if (txtNormaRada.Text == String.Empty)
            {
                lblErrNorma.Text = "Obavezan unos";
                lblErrNorma.Visible = true;
                return false;
            }
            else if (NormaRada <= 0)
            {
                lblErrNorma.Text = "Nevalidan unos";
                lblErrNorma.Visible = true;
                return false;
            }
            else if (txtStaz.Text == String.Empty)
            {
                lblErrStaz.Text = "Obavezan unos";
                lblErrStaz.Visible = true;
                return false;
            }
            else if (PocetniStaz < 0)
            {
                lblErrStaz.Text = "Nevalidan unos";
                lblErrStaz.Visible = true;
                return false;
            }
            else if (txtKoeficijent.Text == String.Empty)
            {
                lblErrKoef.Text = "Obavezan unos";
                lblErrKoef.Visible = true;
                return false;
            }
            else if (KoeficijentRadnogMesta <= 0)
            {
                lblErrKoef.Text = "Nevalidan unos";
                lblErrKoef.Visible = true;
                return false;
            }

            return true;
        }

        #region KeyPress

        private void txtIme_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtSrednjeSlovo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar) && e.KeyChar != '.')
                e.Handled = true;
        }

        private void txtPrezime_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtMestoRodjenja_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtBrojTelefona_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar) && e.KeyChar != '-' && e.KeyChar != '/')
                e.Handled = true;
        }

        private void txtRadnoMesto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar) && !Char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void txtTekuciRacun_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar) && e.KeyChar != '-')
                e.Handled = true;
        }

        private void txtNormaRada_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtStaz_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtKoeficijentRadnogMesta_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != ',')
                e.Handled = true;
        }
        #endregion

       
    }
}
