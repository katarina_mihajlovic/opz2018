﻿namespace SharedComponents.Forms
{
	partial class MenadzerOsnovniPodaciForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.ltel = new MetroFramework.Controls.MetroLabel();
            this.txtTelefon = new MetroFramework.Controls.MetroTextBox();
            this.Ladresa = new MetroFramework.Controls.MetroLabel();
            this.txtAdresa = new MetroFramework.Controls.MetroTextBox();
            this.Lnorma = new MetroFramework.Controls.MetroLabel();
            this.txtNorma = new MetroFramework.Controls.MetroTextBox();
            this.LradnoMesto = new MetroFramework.Controls.MetroLabel();
            this.txtRadnoMesto = new MetroFramework.Controls.MetroTextBox();
            this.LdatPoc = new MetroFramework.Controls.MetroLabel();
            this.txtDatum = new MetroFramework.Controls.MetroTextBox();
            this.Lime = new MetroFramework.Controls.MetroLabel();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.txtSankcija = new MetroFramework.Controls.MetroTextBox();
            this.Lsankcija = new MetroFramework.Controls.MetroLabel();
            this.btnSankcija = new MetroFramework.Controls.MetroButton();
            this.btnStimulacija = new MetroFramework.Controls.MetroButton();
            this.btnOk = new MetroFramework.Controls.MetroButton();
            this.btnKasnjenja = new MetroFramework.Controls.MetroButton();
            this.btnIstorija = new MetroFramework.Controls.MetroButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroPanel2.SuspendLayout();
            this.metroPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ltel
            // 
            this.ltel.AutoSize = true;
            this.ltel.Location = new System.Drawing.Point(12, 10);
            this.ltel.Name = "ltel";
            this.ltel.Size = new System.Drawing.Size(87, 19);
            this.ltel.TabIndex = 7;
            this.ltel.Text = "Broj telefona:";
            // 
            // txtTelefon
            // 
            // 
            // 
            // 
            this.txtTelefon.CustomButton.Image = null;
            this.txtTelefon.CustomButton.Location = new System.Drawing.Point(383, 1);
            this.txtTelefon.CustomButton.Name = "";
            this.txtTelefon.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtTelefon.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtTelefon.CustomButton.TabIndex = 1;
            this.txtTelefon.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtTelefon.CustomButton.UseSelectable = true;
            this.txtTelefon.CustomButton.Visible = false;
            this.txtTelefon.Enabled = false;
            this.txtTelefon.Lines = new string[0];
            this.txtTelefon.Location = new System.Drawing.Point(12, 32);
            this.txtTelefon.MaxLength = 32767;
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.PasswordChar = '\0';
            this.txtTelefon.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTelefon.SelectedText = "";
            this.txtTelefon.SelectionLength = 0;
            this.txtTelefon.SelectionStart = 0;
            this.txtTelefon.ShortcutsEnabled = true;
            this.txtTelefon.Size = new System.Drawing.Size(405, 23);
            this.txtTelefon.Style = MetroFramework.MetroColorStyle.Pink;
            this.txtTelefon.TabIndex = 6;
            this.txtTelefon.UseSelectable = true;
            this.txtTelefon.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtTelefon.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // Ladresa
            // 
            this.Ladresa.AutoSize = true;
            this.Ladresa.Location = new System.Drawing.Point(12, 59);
            this.Ladresa.Name = "Ladresa";
            this.Ladresa.Size = new System.Drawing.Size(53, 19);
            this.Ladresa.TabIndex = 9;
            this.Ladresa.Text = "Adresa:";
            // 
            // txtAdresa
            // 
            // 
            // 
            // 
            this.txtAdresa.CustomButton.Image = null;
            this.txtAdresa.CustomButton.Location = new System.Drawing.Point(341, 2);
            this.txtAdresa.CustomButton.Name = "";
            this.txtAdresa.CustomButton.Size = new System.Drawing.Size(61, 61);
            this.txtAdresa.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAdresa.CustomButton.TabIndex = 1;
            this.txtAdresa.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAdresa.CustomButton.UseSelectable = true;
            this.txtAdresa.CustomButton.Visible = false;
            this.txtAdresa.Enabled = false;
            this.txtAdresa.Lines = new string[0];
            this.txtAdresa.Location = new System.Drawing.Point(12, 82);
            this.txtAdresa.MaxLength = 32767;
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.PasswordChar = '\0';
            this.txtAdresa.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAdresa.SelectedText = "";
            this.txtAdresa.SelectionLength = 0;
            this.txtAdresa.SelectionStart = 0;
            this.txtAdresa.ShortcutsEnabled = true;
            this.txtAdresa.Size = new System.Drawing.Size(405, 66);
            this.txtAdresa.Style = MetroFramework.MetroColorStyle.Pink;
            this.txtAdresa.TabIndex = 8;
            this.txtAdresa.UseSelectable = true;
            this.txtAdresa.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAdresa.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // Lnorma
            // 
            this.Lnorma.AutoSize = true;
            this.Lnorma.Location = new System.Drawing.Point(9, 81);
            this.Lnorma.Name = "Lnorma";
            this.Lnorma.Size = new System.Drawing.Size(85, 19);
            this.Lnorma.TabIndex = 11;
            this.Lnorma.Text = "Norma rada:";
            // 
            // txtNorma
            // 
            // 
            // 
            // 
            this.txtNorma.CustomButton.Image = null;
            this.txtNorma.CustomButton.Location = new System.Drawing.Point(204, 1);
            this.txtNorma.CustomButton.Name = "";
            this.txtNorma.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNorma.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNorma.CustomButton.TabIndex = 1;
            this.txtNorma.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNorma.CustomButton.UseSelectable = true;
            this.txtNorma.CustomButton.Visible = false;
            this.txtNorma.Lines = new string[0];
            this.txtNorma.Location = new System.Drawing.Point(204, 79);
            this.txtNorma.MaxLength = 6;
            this.txtNorma.Name = "txtNorma";
            this.txtNorma.PasswordChar = '\0';
            this.txtNorma.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNorma.SelectedText = "";
            this.txtNorma.SelectionLength = 0;
            this.txtNorma.SelectionStart = 0;
            this.txtNorma.ShortcutsEnabled = true;
            this.txtNorma.Size = new System.Drawing.Size(226, 23);
            this.txtNorma.Style = MetroFramework.MetroColorStyle.Pink;
            this.txtNorma.TabIndex = 0;
            this.txtNorma.UseSelectable = true;
            this.txtNorma.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNorma.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNorma.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNorma_KeyPress);
            // 
            // LradnoMesto
            // 
            this.LradnoMesto.AutoSize = true;
            this.LradnoMesto.Location = new System.Drawing.Point(9, 43);
            this.LradnoMesto.Name = "LradnoMesto";
            this.LradnoMesto.Size = new System.Drawing.Size(90, 19);
            this.LradnoMesto.TabIndex = 13;
            this.LradnoMesto.Text = "Radno mesto:";
            // 
            // txtRadnoMesto
            // 
            // 
            // 
            // 
            this.txtRadnoMesto.CustomButton.Image = null;
            this.txtRadnoMesto.CustomButton.Location = new System.Drawing.Point(204, 1);
            this.txtRadnoMesto.CustomButton.Name = "";
            this.txtRadnoMesto.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtRadnoMesto.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtRadnoMesto.CustomButton.TabIndex = 1;
            this.txtRadnoMesto.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtRadnoMesto.CustomButton.UseSelectable = true;
            this.txtRadnoMesto.CustomButton.Visible = false;
            this.txtRadnoMesto.Enabled = false;
            this.txtRadnoMesto.Lines = new string[0];
            this.txtRadnoMesto.Location = new System.Drawing.Point(204, 41);
            this.txtRadnoMesto.MaxLength = 32767;
            this.txtRadnoMesto.Name = "txtRadnoMesto";
            this.txtRadnoMesto.PasswordChar = '\0';
            this.txtRadnoMesto.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtRadnoMesto.SelectedText = "";
            this.txtRadnoMesto.SelectionLength = 0;
            this.txtRadnoMesto.SelectionStart = 0;
            this.txtRadnoMesto.ShortcutsEnabled = true;
            this.txtRadnoMesto.Size = new System.Drawing.Size(226, 23);
            this.txtRadnoMesto.Style = MetroFramework.MetroColorStyle.Pink;
            this.txtRadnoMesto.TabIndex = 12;
            this.txtRadnoMesto.UseSelectable = true;
            this.txtRadnoMesto.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtRadnoMesto.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // LdatPoc
            // 
            this.LdatPoc.Location = new System.Drawing.Point(9, 118);
            this.LdatPoc.Name = "LdatPoc";
            this.LdatPoc.Size = new System.Drawing.Size(189, 21);
            this.LdatPoc.TabIndex = 15;
            this.LdatPoc.Text = "Datum stupanja u radni odnos:";
            this.LdatPoc.WrapToLine = true;
            // 
            // txtDatum
            // 
            // 
            // 
            // 
            this.txtDatum.CustomButton.Image = null;
            this.txtDatum.CustomButton.Location = new System.Drawing.Point(204, 1);
            this.txtDatum.CustomButton.Name = "";
            this.txtDatum.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtDatum.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtDatum.CustomButton.TabIndex = 1;
            this.txtDatum.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtDatum.CustomButton.UseSelectable = true;
            this.txtDatum.CustomButton.Visible = false;
            this.txtDatum.Enabled = false;
            this.txtDatum.Lines = new string[0];
            this.txtDatum.Location = new System.Drawing.Point(204, 117);
            this.txtDatum.MaxLength = 32767;
            this.txtDatum.Name = "txtDatum";
            this.txtDatum.PasswordChar = '\0';
            this.txtDatum.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDatum.SelectedText = "";
            this.txtDatum.SelectionLength = 0;
            this.txtDatum.SelectionStart = 0;
            this.txtDatum.ShortcutsEnabled = true;
            this.txtDatum.Size = new System.Drawing.Size(226, 23);
            this.txtDatum.Style = MetroFramework.MetroColorStyle.Pink;
            this.txtDatum.TabIndex = 14;
            this.txtDatum.UseSelectable = true;
            this.txtDatum.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtDatum.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // Lime
            // 
            this.Lime.Location = new System.Drawing.Point(30, 296);
            this.Lime.Name = "Lime";
            this.Lime.Size = new System.Drawing.Size(225, 19);
            this.Lime.TabIndex = 1;
            this.Lime.Text = "Ime:";
            this.Lime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroPanel2
            // 
            this.metroPanel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.metroPanel2.Controls.Add(this.ltel);
            this.metroPanel2.Controls.Add(this.txtTelefon);
            this.metroPanel2.Controls.Add(this.txtAdresa);
            this.metroPanel2.Controls.Add(this.Ladresa);
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(30, 332);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(436, 164);
            this.metroPanel2.TabIndex = 18;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // metroPanel3
            // 
            this.metroPanel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.metroPanel3.Controls.Add(this.txtSankcija);
            this.metroPanel3.Controls.Add(this.Lsankcija);
            this.metroPanel3.Controls.Add(this.LdatPoc);
            this.metroPanel3.Controls.Add(this.txtDatum);
            this.metroPanel3.Controls.Add(this.LradnoMesto);
            this.metroPanel3.Controls.Add(this.txtRadnoMesto);
            this.metroPanel3.Controls.Add(this.Lnorma);
            this.metroPanel3.Controls.Add(this.txtNorma);
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(282, 68);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(447, 225);
            this.metroPanel3.TabIndex = 21;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // txtSankcija
            // 
            // 
            // 
            // 
            this.txtSankcija.CustomButton.Image = null;
            this.txtSankcija.CustomButton.Location = new System.Drawing.Point(204, 1);
            this.txtSankcija.CustomButton.Name = "";
            this.txtSankcija.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtSankcija.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtSankcija.CustomButton.TabIndex = 1;
            this.txtSankcija.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtSankcija.CustomButton.UseSelectable = true;
            this.txtSankcija.CustomButton.Visible = false;
            this.txtSankcija.Enabled = false;
            this.txtSankcija.Lines = new string[0];
            this.txtSankcija.Location = new System.Drawing.Point(204, 155);
            this.txtSankcija.MaxLength = 32767;
            this.txtSankcija.Name = "txtSankcija";
            this.txtSankcija.PasswordChar = '\0';
            this.txtSankcija.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSankcija.SelectedText = "";
            this.txtSankcija.SelectionLength = 0;
            this.txtSankcija.SelectionStart = 0;
            this.txtSankcija.ShortcutsEnabled = true;
            this.txtSankcija.Size = new System.Drawing.Size(226, 23);
            this.txtSankcija.Style = MetroFramework.MetroColorStyle.Pink;
            this.txtSankcija.TabIndex = 17;
            this.txtSankcija.UseSelectable = true;
            this.txtSankcija.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtSankcija.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // Lsankcija
            // 
            this.Lsankcija.Location = new System.Drawing.Point(9, 155);
            this.Lsankcija.Name = "Lsankcija";
            this.Lsankcija.Size = new System.Drawing.Size(145, 23);
            this.Lsankcija.TabIndex = 16;
            this.Lsankcija.Text = "Sankcija / Stimulacija:";
            this.Lsankcija.WrapToLine = true;
            // 
            // btnSankcija
            // 
            this.btnSankcija.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnSankcija.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnSankcija.Location = new System.Drawing.Point(484, 412);
            this.btnSankcija.Name = "btnSankcija";
            this.btnSankcija.Size = new System.Drawing.Size(241, 23);
            this.btnSankcija.TabIndex = 3;
            this.btnSankcija.Text = "Dodaj sankciju";
            this.btnSankcija.UseSelectable = true;
            this.btnSankcija.Click += new System.EventHandler(this.btnSankcija_Click);
            // 
            // btnStimulacija
            // 
            this.btnStimulacija.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnStimulacija.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnStimulacija.Location = new System.Drawing.Point(484, 441);
            this.btnStimulacija.Name = "btnStimulacija";
            this.btnStimulacija.Size = new System.Drawing.Size(241, 23);
            this.btnStimulacija.TabIndex = 4;
            this.btnStimulacija.Text = "Dodaj stimulaciju";
            this.btnStimulacija.UseSelectable = true;
            this.btnStimulacija.Click += new System.EventHandler(this.btnStimulacija_Click);
            // 
            // btnOk
            // 
            this.btnOk.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnOk.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnOk.Location = new System.Drawing.Point(484, 470);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(241, 23);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "Sačuvaj normu";
            this.btnOk.UseSelectable = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnKasnjenja
            // 
            this.btnKasnjenja.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnKasnjenja.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnKasnjenja.Location = new System.Drawing.Point(484, 325);
            this.btnKasnjenja.Name = "btnKasnjenja";
            this.btnKasnjenja.Size = new System.Drawing.Size(241, 23);
            this.btnKasnjenja.TabIndex = 1;
            this.btnKasnjenja.Text = "Pregled kašnjenja";
            this.btnKasnjenja.UseSelectable = true;
            this.btnKasnjenja.Click += new System.EventHandler(this.btnKasnjenja_Click);
            // 
            // btnIstorija
            // 
            this.btnIstorija.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnIstorija.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnIstorija.Location = new System.Drawing.Point(484, 383);
            this.btnIstorija.Name = "btnIstorija";
            this.btnIstorija.Size = new System.Drawing.Size(241, 23);
            this.btnIstorija.TabIndex = 5;
            this.btnIstorija.Text = "Pregled sankcija i stimulacija";
            this.btnIstorija.UseSelectable = true;
            this.btnIstorija.Click += new System.EventHandler(this.btnIstorija_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pictureBox1.ErrorImage = global::SharedComponents.Properties.Resources.default_person;
            this.pictureBox1.Image = global::SharedComponents.Properties.Resources.default_person;
            this.pictureBox1.InitialImage = global::SharedComponents.Properties.Resources.default_person;
            this.pictureBox1.Location = new System.Drawing.Point(30, 68);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(225, 225);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // metroButton1
            // 
            this.metroButton1.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton1.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.metroButton1.Location = new System.Drawing.Point(484, 354);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(241, 23);
            this.metroButton1.TabIndex = 2;
            this.metroButton1.Text = "Pregled odsustva";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // MenadzerOsnovniPodaciForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 523);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.btnIstorija);
            this.Controls.Add(this.btnKasnjenja);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnStimulacija);
            this.Controls.Add(this.btnSankcija);
            this.Controls.Add(this.metroPanel3);
            this.Controls.Add(this.Lime);
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "MenadzerOsnovniPodaciForm";
            this.Padding = new System.Windows.Forms.Padding(27, 92, 27, 31);
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Pink;
            this.Text = "Podaci o zaposlenom";
            this.Load += new System.EventHandler(this.MenadzerOsnovniPodaciForm_Load);
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            this.metroPanel3.ResumeLayout(false);
            this.metroPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion
		private MetroFramework.Controls.MetroLabel ltel;
		private MetroFramework.Controls.MetroTextBox txtTelefon;
		private MetroFramework.Controls.MetroLabel Ladresa;
		private MetroFramework.Controls.MetroTextBox txtAdresa;
		private MetroFramework.Controls.MetroLabel Lnorma;
		private MetroFramework.Controls.MetroTextBox txtNorma;
		private MetroFramework.Controls.MetroLabel LradnoMesto;
		private MetroFramework.Controls.MetroTextBox txtRadnoMesto;
		private MetroFramework.Controls.MetroLabel LdatPoc;
		private MetroFramework.Controls.MetroTextBox txtDatum;
		private MetroFramework.Controls.MetroLabel Lime;
		private System.Windows.Forms.PictureBox pictureBox1;
		private MetroFramework.Controls.MetroPanel metroPanel2;
		private MetroFramework.Controls.MetroPanel metroPanel3;
		private MetroFramework.Controls.MetroTextBox txtSankcija;
		private MetroFramework.Controls.MetroLabel Lsankcija;
		private MetroFramework.Controls.MetroButton btnSankcija;
		private MetroFramework.Controls.MetroButton btnStimulacija;
		private MetroFramework.Controls.MetroButton btnOk;
        private MetroFramework.Controls.MetroButton btnKasnjenja;
        private MetroFramework.Controls.MetroButton btnIstorija;
        private MetroFramework.Controls.MetroButton metroButton1;
    }
}