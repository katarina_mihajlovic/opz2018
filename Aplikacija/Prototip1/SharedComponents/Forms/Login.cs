﻿using MetroFramework.Forms;
using Persistence.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using MetroFramework;

namespace SharedComponents.Forms
{
    public partial class Login : MetroForm
    {
        public interface IUsernameOwner
        {
            string Username { get; set; }
        }
        public abstract class FormFactory
        {
            public abstract MetroForm MakeForm(string username);
        }

        public class Uloga
        {
            public String ImeUloge
            {
                get; set;
            }
            public FormFactory Forma
            {
                get; set;
            }

            public Uloga(String i, FormFactory f)
            {
                ImeUloge = i;
                Forma = f;
            }
        }

        private Uloga[] uloge;
        private bool forceClose;

        public Login()
        {
            InitializeComponent();
            forceClose = false;
            tbKorisnickoIme.Focus();
        }

        public Login(Uloga[] u)
            : this()
        {
            uloge = u;
            lblSpinner.Visible = false;
            circularProgressBar1.Visible = false;
            Personalize(u);
        }

        private void Personalize(Uloga[] u)
        {
            if (u == null || u.Count() == 0)
            {
                return;
            }

            string naslov = "Dobro došli!";
            MetroColorStyle style = MetroColorStyle.Blue;
            
            if(u[0].ImeUloge == "Knjigovođa")
            {
                //naslov += "finansijsku službu!";
                //ostaje plav
            }
            else if (u[0].ImeUloge == "Portir")
            {
                //naslov += "portirsku službu!";
                style = MetroColorStyle.Green;
                circularProgressBar1.ProgressColor = Color.ForestGreen;
            }
            else
            {
                //naslov += "opštu službu!";
                style = MetroColorStyle.Purple;
                circularProgressBar1.ProgressColor = Color.Purple;
            }
            this.Text = naslov;
            this.Style = style;
            tbKorisnickoIme.Style = style;
            tbLozinka.Style = style;
        }

        private void tbKorisnickoIme_Click(object sender, EventArgs e)
        {
            lblKorisnickoIme.Visible = false;
            lblPassword.Visible = false;
            lblBazaPodataka.Visible = false;
        }

        private void tbLozinka_Click(object sender, EventArgs e)
        {
            lblPassword.Visible = false;
            lblKorisnickoIme.Visible = false;
            lblBazaPodataka.Visible = false;
        }



        private void metroButton1_Click(object sender, EventArgs e)
        {
            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void tbLozinka_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals((char)13))
            {
                metroButton1_Click(this, e);
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
            tbKorisnickoIme.Focus();
        }

        //poziva se nad pribavljenom listom zaposlenih prema korisnickom imenu
        private void CheckLogin(List<Zaposleni> lista)
        {
            if (lista == null)
            {
                Del2 dd = DatabaseError;
                Invoke(dd);
                return;

            } else if(lista.Count == 0)
            {
                Del2 dd = ShowError;
                Invoke(dd);
                return;
            }
            Zaposleni z = lista[0];
            String pass = tbLozinka.Text + z.Salt;

            System.Text.Encoding enc = System.Text.Encoding.ASCII;
            SHA256Managed alg = new SHA256Managed();
            byte[] passwordAndSaltHashed = alg.ComputeHash(new MemoryStream(Encoding.ASCII.GetBytes(pass)));
            string forCompare = enc.GetString(passwordAndSaltHashed);

            if (z.LozinkaHash != forCompare)
            {
                Del2 dd = ShowError;
                Invoke(dd);
                return;
            }
            foreach (Uloga u in uloge)
            {
                if (z.UlogaKorisnickogNaloga == u.ImeUloge)
                {
                    Hide();
                    MetroForm mf = u.Forma.MakeForm(z.KorisnickoIme);
                    if (mf.ShowDialog() == DialogResult.OK)
                    {
                        //tbKorisnickoIme.Text = String.Empty;
                        tbLozinka.Text = String.Empty;
                        lblPassword.Visible = false;
                        lblKorisnickoIme.Visible = false;
                        Show();
                        return;
                    }
                    else
                    {
                        forceClose = true;
                        Close();
                        return;
                    }
                }
            }
            lblPassword.Visible = true;
            lblKorisnickoIme.Visible = true;
        }

        private delegate void Del1(List<Zaposleni> lista);
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Del2 spnstop = StopSpinning;
            Del1 del = CheckLogin;
            String filter = (String)e.Argument;
            Invoke(spnstart);
            List<Zaposleni> lista = null;
            try
            {
                using (OpzContext kontekst = new OpzContext())
                {
                    lista = kontekst.ListaZaposlenih.Where(zap => zap.KorisnickoIme == tbKorisnickoIme.Text).ToList();
                }
            }
            catch (Exception)
            {
                lista = null;
            }

            
            Invoke(del, new object[] {lista});
        }

        private delegate void Del2();
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Del2 spnstop = StopSpinning;
            //zaustavljanje spinnera
            Invoke(spnstop);
        }

        private void StartSpinning()
        {
            lblSpinner.Visible = true;
            circularProgressBar1.Visible = true;

            metroButton1.Enabled = false;
            tbLozinka.Enabled = false;
            tbKorisnickoIme.Enabled = false;
        }

        private void ShowError()
        {
            lblPassword.Visible = true;
            lblKorisnickoIme.Visible = true;
        }

        private void DatabaseError()
        {
            lblBazaPodataka.Visible = true;
        }

        private void StopSpinning()
        {
            lblSpinner.Visible = false;
            circularProgressBar1.Visible = false;

            metroButton1.Enabled = true;
            tbLozinka.Enabled = true;
            tbKorisnickoIme.Enabled = true;
        }

        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorker1.IsBusy && !forceClose)
            {
                e.Cancel = true;
            }
        }
    }
}
