﻿using System.Data;
using System.Linq;
using MetroFramework.Forms;
using Persistence.Entities;
using System.Windows.Forms;

namespace SharedComponents.Forms
{
    public partial class SankStimHistoryForm : MetroForm
    {
        public SankStimHistoryForm()
        {
            InitializeComponent();
        }

        public SankStimHistoryForm(int idZaposlenog)
            : this()
        {
            using (OpzContext kontekst = new OpzContext())
            {
                Zaposleni zap = kontekst.ListaZaposlenih.Find(idZaposlenog);

                gridSankStim.DataSource = (from ss in zap.SankcijeStimulacije
                                           orderby ss.DatumIzricanja descending
                                           select new
                                           {
                                               DatumIzricanja = ss.DatumIzricanja,
                                               Vrednost = ss.Vrednost,
                                               Opis = ss.Opis
                                           }).ToList();
            }

            foreach (DataGridViewColumn dgvcol in gridSankStim.Columns)
            {
                dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            gridSankStim.Columns[0].HeaderText = "Datum izricanja";
        }
    }
}
