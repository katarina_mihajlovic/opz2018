﻿using MetroFramework.Forms;
using MetroFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Persistence.Entities;
using MetroFramework.Controls;

namespace SharedComponents.Forms
{
    public partial class DodavanjeSmeneForm : MetroForm
    {
        private string smenaZaIzmenu = null;

        public DodavanjeSmeneForm()
        {
            InitializeComponent();
        }

        public DodavanjeSmeneForm(string s)
        {
            InitializeComponent();
            smenaZaIzmenu = s;

        }

        private class SmenaPrint
        {
            public string NazivSmene { get; set; }
            public Tuple<string, string> Ponedeljak { get; set; }
            public Tuple<string, string> Utorak { get; set; }
            public Tuple<string, string> Sreda { get; set; }
            public Tuple<string, string> Cetvrtak { get; set; }
            public Tuple<string, string> Petak { get; set; }
            public Tuple<string, string> Subota { get; set; }
            public Tuple<string, string> Nedelja { get; set; }

            public SmenaPrint()
            {

            }
        }

        private void DodavanjeSmeneForm_Load(object sender, EventArgs e)
        {
            if (smenaZaIzmenu != null)
            {
                tbNazivSmene.Enabled = false;
                tbNazivSmene.Text = smenaZaIzmenu;
                this.Text = "Izmena smene";

                using (OpzContext kontekst = new OpzContext())
                {
                    DateTime maxDatum = (from p in kontekst.ListaSmena where p.TipSmene == smenaZaIzmenu select p.DatumDonosenja).Max();
                    List<Smena> poDanima = (from p in kontekst.ListaSmena where p.DatumDonosenja == maxDatum && p.TipSmene == smenaZaIzmenu select p).ToList();

                    SmenaPrint current = new SmenaPrint();
                    current.NazivSmene = smenaZaIzmenu;
                    foreach (Smena a in poDanima)
                    {
                        if (a.DanUNedelji == "ponedeljak")
                        {
                            current.Ponedeljak = new Tuple<string, string>(a.Pocetak.ToString("HH:mm"), a.Kraj.ToString("HH:mm"));
                        }
                        else if (a.DanUNedelji == "utorak")
                        {
                            current.Utorak = new Tuple<string, string>(a.Pocetak.ToString("HH:mm"), a.Kraj.ToString("HH:mm"));
                        }
                        else if (a.DanUNedelji == "sreda")
                        {
                            current.Sreda = new Tuple<string, string>(a.Pocetak.ToString("HH:mm"), a.Kraj.ToString("HH:mm"));
                        }
                        else if (a.DanUNedelji == "cetvrtak")
                        {
                            current.Cetvrtak = new Tuple<string, string>(a.Pocetak.ToString("HH:mm"), a.Kraj.ToString("HH:mm"));
                        }
                        else if (a.DanUNedelji == "petak")
                        {
                            current.Petak = new Tuple<string, string>(a.Pocetak.ToString("HH:mm"), a.Kraj.ToString("HH:mm"));
                        }
                        else if (a.DanUNedelji == "subota")
                        {
                            current.Subota = new Tuple<string, string>(a.Pocetak.ToString("HH:mm"), a.Kraj.ToString("HH:mm"));
                        }
                        else if (a.DanUNedelji == "nedelja")
                        {
                            current.Nedelja = new Tuple<string, string>(a.Pocetak.ToString("HH:mm"), a.Kraj.ToString("HH:mm"));
                        }
                    }
                    //sad za neradne dane...
                    if (current.Ponedeljak == null || current.Ponedeljak.Equals(""))
                    {
                        current.Ponedeljak = new Tuple<string, string>("", "");
                    }
                    if (current.Utorak == null || current.Ponedeljak.Equals(""))
                    {
                        current.Utorak = new Tuple<string, string>("", "");
                    }
                    if (current.Sreda == null || current.Ponedeljak.Equals(""))
                    {
                        current.Sreda = new Tuple<string, string>("", "");
                    }
                    if (current.Cetvrtak == null || current.Ponedeljak.Equals(""))
                    {
                        current.Cetvrtak = new Tuple<string, string>("", "");
                    }
                    if (current.Petak == null || current.Ponedeljak.Equals(""))
                    {
                        current.Petak = new Tuple<string, string>("", "");
                    }
                    if (current.Subota == null || current.Ponedeljak.Equals(""))
                    {
                        current.Subota = new Tuple<string, string>("", "");
                    }
                    if (current.Nedelja == null || current.Ponedeljak.Equals(""))
                    {
                        current.Nedelja = new Tuple<string, string>("", "");
                    }

                    tbPonedeljak1.Text = current.Ponedeljak.Item1;
                    tbPonedeljak2.Text = current.Ponedeljak.Item2;
                    tbUtorak1.Text = current.Utorak.Item1;
                    tbUtorak2.Text = current.Utorak.Item2;
                    tbSreda1.Text = current.Sreda.Item1;
                    tbSreda2.Text = current.Sreda.Item2;
                    tbCetvrtak1.Text = current.Cetvrtak.Item1;
                    tbCetvrtak2.Text = current.Cetvrtak.Item2;
                    tbPetak1.Text = current.Petak.Item1;
                    tbPetak2.Text = current.Petak.Item2;
                    tbSubota1.Text = current.Subota.Item1;
                    tbSubota2.Text = current.Subota.Item2;
                    tbNedelja1.Text = current.Nedelja.Item1;
                    tbNedelja2.Text = current.Nedelja.Item2;
                }

                metroButton2.Text = "Izmeni smenu";
            }
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            if (smenaZaIzmenu == null)
            {
                if (tbNazivSmene.Text == String.Empty)
                {
                    MetroMessageBox.Show(this, "Smena ne može da ima prazno ime.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                    return;
                }
                
                //dodavanje nove smene
                using (OpzContext c = new OpzContext())
                {
                    var a = (from p in c.ListaSmena where p.TipSmene == tbNazivSmene.Text select p.TipSmene).Distinct().ToList();
                    if (a.Count != 0)
                    {
                        MetroMessageBox.Show(this, "Smena sa tim imenom već postoji.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                        return;
                    }
                }

                string[] dani = new string[] { "ponedeljak", "utorak", "sreda", "cetvrtak", "petak", "subota", "nedelja" };

                List<Tuple<MetroTextBox, MetroTextBox, string>> lista = new List<Tuple<MetroTextBox, MetroTextBox, string>>()
            {
                new Tuple<MetroTextBox, MetroTextBox, string>(tbPonedeljak1,tbPonedeljak2,"ponedeljak"),
                new Tuple<MetroTextBox, MetroTextBox, string>(tbUtorak1,tbUtorak2,"utorak"),
                new Tuple<MetroTextBox, MetroTextBox, string>(tbSreda1,tbSreda2,"sreda"),
                new Tuple<MetroTextBox, MetroTextBox, string>(tbCetvrtak1,tbCetvrtak2,"cetvrtak"),
                new Tuple<MetroTextBox, MetroTextBox, string>(tbPetak1,tbPetak2,"petak"),
                new Tuple<MetroTextBox, MetroTextBox, string>(tbSubota1,tbSubota2,"subota"),
                new Tuple<MetroTextBox, MetroTextBox, string>(tbNedelja1,tbNedelja2,"nedelja")

            };

                try
                {
                    List<Smena> smene = new List<Smena>();
                    DateTime trenutni = DateTime.Now;
                    foreach (var a in lista)
                    {
                        if (a.Item1.Text == "" || a.Item2.Text == "") continue;

                        DateTime datumPocetka = DateTime.Today;
                        DateTime datumKraja = DateTime.Today;

                        string[] vremePocetka = a.Item1.Text.Split(':');
                        if (Int32.Parse(vremePocetka[0]) < 0 || Int32.Parse(vremePocetka[0]) > 23 || Int32.Parse(vremePocetka[1]) < 0 || Int32.Parse(vremePocetka[1]) > 59)
                        {
                            MetroMessageBox.Show(this, "Neko od vremena nije ispravno unešeno.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                            return;
                        }
                        datumPocetka = datumPocetka.AddHours(Int32.Parse(vremePocetka[0]));
                        datumPocetka = datumPocetka.AddMinutes(Int32.Parse(vremePocetka[1]));



                        string[] vremeKraja = a.Item2.Text.Split(':');
                        if (Int32.Parse(vremeKraja[0]) < 0 || Int32.Parse(vremeKraja[0]) > 23 || Int32.Parse(vremeKraja[1]) < 0 || Int32.Parse(vremeKraja[1]) > 59)
                        {
                            MetroMessageBox.Show(this, "Neko od vremena nije ispravno unešeno.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                            return;
                        }
                        datumKraja = datumKraja.AddHours(Int32.Parse(vremeKraja[0]));
                        datumKraja = datumKraja.AddMinutes(Int32.Parse(vremeKraja[1]));


                        Smena s = new Smena();
                        s.Pocetak = datumPocetka;
                        s.Kraj = datumKraja;
                        s.DatumDonosenja = trenutni;
                        s.DanUNedelji = a.Item3;
                        s.TipSmene = tbNazivSmene.Text;

                        smene.Add(s);
                    }

                    if (smene.Count == 0)
                    {
                        MetroMessageBox.Show(this, "Nije unešeno dovoljno podataka.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                        return;
                    }

                    using (OpzContext c = new OpzContext())
                    {
                        c.ListaSmena.AddRange(smene);
                        c.SaveChanges();
                    }
                    MetroMessageBox.Show(this, "Nova smena je uspešno dodata.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                    Close();
                }
                catch (Exception)
                {
                    MetroMessageBox.Show(this, "Neko od vremena nije ispravno unešeno.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                    return;
                }
            }
            else
            {
                //azurira se smena
                List<Tuple<MetroTextBox, MetroTextBox, string>> lista = new List<Tuple<MetroTextBox, MetroTextBox, string>>()
            {
                new Tuple<MetroTextBox, MetroTextBox, string>(tbPonedeljak1,tbPonedeljak2,"ponedeljak"),
                new Tuple<MetroTextBox, MetroTextBox, string>(tbUtorak1,tbUtorak2,"utorak"),
                new Tuple<MetroTextBox, MetroTextBox, string>(tbSreda1,tbSreda2,"sreda"),
                new Tuple<MetroTextBox, MetroTextBox, string>(tbCetvrtak1,tbCetvrtak2,"cetvrtak"),
                new Tuple<MetroTextBox, MetroTextBox, string>(tbPetak1,tbPetak2,"petak"),
                new Tuple<MetroTextBox, MetroTextBox, string>(tbSubota1,tbSubota2,"subota"),
                new Tuple<MetroTextBox, MetroTextBox, string>(tbNedelja1,tbNedelja2,"nedelja")

            };

                try
                {
                    List<Smena> smene = new List<Smena>();
                    DateTime trenutni = DateTime.Now;
                    foreach (var a in lista)
                    {
                        if (a.Item1.Text == "" || a.Item2.Text == "") continue;

                        DateTime datumPocetka = DateTime.Today;
                        DateTime datumKraja = DateTime.Today;

                        string[] vremePocetka = a.Item1.Text.Split(':');
                        if (Int32.Parse(vremePocetka[0]) < 0 || Int32.Parse(vremePocetka[0]) > 23 || Int32.Parse(vremePocetka[1]) < 0 || Int32.Parse(vremePocetka[1]) > 59)
                        {
                            MetroMessageBox.Show(this, "Neko od vremena nije ispravno unešeno.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                            return;
                        }
                        datumPocetka = datumPocetka.AddHours(Int32.Parse(vremePocetka[0]));
                        datumPocetka = datumPocetka.AddMinutes(Int32.Parse(vremePocetka[1]));



                        string[] vremeKraja = a.Item2.Text.Split(':');
                        if (Int32.Parse(vremeKraja[0]) < 0 || Int32.Parse(vremeKraja[0]) > 23 || Int32.Parse(vremeKraja[1]) < 0 || Int32.Parse(vremeKraja[1]) > 59)
                        {
                            MetroMessageBox.Show(this, "Neko od vremena nije ispravno unešeno.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                            return;
                        }
                        datumKraja = datumKraja.AddHours(Int32.Parse(vremeKraja[0]));
                        datumKraja = datumKraja.AddMinutes(Int32.Parse(vremeKraja[1]));


                        Smena s = new Smena();
                        s.Pocetak = datumPocetka;
                        s.Kraj = datumKraja;
                        s.DatumDonosenja = trenutni;
                        s.DanUNedelji = a.Item3;
                        s.TipSmene = tbNazivSmene.Text;

                        smene.Add(s);
                    }

                    if (smene.Count == 0)
                    {
                        MetroMessageBox.Show(this, "Nije unešeno dovoljno podataka.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                        return;
                    }

                    using (OpzContext c = new OpzContext())
                    {
                        c.ListaSmena.AddRange(smene);
                        c.SaveChanges();
                    }
                    MetroMessageBox.Show(this, "Smena je uspešno ažurirana.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                    DialogResult = DialogResult.None;
                    Close();
                }
                catch (Exception)
                {
                    MetroMessageBox.Show(this, "Neko od vremena nije ispravno unešeno.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                    return;
                }
            }
        }

        private void Vreme_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar) && e.KeyChar != ':')
                e.Handled = true;
        }
    }
}
