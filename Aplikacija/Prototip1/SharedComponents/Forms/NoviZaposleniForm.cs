﻿using MetroFramework;
using MetroFramework.Forms;
using SharedComponents.Presenters;
using System;
using System.Windows.Forms;

namespace SharedComponents.Forms
{
    public partial class NoviZaposleniForm : MetroForm, Views.INoviZaposleniView
    {
        public NoviZaposleniForm()
        {
            InitializeComponent();
            pictureBox1.Image = Properties.Resources.default_person;
            cbUlogaKorisnickogNaloga.SelectedIndex = 3;
        }

        public void PrikaziObavestenje(string a)
        {
            MetroMessageBox.Show(this, a, "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
        }

        public string Ime { get { return tbIme.Text; } set { tbIme.Text = value; } }
        public string SrednjeSlovo { get { return tbSrednjeSlovo.Text; } set { tbSrednjeSlovo.Text = value; } }
        public string Prezime { get { return tbPrezime.Text; } set { tbPrezime.Text = value; } }

        public bool SaKorisnickimNalogom { get { return rbNalogDa.Checked; } }

        public int PocetniStaz { get { return Int32.Parse(tbPocetniStaz.Text); } set { tbPocetniStaz.Text = value.ToString(); } }

        public DateTime DatumRodjenja { get { return dtpDatumRodjenja.Value; } set { dtpDatumRodjenja.Value = value; } }
        public string Adresa { get { return tbAdresa.Text; } set { tbAdresa.Text = value; } }
        public string MestoRodjenja { get { return tbMestoRodjenja.Text; } set { tbMestoRodjenja.Text = value; } }
        public string BrojTelefona { get { return tbBrojTelefona.Text; } set { tbBrojTelefona.Text = value; } }
        public string RadnoMesto { get { return tbRadnoMesto.Text; } set { tbRadnoMesto.Text = value; } }
        public string TekuciRacun { get { return tbTekuciRacun.Text; } set { tbTekuciRacun.Text = value; } }
        public int NormaRada
        {
            get
            {
                try
                {
                    return Int32.Parse(tbNormaRada.Text);

                }
                catch (Exception)
                {

                    return 0;
                };
            }
            set { tbIme.Text = value.ToString(); }
        }
        public string PutanjaDoSlike { get { return openFileDialog1.FileName; } set { } }
        public string KorisnickoIme { get { return tbKorisnickoIme.Text; } set { tbKorisnickoIme.Text = value; } }
        public string Lozinka { get { return tbLozinka.Text; } set { tbLozinka.Text = value; } }
        public string Uloga { get { return cbUlogaKorisnickogNaloga.Text; } set { cbUlogaKorisnickogNaloga.Text = value; } }
        public NoviZaposleniPresenter Presenter { get; set; }
        public PictureBox SlikaBox { get { return pictureBox1; } }

        public string Lozinka2 { get { return tbPonovljenaLozinka.Text; } set { tbPonovljenaLozinka.Text = value; } }

        public double KoeficijentRadnogMesta { get { return Double.Parse(tbKoeficijentRadnogMesta.Text); } set => throw new NotImplementedException(); }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                PutanjaDoSlike = openFileDialog1.FileName;
                Presenter.UcitajSliku();
            }
        }

        private void rbNalogDa_CheckedChanged(object sender, EventArgs e)
        {
            if (rbNalogDa.Checked)
            {
                tbKorisnickoIme.Enabled = true;
                tbLozinka.Enabled = true;
                tbPonovljenaLozinka.Enabled = true;
                cbUlogaKorisnickogNaloga.Enabled = true;

            }
            else
            {
                tbKorisnickoIme.Enabled = false;
                tbLozinka.Enabled = false;
                tbPonovljenaLozinka.Enabled = false;
                cbUlogaKorisnickogNaloga.Enabled = false;

            }
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            if (tbIme.Text == String.Empty)
            {
                neodIme.Visible = true;
                return;
            }
            else if (tbSrednjeSlovo.Text == String.Empty)
            {
                neodSrednjeSlovo.Visible = true;
                return;
            }
            else if (tbPrezime.Text == String.Empty)
            {
                neodPrezime.Visible = true;
                return;
            }
            else if (tbAdresa.Text == String.Empty)
            {
                neodAdresa.Visible = true;
                return;
            }
            else if (tbMestoRodjenja.Text == String.Empty)
            {
                neodMestoRodjenja.Visible = true;
                return;
            }
            else if (tbBrojTelefona.Text == String.Empty)
            {
                neodBrojTelefona.Visible = true;
                return;
            }
            else if (tbRadnoMesto.Text == String.Empty)
            {
                NeodRadnoMesto.Visible = true;
                return;
            }
            else if (tbTekuciRacun.Text == String.Empty)
            {
                neodTekuciRacun.Visible = true;
                return;
            }
            else if (tbNormaRada.Text == String.Empty)
            {
                neodNormaRada.Text = "Obavezna vrednost";
                neodNormaRada.Visible = true;
                return;
            }

            Int32 n;
            if (!Int32.TryParse(tbNormaRada.Text, out n) || n <= 0)
            {
                neodNormaRada.Text = "Nevalidna vrednost";
                neodNormaRada.Visible = true;
                return;
            }

            if (tbPocetniStaz.Text == String.Empty)
            {
                neodPocetniStaz.Text = "Obavezna vrednost";
                neodPocetniStaz.Visible = true;
                return;
            }
            Int32 p;
            if (!Int32.TryParse(tbPocetniStaz.Text, out p) || p < 0)
            {
                neodPocetniStaz.Text = "Nevalidna vrednost";
                neodPocetniStaz.Visible = true;
                return;
            }

            if (tbKoeficijentRadnogMesta.Text == String.Empty)
            {
                neodKoeficijentRadnogMesta.Text = "Obavezna vrednost";
                neodKoeficijentRadnogMesta.Visible = true;
                return;
            }
            double a;
            if (!Double.TryParse(tbKoeficijentRadnogMesta.Text, out a) || a <= 0)
            {
                neodKoeficijentRadnogMesta.Text = "Nevalidna vrednost";
                neodKoeficijentRadnogMesta.Visible = true;
                return;
            }

            if (rbNalogDa.Checked)
            {
                if(tbKorisnickoIme.Text == String.Empty)
                {
                    neodgovarajuciUnosKorisnickoIme.Text = "Obavezan unos";
                    neodgovarajuciUnosKorisnickoIme.Visible = true;
                    return;
                }
                else if (Presenter.DaLiPostojiKorisnickoIme())
                {
                    neodgovarajuciUnosKorisnickoIme.Text = "Korisničko ime je zauzeto";
                    neodgovarajuciUnosKorisnickoIme.Visible = true;
                    return;
                } else if (tbLozinka.Text == String.Empty)
                {
                    neodgovarajuciUnosLozinka.Text = "Lozinka je obavezna";
                    neodgovarajuciUnosLozinka.Visible = true;
                    return;
                } else if(tbPonovljenaLozinka.Text == String.Empty)
                {
                    neodgovarajuciUnosPonovljenaLozinka.Text = "Lozinka je obavezna";
                    neodgovarajuciUnosPonovljenaLozinka.Visible = true;
                    return;
                }
                else if (tbLozinka.Text != tbPonovljenaLozinka.Text)
                {
                    neodgovarajuciUnosPonovljenaLozinka.Text = "Lozinka je obavezna";
                    neodgovarajuciUnosLozinka.Text = "Lozinka je obavezna";
                    neodgovarajuciUnosLozinka.Visible = true;
                    neodgovarajuciUnosPonovljenaLozinka.Visible = true;
                    return;
                }
            }
            try
            {
                if (Presenter.DodajZaposlenog())
                {
                    MetroMessageBox.Show(this, "Novi zaposleni je uspešno dodat.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
                    DialogResult = DialogResult.OK;
                    Close();
                }
            }
            catch (Exception e1)
            {
                MetroMessageBox.Show(this, "Neka od unešenih vrednosti nije adekvatna.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
            }
        }

        public string Smena { set => lblSmena.Text = value; }
        public string Nadredjeni { set => lblNadredjeni.Text = value; }

        private void metroButton4_Click(object sender, EventArgs e)
        {
            Presenter.IzaberiNadredjenog();
        }

        private void tbKorisnickoIme_Click(object sender, EventArgs e)
        {
            neodgovarajuciUnosKorisnickoIme.Visible = false;
        }

        private void tbLozinka_Click(object sender, EventArgs e)
        {
            neodgovarajuciUnosPonovljenaLozinka.Text = "Lozinke nisu iste";
            neodgovarajuciUnosLozinka.Text = "Lozinke nisu iste";
            neodgovarajuciUnosLozinka.Visible = false;
            neodgovarajuciUnosPonovljenaLozinka.Visible = false;
        }

        private void tbMestoRodjenja_Click(object sender, EventArgs e)
        {
            neodMestoRodjenja.Visible = false;
        }

        private void tbIme_Click(object sender, EventArgs e)
        {
            neodIme.Visible = false;
        }

        private void tbSrednjeSlovo_Click(object sender, EventArgs e)
        {
            neodSrednjeSlovo.Visible = false;
        }

        private void tbPrezime_Click(object sender, EventArgs e)
        {
            neodPrezime.Visible = false;
        }

        private void tbAdresa_Click(object sender, EventArgs e)
        {
            neodAdresa.Visible = false;
        }

        private void tbBrojTelefona_Click(object sender, EventArgs e)
        {
            neodBrojTelefona.Visible = false;
        }

        private void tbRadnoMesto_Click(object sender, EventArgs e)
        {
            NeodRadnoMesto.Visible = false;
        }

        private void tbTekuciRacun_Click(object sender, EventArgs e)
        {
            neodTekuciRacun.Visible = false;
        }

        private void tbNormaRada_Click(object sender, EventArgs e)
        {
            neodNormaRada.Visible = false;
        }

        private void metroButton3_Click(object sender, EventArgs e)
        {
            Presenter.IzaberiSmenu();
        }

        private void tbPocetniStaz_Click(object sender, EventArgs e)
        {
            neodPocetniStaz.Text = "Obavezan unos";
            neodPocetniStaz.Visible = false;
        }

        private void tbKoeficijentRadnogMesta_Click(object sender, EventArgs e)
        {
            neodKoeficijentRadnogMesta.Text = "Obavezan unos";
            neodKoeficijentRadnogMesta.Visible = false;
        }

        private void metroButton5_Click(object sender, EventArgs e)
        {
            Presenter.ObrisiNadredjenog();
        }

        #region KeyPress

        private void tbIme_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void tbSrednjeSlovo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar) && e.KeyChar != '.')
                e.Handled = true;
        }

        private void tbPrezime_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void tbMestoRodjenja_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void tbBrojTelefona_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar) && e.KeyChar != '-' && e.KeyChar != '/')
                e.Handled = true;
        }

        private void tbRadnoMesto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && !Char.IsControl(e.KeyChar) && !Char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        private void tbTekuciRacun_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar) && e.KeyChar != '-')
                e.Handled = true;
        }

        private void tbNormaRada_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void tbPocetniStaz_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void tbKoeficijentRadnogMesta_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != ',')
                e.Handled = true;
        }
        #endregion

       
    }
}
