﻿using System;
using System.Drawing;
using System.Windows.Forms;
using MetroFramework.Forms;
using SharedComponents.Presenters;
using SharedComponents.Views;
using MetroFramework;

namespace SharedComponents.Forms
{
    public partial class MenadzerOsnovniPodaciForm : MetroForm, IMenadzerOsnovniPodaciView
	{
		public MenadzerOsnovniPodaciForm()
		{
			InitializeComponent();
            Slika = null;
		}

		public string ImeIPrezime { get => Lime.Text; set => Lime.Text = value;  }

		public Image Slika
		{
            get { return null; }
            set { if(value != null) pictureBox1.Image = value; }
		}

		public string BrojTelefona { get => txtTelefon.Text; set => txtTelefon.Text = value; }
		public string Adresa { get => txtAdresa.Text; set => txtAdresa.Text = value; }
		public int NormaRada { get => Convert.ToInt32( txtNorma.Text); set => txtNorma.Text = value.ToString(); }
		public string RadnoMesto { get => txtRadnoMesto.Text; set => txtRadnoMesto.Text = value; }
		public DateTime DatumPocetkaRada { get => Convert.ToDateTime(txtDatum.Text); set => txtDatum.Text = value.ToString("dd.MM.yyyy."); }
		public MenadzerOsnovniPodaciPresenter Presenter { get; set; }
		public double SankcijaStimulacija { get => Convert.ToDouble(txtSankcija.Text); set => txtSankcija.Text = value.ToString("N2"); }

		private void MenadzerOsnovniPodaciForm_Load(object sender, EventArgs e)
		{
			Presenter.MyOnLoad();
		}

		private void btnSankcija_Click(object sender, EventArgs e)
		{
			var sankcija = new SankcijaStimulacijaForm();
			var sprez = new SankcijaStimulacijaPresenter(Presenter.Id, sankcija, true);
			if (sankcija.ShowDialog() == DialogResult.OK)
				Presenter.PromenaSankcijeSlimulacije();
		}

		private void btnStimulacija_Click(object sender, EventArgs e)
		{
			var stimulacija = new SankcijaStimulacijaForm();
			var sprez = new SankcijaStimulacijaPresenter(Presenter.Id, stimulacija, false);
			if (stimulacija.ShowDialog() == DialogResult.OK)
				Presenter.PromenaSankcijeSlimulacije();
		}

		private void txtNorma_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
				e.Handled = true;
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
            if (String.IsNullOrEmpty(txtNorma.Text))
            {
                MetroMessageBox.Show(this, "Norma rada je prazna.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                return;
            }
            Presenter.PromenaNorme();
			DialogResult = DialogResult.OK;
		}

        private void btnKasnjenja_Click(object sender, EventArgs e)
        {
            DnevnikOdsustvovanja f = new DnevnikOdsustvovanja();
            DnevnikOdsustvovanjaPresenter p = new DnevnikOdsustvovanjaPresenter(f, Presenter.Id);
            f.ShowDialog();
        }

        private void btnIstorija_Click(object sender, EventArgs e)
        {
            SankStimHistoryForm ss = new SankStimHistoryForm(Presenter.Id);
            ss.ShowDialog();
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            PregledOdsustvaForm pof = new PregledOdsustvaForm(Presenter.Id, false);
            pof.ShowDialog(this);
        }
    }
}
