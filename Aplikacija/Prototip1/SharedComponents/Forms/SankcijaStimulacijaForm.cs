﻿using System;
using System.Windows.Forms;
using MetroFramework.Forms;
using SharedComponents.Presenters;
using SharedComponents.Views;
using MetroFramework;

namespace SharedComponents.Forms
{
    public partial class SankcijaStimulacijaForm : MetroForm, ISankcijaStimulacijaView
	{
		private bool jeSankcija; //true -> sankcija, false -> stimulacija

		public SankcijaStimulacijaForm()
		{
			InitializeComponent();
		}

		public string ImeIPrezime { get => Lime.Text; set => Lime.Text = value; }
		public double Vrednost
        {
            get
            {
                double v;
                if (!double.TryParse(txtSankcijaStimulacija.Text, out v))
                {
                    v = -1;
                }
                return v;
            }
            set => txtSankcijaStimulacija.Text = value.ToString();
        }
		public string Opis { get => txtOpis.Text; set => txtOpis.Text = value; }
		public bool JeSankcija
		{
			get => jeSankcija;
			set
			{
				jeSankcija = value;
				Text = jeSankcija ? "Dodaj sankciju" : "Dodaj stimulaciju";
			}
		}
		public SankcijaStimulacijaPresenter Presenter { get; set; }

		private void SankcijaStimulacijaForm_Load(object sender, EventArgs e)
		{
			Presenter.MyOnLoad();
			lblError.Text = "";
			this.BringToFront();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
            String sankstim = jeSankcija ? "Sankcija" : "Stimulacija";
            if (Presenter.Sacuvaj())
            {
                MetroMessageBox.Show(this, sankstim + " je uspešno sačuvana.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                DialogResult = DialogResult.OK;
            }
            else
            {
                MetroMessageBox.Show(this, sankstim + " nije sačuvana zato što unešene vrednosti nisu validne.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
            }
		}

		private void txtSankcijaStimulacija_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!Char.IsNumber(e.KeyChar) && !Char.IsControl(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != ',')
				e.Handled = true;
		}

		private void txtSankcijaStimulacija_Leave(object sender, EventArgs e)
		{
			float s;
			if (!float.TryParse(txtSankcijaStimulacija.Text, out s))
            {
                lblError.Text = "Unesite broj između '0' i '100'.";
            }
			if (s <= 0 || s > 100)
            {
                lblError.Text = "Unesite broj između '0' i '100'.";
            }
			else
            {
                lblError.Text = "";
            }
		}
    }
}
