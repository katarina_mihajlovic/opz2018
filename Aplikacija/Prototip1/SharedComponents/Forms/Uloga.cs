﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SharedComponents.Forms
{
    public class Uloga
    {
        public String ImeUloge
        {
            get; set;
        }
        public Form Forma
        {
            get; set;
        }

        public Uloga(String i, Form f)
        {
            ImeUloge = i;
            Forma = f;
        }
    }
}
