﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Forms;
using Persistence.Entities;


namespace SharedComponents.Forms
{
    public partial class ArhiviranjeZaposlenihForm : MetroForm
    {
        public ArhiviranjeZaposlenihForm()
        {
            InitializeComponent();
        }

        private void ArhiviranjeZaposlenih_Load(object sender, EventArgs e)
        {
            ReloadDGV();
        }

        private void btnArhivirajOznacene_Click(object sender, EventArgs e)
        {
            if (gridLevi.SelectedRows.Count == 0)
            {
                return;
            }


            foreach (DataGridViewRow row in gridLevi.SelectedRows)
            {
                Int32? id = row.Cells[0].Value as Int32?;
                if (id == null)
                {
                    return;
                }
                try
                {
                    using (OpzContext context = new OpzContext())
                    {
                        Zaposleni z = context.ListaZaposlenih.Find(id.Value);



                        if((DateTime.Now - z.DatumKrajaRada).Value.TotalDays >= 365){
                            Arhivirani arh = new Arhivirani()
                            {
                                Ime = z.Ime,
                                SSlovo = z.SSlovo,
                                Prezime = z.Prezime,
                                DatumRodjenja = z.DatumRodjenja,
                                Adresa = z.Adresa,
                                MestoRodjenja = z.MestoRodjenja,
                                RadnoMesto = z.RadnoMesto,
                                DatumPocetkaRada = z.DatumPocetkaRada,
                                DatumKrajaRada = z.DatumKrajaRada.Value
                            };

                            context.ListaOdsustva.RemoveRange(z.Odsustva);
                            context.ListaPrisustva.RemoveRange(z.Prisustva);
                            context.ListaRadnihVremena.RemoveRange(z.RadnaVremena);
                            context.ListaSankcijaStimulacija.RemoveRange(z.SankcijeStimulacije);
                            context.ListaObracuna.RemoveRange(z.Obracuni);
                            context.ListaSlika.RemoveRange(z.Slike);

                            context.ListaZaposlenih.Remove(z);
                            context.ListaArhiviranih.Add(arh);
                            context.SaveChanges();
                        }
                        else
                        {
                            MetroMessageBox.Show(this, String.Format("Zaposleni sa brojem {0} mora biti otpušten bar godinu dana da bi bio kandidat za arhiviranje.", id.Value), "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
                        }

                        
                    }
                }
                catch (Exception)
                {
                    MetroMessageBox.Show(this,String.Format("Zaposleni sa brojem {0} je već arhiviran.", id.Value) , "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
                }

            }

            ReloadDGV();
        }

        private void ReloadDGV()
        {
            using (OpzContext context = new OpzContext())
            {
                var colLevi = (from z in context.ListaZaposlenih
                               where z.DatumKrajaRada != null
                               orderby z.Id ascending
                               select new
                               {
                                   Id = z.Id,
                                   Ime = z.Ime,
                                   SSlovo = z.SSlovo,
                                   Prezime = z.Prezime,
                                   DatumRodjenja = z.DatumRodjenja,
                                   Adresa = z.Adresa,
                                   MestoRodjenja = z.MestoRodjenja,
                                   RadnoMesto = z.RadnoMesto,
                                   DatumPocetkaRada = z.DatumPocetkaRada,
                                   DatumKrajaRada = z.DatumKrajaRada
                               }).ToList();

                var colDesni = (from zap in context.ListaArhiviranih
                                orderby zap.DatumKrajaRada descending
                                select new
                                {
                                    Ime = zap.Ime,
                                    SSlovo = zap.SSlovo,
                                    Prezime = zap.Prezime,
                                    DatumRodjenja = zap.DatumRodjenja,
                                    Adresa = zap.Adresa,
                                    MestoRodjenja = zap.MestoRodjenja,
                                    RadnoMesto = zap.RadnoMesto,
                                    DatumPocetkaRada = zap.DatumPocetkaRada,
                                    DatumKrajaRada = zap.DatumKrajaRada
                                }).ToList();

                ListRefresh(colLevi, colDesni);
            }
        }

        private void ListRefresh(Object forLevi, Object forDesni)
        {
            if (forLevi != null)
            {
                gridLevi.DataSource = forLevi;
                gridLevi.Columns["Id"].HeaderText = "Broj radnika";
                gridLevi.Columns["SSlovo"].HeaderText = "Srednje slovo";
                gridLevi.Columns["DatumRodjenja"].HeaderText = "Datum rođenja";
                gridLevi.Columns["MestoRodjenja"].HeaderText = "Mesto rođenja";
                gridLevi.Columns["RadnoMesto"].HeaderText = "Radno mesto";
                gridLevi.Columns["DatumPocetkaRada"].HeaderText = "Datum početka rada";
                gridLevi.Columns["DatumKrajaRada"].HeaderText = "Datum kraja rada";

                /*foreach (DataGridViewColumn dgvcol in gridLevi.Columns)
                {
                    dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }*/
            }

            if (forDesni != null)
            {
                gridDesni.DataSource = forDesni;

                gridDesni.Columns["SSlovo"].HeaderText = "Srednje slovo";
                gridDesni.Columns["DatumRodjenja"].HeaderText = "Datum rođenja";
                gridDesni.Columns["MestoRodjenja"].HeaderText = "Mesto rođenja";
                gridDesni.Columns["RadnoMesto"].HeaderText = "Radno mesto";
                gridDesni.Columns["DatumPocetkaRada"].HeaderText = "Datum početka rada";
                gridDesni.Columns["DatumKrajaRada"].HeaderText = "Datum kraja rada";

                /*foreach (DataGridViewColumn dgvcol in gridDesni.Columns)
                {
                    dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }*/
            }

            foreach (DataGridViewColumn dgvcol in gridLevi.Columns)
            {
                if (dgvcol.Index == 0) continue;
                dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            foreach (DataGridViewColumn dgvcol in gridDesni.Columns)
            {
                if (dgvcol.Index == 0) continue;
                dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }
    }
}
