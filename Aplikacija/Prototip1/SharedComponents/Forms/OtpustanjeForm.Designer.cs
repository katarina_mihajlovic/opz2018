﻿namespace SharedComponents.Forms
{
    partial class OtpustanjeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl = new MetroFramework.Controls.MetroLabel();
            this.lblRadnoMesto = new MetroFramework.Controls.MetroLabel();
            this.btnOk = new MetroFramework.Controls.MetroButton();
            this.btnCancel = new MetroFramework.Controls.MetroButton();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.dtpPocetak = new MetroFramework.Controls.MetroDateTime();
            this.lblIme = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(23, 71);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(51, 19);
            this.lbl.TabIndex = 0;
            this.lbl.Text = "Radnik:";
            // 
            // lblRadnoMesto
            // 
            this.lblRadnoMesto.AutoSize = true;
            this.lblRadnoMesto.Location = new System.Drawing.Point(73, 91);
            this.lblRadnoMesto.Name = "lblRadnoMesto";
            this.lblRadnoMesto.Size = new System.Drawing.Size(84, 19);
            this.lblRadnoMesto.TabIndex = 1;
            this.lblRadnoMesto.Text = "radno mesto";
            // 
            // btnOk
            // 
            this.btnOk.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnOk.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnOk.Location = new System.Drawing.Point(43, 282);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(93, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Potvrdi";
            this.btnOk.UseSelectable = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnCancel.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.btnCancel.Location = new System.Drawing.Point(176, 282);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(93, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Odustani";
            this.btnCancel.UseSelectable = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(17, 166);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(188, 19);
            this.metroLabel3.TabIndex = 8;
            this.metroLabel3.Text = "Datum stupanja u radni odnos:";
            // 
            // dtpPocetak
            // 
            this.dtpPocetak.CustomFormat = "dd.MM.yyyy.";
            this.dtpPocetak.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPocetak.Location = new System.Drawing.Point(17, 191);
            this.dtpPocetak.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtpPocetak.Name = "dtpPocetak";
            this.dtpPocetak.Size = new System.Drawing.Size(279, 29);
            this.dtpPocetak.Style = MetroFramework.MetroColorStyle.Purple;
            this.dtpPocetak.TabIndex = 7;
            // 
            // lblIme
            // 
            this.lblIme.AutoSize = true;
            this.lblIme.Location = new System.Drawing.Point(73, 71);
            this.lblIme.Name = "lblIme";
            this.lblIme.Size = new System.Drawing.Size(83, 19);
            this.lblIme.TabIndex = 9;
            this.lblIme.Text = "metroLabel1";
            // 
            // OtpustanjeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 332);
            this.Controls.Add(this.lblIme);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.dtpPocetak);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.lblRadnoMesto);
            this.Controls.Add(this.lbl);
            this.MaximizeBox = false;
            this.Name = "OtpustanjeForm";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Purple;
            this.Text = "Prekid radnog odnosa";
            this.Load += new System.EventHandler(this.OtpustanjeForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lbl;
        private MetroFramework.Controls.MetroLabel lblRadnoMesto;
        private MetroFramework.Controls.MetroButton btnOk;
        private MetroFramework.Controls.MetroButton btnCancel;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroDateTime dtpPocetak;
        private MetroFramework.Controls.MetroLabel lblIme;
    }
}