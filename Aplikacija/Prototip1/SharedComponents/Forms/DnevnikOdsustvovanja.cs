﻿using System;
using MetroFramework.Forms;
using SharedComponents.Presenters;
using SharedComponents.Views;
using System.Windows.Forms;

namespace SharedComponents.Forms
{
    public partial class DnevnikOdsustvovanja : MetroForm, IDnevnikOdsustvovanjaView
    {
        private object forIzostanci;
        private object forKasnjenja;

        public DnevnikOdsustvovanja()
        {
            InitializeComponent();
        }

        public string Ime { set => lblImeIPrezime.Text = value; }
        public string RadnoMesto { set => lblRadnoMesto.Text = value; }
        public object KasnjenjaDataSource { set => forKasnjenja = value; }
        public object IzostanciDataSource { set => forIzostanci = value; }
        public DnevnikOdsustvovanjaPresenter Presenter { get; set; }
        public bool OvajMesec
        {
            get
            {
                if (cmbMesec.SelectedIndex == 1)
                    return false;
                else
                    return true;
            }
            set
            {
                if (value)
                    cmbMesec.SelectedIndex = 0;
                else
                    cmbMesec.SelectedIndex = 1;
            }
        }

        private void DnevnikOdsustvovanja_Load(object sender, EventArgs e)
        {
            Presenter.MyOnLoad();
            ListRefresh();
        }

        private void ListRefresh()
        {
            gridKasnjenja.DataSource = forKasnjenja;
            gridIzostanci.DataSource = forIzostanci;

            foreach (DataGridViewColumn dgvcol in gridKasnjenja.Columns)
            {
                dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            foreach (DataGridViewColumn dgvcol in gridIzostanci.Columns)
            {
                dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }

            if (forKasnjenja != null)
                gridKasnjenja.Columns[1].HeaderText = "Kašnjenje (min)";
        }

        private void cmbMesec_SelectedIndexChanged(object sender, EventArgs e)
        {
            Presenter.GetData();
            ListRefresh();
        }
    }
}
