﻿using MetroFramework.Forms;
using Persistence.Entities;
using SharedComponents.Presenters;
using SharedComponents.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SharedComponents.Forms
{
    public partial class IzborSmeneForm : MetroForm, IIzborSmeneView
    {
        public IzborSmeneForm()
        {
            InitializeComponent();
            dgv.AutoGenerateColumns = false;
        }

        public IZaposleniPresenter Presenter { get; set; }

        public string Smena => dgv.SelectedRows[0].Cells[0].Value.ToString();

        private void GlavnaForma_Load(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync(String.Empty);
        }

        private void ListRefresh(object col)
        {
            List<SmenaPrint> l = col as List<SmenaPrint>;

            foreach(SmenaPrint p in l)
            {
                dgv.Rows.Add(p.NazivSmene, p.Ponedeljak, p.Utorak, p.Sreda, p.Cetvrtak, p.Petak, p.Subota, p.Nedelja);
            }

            //dgv.DataSource = col;

            dgv.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private class SmenaPrint
        {
            public string NazivSmene { get; set; }
            public string Ponedeljak { get; set; }
            public string Utorak { get; set; }
            public string Sreda { get; set; }
            public string Cetvrtak { get; set; }
            public string Petak { get; set; }
            public string Subota { get; set; }
            public string Nedelja { get; set; }

            public SmenaPrint()
            {

            }
        }

        delegate void Del1(object a);
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            using (OpzContext kontekst = new OpzContext())
            {
                Del2 spnstart = StartSpinning;
                Del2 spnstop = StopSpinning;
                Invoke(spnstart);

                List<string> col = (from p in kontekst.ListaSmena select p.TipSmene).Distinct<string>().OrderBy(x => x.ToUpper()).ToList();

                List<SmenaPrint> smene = new List<SmenaPrint>();

                foreach(string smena in col)
                {
                    DateTime maxDatum = (from p in kontekst.ListaSmena where p.TipSmene == smena select p.DatumDonosenja).Max();
                    List<Smena> poDanima = (from p in kontekst.ListaSmena where p.DatumDonosenja == maxDatum && p.TipSmene == smena select p).ToList();

                    SmenaPrint current = new SmenaPrint();
                    current.NazivSmene = smena;
                    foreach(Smena a in poDanima)
                    {
                        if (a.DanUNedelji == "ponedeljak")
                        {
                            current.Ponedeljak = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }else if (a.DanUNedelji == "utorak")
                        {
                            current.Utorak = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "sreda")
                        {
                            current.Sreda = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "cetvrtak")
                        {
                            current.Cetvrtak = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "petak")
                        {
                            current.Petak = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "subota")
                        {
                            current.Subota = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "nedelja")
                        {
                            current.Nedelja = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                    }
                    //sad za neradne dane...
                    if (current.Ponedeljak == null || current.Ponedeljak.Equals(""))
                    {
                        current.Ponedeljak = "Neradan dan";
                    }
                    if (current.Utorak == null || current.Ponedeljak.Equals(""))
                    {
                        current.Utorak = "Neradan dan";
                    }
                    if (current.Sreda == null || current.Ponedeljak.Equals(""))
                    {
                        current.Sreda = "Neradan dan";
                    }
                    if (current.Cetvrtak == null || current.Ponedeljak.Equals(""))
                    {
                        current.Cetvrtak = "Neradan dan";
                    }
                    if (current.Petak == null || current.Ponedeljak.Equals(""))
                    {
                        current.Petak = "Neradan dan";
                    }
                    if (current.Subota == null || current.Ponedeljak.Equals(""))
                    {
                        current.Subota = "Neradan dan";
                    }
                    if (current.Nedelja == null || current.Ponedeljak.Equals(""))
                    {
                        current.Nedelja = "Neradan dan";
                    }

                    smene.Add(current);
                }

                Del1 del = ListRefresh;
                Invoke(del, new object[] { smene });
            }
        }

        private delegate void Del2();
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Del2 spnstart = StartSpinning;
            Del2 spnstop = StopSpinning;
            //zaustavljanje spinnera
            Invoke(spnstop);
        }

        #region Spinner
        private void StartSpinning()
        {
            lblSpinner.Visible = true;
            circularProgressBar1.Visible = true;

            metroButton2.Enabled = false;
        }

        private void StopSpinning()
        {
            lblSpinner.Visible = false;
            circularProgressBar1.Visible = false;

            metroButton2.Enabled = true;
        }
        #endregion

        private void metroButton2_Click(object sender, EventArgs e)
        {
            Presenter.UzmiSmenu();
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void IzborSmeneForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                e.Cancel = true;
            }
        }
    }
}
