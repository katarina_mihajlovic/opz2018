﻿using MetroFramework.Forms;
using System;
using System.Data;
using System.Linq;
using Persistence.Entities;
using MetroFramework;
using System.Windows.Forms;

namespace SharedComponents.Forms
{
    public partial class PregledOdsustvaForm : MetroForm
    {
        Zaposleni zap;

        public PregledOdsustvaForm()
        {
            InitializeComponent();
        }
        public PregledOdsustvaForm(int idZaposlenog, bool svaOdsustva)
            : this()
        {
            using (OpzContext kontekst = new OpzContext())
            {
                zap = kontekst.ListaZaposlenih.Find(idZaposlenog);

                if (svaOdsustva)
                {
                    gridOdsustva.DataSource = (from ods in zap.Odsustva
                                               orderby ods.Pocetak descending
                                               select new
                                               {
                                                   PocetakOdsustva = ods.Pocetak,
                                                   KrajOdsustva = ods.Kraj,
                                                   BrojUDelovodniku = ods.BrojUDelovodniku,
                                                   TipOdsustva = ods.Tip
                                               }).ToList();
                }
                else
                {
                    gridOdsustva.DataSource = (from ods in zap.Odsustva
                                               where ods.Kraj >= DateTime.Today
                                               orderby ods.Pocetak ascending
                                               select new
                                               {
                                                   PocetakOdsustva = ods.Pocetak,
                                                   KrajOdsustva = ods.Kraj,
                                                   BrojUDelovodniku = ods.BrojUDelovodniku,
                                                   TipOdsustva = ods.Tip
                                               }).ToList();
                }
            }

            gridOdsustva.Columns[0].HeaderText = "Početak odsustva";
            gridOdsustva.Columns[1].HeaderText = "Kraj odsustva";
            gridOdsustva.Columns[2].HeaderText = "Broj u delovodniku";
            gridOdsustva.Columns[3].HeaderText = "Tip odsustva";

            foreach (DataGridViewColumn dgvcol in gridOdsustva.Columns)
            {
                dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }


        }

        private void PregledOdsustvaForm_Load(object sender, EventArgs e)
        {
            Personalize();
        }

        private void Personalize()
        {
            MetroColorStyle stil = (Owner as MetroForm).Style;
            this.Style = stil;
            gridOdsustva.Style = stil;

        }
    }
}
