﻿using MetroFramework.Forms;
using System;
using System.Windows.Forms;
using SharedComponents.Views;
using SharedComponents.Presenters;
using MetroFramework;

namespace SharedComponents.Forms
{
    public partial class OdsustvoForm : MetroForm, IOdsustvoView
    {
        public DateTime PocetakOdsustva
        {
            get { return dtmPocetak.Value.Date; }
            set { dtmPocetak.Value = value; }
        }
        public DateTime KrajOdsustva
        {
            get { return dtmKraj.Value.Date; }
            set { dtmKraj.Value = value; }
        }
        public String TipOdsustva
        {
            get { return cbxOdsustva.Text; }
            set { cbxOdsustva.Text = value; }
        }
        public String BrojUDelovodniku
        {
            get { return txtDelovodnik.Text; }
            set { txtDelovodnik.Text = value; }
        }
        public OdsustvoPresenter Presenter
        {
            get; set;
        }

        public OdsustvoForm()
        {
            InitializeComponent();
        }

        private void btnIzadji_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (Presenter.DodajOdsustvo())
            {
                MetroMessageBox.Show(this, "Novo odsustvo je uspešno dodato.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
                Close();
            }
            else
            {
                MetroMessageBox.Show(this, "Unešene vrednosti nisu validne.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 120);
            }
        }
    }
}
