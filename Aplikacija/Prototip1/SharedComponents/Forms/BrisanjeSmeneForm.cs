﻿using MetroFramework;
using MetroFramework.Forms;
using Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SharedComponents.Forms
{
    public partial class BrisanjeSmeneForm : MetroForm
    {
        public BrisanjeSmeneForm()
        {
            InitializeComponent();
        }

        private class SmenaPrint
        {
            public string NazivSmene { get; set; }
            public string Ponedeljak { get; set; }
            public string Utorak { get; set; }
            public string Sreda { get; set; }
            public string Cetvrtak { get; set; }
            public string Petak { get; set; }
            public string Subota { get; set; }
            public string Nedelja { get; set; }

            public SmenaPrint()
            {

            }
        }

        private void BrisanjeSmeneForm_Load(object sender, EventArgs e)
        {
            dgv.AutoGenerateColumns = false;

            using (OpzContext kontekst = new OpzContext())
            {

                var i = (from p in kontekst.ListaSmena select p.TipSmene).Distinct().OrderBy(x => x.ToUpper()).ToList();
                var j = (from p in kontekst.ListaRadnihVremena select p.Smena).Distinct().ToList();

                var cols = (i.Except(j));

                var col = cols.ToList();

                if (col.Count == 0)
                {
                    metroLabel2.Text = "Ne postoji smena koja može biti obrisana.";
                    metroLabel2.ForeColor = Color.Red;
                    metroLabel2.UseCustomForeColor = true;
                    metroLabel2.UseStyleColors = true;
                    return;
                }

                List<SmenaPrint> smene = new List<SmenaPrint>();

                foreach (string smena in col)
                {
                    DateTime maxDatum = (from p in kontekst.ListaSmena where p.TipSmene == smena select p.DatumDonosenja).Max();
                    List<Smena> poDanima = (from p in kontekst.ListaSmena where p.DatumDonosenja == maxDatum && p.TipSmene == smena select p).ToList();

                    SmenaPrint current = new SmenaPrint();
                    current.NazivSmene = smena;
                    foreach (Smena a in poDanima)
                    {
                        if (a.DanUNedelji == "ponedeljak")
                        {
                            current.Ponedeljak = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "utorak")
                        {
                            current.Utorak = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "sreda")
                        {
                            current.Sreda = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "cetvrtak")
                        {
                            current.Cetvrtak = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "petak")
                        {
                            current.Petak = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "subota")
                        {
                            current.Subota = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                        else if (a.DanUNedelji == "nedelja")
                        {
                            current.Nedelja = a.Pocetak.ToString("HH:mm") + "-" + a.Kraj.ToString("HH:mm");
                        }
                    }
                    //sad za neradne dane...
                    if (current.Ponedeljak == null || current.Ponedeljak.Equals(""))
                    {
                        current.Ponedeljak = "Neradan dan";
                    }
                    if (current.Utorak == null || current.Ponedeljak.Equals(""))
                    {
                        current.Utorak = "Neradan dan";
                    }
                    if (current.Sreda == null || current.Ponedeljak.Equals(""))
                    {
                        current.Sreda = "Neradan dan";
                    }
                    if (current.Cetvrtak == null || current.Ponedeljak.Equals(""))
                    {
                        current.Cetvrtak = "Neradan dan";
                    }
                    if (current.Petak == null || current.Ponedeljak.Equals(""))
                    {
                        current.Petak = "Neradan dan";
                    }
                    if (current.Subota == null || current.Ponedeljak.Equals(""))
                    {
                        current.Subota = "Neradan dan";
                    }
                    if (current.Nedelja == null || current.Ponedeljak.Equals(""))
                    {
                        current.Nedelja = "Neradan dan";
                    }

                    smene.Add(current);
                }

                foreach (SmenaPrint p in smene)
                {
                    dgv.Rows.Add(p.NazivSmene, p.Ponedeljak, p.Utorak, p.Sreda, p.Cetvrtak, p.Petak, p.Subota, p.Nedelja);
                }

                foreach (DataGridViewColumn dgvcol in dgv.Columns)
                {
                    dgvcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            string smena = null;
            try
            {
                smena = dgv.SelectedRows[0].Cells[0].Value.ToString();
            }
            catch (Exception)
            {
                MetroMessageBox.Show(this, "Treba izabrati jednu smenu.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);
                return;
            }

            using (OpzContext c = new OpzContext())
            {
                var i = from p in c.ListaSmena where p.TipSmene == smena select p;

                foreach (var a in i)
                {
                    c.Entry<Smena>(a).State = System.Data.Entity.EntityState.Deleted;
                }
                c.SaveChanges();
            }
            MetroMessageBox.Show(this, "Izabrana smena je uspešno obrisana.", "Obaveštenje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, 200);

            //sada treba obrisati vrstu
            dgv.Rows.RemoveAt(dgv.SelectedRows[0].Index);
        }
    }
}
