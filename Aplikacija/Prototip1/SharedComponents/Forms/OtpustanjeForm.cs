﻿using System;
using System.Windows.Forms;
using MetroFramework.Forms;
using SharedComponents.Presenters;
using SharedComponents.Views;

namespace SharedComponents.Forms
{
    public partial class OtpustanjeForm : MetroForm, IOtpustanjeView
    {
        public OtpustanjeForm()
        {
            InitializeComponent();
        }

        public string Ime { set => lblIme.Text = value; }
        public string RadnoMesto { set => lblRadnoMesto.Text = value; }
        public DateTime DatumPocetka
        {
            set
            {
                dtpPocetak.Value = value;
                dtpPocetak.Enabled = false;
            }
        }
        public OtpustanjePresenter Presenter { get; set; }

        private void OtpustanjeForm_Load(object sender, EventArgs e)
        {
            Presenter.MyOnload();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Presenter.SaveChanges();
            this.DialogResult = DialogResult.OK;
        }
    }
}
