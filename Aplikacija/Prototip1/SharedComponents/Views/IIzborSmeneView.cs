﻿using SharedComponents.Presenters;

namespace SharedComponents.Views
{
    public interface IIzborSmeneView
    {
        string Smena { get; }
        IZaposleniPresenter Presenter { get; set; }
    }
}
