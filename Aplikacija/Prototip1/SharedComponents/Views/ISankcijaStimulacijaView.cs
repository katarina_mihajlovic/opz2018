﻿using SharedComponents.Presenters;

namespace SharedComponents.Views
{
    public interface ISankcijaStimulacijaView
	{
		string ImeIPrezime { get; set; }
		double Vrednost { get; set; }
		string Opis { get; set; }

		bool JeSankcija { get; set; }

		SankcijaStimulacijaPresenter Presenter { get; set; }
	}
}
