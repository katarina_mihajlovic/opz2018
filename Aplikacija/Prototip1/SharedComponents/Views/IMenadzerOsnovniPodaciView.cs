﻿using System;
using SharedComponents.Presenters;
using System.Drawing;

namespace SharedComponents.Views
{
    public interface IMenadzerOsnovniPodaciView
	{
		string ImeIPrezime { get; set; }
		Image Slika { get; set; }

		string BrojTelefona { get; set; }
		string Adresa { get; set; }

		int NormaRada { get; set; }
		string RadnoMesto { get; set; }
		DateTime DatumPocetkaRada { get; set; }
		double SankcijaStimulacija { get; set; }

		MenadzerOsnovniPodaciPresenter Presenter { get; set; }
	}
}
