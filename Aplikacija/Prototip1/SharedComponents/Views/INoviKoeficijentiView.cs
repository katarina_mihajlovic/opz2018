﻿using SharedComponents.Presenters;

namespace SharedComponents.Views
{
    public interface INoviKoeficijentiView
    {
        double CenaRada
        {
            get; set;
        }
        double ProcenatZaExtra
        {
            get; set;
        }
        double ProcenatZaMinuli
        {
            get; set;
        }
        double NeoporezivaOsnovica
        {
            get; set;
        }
        double ProcenatZaPorez
        {
            get; set;
        }
        double DopZaposlenogPIO
        {
            get; set;
        }
        double DopZaposlenogZdravstvo
        {
            get; set;
        }
        double DopZaposlenogNezaposlenost
        {
            get; set;
        }
        double DopPoslodavcaPIO
        {
            get; set;
        }
        double DopPoslodavcaZdravstvo
        {
            get; set;
        }
        double DopPoslodavcaNezaposlenost
        {
            get; set;
        }
        NoviKoeficijentiPresenter Presenter
        {
            get; set;
        }
    }
}
