﻿using SharedComponents.Presenters;
using System;
using System.Windows.Forms;

namespace SharedComponents.Views
{
    public interface INoviZaposleniView
    {
        string Ime { get; set; }
        string SrednjeSlovo { get; set; }
        string Prezime { get; set; }
        bool SaKorisnickimNalogom { get; }
        DateTime DatumRodjenja { get; set; }
        string Adresa { get; set; }
        string MestoRodjenja { get; set; }
        string BrojTelefona { get; set; }
        string RadnoMesto { get; set; }
        string TekuciRacun { get; set; }
        int NormaRada { get; set; }
        string PutanjaDoSlike { get; set; }
        string KorisnickoIme { get; set; }
        string Lozinka { get; set; }
        string Lozinka2 { get; set; }
        string Uloga { get; set; }
        int PocetniStaz { get; set; }
        double KoeficijentRadnogMesta { get; set; }
        string Smena { set; }
        string Nadredjeni { set; }
        NoviZaposleniPresenter Presenter
        {
            get; set;
        }
        void PrikaziObavestenje(string a);
        PictureBox SlikaBox { get; }
    }
}
