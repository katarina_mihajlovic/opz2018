﻿using System;
using SharedComponents.Presenters;

namespace SharedComponents.Views
{
    public interface IOtpustanjeView
    {
        string Ime { set; }
        string RadnoMesto { set; }

        DateTime DatumPocetka { set; }

        OtpustanjePresenter Presenter { get; set; }
    }
}
