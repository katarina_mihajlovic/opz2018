﻿using System;
using SharedComponents.Presenters;

namespace SharedComponents.Views
{
    public interface IOdsustvoView
    {
        DateTime PocetakOdsustva
        {
            get; set;
        }
        DateTime KrajOdsustva
        {
            get; set;
        }
        String TipOdsustva
        {
            get; set;
        }
        String BrojUDelovodniku
        {
            get; set;
        }
        OdsustvoPresenter Presenter
        {
            get; set;
        }
    }
}
