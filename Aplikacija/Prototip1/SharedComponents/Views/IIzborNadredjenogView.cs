﻿using SharedComponents.Presenters;

namespace SharedComponents.Views
{
    public interface IIzborNadredjenogView
    {
        int IndeksNadredjenog { get; }
        IZaposleniPresenter Presenter { get; set; }
    }
}
