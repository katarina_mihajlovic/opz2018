﻿using SharedComponents.Presenters;

namespace SharedComponents.Views
{
    public interface IDnevnikOdsustvovanjaView
    {
        string Ime { set; }
        string RadnoMesto { set; }
        object KasnjenjaDataSource { set; }
        object IzostanciDataSource { set; }
        bool OvajMesec { get; set; }
        DnevnikOdsustvovanjaPresenter Presenter { get; set; }

    }
}
