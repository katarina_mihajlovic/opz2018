﻿using System;
using System.Drawing;
using SharedComponents.Presenters;

namespace SharedComponents.Views
{
    public interface IAzuriranjeZaposlenogView
    {
        string Ime { get; set; }
        string SrednjeSlovo { get; set; }
        string Prezime { get; set; }
        bool SaKorisnickimNalogom { get; set; }
        DateTime DatumRodjenja { get; set; }
        string Adresa { get; set; }
        string MestoRodjenja { get; set; }
        string BrojTelefona { get; set; }
        string RadnoMesto { get; set; }
        string TekuciRacun { get; set; }
        int NormaRada { get; set; }
        Image Slika { get; set; }

        string PutanjaDoSlike { get; set; }

        string KorisnickoIme { get; set; }
        string Uloga { get; set; }
        int PocetniStaz { get; set; }
        double KoeficijentRadnogMesta { get; set; }
        string Smena { set; }
        string Nadredjeni { set; }

        bool IsChangedIdNadredjenog { get; set; }
        bool IsChangedSlika { get; set; }
        bool IsChangedNalog { get; set; }
        bool IsChangedPassword { get; set; }
        bool IsChangedSmena { get; set; }

        AzuriranjeZaposlenogPresenter Presenter { get; set; }
    }
}
