﻿using System;
using SharedComponents.Views;
using Persistence.Entities;

namespace SharedComponents.Presenters
{
    public class OdsustvoPresenter
    {
        private IOdsustvoView view;
        private int idZaposlenog;

        public OdsustvoPresenter(int x, IOdsustvoView y)
        {
            idZaposlenog = x;
            view = y;
            y.Presenter = this;
        }

        public bool DodajOdsustvo()
        {
            if (view.PocetakOdsustva <= DateTime.Today || view.PocetakOdsustva > view.KrajOdsustva)
            {
                return false;
            }
            if (String.IsNullOrEmpty(view.BrojUDelovodniku) || String.IsNullOrEmpty(view.TipOdsustva))
            {
                return false;
            }

            Odsustvo od = new Odsustvo()
            {
                Pocetak = view.PocetakOdsustva,
                Kraj = view.KrajOdsustva,
                BrojUDelovodniku = view.BrojUDelovodniku,
                Tip = view.TipOdsustva
            };

            using (OpzContext kontekst = new OpzContext())
            {
                Zaposleni zap = kontekst.ListaZaposlenih.Find(idZaposlenog);
                zap.Odsustva.Add(od);
                kontekst.SaveChanges();
            }

            return true;
        }
    }
}
