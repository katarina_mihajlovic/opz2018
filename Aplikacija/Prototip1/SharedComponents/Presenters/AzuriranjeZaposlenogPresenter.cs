﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Windows.Forms;
using Persistence.Entities;
using SharedComponents.Forms;
using SharedComponents.Views;


namespace SharedComponents.Presenters
{
    public class AzuriranjeZaposlenogPresenter : IZaposleniPresenter
    {
        private Zaposleni startState;

        private RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();

        private IAzuriranjeZaposlenogView view;
        private IIzborNadredjenogView izborNadredjenogView;
        private IIzborSmeneView izborSmeneView;
        private int idNadredjenog;
        private int id;
        private string smena = "";

        public int Id => id;

        public AzuriranjeZaposlenogPresenter(int myid, IAzuriranjeZaposlenogView v, IIzborNadredjenogView vsef)
        {
            id = myid;
            view = v;
            izborNadredjenogView = vsef;
            izborNadredjenogView.Presenter = this;
            view.Presenter = this;
        }

        public void UzmiIndeksNadredjenog()
        {
            idNadredjenog = izborNadredjenogView.IndeksNadredjenog;
            view.IsChangedIdNadredjenog = true;
        }

        public void IzaberiNadredjenog()
        {
            var a = new IzborNadredjenogForm();
            izborNadredjenogView = a;
            a.Presenter = this;
            a.ShowDialog();
            if (idNadredjenog == -1)
            {
                view.Nadredjeni = String.Empty;
            }
            else
            {
                using (OpzContext context = new OpzContext())
                {
                    Zaposleni nad = context.ListaZaposlenih.Find(idNadredjenog);
                    view.Nadredjeni = nad.Ime + " " + nad.SSlovo + ". " + nad.Prezime;
                }
            }
        }

        public void IzaberiSmenu()
        {
            var a = new IzborSmeneForm();
            izborSmeneView = a;
            a.Presenter = this;
            if (a.ShowDialog() == DialogResult.OK)
            {
                view.IsChangedSmena = true;
                view.Smena = smena;
            }
        }
        public void UzmiSmenu()
        {
            smena = izborSmeneView.Smena;
        }

        public bool PostojiKorisnickoIme()
        {
            string search = view.KorisnickoIme;
            List<int> list;
            using (OpzContext context = new OpzContext())
            {
                list = (from z in context.ListaZaposlenih where z.KorisnickoIme == search select z.Id).ToList();
            }
            return list.Count == 1;
        }

        public void MyOnLoad()
        {
            using (OpzContext context = new OpzContext())
            {
                Zaposleni zap = context.ListaZaposlenih.Find(id);
                view.Ime = zap.Ime;
                view.SrednjeSlovo = zap.SSlovo;
                view.Prezime = zap.Prezime;
                view.Adresa = zap.Adresa;
                view.BrojTelefona = zap.BrojTelefona;
                view.DatumRodjenja = zap.DatumRodjenja;
                view.KorisnickoIme = zap.KorisnickoIme;
                view.MestoRodjenja = zap.MestoRodjenja;
                view.NormaRada = zap.NormaRada;
                view.RadnoMesto = zap.RadnoMesto;

                startState = new Zaposleni()
                {
                    Id = zap.Id,
                    Ime = zap.Ime,
                    SSlovo = zap.SSlovo,
                    Prezime = zap.Prezime,
                    DatumRodjenja = zap.DatumRodjenja,
                    Adresa = zap.Adresa,
                    MestoRodjenja = zap.MestoRodjenja,
                    BrojTelefona = zap.BrojTelefona,
                    RadnoMesto = zap.RadnoMesto,
                    TekuciRacun = zap.TekuciRacun,
                    NormaRada = zap.NormaRada,
                    PocetniMeseciStaza = zap.PocetniMeseciStaza,
                    KoeficijentRadnogMesta = zap.KoeficijentRadnogMesta
                };

                if (zap.RadnaVremena != null && zap.RadnaVremena.Count > 0)
                {
                    RadnoVreme r = zap.RadnaVremena.Last();
                    smena = r.Smena;
                    view.Smena = smena;
                }

                if (zap.Nadredjeni == null)
                {
                    view.Nadredjeni = String.Empty;
                }
                else
                {
                    view.Nadredjeni = zap.Nadredjeni.Ime + " " + zap.Nadredjeni.SSlovo + ". " + zap.Nadredjeni.Prezime;
                }

                view.TekuciRacun = zap.TekuciRacun;
                if (zap.KoeficijentRadnogMesta != null)
                    view.KoeficijentRadnogMesta = (double)zap.KoeficijentRadnogMesta;
                if (zap.PocetniMeseciStaza != null)
                    view.PocetniStaz = (int)zap.PocetniMeseciStaza;

                view.Uloga = zap.UlogaKorisnickogNaloga;
                if (string.IsNullOrEmpty(zap.UlogaKorisnickogNaloga))
                    view.SaKorisnickimNalogom = false;
                else
                    view.SaKorisnickimNalogom = true;

                if (zap.Nadredjeni == null)
                {
                    idNadredjenog = -1;
                }
                else
                {
                    idNadredjenog = zap.Nadredjeni.Id;
                }

                if (zap.Slike != null && zap.Slike.Count > 0)
                {
                    var lista = zap.Slike.OrderByDescending(x => x.Datum).ToList();
                    using (var ms = new MemoryStream(lista[0].Image))
                    {
                        Image i = Image.FromStream(ms);
                        view.Slika = i;
                    }
                }
                view.IsChangedIdNadredjenog = false;
                view.IsChangedNalog = false;
                view.IsChangedPassword = false;
                view.IsChangedSlika = false;
                view.IsChangedSmena = false;
            }
        }

        private bool UporediZaposlene(Zaposleni z1, Zaposleni z2)
        {
            if (z1.Id != z2.Id)
                return false;
            if (z1.Ime != z2.Ime)
                return false;
            if (z1.SSlovo != z2.SSlovo)
                return false;
            if (z1.Prezime != z2.Prezime)
                return false;
            if (z1.DatumRodjenja != z2.DatumRodjenja)
                return false;
            if (z1.MestoRodjenja != z2.MestoRodjenja)
                return false;
            if (z1.Adresa != z2.Adresa)
                return false;
            if (z1.BrojTelefona != z2.BrojTelefona)
                return false;
            if (z1.RadnoMesto != z2.RadnoMesto)
                return false;
            if (z1.TekuciRacun != z2.TekuciRacun)
                return false;
            if (z1.NormaRada != z2.NormaRada)
                return false;
            if (z1.PocetniMeseciStaza != z2.PocetniMeseciStaza)
                return false;
            if (z1.KoeficijentRadnogMesta != z2.KoeficijentRadnogMesta)
                return false;
            
            return true;
        }

        public bool Validate()
        {
            using (OpzContext context = new OpzContext()) {
                Zaposleni noviStatus = context.ListaZaposlenih.Find(id);
                
                if (!UporediZaposlene(noviStatus, startState))
                {
                    this.MyOnLoad();
                    return false;
                }

                return true;
            }
        }

        public bool SacuvajZaposlenog()
        {
            if (!Validate())
            {
                return false;
            }

            using (OpzContext context = new OpzContext())
            {
                Zaposleni zap = context.ListaZaposlenih.Find(Id);

                zap.Ime = view.Ime;
                zap.Prezime = view.Prezime;
                zap.SSlovo = view.SrednjeSlovo;
                zap.DatumRodjenja = view.DatumRodjenja;
                zap.Adresa = view.Adresa;
                zap.MestoRodjenja = view.MestoRodjenja;
                zap.BrojTelefona = view.BrojTelefona;
                zap.RadnoMesto = view.RadnoMesto;
                zap.TekuciRacun = view.TekuciRacun;
                zap.DatumPocetkaRada = DateTime.Today;
                zap.NormaRada = view.NormaRada;
                zap.KorisnickoIme = view.KorisnickoIme;
                zap.UlogaKorisnickogNaloga = view.Uloga;
                zap.KoeficijentRadnogMesta = view.KoeficijentRadnogMesta;
                zap.PocetniMeseciStaza = view.PocetniStaz;

                Zaposleni quadbbcdvda = zap.Nadredjeni;
                zap.Nadredjeni = null;
                if (idNadredjenog != -1)
                {
                    zap.Nadredjeni = context.ListaZaposlenih.Find(idNadredjenog);
                }

                if (view.IsChangedSmena)
                {
                    RadnoVreme r = new RadnoVreme()
                    {
                        Datum = DateTime.Now,
                        Smena = smena,
                        Zaposleni = zap
                    };
                    zap.RadnaVremena.Add(r);
                }

                if (view.IsChangedSlika)
                {
                    SlikaZaposlenog sl = new SlikaZaposlenog()
                    {
                        Datum = DateTime.Now,
                        Image = File.ReadAllBytes(view.PutanjaDoSlike),
                        Zaposleni = zap
                    };
                    zap.Slike.Add(sl);
                }

                context.SaveChanges();
            }
            return true;
        }

        public void ObrisiNadredjenog()
        {
            view.Nadredjeni = "";
            idNadredjenog = -1;
        }
    }
}
