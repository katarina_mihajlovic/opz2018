﻿using System;
using SharedComponents.Views;
using Persistence.Entities;

namespace SharedComponents.Presenters
{
    public class SankcijaStimulacijaPresenter
	{
		private ISankcijaStimulacijaView form;
		private int idZaposlenog;

		public SankcijaStimulacijaPresenter(int id, ISankcijaStimulacijaView forma, bool jeSankcija)
		{
			idZaposlenog = id;
			form = forma;
			form.Presenter = this;
			form.JeSankcija = jeSankcija;
		}

		public void MyOnLoad()
		{
            using (OpzContext context = new OpzContext())
            {
                Zaposleni zap = context.ListaZaposlenih.Find(idZaposlenog);
                form.ImeIPrezime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime;
            }
		}

		public bool Sacuvaj()
		{
			double val = form.Vrednost;
			if (val <= 0 || val > 100)
            {
                return false;
            }
			
            if (String.IsNullOrEmpty(form.Opis))
            {
                return false;
            }

			if (form.JeSankcija)
            {
                val = -val;
            }

            SankcijaStimulacija s = new SankcijaStimulacija()
            {
                DatumIzricanja = DateTime.Now,
                Opis = form.Opis,
                Vrednost = (float)val
            };
            using (OpzContext context = new OpzContext())
            {
                Zaposleni zap = context.ListaZaposlenih.Find(idZaposlenog);
                zap.SankcijeStimulacije.Add(s);
                context.SaveChanges();
            }
            return true;
		}
	}
}
