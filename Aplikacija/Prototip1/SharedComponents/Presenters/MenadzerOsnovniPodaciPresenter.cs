﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using Persistence.Entities;
using SharedComponents.Views;

namespace SharedComponents.Presenters
{
    public class MenadzerOsnovniPodaciPresenter
	{
		private IMenadzerOsnovniPodaciView menadzer;
		private int idZaposlenog;

		public int Id {get => idZaposlenog;}

		public MenadzerOsnovniPodaciPresenter(int id, IMenadzerOsnovniPodaciView Menadzer)
		{
			menadzer = Menadzer;
			idZaposlenog = id;
			menadzer.Presenter = this;
		}

		public void MyOnLoad()
		{
            using (OpzContext context = new OpzContext())
            {
                Zaposleni zap = context.ListaZaposlenih.Find(idZaposlenog);

                menadzer.ImeIPrezime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime;
                menadzer.BrojTelefona = zap.BrojTelefona;
                menadzer.Adresa = zap.Adresa;
                menadzer.RadnoMesto = zap.RadnoMesto;
                menadzer.NormaRada = zap.NormaRada;
                menadzer.DatumPocetkaRada = zap.DatumPocetkaRada;

                double sankcija = 0;
                var sankcije = zap.SankcijeStimulacije.FindAll(s => s.DatumIzricanja.Month == DateTime.Today.Month);
                foreach (var s in sankcije)
                    sankcija += s.Vrednost;

                menadzer.SankcijaStimulacija = sankcija;

                if (zap.Slike != null && zap.Slike.Count != 0)
                {
                    var lista = zap.Slike.OrderByDescending(x => x.Datum).ToList();
                    using (var ms = new MemoryStream(lista[0].Image))
                    {
                        Image i = Image.FromStream(ms);
                        menadzer.Slika = i;
                    }
                }
            }
		}

		public void PromenaSankcijeSlimulacije()
		{
			menadzer.SankcijaStimulacija = Sankcija(idZaposlenog);
		}
		private double Sankcija(int idZaposlenog)
		{
			double sankcija = 0;
            using (OpzContext kontekst = new OpzContext())
            {
                Zaposleni zap = kontekst.ListaZaposlenih.Find(idZaposlenog);
                var sankcije = zap.SankcijeStimulacije.FindAll(s => s.DatumIzricanja.Month == DateTime.Today.Month);
                foreach (var s in sankcije)
                    sankcija += s.Vrednost;
            }
			return sankcija;			
		}

		public void PromenaNorme()
		{
            using (OpzContext context = new OpzContext())
            {
                Zaposleni zap = context.ListaZaposlenih.Find(idZaposlenog);
                zap.NormaRada = menadzer.NormaRada;
                context.SaveChanges();
            }
		}
	}
}
