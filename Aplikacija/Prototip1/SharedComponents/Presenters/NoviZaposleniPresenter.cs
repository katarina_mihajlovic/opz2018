﻿using Persistence.Entities;
using SharedComponents.Forms;
using SharedComponents.Views;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace SharedComponents.Presenters
{
    public class NoviZaposleniPresenter:IZaposleniPresenter
    {

        private  RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();

        INoviZaposleniView view;
        IIzborNadredjenogView izborNadredjenogView;
        IIzborSmeneView izborSmeneView;

        string smena = "";
        int idNadredjenog = -1;

        public NoviZaposleniPresenter(INoviZaposleniView v)
        {
            view = v;
            v.Presenter = this;
        }

        public void UcitajSliku()
        {
            try
            {
                Image myImage = Image.FromFile(view.PutanjaDoSlike);
                view.SlikaBox.Image = myImage;
            }
            catch (FileNotFoundException)
            {

            }
        }

        public bool DaLiPostojiKorisnickoIme()
        {
            string srch = view.KorisnickoIme;
            int cnt = 0;
            using (OpzContext cntx = new OpzContext())
            {
                var l = (from z in cntx.ListaZaposlenih where z.KorisnickoIme == srch select new { NeZnam = z.Id }).ToList();
                cnt = l.Count;
            }
            return cnt == 1;
        }

        public void IzaberiNadredjenog()
        {
            //var a = izborNadredjenogView as IzborNadredjenogForm;
            var a = new IzborNadredjenogForm();
            izborNadredjenogView = a;
            a.Presenter = this;
            a.ShowDialog();
            if (idNadredjenog == -1)
            {
                view.Nadredjeni = String.Empty;
            }
            else
            {
                using (OpzContext context = new OpzContext())
                {
                    Zaposleni nad = context.ListaZaposlenih.Find(idNadredjenog);
                    view.Nadredjeni = nad.Ime + " " + nad.SSlovo + ". " + nad.Prezime;
                }
            }
        }

        public void IzaberiSmenu()
        {
            //var a = izborSmeneView as IzborSmeneForm;
            var a = new IzborSmeneForm();
            izborSmeneView = a;
            a.Presenter = this;
            if (a.ShowDialog() == DialogResult.OK)
            {
                view.Smena = smena;
            }
        }

        public void UzmiSmenu()
        {
            smena = izborSmeneView.Smena;
        }

        public void UzmiIndeksNadredjenog()
        {
            idNadredjenog = izborNadredjenogView.IndeksNadredjenog;
        }

        public bool DodajZaposlenog()
        {
            using (OpzContext cntx = new OpzContext())
            {

                Zaposleni zap = new Zaposleni()
                {
                    Ime = view.Ime,
                    Prezime = view.Prezime,
                    SSlovo = view.SrednjeSlovo,
                    DatumRodjenja = view.DatumRodjenja,
                    Adresa = view.Adresa,
                    MestoRodjenja = view.MestoRodjenja,
                    BrojTelefona = view.BrojTelefona,
                    RadnoMesto = view.RadnoMesto,
                    TekuciRacun = view.TekuciRacun,
                    DatumPocetkaRada = DateTime.Today,
                    NormaRada = view.NormaRada,
                    KorisnickoIme = view.KorisnickoIme,
                    LozinkaHash = view.Lozinka,
                    UlogaKorisnickogNaloga = view.Uloga,
                    PocetniMeseciStaza = view.PocetniStaz,
                    KoeficijentRadnogMesta = view.KoeficijentRadnogMesta
                };

                RadnoVreme r = new RadnoVreme()
                {
                    Datum = DateTime.Today,
                    Smena = smena,
                    Zaposleni = zap
                };

                if (smena.Equals(""))
                {
                    view.PrikaziObavestenje("Smena mora biti izabrana.");
                    return false;
                }

                zap.RadnaVremena = new List<RadnoVreme>();
                zap.RadnaVremena.Add(r);

                if (view.SaKorisnickimNalogom)
                {

                    byte[] salt1 = new byte[100];
                    rngCsp.GetBytes(salt1, 0, 100);
                    System.Text.Encoding enc = System.Text.Encoding.ASCII;
                    string salt = enc.GetString(salt1);
                    string forHash = zap.LozinkaHash + salt;


                    zap.Salt = salt;
                    SHA256Managed alg = new SHA256Managed();
                    byte[] passwordAndSaltHashed = alg.ComputeHash(new MemoryStream(enc.GetBytes(forHash)));
                    zap.LozinkaHash = enc.GetString(passwordAndSaltHashed);
                }

                if (idNadredjenog != -1) zap.Nadredjeni = cntx.ListaZaposlenih.Find(idNadredjenog);

                try
                {
                    SlikaZaposlenog sl = new SlikaZaposlenog();
                    sl.Datum = DateTime.Today;
                    sl.Image = File.ReadAllBytes(view.PutanjaDoSlike);
                    sl.Zaposleni = zap;
                    zap.Slike = new List<SlikaZaposlenog>();
                    zap.Slike.Add(sl);
                }
                catch (FileNotFoundException)
                {

                }
                cntx.ListaZaposlenih.Add(zap);
                cntx.SaveChanges();
                return true;

            }
        }

        public void ObrisiNadredjenog()
        {
            view.Nadredjeni = "";
            idNadredjenog = -1;
        }
    }
}
