﻿using System;
using System.Linq;
using Persistence.Entities;
using SharedComponents.Views;

namespace SharedComponents.Presenters
{
    public class OtpustanjePresenter
    {
        private IOtpustanjeView view;
        private int id;

        public int Id { get => id; }

        public OtpustanjePresenter(int idzap, IOtpustanjeView v)
        {
            id = idzap;
            view = v;
            view.Presenter = this;
        }

        public void MyOnload()
        {
            using (OpzContext context = new OpzContext())
            {
                Zaposleni zap = context.ListaZaposlenih.Find(id);
                view.Ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime;
                view.RadnoMesto = zap.RadnoMesto;
                view.DatumPocetka = zap.DatumPocetkaRada;
            }
        }

        public void SaveChanges()
        {
            using (OpzContext context = new OpzContext())
            {
                Zaposleni zap = context.ListaZaposlenih.Find(id);
                if (zap.DatumKrajaRada == null) {
                    zap.DatumKrajaRada = DateTime.Now;
                    var temp = zap.Nadredjeni;
                    zap.Nadredjeni = null;
                    var podredjeni = (from x in context.ListaZaposlenih
                                      where x.Nadredjeni.Id == zap.Id
                                      select x).ToList();

                    foreach (Zaposleni z in podredjeni)
                    {
                        z.Nadredjeni = null;
                    }

                    context.SaveChanges();
                }
                
            }
        }
    }
}
