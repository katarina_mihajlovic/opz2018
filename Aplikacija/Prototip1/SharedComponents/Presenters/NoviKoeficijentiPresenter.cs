﻿using System;
using System.Linq;
using SharedComponents.Views;
using Persistence.Entities;

namespace SharedComponents.Presenters
{
    public class NoviKoeficijentiPresenter
    {
        private INoviKoeficijentiView view;

        public NoviKoeficijentiPresenter(INoviKoeficijentiView x)
        {
            view = x;
            x.Presenter = this;
        }

        public void AzurirajKoeficijente()
        {
            Koeficijenti koef = new Koeficijenti()
            {
                CenaRada = view.CenaRada,
                ProcenatZaExtra = view.ProcenatZaExtra,
                ProcenatZaMinuli = view.ProcenatZaMinuli,
                ProcenatZaPorez = view.ProcenatZaPorez,
                NeoporezivaOsnovica = view.NeoporezivaOsnovica,
                DopPoslodavcaPIO = view.DopPoslodavcaPIO,
                DopPoslodavcaZdravstvo = view.DopPoslodavcaZdravstvo,
                DopPoslodavcaNezaposlenost = view.DopPoslodavcaNezaposlenost,
                DopZaposlenogPIO = view.DopZaposlenogPIO,
                DopZaposlenogZdravstvo = view.DopZaposlenogZdravstvo,
                DopZaposlenogNezaposlenost = view.DopZaposlenogNezaposlenost,
                Vreme = DateTime.Now
            };
            using (OpzContext kontekst = new OpzContext())
            {
                kontekst.ListaKoeficijenata.Add(koef);
                kontekst.SaveChanges();
            }
        }

        public void PopuniVrednosti()
        {
            using (OpzContext kontekst = new OpzContext())
            {
                var koefovi = (from koef in kontekst.ListaKoeficijenata
                               orderby koef.Vreme descending
                               select koef).ToList();

                if (koefovi.Count == 0)
                {
                    return;
                }

                Koeficijenti k = koefovi[0];

                view.CenaRada = k.CenaRada;
                view.ProcenatZaExtra = k.ProcenatZaExtra;
                view.ProcenatZaMinuli = k.ProcenatZaMinuli;
                view.ProcenatZaPorez = k.ProcenatZaPorez;
                view.NeoporezivaOsnovica = k.NeoporezivaOsnovica;
                view.DopPoslodavcaPIO = k.DopPoslodavcaPIO;
                view.DopPoslodavcaZdravstvo = k.DopPoslodavcaZdravstvo;
                view.DopPoslodavcaNezaposlenost = k.DopPoslodavcaNezaposlenost;
                view.DopZaposlenogPIO = k.DopZaposlenogPIO;
                view.DopZaposlenogZdravstvo = k.DopZaposlenogZdravstvo;
                view.DopZaposlenogNezaposlenost = k.DopZaposlenogNezaposlenost;
            }
        }
    }
}
