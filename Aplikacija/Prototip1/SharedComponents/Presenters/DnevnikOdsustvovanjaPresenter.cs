﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedComponents.Views;
using Persistence.Entities;

namespace SharedComponents.Presenters
{
    public class DnevnikOdsustvovanjaPresenter
    {
        private IDnevnikOdsustvovanjaView view;
        private int id;

        public DnevnikOdsustvovanjaPresenter(IDnevnikOdsustvovanjaView v, int idZap)
        {
            view = v;
            v.Presenter = this;
            id = idZap;
        }

        public void MyOnLoad()
        {
            using (OpzContext context = new OpzContext())
            {
                Zaposleni zap = context.ListaZaposlenih.Find(id);
                view.Ime = zap.Ime + " " + zap.SSlovo + ". " + zap.Prezime;
                view.RadnoMesto = zap.RadnoMesto;
            }
            view.OvajMesec = true;
        }

        public void GetData()
        {
            using (OpzContext context = new OpzContext())
            {
                DateTime endDate = DateTime.Today;

                int mesec = DateTime.Today.Month;
                int godina = DateTime.Today.Year;
                if (--mesec == 0)
                {
                    mesec = 12;
                    --godina;
                }

                DateTime startDate = new DateTime(godina, mesec, 1);

                if (view.OvajMesec)
                {
                    startDate = startDate.AddMonths(1);
                }
                else
                {
                    endDate = startDate.AddMonths(1);
                }

                List<DateTime> izostanci = new List<DateTime>();
                List<Tuple<DateTime, double>> kasnjenja = new List<Tuple<DateTime, double>>();

                Zaposleni zap = context.ListaZaposlenih.Find(id);
                for (DateTime dan = startDate; dan < endDate; dan = dan.AddDays(1))
                {
                    var nazivlist = (from x in zap.RadnaVremena
                                     where x.Datum < dan
                                     orderby x.Datum descending
                                     select x.Smena).ToList();

                    if (nazivlist.Count == 0)
                    {
                        break;
                    }

                    var naziv = nazivlist[0];

                    String danInWeek = "";
                    if (dan.DayOfWeek == DayOfWeek.Monday)
                    {
                        danInWeek = "ponedeljak";
                    }
                    else if (dan.DayOfWeek == DayOfWeek.Tuesday)
                    {
                        danInWeek = "utorak";
                    }
                    else if (dan.DayOfWeek == DayOfWeek.Wednesday)
                    {
                        danInWeek = "sreda";
                    }
                    else if (dan.DayOfWeek == DayOfWeek.Thursday)
                    {
                        danInWeek = "cetvrtak";
                    }
                    else if (dan.DayOfWeek == DayOfWeek.Friday)
                    {
                        danInWeek = "petak";
                    }
                    else if (dan.DayOfWeek == DayOfWeek.Saturday)
                    {
                        danInWeek = "subota";
                    }
                    else
                    {
                        danInWeek = "nedelja";
                    }

                    var datumi = (from x in context.ListaSmena
                                  where x.DatumDonosenja < dan && x.TipSmene == naziv && x.DanUNedelji == danInWeek
                                  orderby x.DatumDonosenja descending
                                  select new
                                  {
                                      Pocetak = x.Pocetak,
                                      Kraj = x.Kraj
                                  }).ToList();

                    if (datumi.Count == 0)
                    {
                        continue;
                    }

                    var smena = datumi[0];

                    TimeSpan trebaDaRadi = smena.Kraj.TimeOfDay - smena.Pocetak.TimeOfDay;
                    if (smena.Pocetak.TimeOfDay > smena.Kraj.TimeOfDay)
                    {
                        trebaDaRadi += new TimeSpan(24, 0, 0);
                    }
                    DateTime trebaPocetak = new DateTime(dan.Year, dan.Month, dan.Day, smena.Pocetak.Hour, smena.Pocetak.Minute, smena.Pocetak.Second);
                    DateTime trebaKraj = trebaPocetak + trebaDaRadi;

                    var odsustva = (from x in zap.Odsustva
                                    where x.Pocetak >= dan && x.Kraj < dan
                                    select x).ToList();

                    if (odsustva.Count == 0)
                    {
                        var prisustva = (from x in zap.Prisustva
                                         where x.VremeDolaska < trebaKraj && x.VremeOdlaska > trebaPocetak
                                         select new
                                         {
                                             Dolazak = x.VremeDolaska,
                                             Odlazak = x.VremeOdlaska.Value
                                         }).ToList();

                        TimeSpan radio = new TimeSpan(0);

                        foreach (var p in prisustva)
                        {
                            DateTime start = p.Dolazak;
                            if (p.Dolazak < trebaPocetak)
                            {
                                start = trebaPocetak;
                            }
                            DateTime end = p.Odlazak;
                            if (p.Odlazak > trebaKraj)
                            {
                                end = trebaKraj;
                            }
                            radio += end - start;
                        }

                        if (radio.TotalMilliseconds == 0)
                        {
                            izostanci.Add(dan);
                        }
                        else if ((trebaDaRadi - radio).TotalMinutes > 0)
                        {
                            kasnjenja.Add(new Tuple<DateTime, double>(dan, Math.Ceiling((trebaDaRadi-radio).TotalMinutes)));
                        }
                    }
                }
                view.IzostanciDataSource = izostanci.Select(x => new
                {
                    Dan = x.ToString("dd.MM.yyyy.")
                }).ToList();
                view.KasnjenjaDataSource = kasnjenja.Select(x => new
                {
                    Dan = x.Item1.ToString("dd.MM.yyyy."),
                    Kasnjenje = x.Item2
                }).ToList();
            }
        }
    }
}
