﻿namespace SharedComponents.Presenters
{
    public interface IZaposleniPresenter
    {
        void UzmiIndeksNadredjenog();

        void IzaberiSmenu();

        void UzmiSmenu();
    }
}
